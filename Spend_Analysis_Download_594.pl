use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use BIP_DB_V1;
use BIP_Column_Map_V1;

# my $S_Date=$ARGV[0];
# my $E_Date=$ARGV[3];
# my $S_Year=$ARGV[2];
# my $E_Year=$ARGV[5];
# my $S_Month=$ARGV[1];
# my $E_Month=$ARGV[4];

#print "$S_Date::$S_Month::$S_Year\n";
#print "$E_Date::$E_Month::$E_Year";
my $Input_Buyer_ID = $0;
$Input_Buyer_ID =~ s/\.pl//igs;
$Input_Buyer_ID =$1 if($Input_Buyer_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my %Months_To_Num=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");
my @Months1=("January","February","March","April","May","June","July","August","September","October","November","December","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
my %enddate = ("01" => "31", "02" => "28", "03" => "31", "04" => "30", "05" => "31", "06" => "30", "07" => "31", "08" => "31", "09" => "30", "10" => "31", "11" => "30", "12" => "31");


#my %Filter_Start_Date=()
############Database Initialization########
my $dbh = &BIP_DB_V1::DbConnection();
###########################################
my $hash_ref = &BIP_Column_Map_V1::ReadIniFile();	
my %inihashvalue = %{$hash_ref};

my $Input_Table='BUYERS';
############ Retrieve Spent Data weblink from Buyers table ###########
my ($ID,$Buyer_Name,$Spent_Data_weblink) = &BIP_DB_V1::RetrieveUrl($dbh,$Input_Buyer_ID,$Input_Table);
my $Buyer_ID 			= @$ID[0];
my $Buyer_Name 			= @$Buyer_Name[0];
my $Buyer_Url 			= @$Spent_Data_weblink[0];

###############
print "Buyer_ID	: $Buyer_ID\n";	
print "Buyer_Name	: $Buyer_Name\n";	
print "Buyer_Url	: $Buyer_Url\n";	

my ($Buyer_content,$Status_Line)=&Getcontent($Buyer_Url); &BIP_Column_Map_V1::Link_Status_Update($dbh,$Buyer_ID,$Status_Line);
open(ts,">Buyer_content215.html");
print ts "$Buyer_content";
close ts;

my ($Processed_LinkInfo) = &BIP_DB_V1::LinkInfo_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
my @Processed_LinkInfo=@$Processed_LinkInfo;

# while($Buyer_content=~m/<td class="left titlecol Title">([^>]*?)<\/td>[\w\W]*?<a href="([^>]*?)" title="[^>]*?CSV"/igs)
while($Buyer_content=~m/property="dcat:accessURL">\s*([^>]*?)\s*-\s*[^>]*?<[\w\W]*?<a[^>]*?href="([^>]*?\.(?:csv|xlsx))"[^>]*?>/igs)
{
my ($LinkInfo,$Doc_Link,$File_Month,$File_Year,$Doc_Name);
my $Doc_Link=$2;
my $LinkInfo= $1;
	next if($Doc_Link=~m/\.pdf/is);
	my $Doc_Type;
	if ( $Doc_Link =~ m/\/[^>]\.([^>]*?)\s*$/is )
	{
		$Doc_Type=$1;
	}
	$LinkInfo=~s/\£//igs;
	$LinkInfo=~s/\<[^>]*?\>//igs;
	$LinkInfo=~s/\s+/ /igs;
	$LinkInfo=~s/\s+$//igs;
	if($LinkInfo ~~ @Processed_LinkInfo || $LinkInfo =~ m/^\s*$/)
	{
		print "File Already Processed\n";
		next;
	}
	my $File_Type;
	if($Doc_Link=~m/[^>]*?\.(\w+)$/is)
	{
		$File_Type=$1;
	}
	# Get Date, Month, Year from the link info for file location Creation
	if($LinkInfo=~m/(\w+\s*)\s*(\d{4}(?:\=\d+)?)\s*[^>]*?/is)
	{
		$File_Month=$1;
		$File_Year=$2;
		$File_Year=~s/\s*//igs;
		# $File_Year='20'.$1 if($File_Year=~m/^(\d{2})$/is);
		print "File_Month	: $File_Month\n";
		print "File_Year	: $File_Year\n";
	}
	# Get Date, Month, Year from the link info for file location Creation
	elsif($LinkInfo=~m/(\d{4}(?:\=\d+)?)\s*[^>]*?/is)
	{
		$File_Year=$1;
		$File_Year=~s/\s*//igs;
		# $File_Year='20'.$1 if($File_Year=~m/^(\d{2})$/is);
		print "File_Month	: $File_Month\n";
		print "File_Year	: $File_Year\n";
	}
	my $Month_Of_Document = "$File_Month $File_Year";
	print "LinkInfo	: <$LinkInfo>\n";
	
	if($Doc_Link!~m/http/is)
	{
		my $u1=URI::URL->new($Doc_Link,$Buyer_Url);
		my $u2=$u1->abs;
		$Doc_Link=$u2;
	}
	print "Doc_Link	: $Doc_Link\n";
	
	# File name creation from Link Info
	$File_Type = $Doc_Type if ($Doc_Type ne '');
	
	$Doc_Name=$LinkInfo;
	$Doc_Name=~s/\W/\_/igs;
	$Doc_Name=~s/_+/\_/igs;
	$Doc_Name.='.'.$File_Type;
	print "Doc_Name	: $Doc_Name\n"; 
# for(my $Year='2016'; $Year<=2020; $Year++)
# { 
	# for(my $Month=1;$Month<=12;$Month++)
	# {
		# $Month='0'.$Month if (length($Month)==1);
		
		# my $File_Month=$Month;
		# my $File_Year=$Year;
		
		# my $LinkInfo=$File_Month." ".$File_Year;
		# if($LinkInfo ~~ @Processed_LinkInfo || $LinkInfo =~ m/^\s*$/)
		# {
			# print "File Already Processed\n";
			# next;
		# }
		# my $end_date;
		# if( 0 == $Year % 4 and 0 != $Year % 100 or 0 == $Year % 400 )
		# {
			# if ($Month==2)
			# {
				# $end_date=$enddate{$Month}+1;
			# }
			# else
			# {
				# $end_date=$enddate{$Month};
			# }
		# }
		# else
		# {
			# $end_date=$enddate{$Month};
		# }
		# my $cont = 'searchtext=&OrderByColumn=%5BBodyName%5D&OrderByDirection=ASC&Download=csv&chartType=table&filter%5B0%5D.ColumnToSearch=Date&filter%5B0%5D.SearchOperator=contains&filter%5B0%5D.SearchText=&filter%5B0%5D.SearchOperatorNumber=greaterthan&filter%5B0%5D.SearchNumber=&filter%5B0%5D.From=01%2F'.$Month.'%2F'.$Year.'&filter%5B0%5D.To='.$end_date.'%2F'.$Month.'%2F'.$Year.'&VisibleColumns=0_Amount&VisibleColumns=1_BodyName&VisibleColumns=2_CapitalorRevenue&VisibleColumns=3_Date&VisibleColumns=4_ExpensesType&VisibleColumns=5_MerchantCategory&VisibleColumns=6_ServiceAreaCategorisation&VisibleColumns=7_ServiceDivisionCategorisation&VisibleColumns=8_SupplierName&VisibleColumns=9_TransactionNumber&getVisualisationData=false&chartCurrentPage=1&chartNumberToShow=10&CurrentPage=1&PageNumber=&&radio=on';
		# my $cont = "OrderByColumn=%5BBodyName%5D&OrderByDirection=ASC&Download=csv&radio=on&chartType=table&filter%5B0%5D.ColumnToSearch=Date&filter%5B0%5D.SearchOperator=contains&filter%5B0%5D.SearchText=&filter%5B0%5D.SearchOperatorNumber=greaterthan&filter%5B0%5D.SearchNumber=&filter%5B0%5D.From=01%2F".$File_Month."%2F".$File_Year."&filter%5B0%5D.To=".$end_date."%2F".$File_Month."%2F".$File_Year."&VisibleColumns=0_Amount&VisibleColumns=1_BodyName&VisibleColumns=2_CapitalorRevenue&VisibleColumns=3_Date&VisibleColumns=4_ExpensesType&VisibleColumns=5_MerchantCategory&VisibleColumns=6_ServiceAreaCategorisation&VisibleColumns=7_ServiceDivisionCategorisation&VisibleColumns=8_SupplierName&VisibleColumns=9_TransactionNumber&getVisualisationData=false&xAxis=Text%23BodyName&yAxis=Number%23TransactionNumber&yAxisAggregate=sum&chartCurrentPage=1&chartNumberToShow=10&numberToShow=10&CurrentPage=1&PageNumber=";
		
		#my $cont = 'searchtext=&OrderByColumn=%5BBodyName%5D&OrderByDirection=ASC&Download=csv&chartType=table&filter%5B0%5D.ColumnToSearch=Date&filter%5B0%5D.SearchOperator=contains&filter%5B0%5D.SearchText=&filter%5B0%5D.SearchOperatorNumber=greaterthan&filter%5B0%5D.SearchNumber=&filter%5B0%5D.From=01%2F01%2F2016&filter%5B0%5D.To=31%2F05%2F2016&VisibleColumns=0_Amount&VisibleColumns=1_BodyName&VisibleColumns=2_CapitalorRevenue&VisibleColumns=3_Date&VisibleColumns=4_ExpensesType&VisibleColumns=5_MerchantCategory&VisibleColumns=6_ServiceAreaCategorisation&VisibleColumns=7_ServiceDivisionCategorisation&VisibleColumns=8_SupplierName&VisibleColumns=9_TransactionNumber&getVisualisationData=false&chartCurrentPage=1&chartNumberToShow=10&CurrentPage=1&PageNumber=&&radio=on';
		# my $Doc_Type="csv";
		# my $Doc_Link=$Buyer_Url;
		# print "Content :: $cont\n";
		# my ($Doc_content,$status_line,$File_Type,$File_Type_cont)=&Postcontent($Buyer_Url,$cont,"data.peterborough.gov.uk","$Buyer_Url");
		# open(ts,">Doc_content594.html");
		# print ts "$Doc_content";
		# close ts;exit;
		# if($Doc_content!~m/\d+\/\d+\/\d+/is){
			# print "File not yet available";
			# exit;
		# }		
		my $Month_Of_Document = "$LinkInfo";	
		my $File_Type = $Doc_Type if ($Doc_Type ne '');
		$File_Year=~s/\W+//igs;
		my $Doc_Name=$Month_Of_Document;
		$Doc_Name=~s/\W/\_/igs;
		$Doc_Name=~s/_+/\_/igs;
		$Doc_Name.='.csv';
		print "#########################################\n";
		print "Doc_Name	: $Doc_Name\n"; 
		print "File_Year		: $File_Year\n";
		print "Doc_Type		: $Doc_Type\n";
		print "Doc_Link	: $Doc_Link\n";
		print "Month_Of_Document	: $Month_Of_Document\n";
		print "#########################################";
		# File Path creation
		# my $Filestorepath='D:/Arul/BIP/'.$Buyer_ID."/".$File_Year."/Source";
			
		my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source"; 

		# File location creation
		unless ( -d $Filestorepath ) 
		{
			$Filestorepath=~s/\//\\/igs;
			system("mkdir $Filestorepath");
		}
		my $Storefile = "$Filestorepath\\$Doc_Name";
		print "STOREPATH  : $Storefile \n";
		my $StartDate=DateTime->now();
		# Spend file download
		my ($Doc_content,$status_line)=&Getcontent($Doc_Link);
		my $fh=FileHandle->new("$Storefile",'w');
		binmode($fh);
		$fh->print($Doc_content);
		$fh->close();
		my ($Status,$Failed_Reason);
		# if($status_line!~m/20/is)
		# {
			# $Failed_Reason=$status_line;
		# }
		# else
		# {
			$Status='Downloaded';
		# }

		print "\nStatus:::: $Status\n";
		<>;
		&BIP_Column_Map_V1::ReadSource($Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
		print "Completed \n";
		# exit;
		
	# }
}

sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	print "\nFile_Type---> $File_Type\n";
	print "\nFile_Type_cont---> $File_Type_cont\n";
	# my $get_cookie=$res->header('set-cookie');
	print "\nCODE---> $code\n";
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$File_Type,$File_Type_cont);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$File_Type,$File_Type_cont);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	if($Data eq '')
	{
		# $Data='Not provided';
	}
	return $Data;
}

sub clean()
{
	my $C_Clean=shift;
	$C_Clean=~s/<[^>]*?>//igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/\&nbsp\;/ /igs;
	$C_Clean=~s/\&amp\;/ /igs;
	$C_Clean=~s/\&pound\;/ /igs;
	$C_Clean=~s/\&\#8211\;/ /igs;
	$C_Clean=~s/ndash\;/ /igs;
	$C_Clean=~s/[^[:print:]]+//igs;
	
	
	$C_Clean=~s/\&nbsp\;//igs;
	$C_Clean=~s/\#160\;//igs;
	$C_Clean=~s/\#163\;//igs;
	$C_Clean=~s/\&/ /igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	return $C_Clean;
}
#}