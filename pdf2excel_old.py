#-*- coding: utf-8 -*-
import base64
import requests
import json
import pybase64
import io
from io import StringIO
import PyPDF2
import xlrd
import csv
from os import sys
import os
import ntpath

'''
Usage :  python pdf2excel.py "F:/MuthubabuMasterFolder/POC/SolidFramework/Summary_Part_B_Activities_sept2018 (2).pdf"

'''

def stringToBase64(s):
    return base64.b64encode(s.encode('utf-8'))

def base64ToString(b):
    return base64.b64decode(b)

def fileConversion(pdfFile,fileName,path):

    with open(pdfFile, "rb") as pdf_file:
        encoded_string = base64.b64encode(pdf_file.read())

    url = 'http://172.27.139.76:83/api/conversion/pdftoexcel'

    postData = '''{
    "PdfFile": "%s",
    "PdfConversionProperties": {
        "NonTableContent": true,
        "Singlesheet": false,
        "T_seperator": "Comma",
        "D_symbol": "Comma",
        "Delimiters": true,
        "Textrecovery": "Automatic",
        "Optional_password": ""
        }
    }''' % encoded_string.decode("utf-8")



    header= {"Content-Type" : "application/json"}
    source = requests.post(url, data = postData, headers = header)

    postDataSource = json.loads(source.text)


    string = base64ToString(postDataSource["Data"]["ResponseBase64String"])

    excelfile = open(str(path)+"/"+str(fileName)+".xlsx","wb")
    excelfile.write(string)
    excelfile.close()
    return str(path)+"/"+str(fileName)+".xlsx"

def csv_from_excel(excel_file,fileName,path):
    workbook = xlrd.open_workbook(excel_file)
    all_worksheets = workbook.sheet_names()
    # your_csv_file = open(str(path)+'/'+str(fileName)+'.csv', 'a',newline='')
    your_csv_file = open(str(path)+'/'+str(fileName)+'.csv', 'wb')
    for worksheet_name in all_worksheets:
        worksheet = workbook.sheet_by_name(worksheet_name)
        
        wr = csv.writer(your_csv_file, quoting=csv.QUOTE_ALL)
        for rownum in range(worksheet.nrows):
            wr.writerow([str(entry).encode("utf-8").decode("utf-8") for entry in worksheet.row_values(rownum)])
    return str(path)+'/'+str(fileName)+'.csv'

def path_leaf(path):
    head, tail = ntpath.split(path)
    return tail,head

if __name__ == "__main__": 
    pdfFile = sys.argv[1]
    name, path = path_leaf(pdfFile)
    fileName = name.replace('.pdf','')
    excelFile = fileConversion(pdfFile,fileName,path)
    csvFile = csv_from_excel(excelFile,fileName,path)
    # os.remove(excelFile)