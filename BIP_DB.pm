package BIP_DB;
### Package Name ####
use strict;
use DBI;
use DBD::ODBC;
use Config::IniFiles;
require Exporter;
my @ISA = qw(Exporter);
my @EXPORT = qw(ImageDownload);

###### DB Connection ####
sub DbConnection()
{
	my $cfg = new Config::IniFiles( -file => 'C:\Perl\lib\BIP_Config.ini', -nocase => 1);
	my %hash_ini;
	my @FileExten = $cfg -> Parameters('DBDETAILS');
	foreach my $FileExten (@FileExten)
	{
		my $FileExt =  $cfg -> val('DBDETAILS', $FileExten);
		$hash_ini{$FileExten}=$FileExt;
	}
	my $database_name = $hash_ini{'database_name'};
	my $server_name = $hash_ini{'server_name'};
	my $database_user = $hash_ini{'database_user'};
	my $database_pass = $hash_ini{'database_pass'};
	my $driver = "mysql"; 
	my $dsn = "DBI:$driver:database=$database_name;host=$server_name;";
	my $dbh = DBI->connect($dsn, $database_user, $database_pass ) or die $DBI::errstr;
	return $dbh;
}

###### Retrieve DocumentURL ####
sub RetrieveUrl()
{
	my $dbh 		= shift;
	my $Buyer_ID 	= shift;
	my $Input_Table	= shift;
	
	my $query = "select ID,Buyer_Name,Spent_Data_weblink from $Input_Table  where ID=\'$Buyer_ID\'";
	
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@ID,@Buyer_Name,@Spent_Data_weblink,@End_Date,@Type_of_Government,@Expressions,@Request_method,@Host,@Referer,@Hidden_Content,@File_Type,@Robot_Name);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@ID,&Trim($record[0]));
		push(@Buyer_Name,&Trim($record[1]));
		push(@Spent_Data_weblink,&Trim($record[2]));
		push(@End_Date,&Trim($record[3]));
		push(@Type_of_Government,&Trim($record[4]));
		push(@Expressions,&Trim($record[5]));
		push(@Request_method,&Trim($record[6]));
		push(@Host,&Trim($record[7]));
		push(@Referer,&Trim($record[8]));
		push(@Hidden_Content,&Trim($record[9]));
		push(@File_Type,&Trim($record[10]));
		push(@Robot_Name,&Trim($record[11]));
	}
	$sth->finish();
	return (\@ID,\@Buyer_Name,\@Spent_Data_weblink,\@End_Date,\@Type_of_Government,\@Expressions,\@Request_method,\@Host,\@Referer,\@Hidden_Content,\@File_Type,\@Robot_Name);
}

sub Retrieve_Buyers_Download_Details()
{
	my $dbh 			= shift;
	my $Input_Table		= shift;
	my $Field_Match		= shift;
	my $Import_Flag		= shift;
	my $Formatted_Flag	= shift;
	my $Supplier_Match	= shift;
	my $Export_Flag		= shift;
	my $Batch_ID_List	= shift;
	
	my $query = "select Buyer_ID,Batch_ID,FileName,DATE_FORMAT(StartDate, '%d-%m-%Y'),Source_File_Location from $Input_Table  where Formatted_Flag=\'$Formatted_Flag\' and Export_flag=\'$Export_Flag\'";
	
	# my $query = "select Buyer_ID,Batch_ID,FileName,DATE_FORMAT(StartDate, '%d-%m-%Y'),Source_File_Location from $Input_Table where Export_Flag=\'$Export_Flag\' and Batch_ID in ($Batch_ID_List)";
	print "$query\n";

	
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@Buyer_ID,@Batch_ID,@FileName,@StartDate,@Source_File_Location);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@Buyer_ID,Trim($record[0]));
		push(@Batch_ID,Trim($record[1]));
		push(@FileName,Trim($record[2]));
		push(@StartDate,Trim($record[3]));
		push(@Source_File_Location,Trim($record[4]));
	}
	$sth->finish();
	return (\@Buyer_ID,\@Batch_ID,\@FileName,\@StartDate,\@Source_File_Location);
}
sub Retrieve_Buyers_Download_Details_For_Clean()
{
	my $dbh 		= shift;
	my $Input_Table	= shift;
	my $Import_Flag	= shift;
	my $Formatted_Flag	= shift;
	
	# my $query = "select Buyer_ID,Batch_ID from $Input_Table where Import_Flag=\'$Import_Flag\' and Formatted_Flag=\'$Formatted_Flag\'";
	my $query = "select Buyer_ID,Batch_ID,Linkinfo from $Input_Table where No_Of_Records!=0 and Formatted_Flag=\'$Formatted_Flag\'";
	
	print "\n$query\n";

	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@Buyer_ID,@Batch_ID,@Linkinfo);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@Buyer_ID,Trim($record[0]));
		push(@Batch_ID,Trim($record[1]));
		push(@Linkinfo,Trim($record[2]));
	}
	$sth->finish();
	return (\@Buyer_ID,\@Batch_ID,\@Linkinfo);
}
sub Retrieve_Source_Data()
{
	my $dbh 		= shift;
	my $Buyer_ID 	= shift;
	my $Batch_ID 	= shift;
	my $Input_Table	= shift;

	
		my $query = "select Supplier_Name,'N' as Status,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Department_Name,Requirement_2 from $Input_Table where Buyer_ID=\'$Buyer_ID\' and Batch_ID=\'$Batch_ID\' order by sno";
	
	# my $query = "SELECT 
    # CASE WHEN M1.Supplier_Name IS NULL THEN M3.SUPPLIER_NAME ELSE M1.Supplier_Name END SUPPLIERNAME,
    # CASE WHEN M1.Supplier_Name IS NULL THEN 'N' ELSE 'Y' END STATUS,MONTH_OF_DOCUMENT,PROCESSED_DATE,SNO,TRANSACTION_ID,REQUIREMENT,AMOUNT,START_DATE,END_DATE,DEPARTMENT_NAME,REQUIREMENT_2
					# FROM UNIQUE_SUPPLIER_LIST M1
					# INNER JOIN SUPPLIER_LIST M2
					# ON M2.Matching_ID=M1.id
					# RIGHT OUTER JOIN supplier_source_data M3
					# ON REPLACE(M3.SUPPLIER_NAME,' ','') =M2.Supplier_Name_withoutspace 
					# WHERE  Buyer_ID=\'$Buyer_ID\' and Batch_ID=\'$Batch_ID\'";
	
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@Supplier_Name,@Status,@Month_Of_Document,@Processed_Date,@Sno,@Transaction_ID,@Requirement,@Amount,@Start_Date,@End_Date,@Department_Name,@Requirement_2);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@Supplier_Name,&Trim($record[0]));
		push(@Status,&Trim($record[1]));
		push(@Month_Of_Document,&Trim($record[2]));
		push(@Processed_Date,&Trim($record[3]));
		push(@Sno,&Trim($record[4]));
		push(@Transaction_ID,&Trim($record[5]));
		push(@Requirement,&Trim($record[6]));
		push(@Amount,&Trim($record[7]));
		push(@Start_Date,&Trim($record[8]));
		push(@End_Date,&Trim($record[9]));
		push(@Department_Name,&Trim($record[10]));
		push(@Requirement_2,&Trim($record[11]));
	}
	$sth->finish();
	return (\@Supplier_Name,\@Status,\@Month_Of_Document,\@Processed_Date,\@Sno,\@Transaction_ID,\@Requirement,\@Amount,\@Start_Date,\@End_Date,\@Department_Name,\@Requirement_2);
}

sub Column_Name_RetrieveUrl()
{
	my $dbh 		= shift;
	my $Buyer_ID 	= shift;
	
	my $query = "select Type,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2 from field_map_details_Automation  where Buyer_ID=\'$Buyer_ID\'";
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my ($Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		$Type=&Trim($record[0]);
		$Transaction_ID=&Trim($record[1]);
		$Requirement=&Trim($record[2]);
		$Amount=&Trim($record[3]);
		$Start_Date=&Trim($record[4]);
		$End_Date=&Trim($record[5]);
		$Supplier_Name=&Trim($record[6]);
		$Department_Name=&Trim($record[7]);
		$Requirement_2=&Trim($record[8]);
	}
	$sth->finish();
	return ($Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2);
}


sub Supplier_Name_Matching()
{
	my $dbh 		= shift;
	my $Supplier_Name_withoutspace 	= shift;
	
	my $query = "select Supplier_Name from SUPPLIER_LIST where ID=(select Matching_ID from SUPPLIER_LIST where Supplier_Name_withoutspace=\'$Supplier_Name_withoutspace\')";
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my ($Supplier_Name);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		$Supplier_Name=&Trim($record[0]);
	}
	$sth->finish();
	return ($Supplier_Name);
}


sub BatchID_RetrieveUrl()
{
	my $dbh 		= shift;
	my $Buyer_ID 	= shift;
	my $Input_Table 	= shift;
	
	my $query = "select max(Batch_ID) from $Input_Table  where Buyer_ID=\'$Buyer_ID\'";
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my ($Batch_ID);
	while(my @record = $sth->fetchrow)
	{
		$Batch_ID=&Trim($record[0]);
	}
	$sth->finish();
	return ($Batch_ID);
}

sub Retrieve_Month_Document()
{
	my $dbh 		= shift;
	my $Buyer_ID 	= shift;
	my $Batch_ID 	= shift;
	my $Input_Table = shift;
	
	my $query = "select Month_Of_Document from $Input_Table  where Buyer_ID=\'$Buyer_ID\' and Batch_ID=\'$Batch_ID\' limit 1";
	# my $query = "select Month_Of_Document from $Input_Table limit 1";
	# print "$query\n";
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my ($Month_Of_Document);
	while(my @record = $sth->fetchrow)
	{
		$Month_Of_Document=&Trim($record[0]);
	}
	$sth->finish();
	$Month_Of_Document='Apr 2015' if($Month_Of_Document eq '');
	return ($Month_Of_Document);
}
sub Retrieve_Buyer_Name()
{
	my $dbh 		= shift;
	my $Buyer_ID 	= shift;
	my $Input_Table = shift;
	
	my $query = "select Buyer_Name,Upload_Buyer_Name from $Input_Table where ID=\'$Buyer_ID\'";
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my ($Buyer_Name,$Upload_Buyer_Name);
	while(my @record = $sth->fetchrow)
	{
		$Buyer_Name=&Trim($record[0]);
		$Upload_Buyer_Name=&Trim($record[1]);
	}
	$sth->finish();
	return ($Buyer_Name,$Upload_Buyer_Name);
}

sub LinkInfo_RetrieveUrl()
{
	my $dbh 		= shift;
	my $Buyer_ID 	= shift;
	my $Input_Table 	= shift;
	
	my $query = "select LinkInfo from $Input_Table  where Buyer_ID=\'$Buyer_ID\'";
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my (@LinkInfo);
	while(my @record = $sth->fetchrow)
	{
		push(@LinkInfo,&Trim($record[0]));
	}
	$sth->finish();
	return (\@LinkInfo);
}

# ###### Pdf Download Path ####
# sub DownloadPath()
# {
	# my $dbh 			= shift;
	# my $downloadpath;
	# my $query = "Select Picklistvalue from PickList where PicklistCategory='ONE_APP' and PickListField='PDF_DOWNLOAD_PATH' AND ID=436";
	# my $sth = $dbh->prepare($query);
	# $sth->execute();
	# while(my @record = $sth->fetchrow)
	# {
		# $downloadpath = Trim($record[0]);
	# }
	# $sth->finish();
	# return $downloadpath;
# }

sub Update_Download_Details()
{
	my $dbh 		= shift;
	my $Table_Name	= shift;
	my $Buyer_ID	= shift;
	my $Batch_ID 	= shift;
	my $Supplier_Name_Matched_Count 	= shift;
	my $Supplier_Name_NotMatched_Count	= shift;
	my $Formatted_Flag	= shift;
	my $Supplier_Match	= shift;
	my $MOD	= shift;
	
	my $query = "Update $Table_Name set No_of_Suppliers_Matched=\'$Supplier_Name_Matched_Count\',No_of_Suppliers_Not_Matched=\'$Supplier_Name_NotMatched_Count\',Formatted_Flag=\'$Formatted_Flag\',Supplier_Match=\'$Supplier_Match\',Month_Of_Doc=\'$MOD\' where Buyer_ID=\'$Buyer_ID\' and Batch_ID=\'$Batch_ID\'";
	my $sth = $dbh->prepare($query);
	if($sth->execute())
	{
		print "Updated\n";
		$sth->finish();	
	}
	else
	{
		print "QUERY:: $query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

sub Update_Import_Flag()
{
	my $dbh 		= shift;
	my $Table_Name	= shift;
	my $Buyer_ID	= shift;
	my $Batch_ID 	= shift;
	my $Import_Flag	= shift;
	my $Field_Match	= shift;
	
	my $query = "Update $Table_Name set Import_Flag=\'$Import_Flag\', Field_Match=\'$Field_Match\',EndDate=now() where Buyer_ID=\'$Buyer_ID\' and Batch_ID=\'$Batch_ID\'";
	print "\nquery--> $query\n";
	my $sth = $dbh->prepare($query);
	if($sth->execute())
	{
		print "Updated\n";
		$sth->finish();	
	}
	else
	{
		print "QUERY:: $query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

###### Update DB ####
sub Update_Buyers_Details
{
	my $dbh			= shift;
	my $Buyer_ID 	= shift;
	my $Date_Researched 	= shift;
	my $No_of_Document_Researched 	= shift;
	my $Document_Month_Researched 	= shift;

	my $query = "Update Buyers_Tmp  set Date_Researched=\'$Date_Researched\', No_of_Document_Researched=\'$No_of_Document_Researched\', Document_Month_Researched=\'$Document_Month_Researched\' where ID=\'$Buyer_ID\'";

	my $sth = $dbh->prepare($query);
	if($sth->execute())
	{
		print "Updated\n";
		$sth->finish();	
	}
	else
	{
		print "QUERY:: $query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

###### Select Count Details ####
sub Retrieve_Count_Details
{
	my $dbh			= shift;
	my $Table_Name 	= shift;
	my $Buyer_ID 	= shift;
	my $Batch_ID 	= shift;

	my $query = "select count(*) from $Table_Name where Buyer_ID=\'$Buyer_ID\' and Batch_ID=\'$Batch_ID\'";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my ($Count);
	while(my @record = $sth->fetchrow)
	{
		$Count=&Trim($record[0]);
	}
	$sth->finish();
	return ($Count);
}

sub Trim()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}

sub Insert_Spend_Data()
{
	my $dbh 			= shift;
	my $Table_Name		= shift;
	my $Buyer_ID		= shift;
	my $Batch_ID		= shift;
	my $S_No			= shift;
	my $Month_Of_Document= shift;
	my $Transaction_ID	= shift;
	my $Requirement		= shift;
	my $Amount			= shift;
	my $Start_Date		= shift;
	my $End_Date		= shift;
	my $Supplier_Name	= shift;
	my $Department_Name	= shift;
	my $Requirement_2	= shift;

	$Supplier_Name=~s/\\'/\'/igs;
	$Department_Name=~s/\\'/\'/igs;
	$Requirement=~s/\\'/\'/igs;
	$Requirement_2=~s/\\'/\'/igs;
	
	$Supplier_Name=~s/\'/\'\'/igs;
	$Department_Name=~s/\'/\'\'/igs;
	$Requirement=~s/\'/\'\'/igs;
	$Requirement_2=~s/\'/\'\'/igs;
	
	
	my $insert_query = "insert into $Table_Name   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values (\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',now(),\'$S_No\',\'$Transaction_ID\',\'$Requirement\',\'$Amount\',\'$Start_Date\',\'$End_Date\',\'$Supplier_Name\',\'$Department_Name\',\'$Requirement_2\') ";
	print "$insert_query\n";
	my $sth = $dbh->prepare($insert_query);
	if($sth->execute())
	{
		# print "Executed\n";
	}
	else
	{
		print "QUERY:: $insert_query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $insert_query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

sub Insert_Cleaned_Spend_Data()
{
	my $dbh 			= shift;
	# my $Table_Name		= shift;
	# my $Buyer_ID		= shift;
	# my $Batch_ID		= shift;
	# my $Month_Of_Document= shift;
	# my $Processed_Date	= shift;
	# my $S_No			= shift;
	# my $Transaction_ID	= shift;
	# my $Requirement		= shift;
	# my $Amount			= shift;
	# my $Start_Date		= shift;
	# my $End_Date		= shift;
	# my $Supplier_Name	= shift;
	# my $Department_Name	= shift;
	# my $Requirement_2	= shift;
	# my $Supplier_Match_Flag	= shift;
	my $insert_query	= shift;

	# $Supplier_Name=~s/\'/\'\'/igs;
	# $Department_Name=~s/\'/\'\'/igs;
	# $Requirement=~s/\'/\'\'/igs;
	# $Requirement_2=~s/\'/\'\'/igs;
	
	# my $insert_query = "insert into $Table_Name   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2,Supplier_Match_Flag) values (\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',\'$Processed_Date\',\'$S_No\',\'$Transaction_ID\',\'$Requirement\',\'$Amount\',\'$Start_Date\',\'$End_Date\',\'$Supplier_Name\',\'$Department_Name\',\'$Requirement_2\',\'$Supplier_Match_Flag\') ";
	
	my $sth = $dbh->prepare($insert_query);
	if($sth->execute())
	{
		# print "Executed\n";
	}
	else
	{
		print "QUERY:: $insert_query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $insert_query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

sub Insert_Download_Details()
{
	my $dbh 			= shift;
	my $Table_Name		= shift;
	my $Buyer_ID		= shift;
	my $LinkInfo		= shift;
	my $FileName		= shift;
	my $StartDate		= shift;
	my $EndDate			= shift;
	my $No_Of_Records	= shift;
	my $Status			= shift;
	my $Failed_Reason	= shift;
	
	$Failed_Reason=~s/\'/\'\'/igs;
	$Status=~s/\'/\'\'/igs;
	
	my $insert_query = "insert into $Table_Name  (Buyer_ID,LinkInfo,FileName,StartDate,No_Of_Records,Status,Failed_Reason) values (\'$Buyer_ID\',\'$LinkInfo\',\'$FileName\',\'$StartDate\',\'$No_Of_Records\',\'$Status\',\'$Failed_Reason\') ";
	my $sth = $dbh->prepare($insert_query);
	
	# print "\ninsert_query ---> $insert_query\n";
	
	if($sth->execute())
	{
		# print "Executed\n";
	}
	else
	{
		print "QUERY:: $insert_query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $insert_query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

sub Insert_Errors()
{
	my $dbh 	= shift;
	my $Buyer_ID= shift;
	my $Batch_ID	= shift;
	# my $Date	= shift;
	my $Error	= shift;
	my $S_no	= shift;
	
	
	my $insert_query = "insert into Logs  (Buyer_ID,Batch_ID,Researched_Date,Error,Sno) values (\'$Buyer_ID\',\'$Batch_ID\',now(),\'$Error\',\'$S_no\') ";
	print "$insert_query\n";
	# 
	my $sth = $dbh->prepare($insert_query);
	if($sth->execute())
	{
		# print "Executed\n";
	}
	else
	{
		print "QUERY:: $insert_query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $insert_query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

sub Insert_Batch_Details()
{
	my $dbh 	= shift;
	my $Buyer_ID= shift;
	my $Batch_ID	= shift;
	my $No_of_Records	= shift;
	
	Retry:
	my $insert_query = "insert into Batch_Details  (Buyer_ID,Batch_ID,No_of_Records) values (\'$Buyer_ID\',\'$Batch_ID\','$No_of_Records\') ";
	print "$insert_query\n";
	# 
	my $sth = $dbh->prepare($insert_query);
	if($sth->execute())
	{
		# print "Executed\n";
	}
	else
	{
		print "QUERY:: $insert_query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $insert_query."\n";
		close ERR;
		$dbh=&DbConnection();
		goto Retry;
	}
}
sub Retrieve_Schedule_Frquency()
{
	my $dbh 		= shift;
	my $Input_Table	= shift;
	my $Today		= shift;
	my $Priority_ID		= shift;
	
	my $query = "select ID,Buyer_Name,Schedule_Frquency,File_Path from $Input_Table  where Schedule_Frquency=\'$Today\' and Schedule_Serial=\'$Priority_ID\' and Status='Active'";
	
	print "\n$query\n";

	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@Buyer_ID,@Buyer_Name,@Schedule_Frquency,@File_Path);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@Buyer_ID,&Trim($record[0]));
		push(@Buyer_Name,&Trim($record[1]));
		push(@Schedule_Frquency,&Trim($record[2]));
		push(@File_Path,&Trim($record[3]));
	}
	$sth->finish();
	return (\@Buyer_ID,\@Buyer_Name,\@Schedule_Frquency,\@File_Path);
}

sub Update_Export_Flag()
{
	my $dbh 			= shift;
	my $Table_Name		= shift;
	my $Buyer_ID		= shift;
	my $Batch_ID 		= shift;
	my $Export_Flag		= shift;
	my $Filestorepath	= shift;

	
	Try3:
	my $query = "Update $Table_Name set Export_flag=\'$Export_Flag\' , Exported_Location=\'$Filestorepath\' where Buyer_ID=\'$Buyer_ID\' and Batch_ID=\'$Batch_ID\'";
	my $sth = $dbh->prepare($query);
	if($sth->execute())
	{
		print "Updated\n";
		$sth->finish();	
	}
	else
	{
		print "QUERY:: $query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $query."\n";
		close ERR;
		$dbh=&DbConnection();
		goto Try3;
	}
}

sub Retrive_Sign_Check()
{
	my $dbh 		= shift;
	my $Buyer_ID 	= shift;
	my $Input_Table 	= shift;
	
	my $query = "select SIGN_CHECK from $Input_Table where Buyer_ID=\'$Buyer_ID\'";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my ($SIGN_CHECK);
	while(my @record = $sth->fetchrow)
	{
		$SIGN_CHECK=&Trim($record[0]);
	}
	$sth->finish();
	return ($SIGN_CHECK);
}

sub Retrieve_Buyers_Download_Details_For_Import()
{
	my $dbh 		= shift;
	my $Input_Table	= shift;
	my $Batch_ID	= shift;
	my $Export_Flag	= 'Y';
	
	my $query = "select Buyer_ID,Batch_ID,FileName,Exported_Location,Import_BITOOL,Month_Of_Doc,No_Of_Records,source_file_type,Manual_Import from $Input_Table where Export_flag=\'$Export_Flag\' and (Upload_Reference=\'\' or Upload_Reference is null) and Import_BITOOL=\'N\' and QC_Status=1 limit 1";
	print "\n$query\n";

	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@Buyer_ID,@Batch_ID,@FileName,@Exported_Location,@Import_BITOOL,@Month_Of_Doc,@No_Of_Records,@Source_File_Type,@Manual_Import);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@Buyer_ID,&Trim($record[0]));
		push(@Batch_ID,&Trim($record[1]));
		push(@FileName,&Trim($record[2]));
		push(@Exported_Location,&Trim($record[3]));
		push(@Import_BITOOL,&Trim($record[4]));
		push(@Month_Of_Doc,&Trim($record[5]));
		push(@No_Of_Records,&Trim($record[6]));
		push(@Source_File_Type,&Trim($record[7]));
		push(@Manual_Import,&Trim($record[8]));
	}
	$sth->finish();
	return (\@Buyer_ID,\@Batch_ID,\@FileName,\@Exported_Location,\@Import_BITOOL,\@Month_Of_Doc,\@No_Of_Records,\@Source_File_Type,\@Manual_Import);
}

sub Retrieve_Uploaded_Buyers_Download_Details()
{
	my $dbh 		= shift;
	my $Input_Table	= shift;
	
	my $query = "
Select D.Buyer_ID,D.Batch_ID,D.Filename,B.upload_Buyer_Name as Buyer_Name,B.Updated_Instructions as Instructions,B.Spent_Data_weblink,D.Upload_Reference,D.Source_File_Location,D.Exported_Location,DATE_FORMAT(D.EndDate,'%d/%m/%Y') from BUYERS_DOWNLOAD_DETAILS D 
inner join Buyers B on D.Buyer_ID=B.ID
where date(EndDate)=CURDATE() and (D.Upload_Reference is not null or D.Upload_Reference!='')
order by D.Buyer_ID, D.Batch_ID";
	print "\n$query\n";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@Buyer_ID,@Batch_ID,@FileName,@Buyer_Name,@Instructions,@Spent_Data_weblink,@Upload_Reference,@Source_File_Location,@Exported_Location,@EndDate);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@Buyer_ID,&Trim($record[0]));
		push(@Batch_ID,&Trim($record[1]));
		push(@FileName,&Trim($record[2]));
		push(@Buyer_Name,&Trim($record[3]));
		push(@Instructions,&Trim($record[4]));
		push(@Spent_Data_weblink,&Trim($record[5]));
		push(@Upload_Reference,&Trim($record[6]));
		push(@Source_File_Location,&Trim($record[7]));
		push(@Exported_Location,&Trim($record[8]));
		push(@EndDate,&Trim($record[9]));
	}
	$sth->finish();
	return (\@Buyer_ID,\@Batch_ID,\@FileName,\@Buyer_Name,\@Instructions,\@Spent_Data_weblink,\@Upload_Reference,\@Source_File_Location,\@Exported_Location,\@EndDate);
}

sub Retrieve_Error_In_Upload_Buyers_Download_Details()
{
	my $dbh 		= shift;
	my $Input_Table	= shift;
	
	my $query = "
Select D.Buyer_ID,D.Batch_ID,D.Filename,B.upload_Buyer_Name as Buyer_Name,B.Updated_Instructions as Instructions,B.Spent_Data_weblink,D.Upload_Reference,D.Source_File_Location,D.Exported_Location,DATE_FORMAT(D.EndDate,'%d/%m/%Y') from BUYERS_DOWNLOAD_DETAILS D 
inner join Buyers B on D.Buyer_ID=B.ID
where date(EndDate)=CURDATE() and D.QC_Status=2
order by D.Buyer_ID, D.Batch_ID";
	print "\n$query\n";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@Buyer_ID,@Batch_ID,@FileName,@Buyer_Name,@Instructions,@Spent_Data_weblink,@Upload_Reference,@Source_File_Location,@Exported_Location,@EndDate);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@Buyer_ID,&Trim($record[0]));
		push(@Batch_ID,&Trim($record[1]));
		push(@FileName,&Trim($record[2]));
		push(@Buyer_Name,&Trim($record[3]));
		push(@Instructions,&Trim($record[4]));
		push(@Spent_Data_weblink,&Trim($record[5]));
		push(@Upload_Reference,&Trim($record[6]));
		push(@Source_File_Location,&Trim($record[7]));
		push(@Exported_Location,&Trim($record[8]));
		push(@EndDate,&Trim($record[9]));
	}
	$sth->finish();
	return (\@Buyer_ID,\@Batch_ID,\@FileName,\@Buyer_Name,\@Instructions,\@Spent_Data_weblink,\@Upload_Reference,\@Source_File_Location,\@Exported_Location,\@EndDate);
}


sub dt_format()
{
	my $dbh 		= shift;
	my $value	 	= shift;
	
	my $query = "select dt_format(\'$value\')";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	while(my @record = $sth->fetchrow)
	{
		$value=&Trim($record[0]);
	}
	$sth->finish();
	return ($value);
}

sub Today()
{
	my $dbh 		= shift;
	
	my $query = "select DAYNAME(now())";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my ($value);
	while(my @record = $sth->fetchrow)
	{
		$value=&Trim($record[0]);
	}
	$sth->finish();
	return ($value);
}

sub Retrieve_No_Longer_Updated()
{
	my $dbh 		= shift;
	my $Input_Table	= shift;
	
	my $query = "
		select b1.id Buyer_Id,b1.buyer_name Buyer_Name,b1.Instructions ,b1.spent_data_weblink,DATE_FORMAT(b2.startdate,'%d/%m/%Y')Last_Download,DATE_FORMAT(b2.EndDate,'%d/%m/%Y')Last_Upload ,LF.`Last File Taken`Last_File_Taken_Client,LF.Last_File_Taken_Merit,DATEDIFF(date(now()),date(enddate)) Number_Of_Days_Ago
	from buyers b1
	join
	(select buyer_id,startdate,enddate from (
	SELECT    ( CASE a.buyer_id WHEN \@curType THEN \@curRow := \@curRow + 1 ELSE \@curRow := 1 AND \@curType := a.buyer_id END ) + 1 AS rank,a.*
	FROM      buyers_download_details a, (SELECT \@curRow := 0, \@curType := '') r
	ORDER BY  a.buyer_id, StartDate desc
	)t where rank=2 and DATEDIFF(date(now()),date(enddate))>30) b2 on b1.ID = b2.buyer_id
	join spendanalysisreportfile LF on LF.ID=b1.id
	where substr(b1.link_status,1,3) = '200' and status='Active';";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@Buyer_ID,@Buyer_Name,@Instructions,@Spent_Data_weblink,@Last_Download,@Last_Upload,@Last_File_Taken_Client,@Last_File_Taken_Merit,@Number_Of_Days_Ago);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@Buyer_ID,&Trim($record[0]));
		push(@Buyer_Name,&Trim($record[1]));
		push(@Instructions,&Trim($record[2]));
		push(@Spent_Data_weblink,&Trim($record[3]));
		push(@status,&Trim($record[4]));
		push(@Last_Download,&Trim($record[5]));
		push(@Last_Upload,&Trim($record[6]));
		push(@Last_File_Taken_Client,&Trim($record[7]));
		push(@Last_File_Taken_Merit,&Trim($record[8]));
		push(@Number_Of_Days_Ago,&Trim($record[9]));
	}
	$sth->finish();
	return (\@Buyer_ID,\@Buyer_Name,\@Instructions,\@Spent_Data_weblink,\@Last_Download,\@Last_Upload,\@Last_File_Taken_Client,\@Last_File_Taken_Merit,\@Number_Of_Days_Ago);
}

sub Retrieve_Link_Not_Working_Buyers()
{
	my $dbh 		= shift;
	my $Input_Table	= shift;
	
	my $query = "select ID,Buyer_name,Instructions, Spent_Data_Weblink,Link_Status from buyers where Link_Status not like '%200%' and Link_Status not like '%Removed%'";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@Buyer_ID,@Buyer_Name,@Instructions,@Spent_Data_weblink,@Link_Status);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@Buyer_ID,&Trim($record[0]));
		push(@Buyer_Name,&Trim($record[1]));
		push(@Instructions,&Trim($record[2]));
		push(@Spent_Data_weblink,&Trim($record[3]));
		push(@Link_Status,&Trim($record[4]));
	}
	$sth->finish();
	return (\@Buyer_ID,\@Buyer_Name,\@Instructions,\@Spent_Data_weblink,\@Link_Status);
}

sub Check_Records_Count_For_Dup_Check()
{
	my $dbh 		= shift;
	my $Input_Buyer_ID	= shift;
	my $No_Of_Records	= shift;
	my $Table_Name	= shift;
	
	my $query = "SELECT count(Buyer_ID) FROM $Table_Name WHERE QC_Status=1 AND Buyer_ID=$Input_Buyer_ID AND No_Of_Records=$No_Of_Records";
	print "\nDup Check:$query\n";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my $Count=$sth->fetchrow;
	$sth->finish();
	return ($Count);
}