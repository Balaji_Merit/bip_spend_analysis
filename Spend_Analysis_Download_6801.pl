use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use BIP_DB_V1;
use BIP_Column_Map_V1;
# use NET::SSL;

my $Input_Buyer_ID = $0;
$Input_Buyer_ID =~ s/\.pl//igs;
$Input_Buyer_ID =$1 if($Input_Buyer_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

print "\nInput_Buyer_ID---> $Input_Buyer_ID\n";
$Input_Buyer_ID='680';
# my $ua=LWP::UserAgent->new(show_progress=>1);
my $ua=LWP::UserAgent->new(ssl_opts => {SSL_verify_mode => 'SSL_VERIFY_PEER',verify_hostname => 0 }, show_progress=>1);
# my $ua = LWP::UserAgent->new(ssl_opts => { verify_hostname => 1 });
$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/\.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my %Months_To_Num=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");
my @Months1=("January","February","March","April","May","June","July","August","September","October","November","December","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");

############Database Initialization########
my $dbh = &BIP_DB_V1::DbConnection();
###########################################

my $Input_Table='BUYERS';
############ Retrieve Spent Data weblink from Buyers table ###########
my ($ID,$Buyer_Name,$Spent_Data_weblink) = &BIP_DB_V1::RetrieveUrl($dbh,$Input_Buyer_ID,$Input_Table);
my $Buyer_ID 			= @$ID[0];
my $Buyer_Name 			= @$Buyer_Name[0];
my $Buyer_Url 			= @$Spent_Data_weblink[0];
$Buyer_Url="https://www.tewkesbury.gov.uk/transparency#payments-to-suppliers-over-500";
###############
print "Buyer_ID	: $Buyer_ID\n";	
print "Buyer_Name	: $Buyer_Name\n";	
print "Buyer_Url	: $Buyer_Url\n";	

my ($Buyer_content,$Status_Line)=&Getcontent($Buyer_Url); &BIP_Column_Map_V1::Link_Status_Update($dbh,$Buyer_ID,$Status_Line);
open(ts,">Buyer_content_680.html");
print ts "$Buyer_content";
close ts;
# exit;

# To get the Doc_url from the Buyer_content

	
#while($Buyer_content=~m/<a\s*class\s*\=\s*\"media\s*[^>]*?\s*typeexcel\s*\"\s*href\s*\=\s*\"\s*([^>]*?)\s*\"\s*>\s*([^>]*?)\s*<\/a>/igs)
#{
	my $Doc_Link="https://www.tewkesbury.gov.uk/api/census/RecordHit?crumb=BTUWMGOO_FMhMmFjYzhmMTJiZGYzZTg3MGM0ZDExZDE0NTMxMDU0";	
    my $Doc_Link="https://www.tewkesbury.gov.uk/api/census/RecordHit?crumb=BcwbXl945XmPZDViMjI4YWQ4Mzk3ZTVjMGM5NWJiMWNlYjE1Zjcw";

	my $post_url="event=1&data=%7B%22queryString%22%3A%22%22%2C%22referrer%22%3A%22%22%2C%22localStorageSupported%22%3Atrue%2C%22websiteId%22%3A%22573342b927d4bda40384c3ac%22%2C%22templateId%22%3A%2252a74dafe4b073a80cd253c5%22%2C%22website_locale%22%3A%22en-GB%22%2C%22userAgent%22%3A%22Mozilla%2F5.0%20(Windows%20NT%206.3%3B%20WOW64%3B%20rv%3A56.0)%20Gecko%2F20100101%20Firefox%2F56.0%22%2C%22clientDate%22%3A1528899552554%2C%22viewportInnerHeight%22%3A971%2C%22viewportInnerWidth%22%3A1471%2C%22screenHeight%22%3A1080%2C%22screenWidth%22%3A1920%2C%22url%22%3A%22%2Ftransparency%22%2C%22title%22%3A%22Transparency%22%2C%22collectionId%22%3A%2257a88d09893fc0f0fd972d83%22%7D&ss_cvr=69e3ed26-7b78-4c55-8f5f-9c3a18cd8155%7C1527754718608%7C1528888437642%7C1528899500325%7C3";
	my $LinkInfo="Local_spending_data_January_2018.csv";
	
	$LinkInfo=&clean($LinkInfo);			
	my ($File_Date,$File_Month,$File_Year,$Doc_Name,$Doc_Type);	
	my $LinkInfo_Temp=$LinkInfo;
	my $Doc_Link_Temp=$Doc_Link;
	my ($Doc_content,$status_line,$File_Type,$File_Type_cont)=&Postcontent($Doc_Link,$post_url,"www.tewkesbury.gov.uk","https://www.tewkesbury.gov.uk/transparency");
	open(ts,">Buyer_content_6801.html");
print ts "$Doc_content";
close ts;
exit;
	print "LINFO: $LinkInfo_Temp\n";
	#differentiate which is excel and CSV file
	# if($LinkInfo_Temp=~m/\[(\d+)[^>]*?\]/is) 
	# {	
		# my $size_count=$1;
		# if($size_count <= 50)
		# {
			# print "\n This is CSV File \n";
		# }
		# else
		# {
			# print "\n This is Excel File \n";
			# next;
		# }		
	# }
	
	
	my ($Processed_LinkInfo) = &BIP_DB_V1::LinkInfo_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
	
	my @Processed_LinkInfo=@$Processed_LinkInfo;
	# Get Title of the file name
	if($LinkInfo ne '') 
	{	
		$LinkInfo=~s/\s\s+/ /igs;
		print "LinkInfo	: $LinkInfo\n";
		
		if($LinkInfo ~~ @Processed_LinkInfo || $LinkInfo =~ m/^\s*$/)
		{
			print "File Already Processed\n";
			next;
		}
		else
		{			
			print "\nThis is new file to download\n";
		}
	}	
	# Get file link 		
	if($Doc_Link!~m/^http/is)
	{	
		print "\nhi\n";
		my $u1=URI::URL->new($Doc_Link,$Buyer_Url);
		my $u2=$u1->abs;
		$Doc_Link=$u2;
		$Doc_Link=~s/\&amp\;/&/igs;
	}
	print "\nDoc_Link   ---------> $Doc_Link\n";
	my ($Doc_content,$status_line,$File_Type,$File_Type_cont)=&Postcontent($Doc_Link,$post_url,"www.tewkesbury.gov.uk","https://www.tewkesbury.gov.uk/transparency");
	if($Doc_content=~m/xml/is)
	{
		next;
	}
	if($File_Type=~m/\.pdf/is)
	{
		next;
	}
	elsif($File_Type_cont=~m/\.pdf/is)
	{
		next;
	}		
	
	# if($LinkInfo_Temp=~m/(Quarter\s*\d+)\s*(\d+\s*\-\s*\d+)\s*/is)
	# {
		# $File_Month=$1;
		# $File_Year=$2;		
	# }		
		
	
	# Get Date, Month, Year from the link info for file location Creation
	if($LinkInfo_Temp=~m/Data\s*(?:\-)?\s*([\w]*?)\s*(2\d{3})/is) 
	{		
		$File_Month=$1;
		$File_Year=$2;
		# print "File_Date	: $File_Date\n";
		print "File_Month	: $File_Month\n";
		print "File_Year	: $File_Year\n";
	}
	
	
	if($File_Type=~m/(csv|xlsx|xls)/is)
	{
		$Doc_Type=$1;
		print "\nI:1\n";
	}
	elsif($File_Type_cont=~m/(csv)/is)
	{
		$Doc_Type="$1";
		print "\nI:2\n";	
	}
	elsif($Doc_Link_Temp=~m/^[^>]*?\.([\w]*?)\s*$/is)
	{
		$Doc_Type="$1";		
		print "\nI:5\n";	
	}			
	
	
	$Doc_Type=~s/\"//igs;
	$Doc_Type=~s/\'//igs;
	$Doc_Type=~s/\;//igs;
	$Doc_Type=~s/\://igs;
	
	# if($File_Year eq '')
	# {
		 # if($LinkInfo_Temp=~m/(Month\s*\d+)\s*[^>]*?\s*(\d+\s*(?:\-|\/)\s*\d+)/is)
		# {
			# $File_Month=$1;
			# $File_Year=$2;		
		# }		
	# }	
	my $Month_Of_Document = "$File_Month $File_Year";	
	print "**************************************************************\n";
	
	print "DOC LINKS	---> $Doc_Link\n";
	print "LINKINOF_TEMP---> $LinkInfo_Temp\n";
	print "FILE MONTH	----> $File_Month\n";
	print "FILE YEAR-----> $File_Year\n";
	print "DOC TYPE------>$Doc_Type\n";
	print "MONTH OF DOC----->$File_Month $File_Year\n";
	
	print "\nFILE TYPE---> $File_Type\n";
	print "FILE TYPE CONTENT---> $File_Type_cont\n";
	
	print "**************************************************************\n";
	
	$File_Year=~s/\W//igs;
	# File name creation from Link Info
	my $File_Type = $Doc_Type if ($Doc_Type ne '');
	
	$Doc_Name=$LinkInfo;
	$Doc_Name=~s/\W/\_/igs;
	$Doc_Name=~s/_+/\_/igs;
	$Doc_Name.='.csv';
	print "Doc_Name	: $Doc_Name\n"; 
	
	# File Path creation
	my $hash_ref = &BIP_Column_Map_V1::ReadIniFile();	my %inihashvalue = %{$hash_ref};	my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source";  
	
	# File location creation
	unless ( -d $Filestorepath ) 
	{
		$Filestorepath=~s/\//\\/igs;
		system("mkdir $Filestorepath");
	}
	my $Storefile = "$Filestorepath/$Doc_Name";
	print "STOREPATH  : $Storefile \n";
	my $StartDate=DateTime->now();
	# Spend file download
	
	my $fh=FileHandle->new("$Storefile",'w');
	binmode($fh);
	$fh->print($Doc_content);
	$fh->close();
	my ($Status,$Failed_Reason);
	if($status_line!~m/20/is)
	{
		$Failed_Reason=$status_line;
	}
	else
	{
		$Status='Downloaded';
	}
	print "\nStatus:::: $Status\n";
	<>;
	&BIP_Column_Map_V1::ReadSource($Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
	print "Completed \n";
#}	
	



sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	my $flag=1;
	Home:
	if($flag ne '1')
	{
		$url=$redir_url;
	}
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"www.bassetlawccg.nhs.uk");
	# $req->header("Referer"=>"");
	# $req->header("Referer"=>"");
	# $req->header("Content-Type"=> "text/html; charset=UTF-8");
	# $req->header("Accept"=>"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	# $req->header("Referer"=>"http://www.bassetlawccg.nhs.uk/publication/9215-expenditure-june-2015");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	# print "\nFile_Type---> $File_Type\n";
	# print "\nFile_Type_cont---> $File_Type_cont\n";
	# my $get_cookie=$res->header('set-cookie');
	print "\nCODE---> $code\n";
	print "\nstatus_line---> $status_line\n";
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		print "\nloc--->$loc\n";
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			$flag++;
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$File_Type,$File_Type_cont);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}
sub clean()
{
	my $C_Clean=shift;
	$C_Clean=~s/^\s*|\s*$//igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/<[^>]*?>/ /igs;
	$C_Clean=~s/\&nbsp\;/ /igs;
	$C_Clean=~s/\&pound\;/ /igs;	
	$C_Clean=~s/\#160\;//igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	$C_Clean=~s/\s*\[[0-9]+\s*kb\]$//igs;
	return $C_Clean;
}