#!/usr/bin/perl -w

## Imports
use strict;
use LWP::UserAgent;
use HTTP::Cookies;
use JSON;
use MIME::Lite;
use Cwd;
require BIP_DB;

my %Months=("Jan" => "01", "Jeb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");


# Get Month(MM) and year(YYYY)
my @Current_Date=split(" ",localtime(time));
my $Current_Month_txt=$Current_Date[1];
my $Current_Year=$Current_Date[4];
my $Current_Date=$Current_Date[2];
my $Current_Month=$Months{$Current_Month_txt};

print "Current_Month_txt-->$Current_Month_txt\n";
print "Current_Year-->$Current_Year\n";
print "Current_Date-->$Current_Date\n";
print "Current_Month-->$Current_Month\n";

## User agent setup
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Merit BI OUToader");
$ua->timeout(50);
push @{ $ua->requests_redirectable }, 'POST';

my $cookiefile = $0;
$cookiefile =~s/\.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1);
$ua->cookie_jar($cookie);
my $Input_Table='BUYERS_DOWNLOAD_DETAILS';
############Database Initialization########
my $dbh = &BIP_DB::DbConnection();
###########################################
my ($Buyer_ID1,$Batch_ID1,$FileName1,$Buyer_Name1,$Instructions1,$Spent_Data_weblink1,$Upload_Reference1,$Source_File_Location1,$Exported_Location1,$EndDate1) = &BIP_DB::Retrieve_Uploaded_Buyers_Download_Details($dbh,$Input_Table);
my @Buyer_ID=@$Buyer_ID1;
my @Batch_ID=@$Batch_ID1;
my @FileName=@$FileName1;
my @Buyer_Name=@$Buyer_Name1;
my @Instructions=@$Instructions1;
my @Spent_Data_weblink=@$Spent_Data_weblink1;
my @Upload_Reference=@$Upload_Reference1;
my @Source_File_Location=@$Source_File_Location1;
my @Exported_Location=@$Exported_Location1;
my @EndDate=@$EndDate1;

open(DATA,">Uploaded_Status.xls");
print DATA "Buyer_ID\tBatch_ID\tFileName\tBuyer_Name\tInstructions\tSpent_Data_weblink\tUpload_Reference\tSource_File_Location\tExported_Location\tEndDate\n";
close DATA;
for(my $i=0;$i<@Buyer_ID;$i++)
{
	my $Buyer_ID=$Buyer_ID[$i];
	my $Batch_ID=$Batch_ID[$i];
	my $FileName=$FileName[$i];
	my $Buyer_Name=$Buyer_Name[$i];
	my $Instructions=$Instructions[$i];
	my $Spent_Data_weblink=$Spent_Data_weblink[$i];
	my $Upload_Reference=$Upload_Reference[$i];
	my $Source_File_Location=$Source_File_Location[$i];
	my $Exported_Location=$Exported_Location[$i];
	my $EndDate=$EndDate[$i];
	$FileName=$Upload_Reference."_Export_".$Buyer_ID."_".$FileName;
	open(DATA,">>Uploaded_Status.xls");
	print DATA "$Buyer_ID\t$Batch_ID\t$FileName\t$Buyer_Name\t$Instructions\t$Spent_Data_weblink\t$Upload_Reference\t$Source_File_Location\t$Exported_Location\t$EndDate\n";
	close DATA;
}

&send_mail("BIP Uploaded Status on $Current_Date-$Current_Month-$Current_Year","Uploaded_Status.xls");

my ($Buyer_ID1,$Batch_ID1,$FileName1,$Buyer_Name1,$Instructions1,$Spent_Data_weblink1,$Upload_Reference1,$Source_File_Location1,$Exported_Location1,$EndDate1) = &BIP_DB::Retrieve_Error_In_Upload_Buyers_Download_Details($dbh,$Input_Table);
my @Buyer_ID=@$Buyer_ID1;
my @Batch_ID=@$Batch_ID1;
my @FileName=@$FileName1;
my @Buyer_Name=@$Buyer_Name1;
my @Instructions=@$Instructions1;
my @Spent_Data_weblink=@$Spent_Data_weblink1;
my @Upload_Reference=@$Upload_Reference1;
my @Source_File_Location=@$Source_File_Location1;
my @Exported_Location=@$Exported_Location1;
my @EndDate=@$EndDate1;

open(DATA,">Error_Upload_Status.xls");
print DATA "Buyer_ID\tBatch_ID\tFileName\tBuyer_Name\tInstructions\tSpent_Data_weblink\tUpload_Reference\tSource_File_Location\tExported_Location\tEndDate\n";
close DATA;
for(my $i=0;$i<@Buyer_ID;$i++)
{
	my $Buyer_ID=$Buyer_ID[$i];
	my $Batch_ID=$Batch_ID[$i];
	my $FileName=$FileName[$i];
	my $Buyer_Name=$Buyer_Name[$i];
	my $Instructions=$Instructions[$i];
	my $Spent_Data_weblink=$Spent_Data_weblink[$i];
	my $Upload_Reference=$Upload_Reference[$i];
	my $Source_File_Location=$Source_File_Location[$i];
	my $Exported_Location=$Exported_Location[$i];
	my $EndDate=$EndDate[$i];
	$FileName=$Upload_Reference."_Export_".$Buyer_ID."_".$FileName;
	open(DATA,">>Error_Upload_Status.xls");
	print DATA "$Buyer_ID\t$Batch_ID\t$FileName\t$Buyer_Name\t$Instructions\t$Spent_Data_weblink\t$Upload_Reference\t$Source_File_Location\t$Exported_Location\t$EndDate\n";
	close DATA;
}
&send_mail("BIP Error Uploaded Status on $Current_Date-$Current_Month-$Current_Year","Error_Upload_Status.xls");

sub send_mail()
{
	my $subject = shift;
	my $File_Name = shift;
	my $dir=getcwd();
	$dir=~s/\//\\/igs;
	$dir=$dir.'\\'.$File_Name;	
	# my $host ='mail.meritgroup.co.uk'; 
	# my $from='autoemailsender@meritgroup.co.uk';
	# my $user='autoemailsender';
	# my $to ='arul.kumaran@meritgroup.co.uk';
	# my $pass='T!me#123456';	
	
	my $host ='74.80.234.196'; 
	my $from='autoemailsender@meritgroup.co.uk';
	my $user='meritgroup';
	my $to ='arul.kumaran@meritgroup.co.uk';
	my $pass='11meritgroup11';
	my $body = "Hi Arul, <br><br>\t\tPlease find the attached status file<br><br>Regards<br>BIP TEAM";
	
	my $msg = MIME::Lite->new (
	  From => $from,
	  To => $to,
	  # Cc => $cc,
	  Subject => $subject,
	  Data => $body,
	  Type =>'multipart/mixed'
	) or die "Error creating multipart container: $!\n";
	$msg->attach(
		Type=> "text/html",
		Data     => $body
	);
	$msg->attach (
	   Type => 'application/csv',
	   Path=> $dir,
	   Filename => $File_Name,
	   Disposition => 'attachment'
	) or die "Error adding $!\n";
	
    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	$msg->send;
}