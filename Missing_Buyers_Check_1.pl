#!/usr/bin/perl -w

## Imports
use strict;
use LWP::UserAgent;
use HTTP::Cookies;
use JSON;
use MIME::Lite;
use Date::Parse;
use Date::Manip;
use POSIX 'strftime';
use Cwd;
require BIP_DB;

my %Months=("Jan" => "01", "Jeb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");

# Get Month(MM) and year(YYYY)
my @Current_Date=split(" ",localtime(time));
my $Current_Month_txt=$Current_Date[1];
my $Current_Year=$Current_Date[4];
my $Current_Date=$Current_Date[2];
my $Current_Month=$Months{$Current_Month_txt};

print "Current_Month_txt-->$Current_Month_txt\n";
print "Current_Year-->$Current_Year\n";
print "Current_Date-->$Current_Date\n";
print "Current_Month-->$Current_Month\n";

## User agent setup
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Merit BI OUToader");
$ua->timeout(50);
push @{ $ua->requests_redirectable }, 'POST';

my $cookiefile = $0;
$cookiefile =~s/\.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1);
$ua->cookie_jar($cookie);
my $Input_Table='BUYERS_DOWNLOAD_DETAILS';
my $Input_Table1='BUYERS';
############Database Initialization########
my $dbh = &BIP_DB::DbConnection();
###########################################
my ($Buyer_Id,$Buyer_Name,$Instructions,$Spent_Data_weblink,$Start_Date,$End_Date,$Last_File_Taken_Client,$Last_File_Taken_Merit,$Number_Of_Days_Ago) = &BIP_DB::Retrieve_No_Longer_Updated($dbh,$Input_Table);
my @Buyer_ID=@$Buyer_Id;
my @Buyer_Name=@$Buyer_Name;
my @Instructions=@$Instructions;
my @Spent_Data_weblink=@$Spent_Data_weblink;
# my @status=@$status;
my @Start_Date=@$Start_Date;
my @End_Date=@$End_Date;
my @Last_File_Taken_Client=@$Last_File_Taken_Client;
my @Last_File_Taken_Merit=@$Last_File_Taken_Merit;
my @Number_Of_Days_Ago=@$Number_Of_Days_Ago;

open(DATA,">No_Longer_Update.xls");
print DATA "Buyer_ID\tBuyer_Name\tInstructions\tSpent_Data_weblink\tLast_File_Downloaded\tLast_File_Uploaded\tLast_File_Taken_Client\tLast_File_Taken_Merit\tNumber_Of_Days_Ago\n";
for(my $i=0;$i<@Buyer_ID;$i++)
{
	my $Buyer_ID=$Buyer_ID[$i];
	my $Buyer_Name=$Buyer_Name[$i];
	my $Instructions=$Instructions[$i];
	my $Spent_Data_weblink=$Spent_Data_weblink[$i];
	my $File_Path='C:\BIP\BIP_Live';
	my $Execution_Status;
	my $Try_Again=0;
	my $Script_Name='Spend_Analysis_Download_'.$Buyer_ID.'.pl';
	print "Buyer_ID: $Buyer_ID\n";
	my $Return_Code;
	open(TIME,">>Time_Log.txt");
	print TIME "Start:\t$Buyer_ID\t".localtime()."\n";
	eval{
		$Return_Code=system("PERL","$File_Path/$Script_Name");
		print "Return: $Return_Code\n";
	};
	open(ERR,">>Failed_Log.txt");
	if($Return_Code!=0)
	{
		$Execution_Status="Failed to Execute";
		print "Status: $Buyer_ID: $Execution_Status : $!\n";
		print ERR "$Buyer_ID\t$Execution_Status\t$!\n";
	}
	else
	{
		$Execution_Status="Success";
		print "Status: $Execution_Status : $!\n";
		print ERR "$Buyer_ID\t$Execution_Status: $!\n";
	}
	close ERR;
	print TIME "End:\t$Buyer_ID\t".localtime()."\n";
	close TIME;
	Try_Again:
	my $query = "Update $Input_Table1 set Execution_Status=\'$Execution_Status\',Date_Researched=now() where ID=\'$Buyer_ID\'";
	my $sth = $dbh->prepare($query);
	if($sth->execute())
	{
		print "Updated\n";
		$sth->finish();	
	}
	else
	{
		$dbh=&BIP_DB::DbConnection();
		if($Try_Again==0)
		{
			$Try_Again=1;
			goto Try_Again;
		}
		else
		{
			open(ERR,">>Exec_Failed_Query.txt");
			print ERR $query."\n";
			close ERR;
		}	
	}
	# my $status=$status[$i];
	my $Start_Date=$Start_Date[$i];
	my $End_Date=$End_Date[$i];
	my $Last_File_Taken_Client=$Last_File_Taken_Client[$i];
	my $Last_File_Taken_Merit=$Last_File_Taken_Merit[$i];
	my $Number_Of_Days_Ago=$Number_Of_Days_Ago[$i];
	print DATA "$Buyer_ID\t$Buyer_Name\t$Instructions\t$Spent_Data_weblink\t$Start_Date\t$End_Date\t$Last_File_Taken_Client\t$Last_File_Taken_Merit\t$Number_Of_Days_Ago\n";
}
close DATA;
my $fulldate = strftime "%b %d, %Y %A %H:%M:%S", localtime;
# &send_mail("BiP Solutions_SA_ No Longer Update Status_$fulldate","No_Longer_Update.xls");

sub send_mail()
{
	my $subject = shift;
	my $File_Name = shift;
	my $dir=getcwd();
	$dir=~s/\//\\/igs;
	$dir=$dir.'\\'.$File_Name;	

	# my $host ='mail.meritgroup.co.uk'; 
	# my $from='autoemailsender@meritgroup.co.uk';
	# my $user='autoemailsender';
	# my $to ='arul.kumaran@meritgroup.co.uk';
	# my $pass='T!me#123456';	
	
	my $host ='74.80.234.196'; 
	my $from='autoemailsender@meritgroup.co.uk';
	my $user='meritgroup';
	my $to ='spend.reports@meritgroup.co.uk';
	# my $to ='balaji.ekambaram@meritgroup.co.uk';
	my $cc ='arul.kumaran@meritgroup.co.uk,balaji.ekambaram@meritgroup.co.uk,vinothkumar.rajendren@meritgroup.co.uk';
	my $pass='sXNdrc6JU';
	my $body = "Greetings from Software support..!<br><br>\t\tThe attached file contains the list of buyer which is not uploaded recently. <br>Please find the attached status file.<br><br>Have a nice day..!<br><br>"."Thanks &amp; Regards,<br>Merit Software Support.";
	
	my $msg = MIME::Lite->new (
	  From => $from,
	  To => $to,
	  Cc => $cc,
	  Subject => $subject,
	  Data => $body,
	  Type =>'multipart/mixed'
	) or die "Error creating multipart container: $!\n";
	$msg->attach(
		Type=> "text/html",
		Data     => $body
	);
	$msg->attach (
	   Type => 'application/csv',
	   Path=> $dir,
	   Filename => $File_Name,
	   Disposition => 'attachment'
	) or die "Error adding $!\n";
	
    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	$msg->send;
}