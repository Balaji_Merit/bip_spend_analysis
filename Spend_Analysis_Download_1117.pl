use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use BIP_DB_V1;
use BIP_Column_Map_V1;

my $Input_Buyer_ID = $0;
$Input_Buyer_ID =~ s/\.pl//igs;
$Input_Buyer_ID =$1 if($Input_Buyer_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

# $Input_Buyer_ID='689';
# my $ua  = LWP::UserAgent->new(
        # ssl_opts => { verify_hostname => 1 },
    # );
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my %Months_To_Num=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");
my @Months1=("January","February","March","April","May","June","July","August","September","October","November","December","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");

############Database Initialization########
my $dbh = &BIP_DB_V1::DbConnection();
###########################################

my $hash_ref = &BIP_Column_Map_V1::ReadIniFile();
my %inihashvalue = %{$hash_ref};
my $Input_Table='BUYERS';
############ Retrieve Spent Data weblink from Buyers table ###########
my ($ID,$Buyer_Name,$Spent_Data_weblink) = &BIP_DB_V1::RetrieveUrl($dbh,$Input_Buyer_ID,$Input_Table);
my $Buyer_ID 			= @$ID[0];
my $Buyer_Name 			= @$Buyer_Name[0];
my $Buyer_Url 			= @$Spent_Data_weblink[0];
$Buyer_Url 			="https://www.whh.nhs.uk/about-us/corporate-publications-and-statutory-information/statutory-information";
$Buyer_Url 			="https://whh.nhs.uk/about-us/how-we-work-our-board-and-governors/finance-and-commercial-development-division";
###############
print "Buyer_ID	: $Buyer_ID\n";	
print "Buyer_Name	: $Buyer_Name\n";	
print "Buyer_Url	: $Buyer_Url\n";	

my ($Buyer_content,$Status_Line)=&Getcontent($Buyer_Url); &BIP_Column_Map_V1::Link_Status_Update($dbh,$Buyer_ID,$Status_Line);
open(ts,">Buyer_content215.html");
print ts "$Buyer_content";
close ts;

# my ($Processed_LinkInfo) = &BIP_DB_V1::LinkInfo_RetrieveUrl($dbh,$Buyer_ID,"bip_pdf_json_status");
my ($Processed_LinkInfo) = &BIP_DB_V1::LinkInfo_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
my @Processed_LinkInfo=@$Processed_LinkInfo;
# print "Processed_LinkInfo :: @Processed_LinkInfo \n";exit;

# while($Buyer_content=~m/href\=\"([^>]*?\s*\.pdf)\"\s*target\=\"_blank\">\s*<span\s*class\=\"pdf\">Spend\s*by\s*([^>]*?)\s*<\/span>/igs)
while($Buyer_content=~m/<a[^>]*?href="([^>]*?download[^>]*?)"[^>]*?>Spend\s*by\s*([^>]*?)\s*<\/a>/igs)
{
	my $Doc_Link=$1;
	my $LinkInfo=$2;
	$LinkInfo=&clean($LinkInfo);		
	my $LinkInfo_Temp=$LinkInfo;
	my ($File_Date,$File_Month,$File_Year,$Doc_Name,$Doc_Type);
	next if($Doc_Link=~m/\.pdf/is);		
	# Get Title of the file name
	print "LinkInfo :: $LinkInfo \n";
	if($LinkInfo ne '') 
	{			
		$LinkInfo=~s/\s\s+/ /igs;
		print "LinkInfo	: $LinkInfo\n";
		
		if($LinkInfo ~~ @Processed_LinkInfo || $LinkInfo =~ m/^\s*$/)
		{
			print "File Already Processed\n";
			next;
		}
		else
		{			
			print "\nThis is new file to download\n";
		}		
	}	
	####### Get file link 	#################################	
	if($Doc_Link!~m/^http/is)
	{	
		print "\nhi\n";
		my $u1=URI::URL->new($Doc_Link,$Buyer_Url);
		my $u2=$u1->abs;
		$Doc_Link=$u2;
		$Doc_Link=~s/\&amp\;/&/igs;
	}	
	
	my $Doc_Link_Temp=$Doc_Link;	
		
	foreach my $monthss(@Months1)	
	{		
		if($LinkInfo_Temp=~m/$monthss/is)
		{
			$File_Month=$monthss;
			if($LinkInfo_Temp=~m/(2\d{3})/is)
			{
				$File_Year=$1;
			}			
			else
			{
				while($LinkInfo_Temp=~m/(\d{2,4})/igs)
				{					
					$File_Year=$1;
				}
			}	
			last;
		}			
	}	
	###### print "Doc_Link :: $Doc_Link \n";exit;
	my ($Doc_content,$status_line,$File_Type,$File_Type_cont)=&Getcontent($Doc_Link);
	
	if($File_Type=~m/(csv|xlsx|xls|pdf)/is)
	{
		$Doc_Type="$1";
		print "\nI:1\n";
	}
	elsif($File_Type_cont=~m/(csv|pdf)/is)
	{
		$Doc_Type="$1";
		print "\nI:2\n";	
	}
	elsif($Doc_Link_Temp=~m/^[^>]*?\.([\w]*?)\s*$/is)
	{
		$Doc_Type="$1";		
		print "\nI:5\n";	
	}			

	next if($File_Type=~m/\.pdf/is);		
	$Doc_Type=~s/\"//igs;
	$Doc_Type=~s/\'//igs;
			
	# if($LinkInfo_Temp=~m/(Month\s*\d+)\s(\d+\s*\-\s*\d+)/is)
	# {
		# $File_Month=$1;
		# $File_Year=$2;		
	# }
	my $Month_Of_Document = "$File_Month $File_Year";	
	
	# File name creation from Link Info
	my $File_Type = $Doc_Type if ($Doc_Type ne '');
	$File_Year=~s/\W+//igs;
	$Doc_Name=$LinkInfo;
	$Doc_Name=~s/\W/\_/igs;
	$Doc_Name=~s/_+/\_/igs;
	$Doc_Name.='.'.$File_Type if ($File_Type ne '');
	$Doc_Name.='.xlsx' if ($File_Type eq '');
	print "#########################################\n";
	print "Doc_Name	: $Doc_Name\n"; 
	print "File_Month		: $File_Month\n";
	print "File_Year		: $File_Year\n";
	print "Doc_Type		: $Doc_Type\n";
	print "Doc_Link	: $Doc_Link\n";
	print "Month_Of_Document	: $Month_Of_Document\n";
	print "#########################################";
	# File Path creation
	# my $Filestorepath='D:/Arul/BIP/'.$Buyer_ID."/".$File_Year."/Source";
	my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source";  
	
	# File location creation
	unless ( -d $Filestorepath ) 
	{
		$Filestorepath=~s/\//\\/igs;
		system("mkdir $Filestorepath");
	}
	my $Storefile = "$Filestorepath/$Doc_Name";
	print "STOREPATH  : $Storefile \n";
	my $StartDate=DateTime->now();
	# Spend file download
	
	my $fh=FileHandle->new("$Storefile",'w');
	binmode($fh);
	$fh->print($Doc_content);
	$fh->close();
	my ($Status,$Failed_Reason);
	if($status_line!~m/20/is)
	{
		$Failed_Reason=$status_line;
	}
	else
	{
		$Status='Downloaded';
	}
	print "\nStatus:::: $Status\n";
	<>;
	# &BIP_DB_V1::Insert_Pdf_Json_Details($dbh,$Buyer_ID,$Storefile,$LinkInfo,$Month_Of_Document);
	# print "Storefile :: $Storefile \n";exit;
	&ReadSource($Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
	# &BIP_Column_Map_V1::ReadSource($Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
	print "Completed \n";
}


sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"");
	# $req->header("Referer"=>"");
	# $req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	print "\nFile_Type---> $File_Type\n";
	print "\nFile_Type_cont---> $File_Type_cont\n";
	# my $get_cookie=$res->header('set-cookie');
	print "\nCODE---> $code\n";
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$File_Type,$File_Type_cont);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	if($Data eq '')
	{
		# $Data='Not provided';
	}
	return $Data;
}

sub clean()
{
	my $C_Clean=shift;
	$C_Clean=~s/<[^>]*?>//igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/\&nbsp\;/ /igs;
	$C_Clean=~s/\&amp\;/ /igs;
	$C_Clean=~s/\&pound\;/ /igs;
	$C_Clean=~s/\&\#8211\;/ /igs;
	$C_Clean=~s/ndash\;/ /igs;
	$C_Clean=~s/[^[:print:]]+//igs;
	
	
	$C_Clean=~s/\&nbsp\;//igs;
	$C_Clean=~s/\#160\;//igs;
	$C_Clean=~s/\#163\;//igs;
	$C_Clean=~s/\&/ /igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	return $C_Clean;
}

sub ReadSource()
{
	my $Buyer_ID		= shift;
	my $Filestorepath	= shift;
	my $Doc_Name		= shift;
	my $LinkInfo		= shift;
	my $Month_Of_Document = shift;
	my $StartDate		= shift;
	my $Status			= shift;
	my $Failed_Reason	= shift;
	my ($EndDate,$Number_Of_Records,$Batch_ID,$Mapped_Header_Data_Avail_Column);
	$EndDate="";
	$Number_Of_Records=0;
	my $PDF_to_Json_flag=0;
	################ Retrived Column mapping details from Field_Map_Details ###########################
	my ($File_Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field,$Entity_Code) = &BIP_DB_V1::Column_Name_RetrieveUrl($dbh,$Buyer_ID);
	
	my @Headerarray;
	push(@Headerarray,($Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field));
	@Headerarray = grep { $_ ne '' } @Headerarray;
	print "########################## Headerarray ::@Headerarray\n";
	my $Header_Data_Avail_Column = $#Headerarray+1;
	my $Source_File_Type=$1 if($Doc_Name=~m/\.(xlsx?|csv|pdf|ods)\s*$/is);
		if($Doc_Name=~m/\.xlsx?\s*$/is)
		{
			($Number_Of_Records,$Mapped_Header_Data_Avail_Column) = &Read_Excel_File($Buyer_ID,$Batch_ID,$Filestorepath,$Doc_Name,$Month_Of_Document,\@Headerarray,,$LinkInfo);
		}
		elsif($Doc_Name=~m/\.csv\s*$/is)
		{
			($Number_Of_Records,$Mapped_Header_Data_Avail_Column) = &Read_Csv_File($Buyer_ID,$Batch_ID,$Filestorepath,$Doc_Name,$Month_Of_Document,\@Headerarray);
		}
		elsif($Doc_Name=~m/\.pdf\s*$/is)
		{			
			($Number_Of_Records,$Mapped_Header_Data_Avail_Column) = &Read_Json_File($Buyer_ID,$Batch_ID,$Filestorepath,\@Headerarray,$Month_Of_Document);
			$PDF_to_Json_flag=1;
		}
		# my $Field_Match='N';
		# if($Mapped_Header_Data_Avail_Column==$Header_Data_Avail_Column)
		# {
			# $Field_Match='Y';
		# }
		
		# my ($Records_Count_DB)=&BIP_DB_V1::Retrieve_Count_Details($dbh,"SUPPLIER_SOURCE_DATA",$Buyer_ID,$Batch_ID);
		# my $Import_Flag='N';
		# if($Records_Count_DB==$Number_Of_Records)
		# {
			# $Import_Flag='Y';
		# }
		# print "Import_Flag: $Import_Flag\n";
		# print "Field_Match: $Field_Match\n";
		# $EndDate=DateTime->now();
		# To update Supplier_Match, No_of_Suppliers_Matched, No_of_Suppliers_Not_Matched, Formatted_Flag 
		# &BIP_DB_V1::Update_Import_Flag($dbh,"BUYERS_DOWNLOAD_DETAILS",$Buyer_ID,$Batch_ID,$Import_Flag,$Field_Match,$EndDate,$Number_Of_Records);
		############		
	
}

sub Read_Excel_File
{
	my $Buyer_ID 			= shift;
	my $Batch_ID 			= shift;
	my $Filepath 			= shift;
	my $Docname				= shift;
	my $Month_Of_Document	= shift;
	my $Header_array		= shift;
	my $file_month			= shift;
	my $LinkInfo			= shift;
	my $Doc_Name			= shift;
	my $StartDate			= shift;
	my $EndDate				= shift;
	my $Number_Of_Records	= shift;
	my $Status				= shift;
	my $Failed_Reason		= shift;
	my @Headerarray = @{$Header_array};
	my $Header_Data_Avail_Column = $#Headerarray+1;
	my $Source_File_Type=$1 if($Doc_Name=~m/\.(xlsx?|csv|pdf|ods)\s*$/is);
	my $Outfile = "$Filepath\\$Docname";
	print "LOADFILE : $Outfile\n";
	my ($File_Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field,$Entity_Code) = &BIP_DB_V1::Column_Name_RetrieveUrl($dbh,$Buyer_ID);
	
	my $Book = Win32::OLE->GetObject($Outfile) or die "$!";
	$Book->{'Visible'} = 1;
	$Book->{DisplayAlerts} = 0;
	# Win32::OLE->LastError(0);
	my $Sheets;
	eval{$Sheets = $Book->Worksheets->Count;};
	my $StartDate=DateTime->now();
	my $EndDate='';
	$Number_Of_Records=0;
	
	
	my %Header_col_position;
	
	my (%Hash_buyer,%Hash_batch,%Hash_count,%Hash_Source_Count);
	$Hash_buyer{$Entity_Code} = $Buyer_ID;
	$Hash_batch{$Entity_Code} = $Batch_ID;
	# print "Headerarray :: @Headerarray\n";
	
	foreach my $Sheet (1..$Sheets)
	{
		undef %Header_col_position;
		my $Sno = 0;
		my $insert_collect = 0;
		my $insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
		my $eSheet = $Book->Worksheets($Sheet);
		my $SheetName =$eSheet->Name;
		
		print "SheetName=>$SheetName\n";
		my $LinkInfo =$SheetName;
		my $file_type=$1 if($Docname=~m/(xlsx?)/is);
		$Docname=$LinkInfo;
		$Docname=~s/\W/\_/igs;
		$Docname=~s/_+/\_/igs;		
		$Docname=$Docname.".$file_type";
		print "LinkInfo :: $LinkInfo\n";
		print "Docname :: $Docname\n";

		my ($File_Month,$File_Year,$Month_Of_Document);
		foreach my $monthss(@Months1)	
		{		
			if($LinkInfo=~m/$monthss/is)
			{
				$File_Month=$monthss;
				if($LinkInfo=~m/(2\d{3})/is)
				{
					$File_Year=$1;
				}			
				else
				{
					while($LinkInfo=~m/(\d{2,4})/igs)
					{					
						$File_Year=$1;
					}
				}	
				last;
			}			
		}	
		$Month_Of_Document="$File_Month $File_Year";
		if($LinkInfo ~~ @Processed_LinkInfo || $LinkInfo =~ m/^\s*$/)
		{
			print "File Already Processed\n";
			next;
		}
		
		&BIP_DB_V1::Insert_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Buyer_ID,$LinkInfo,$Docname,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);
		#Get Batch ID from BUYERS_DOWNLOAD_DETAILS
		$Batch_ID = &BIP_DB_V1::BatchID_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
		print "Batch_ID :: $Batch_ID \n";
		
		my $Rowcount = $eSheet->UsedRange->Rows->{'Count'}; 

		my $Colcount = $eSheet->UsedRange->Columns->{'Count'}; 
		my $Headercheck = 0;
		foreach my $Row (1 .. $Rowcount)
		{
			my ($Transaction_ID_Value,$Requirement_Value,$Amount_Value,$Start_Date_Value,$End_Date_Value,$Supplier_Name_Value,$Department_Name_Value,$Requirement_2_Value,$Entity_Field_Value);
			# print "HEADER : $Headercheck\n";
			if ( $Headercheck == 0 )
			{
				foreach my $Col (1 .. $Colcount)
				{
					my $Value 						= &Trim($eSheet->Cells($Row,$Col)->{'Value'});
					my @Header = grep { $Value =~ /$_/is } @Headerarray;
					# print "HEADER :: $Value ::  @Header \n";
					$Header_col_position{$Header[0]} = $Col if (@Header);
				}
				
				if ($Header_col_position{$Transaction_ID}||$Header_col_position{$Requirement}||$Header_col_position{$Amount}||$Header_col_position{$Start_Date}||$Header_col_position{$End_Date}||$Header_col_position{$Supplier_Name}||$Header_col_position{$Department_Name}||$Header_col_position{$Requirement_2})
				{
					$Headercheck = 1;
				}
				
			}
			else
			{
				# exit;
				$Transaction_ID_Value 	= &Trim($eSheet->Cells($Row,$Header_col_position{$Transaction_ID})->{'Value'}) if ($Header_col_position{$Transaction_ID} ne '');
				$Requirement_Value 		= &Trim($eSheet->Cells($Row,$Header_col_position{$Requirement})->{'Value'}) if ($Header_col_position{$Requirement} ne '');
				$Amount_Value 			= &Trim($eSheet->Cells($Row,$Header_col_position{$Amount})->{'Value'}) if ($Header_col_position{$Amount} ne '');
				$Start_Date_Value 		= &Trim($eSheet->Cells($Row,$Header_col_position{$Start_Date})->{'Text'}) if ($Header_col_position{$Start_Date} ne '');
				$End_Date_Value 		= &Trim($eSheet->Cells($Row,$Header_col_position{$End_Date})->{'Text'}) if ($Header_col_position{$End_Date} ne '');
				# $Supplier_Name_Value 	= &Trim($eSheet->Cells($Row,$Header_col_position{$Supplier_Name})->{'Value'}) if ($Header_col_position{$Supplier_Name} ne '');
				$Supplier_Name_Value 	= &Trim($eSheet->Cells($Row,$Header_col_position{$Supplier_Name})->{'Text'}) if ($Header_col_position{$Supplier_Name} ne '');
				# print "\nSupplier_Name_Value--->$Supplier_Name_Value\n";
				$Department_Name_Value 	= &Trim($eSheet->Cells($Row,$Header_col_position{$Department_Name})->{'Value'}) if ($Header_col_position{$Department_Name} ne '');
				$Requirement_2_Value	= &Trim($eSheet->Cells($Row,$Header_col_position{$Requirement_2})->{'Value'}) if ($Header_col_position{$Requirement_2} ne '');
				$Entity_Field_Value  	= &Trim($eSheet->Cells($Row,$Header_col_position{$Entity_Field})->{'Value'}) if ($Header_col_position{$Entity_Field} ne '');
				if($Start_Date_Value =~ m/\#/is)
				{
					$Start_Date_Value   = &Trim($eSheet->Cells($Row,$Header_col_position{$Start_Date})->{'Value'}) if ($Header_col_position{$Start_Date} ne '');
				}
				if($End_Date_Value =~ m/\#/is)
				{
					$End_Date_Value     = &Trim($eSheet->Cells($Row,$Header_col_position{$End_Date})->{'Value'}) if ($Header_col_position{$End_Date} ne '');
				}
				if($Entity_Field)
				{
					unless($Hash_buyer{$Entity_Field_Value})
					{
						my $Ret_Buyer_ID = &BIP_DB_V1::RetrieveBuyerID($dbh,"field_map_details_automation",$Buyer_ID,$Entity_Field_Value);
						# print "Ret_Buyer_ID :: $Ret_Buyer_ID \n";
						if($Ret_Buyer_ID)
						{
							&BIP_DB_V1::Insert_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Ret_Buyer_ID,$LinkInfo,$Doc_Name,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);
							$Batch_ID = &BIP_DB_V1::BatchID_RetrieveUrl($dbh,$Ret_Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
							# print "Batch_ID :: $Entity_Field_Value ::  $Batch_ID \n";<STDIN>;
							$Hash_buyer{$Entity_Field_Value} = $Ret_Buyer_ID;
							$Hash_batch{$Entity_Field_Value} = $Batch_ID;
						}
						else
						{
							$Hash_buyer{$Entity_Field_Value} = '-';
							$Hash_batch{$Entity_Field_Value} = '-';
						}
					}
					next if($Hash_buyer{$Entity_Field_Value} eq '-');
					if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
					{
						$insert_collect++;
						$Supplier_Name_Value=~s/\'/\'\'/igs;
						$Department_Name_Value=~s/\'/\'\'/igs;
						$Requirement_Value=~s/\'/\'\'/igs;
						$Requirement_2_Value=~s/\'/\'\'/igs;
						$Hash_count{$Entity_Field_Value}++;
						$Hash_Source_Count{$Entity_Field_Value}++;
						if($insert_collect <= 990)
						{
						$insert_query .= "(\'$Hash_buyer{$Entity_Field_Value}\',\'$Hash_batch{$Entity_Field_Value}\',\'$Month_Of_Document\',date(now()),\'$Hash_count{$Entity_Field_Value}\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
						}
						else
						{
							$insert_query .= "(\'$Hash_buyer{$Entity_Field_Value}\',\'$Hash_batch{$Entity_Field_Value}\',\'$Month_Of_Document\',date(now()),\'$Hash_count{$Entity_Field_Value}\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
							$insert_query =~ s/\,\s*$//igs;
							$insert_query .= ';';
							&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
							$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
							$insert_collect = 0;
						}
					}
				}
				else
				{
					if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
					{
						$Sno++;
						$insert_collect++;
						$Supplier_Name_Value=~s/\'/\'\'/igs;
						$Department_Name_Value=~s/\'/\'\'/igs;
						$Requirement_Value=~s/\'/\'\'/igs;
						$Requirement_2_Value=~s/\'/\'\'/igs;
						# print "Insert_collec_collect	:: $insert_collect\n";
						if($insert_collect <= 990)
						{
						$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
						}
						else
						{
							$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
							$insert_query =~ s/\,\s*$//igs;
							$insert_query .= ';';
							&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
							$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
							$insert_collect = 0;
						}
					}
				}
			}
		}
		if($insert_collect >= 1)
		{
			$insert_query =~ s/\,\s*$//igs;
			$insert_query .= ';';
			&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
			print "Inside the Outer Insert\n";
		}
		my @Value = values %Header_col_position;
		@Value = grep { $_ ne '' } @Value;
		my $Mapped_Col = $#Value+1;
		my $Field_Match='N';
		if($Mapped_Col==$Header_Data_Avail_Column)
		{
			$Field_Match='Y';
		}
		
		my ($Records_Count_DB)=&BIP_DB_V1::Retrieve_Count_Details($dbh,"SUPPLIER_SOURCE_DATA",$Buyer_ID,$Batch_ID);
		my $Import_Flag='N';
		if($Records_Count_DB==$Sno)
		{
			$Import_Flag='Y';
		}
		print "$Mapped_Col			::	$Header_Data_Avail_Column\n";
		print "$Records_Count_DB	::	$Sno\n";
		print "Import_Flag: $Import_Flag\n";
		print "Field_Match: $Field_Match\n";
		$EndDate=DateTime->now();
		# To update Supplier_Match, No_of_Suppliers_Matched, No_of_Suppliers_Not_Matched, Formatted_Flag 
		&BIP_DB_V1::Update_Import_Flag($dbh,"BUYERS_DOWNLOAD_DETAILS",$Buyer_ID,$Batch_ID,$Import_Flag,$Field_Match,$EndDate,$Sno,$Filepath,$Source_File_Type);
		#############
	}
	$Book->quit();
	
	
	# return ($Sno,$Mapped_Col,\%Hash_buyer,\%Hash_batch,\%Hash_count,\%Hash_Source_Count);
}

sub Trim()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	
	return $txt;
}
