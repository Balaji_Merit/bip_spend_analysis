package BIP_Column_Map;
### Package Name ####
use strict;
use DBI;
use DBD::ODBC;
use Text::CSV;
use Tie::IxHash;
use List::Util 'max';
require Exporter;
my @ISA = qw(Exporter);
my @EXPORT = qw(ImageDownload);
require BIP_DB.pm;

sub Map()
{
	my $Filestorepath=shift;
	my $Doc_Name=shift;
	my $Transaction_ID=shift;
	my $Requirement=shift;
	my $Amount=shift;
	my $Start_Date=shift;
	my $End_Date=shift;
	my $Supplier_Name=shift;
	my $Department_Name=shift;
	my $Requirement_2=shift;
	
	$Transaction_ID=~s/^\s+|\s+$//igs;
	$Requirement=~s/^\s+|\s+$//igs;
	$Amount=~s/^\s+|\s+$//igs;
	$Start_Date=~s/^\s+|\s+$//igs;
	$End_Date=~s/^\s+|\s+$//igs;
	$Supplier_Name=~s/^\s+|\s+$//igs;
	$Requirement_2=~s/^\s+|\s+$//igs;
	$Department_Name=~s/^\s+|\s+$//igs;
	
	
	
	# print "\nTransaction_ID::: $Transaction_ID\n";
	# print "\nAmount::: $Amount\n";
	# print "\nSupplier_Name::: $Supplier_Name\n";
	# print "\nDepartment_Name::: $Department_Name\n";<STDIN>;

	my $csv = Text::CSV->new ({
	binary => 1,
	auto_diag => 1,
	allow_loose_quotes => 1,
	sep_char => ','
	});

	my $sum = 0;
	my $Valid_data=0;
	print "loc::: $Filestorepath$Doc_Name\n";
	
	open(my $data, '<', "$Filestorepath/$Doc_Name") or warn "Could not open '$Doc_Name' $!\n";
	my ($Transaction_ID_number,$Requirement_Number,$Amount_Number,$Start_Date_Number,$End_Date_Number,$Supplier_Name_Number,$Department_Name_Number,$Requirement_2_Number);
	# tie(my %Columns_Count, 'Tie::IxHash');
	my %Columns_Count;
	my $line_no=0;
	while (my $fields = $csv->getline($data)) 
	{
		my @File_Headers = grep(~s/[^[:print:]]//igs,@{$fields});
		my @Data_Avail_Column = grep(/[A-Za-z]/, @File_Headers);
		
		# print "\nData_Avail_Column  :: @Data_Avail_Column\n";
		my $cnt=@Data_Avail_Column;
		$Columns_Count{$line_no}=$cnt;
		$line_no++;
	}	
	my @Columns_Count=values %Columns_Count;
	# print join("\n",@Columns_Count);exit;
	my $Number_Of_Columns=max(@Columns_Count);
	print "\nNumber_Of_Columns---> $Number_Of_Columns\n";
	my $Header_line_number;
	for my $k(keys %Columns_Count)
	{
		# print "\nkey ---- > $k\n";
		# print "\nkey ---- > $Columns_Count{$k} \n";
		if($Number_Of_Columns eq $Columns_Count{$k})
		{
			$Header_line_number=$k;
			last;
		}
	}
	print "Header_line_number: $Header_line_number\n";
	print "Data Avail Column: $Number_Of_Columns\n";
	close $data;
	
	open(my $data, '<', "$Filestorepath/$Doc_Name") or die "Could not open '$Doc_Name' $!\n";
	my $line_no=0;
	my (@File_Headers,@File_Headers1);
	while (my $fields = $csv->getline($data)) 
	{
		@File_Headers1 = @{$fields};
		print "\nFile_Headers1 :: @File_Headers1\n";
		print "\nline_no :: $line_no\n";
		if($line_no == $Header_line_number)
		{
			@File_Headers1 = @{$fields};
			last;
		}	
		$line_no++;
	}
	for my $Each_Header(@File_Headers1)
	{
		$Each_Header=~s/[\[\]\(\)\?\*]/ /igs;
		$Each_Header=~s/\s\s+/ /igs;
		push(@File_Headers,$Each_Header);
	}
	# print "\nFile_Headers:: @File_Headers\n";
	my $Number_Of_Columns=@File_Headers;
	for my $Column_Position(0..$Number_Of_Columns-1)
	{				
		# $File_Headers[$Column_Position]=~s/^\W+|\W+$//igs;
		quotemeta($File_Headers[$Column_Position]);		
		if($Transaction_ID ne '')
		{
			if($Transaction_ID_number eq '')
			{
				if(($File_Headers[$Column_Position]=~m/$Transaction_ID/is) or ($Transaction_ID=~m/$File_Headers[$Column_Position]/is))
				{
					$Transaction_ID_number=$Column_Position;
					# print "Transaction_ID_number:: $Transaction_ID_number\n";
				}
			}	
		}	
		if($Requirement ne '')
		{
			if($Requirement_Number eq '')
			{
				if(($File_Headers[$Column_Position]=~m/$Requirement/is) or ($Requirement=~m/$File_Headers[$Column_Position]/is))
				{
					$Requirement_Number=$Column_Position;
					# print "Requirement_Number:: $Requirement_Number\n";
				}
			}	
		}	
		# print "Amount:: $Amount \n";
		# print "Supplier_Name_Number:: $File_Headers[$Column_Position] \n";
		# print "Amount_Number:: $Amount_Number \n";
					
		if($Amount ne '')
		{
			if($Amount_Number eq '')
			{
				if(($File_Headers[$Column_Position]=~m/$Amount/is) or ($Amount=~m/$File_Headers[$Column_Position]/is))
				{
					$Amount_Number=$Column_Position;
					# print "Amount_Number:: $Amount_Number\n";
					# print "Supplier_Name_Number:: $File_Headers[$Column_Position] \n";
				}
			}	
		}	
		
		if($Start_Date ne '')
		{
			if($Start_Date_Number eq '')
			{
				if(($File_Headers[$Column_Position]=~m/$Start_Date/is) or ($Start_Date=~m/$File_Headers[$Column_Position]/is))
				{
					$Start_Date_Number=$Column_Position;
					# print "Start_Date_Number:: $Start_Date_Number\n";
				}
			}	
		}	
		if($End_Date ne '')
		{
			if($End_Date_Number eq '')
			{
				if(($File_Headers[$Column_Position]=~m/$End_Date/is) or ($End_Date=~m/$File_Headers[$Column_Position]/is))
				{
					$End_Date_Number=$Column_Position;
					# print "End_Date_Number:: $End_Date_Number\n";
				}
			}	
		}	
		
		if($Supplier_Name ne '')
		{
			if($Supplier_Name_Number eq '')
			{
				if(($File_Headers[$Column_Position]=~m/$Supplier_Name/is) or ($Supplier_Name=~m/$File_Headers[$Column_Position]/is))
				{
					$Supplier_Name_Number=$Column_Position;
					# print "Supplier_Name_Number:: $Supplier_Name_Number\n";
					# print "Supplier_Name_Number:: $Supplier_Name \n";
					# print "Supplier_Name_Number:: $File_Headers[$Column_Position] \n";
				}
			}	
		}	
		if($Department_Name ne '')
		{
			if($Department_Name_Number eq '')
			{
				if(($File_Headers[$Column_Position]=~m/\Q$Department_Name\E/is) or ($Department_Name=~m/\Q$File_Headers[$Column_Position]\E/is))
				{
					$Department_Name_Number=$Column_Position;
					# print "Department_Name_Number:: $Department_Name_Number\n";
				}
			}	
		}	
		if($Requirement_2 ne '')
		{
			if($Requirement_2_Number eq '')
			{
				if(($File_Headers[$Column_Position]=~m/$Requirement_2/is) or ($Requirement_2=~m/$File_Headers[$Column_Position]/is))
				{
					$Requirement_2_Number=$Column_Position;
					# print "Requirement_2_Number:: $Requirement_2_Number\n";
				}
			}	
		}	
	}	
	if(not $csv->eof)
	{
		$csv->error_diag();
	}
	close $data;	
	return($Transaction_ID_number,$Requirement_Number,$Amount_Number,$Start_Date_Number,$End_Date_Number,$Supplier_Name_Number,$Department_Name_Number,$Requirement_2_Number);
}