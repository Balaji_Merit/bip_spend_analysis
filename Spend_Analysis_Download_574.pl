use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use BIP_DB_V1;
use BIP_Column_Map_V1;

my $Input_Buyer_ID = $0;
$Input_Buyer_ID =~ s/\.pl//igs;
$Input_Buyer_ID =$1 if($Input_Buyer_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

# my $ua=LWP::UserAgent->new(show_progress=>1);
my $ua=LWP::UserAgent->new(ssl_opts =>{SSL_verify_mode => 'SSL_VERIFY_PEER',verify_hostname => 0 }, show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/\.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my %Months_To_Num=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");

############Database Initialization########
my $dbh = &BIP_DB_V1::DbConnection();
###########################################
my $hash_ref = &BIP_Column_Map_V1::ReadIniFile();
my %inihashvalue = %{$hash_ref};

my $Input_Table='BUYERS';
############ Retrieve Spent Data weblink from Buyers table ###########
my ($ID,$Buyer_Name,$Spent_Data_weblink) = &BIP_DB_V1::RetrieveUrl($dbh,$Input_Buyer_ID,$Input_Table);
my $Buyer_ID 			= @$ID[0];
my $Buyer_Name 			= @$Buyer_Name[0];
my $Buyer_Url 			= @$Spent_Data_weblink[0];
# $Buyer_Url="https://www.n-kesteven.gov.uk/your-council/facts-and-figures-about-the-council/council-spending/council-transparency/december-2014-november-2015/";
###############
print "Buyer_ID		: $Buyer_ID\n";	
print "Buyer_Name	: $Buyer_Name\n";	
print "Buyer_Url	: $Buyer_Url\n";	

my ($Buyer_content,$Status_Line)=&Getcontent($Buyer_Url); &BIP_Column_Map_V1::Link_Status_Update($dbh,$Buyer_ID,$Status_Line);
open(ts,">Buyer_content.html");
print ts "$Buyer_content";
close ts;

my ($Processed_LinkInfo) = &BIP_DB_V1::LinkInfo_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
my @Processed_LinkInfo=@$Processed_LinkInfo;


# To select the files block for every month

my $LinkInfo="September 2017";
print "\n LinkInfo::$LinkInfo";

#my $post_con="EasySitePostBack=&ScrollPos=0&LinkState=&__EVENTTARGET=espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce%24ctlAssetManager%24flwBrowseAssets%24console%24consoleTable%24cell_1_29255&__EVENTARGUMENT=&__VIEWSTATEZIP=7R1dc9vGUTgSJEVKlmJbjPNFIbZiWZFI8QP8suRkbFl2NLUVj6W4zWQyLAgcSUQgwOJDH31opy%2FtTJ%2F62D62k%2F6WvrS%2Fo299b2cy6e0CIEHSH5IljWMb1Cy0h7tb7t3u7X3u8Udu5grHZxqNDUO3TUOzHtNfOapJHxmWfUeS935GjxqNNOH%2FQajVMxdMqivU%2FIJFLjyS2nTHNh3Zdkx6V7V6mnT0GKOpuUAt2dYa5Va5VWoWqlmxXKtnRamZz0plWc42C7VaMU8rzQoVF7Z0nZp%2BxuNlcam3KC0Uq6VqtlTMF1l6uZRt1grlbJ6K9XKlTMVauTVC%2FVhZXOpVKtUqTZElrYv5rNgsiFmpUpeycr1erDTLlWpFpgss4W3LovZDSWe1YS60tIM7pnFgUXxrLciGbhkaXWja%2Bn1TVZ6o9ID%2FZ1iTp6vJ3Y7TbeqSqkF1zvCpbKFaqxQqolgVZ5T0hRZ7kNbM5eUJorQVwg0HyXAwwoKx0TRxFoyyl24YYpT%2B42nv2GOQOtaaSZNMdJce2vyfuHVF3RdkTbKsW1d7VMsajk1NQTY0p6tbI6EmtaWrn00nJ%2Fuf0dwqiKCfHkPDGYZyyBqVAtQN5ejqZ%2F0K8B7R1hDf8xP890M8u7mHGM1KWq8jXRUs%2B0ijt662NEOybwoabdlrAn7lTcFU2x0WOlAVu3NTqNc%2FWQuy%2BRT640V5VkGeUY73gwIfyBowEMf7W7pqq9KQij2mlqPZFkFxBlVgJh3LCF6GHVsybVVvb0g2bRvm0SPTkCkjorQzqUfUtHpUttV9mlr6A7cpWUc7qk1%2FTps535jmXO3NjWj3cjAQILMiBIisCE9YjGrotwq5PPytCBuMYWYibunUsU1JWxEeOU1NlZmN3jX2qH5LdzSNy6Q3HJNZF3vDbTLQSnaPeozH73zquU2NdlkKK%2BelyY2kHTAy4KJyIi5YpUZ9GYEE%2BHugJx0lPeUJCF6mPE53bNpLLf02WINBDoeq8qseI6S4IReH3GdRcxN%2Bu4762hAwA4PGX%2Fw%2BMsFNTEz8yD7wHz5ThD0%2BP2WdRYHaX7idI8umXVZuTQOtYILJ3aeseahy7oFq2b8sfPPNuBy3DVttqbLkpg%2BGHjJ1ZWp2aol%2B%2B22EsRdrMApdi29Y6q9porHvUopOTJRfgqdvvoVqSyQmIxODTxwDfsVGSy9BF6imwLZ7kouGknu9JBcD4xB%2FolpqU6NeL43NMTlojlOhUF8voZLhsZM7yvIHT4Bn4oa%2BR4%2BcHr%2BtO12oYUmDYdQd43Dz0MaBZ2NTt1jNbPvRW3rPsW%2FYHdVaEVqSZtH%2BPzacZk%2BoqaW1TMLQ5Y6kt%2BlZU%2FaGl14B5hNnTX9%2B8uw5jvbl4DHPe0WYVWbn4mmp19M8%2Ba1%2BJ%2B1LlmyqPXsu%2BPqwY3e15cOuNjP0tqtN2ozFVYhNICZbVhIRNqNRdf5Lu0PNuTiX5wpckStxIlfmKpeXI%2FG2%2B0lzLWj7w2oSGRpeKyTxjPhZZhMgf3IoPhEoHYsnnDu6H2SbYXqX2LCsDRhq8rGmY9uGnok2Pt65w0aGCkkNy%2FfhKaThimFEvGdLUCHTrxm%2FYSf9JtjzEYsyeE4NhtJeI44ziCjtDvtrwyQBpgfR%2BST%2FHhiKpnEoWB3DtAWFTbiyPVXeo%2BZ8ClsiTCZi3oRv1s2TUEyjpxgHOiaZSV9pEY6wtk2ihCcxEicJMkmSJEWmyDS5QGbILHmHXCSXyGUyR9Lk3fSVWZ7k8zyXbwNS4LkCIkWeKyJS4rkSIiLPiYiUea6MSIXnKohUea6KSI3naojUea4OSCEPgFgBALEiAGIlAMREAMTKAIhVABCrAiBWA0CsDgBYMQ%2BAWAEAsSIAYiUA17Byz6269RNWHblC3iPvkw%2FIh%2BQjkiHzRCAfk6vkGlkgn5DrZJHcIEvkU7JMVkiW5MgqyZMCKZISEUmZVEiV1Eid3CRr6fW3QwSAiQCIlQEQqwAgVgVArAaAWB0AsFIeALECAGJFAMRKAIiJAIiVARCrACBWBUCsBoBYHQAwMQ%2BAWAEAsSIAYiUAxEQAxMoAiFUAEKsCIFYDQKwOAFg5D4BYAQCxIgBiJQDERADEygCIVQAQqwIgVgNArA7gqnpwbSjU91Df33R9jwz1uv1OtsM62U7YyYadbGh0QqMTdrKhvof6fgp954PrleFENuxjQ5sT2pxztjlhHxvq%2B1un74HNnnjY577yBhD2ua9cBKENCvvcUN9DfT8%2FfQ%2F0uZPBDdtZpu1cK83N8vFtwxYsaoPyYb5UuLH7yhtK2De%2FchGEtirsm0N9D%2FX9XPSdD0%2FHvfan4%2FyhVfFf3JjosNZ%2Fc4xK95Js6XapuCJ0LdkwNTXg6CIetzab1apUlsuVQr0k0nyt%2FryajUP9JCZJoIJm%2FEAi5bnJREkU%2FXvQHYxfNFjelmYc3JQc21jrUPAEu1mq5HuHa647WKkGOJzzhbFlBK11OjLLJzYPexI6F7JGkHQDwm1N42MUcfZ2CmpI6lkU3ydkLxQ4A9E%2FTp4m8%2FGOAgtIYQN6zRtQf%2F1PGbjpBRwXlUBM1BsapBMZHn0H%2BQtNWxcYZC0K8shMoqchnFDnYxvoEpCJdkza4rlr4H4gs3rb43%2BILNxYvAZeuo2Bl27jeV66jeN76TZO7qXbOL6XbuPkXrqN43vpNkb8GBsjXros3tRcpzx8AfHBoGzriNyltqRqVsOWmg8ZyqaEEmb1AxuG3lLbjZblJ2QZvzxgBWs4Pfc%2FYySfb2hMs5jGb%2Bq2ah89wjllA%2Fysgy8Wl3Jt9sqdcd5Y1CRHlzte1IoA59OX1gSTMqHq7jn1tf6MKp2Yn%2BanfQ1CL9T5Czy%2FgcgMU5n5Wf6%2Foa68sbpi0i7rzlhfswVWc3FcT8igu8lcBIZ2pfZd2lLBd5hZt%2BJfz7Ov%2BfOJ%2BprbSpexZTGSkDDn115ujOvz7mrEk7N0nJ6meHKybkcDC17hGOH1HiPARMkd8bkDPt59nVrmOT6JVmRXtTXa7oxc2cDyTPNx72oJ9BULDBw9gpP9GyJwRdOL5kZTtnE47L0cvk%2FA49E1Fwk2UHXTe47xia0uK9BXpsY%2FXKWsJixWEwe02cdXLafXM0y7pWrUWlUhsbUqQZm6rmFd3VfpQQPsVsMd5FAl19Ph%2B6J99%2Fv5i%2FzmaajbEO1R5Yf4Z5TvnYoyXObhk44FGT4VWdmxbKPrkZ10hYdsK96kZWg4OVAZkiTo6%2B8LfBY9kTd1qalRpaPMznFcEeDycoTzV1%2Fx2ozUdb1p9daMllAUBh3DKHXC6HMdWDv3FcPTmHarv%2FDuq%2FSMr6kxX1Mvyd4tFFnbpNS9NAOLsr48wWZp4Mp4YadjHGx0qLx3xzikVurTRc%2FYwJ0KX23Bv%2F6FCruMyLahULjwwZrITG9T1ti9WRhjIIGMwCMdZQX86C6VabdJTaGYL1SErLDN%2BkY%2FXM0kBhn98%2BiRvk%2Bmq%2FIdvBGi326iiE5QFJAb75Y%2B5lXW72Kppd2%2Bcfiy%2BR3T7TO5nIKArynxhBRwBJ1P%2Bg6g3s7EO9BgW56TrMci4WZdpcBaBqWAD8o6Fh7BD7dDwuXhcHk43A4J9T3U9%2FPzcwu717B7Dc1NaG7O2dyE3Wuo72%2BTvg%2BuSvIP%2FU0%2FkCxbeGgoakulCj%2BlsWDXC%2FltpD91D%2B7SwVoEpxb%2Fw5%2FPQioP1HLjC4f3DLObu28aTg8WUmHZfrDICll4XNuM%2FPHllmDHqZ%2FBmitUBy6PRuHxU%2BLsuavB14%2FDRH%2F5N%2Bot%2BALEo%2F7yLyDRheMQwkri2WM6wsO%2FY2VC8jAc%2FGD9tmMbDwxZ0uhne40G3Gut6u17KtWU99Z3vEXT0Zj0%2BhNJc8YyMMY5jgOGYjH2iNzWNIbGoUj5VIZrF%2F%2BdPEed%2F%2FRFBR%2FW91gb3luR35%2BFWp25sv9UuHquol99EQPHUXLhRURGFfxYGVxrNre%2BLXXH1DTKndA8AkHkArQa9Tm2Y0u2Y%2FGxN8vWJkAgiTfX1k4G1XDyJW1t0lXF5E%2FG1qbgCx6o%2B5ThU1CmQkoZOfQ15LagpCcUPJmGm3iRmeBei38H5iDMj4RjI%2BH4SDgxEp4cCSdHwqmR8NToPfLDF%2Bg958L54csvo89O7c5XxJ7UplnqCs67xt0A8d1xVE1hgRbDhcHd8hC0cG7jb40gzQib999Y%2BoXXXu47qnKmhxAXxGpFromykm2KJTErVmvVbD1fqmUlUS5Uq5UC%2B19zT3e519SDTN2hZuA%2B%2F8FeK%2BGKf4%2BdYy%2B82G8S%2FumZHN5tmoNzP1s6VCLuVoOV%2BWB9sHM13iDcuK2xBvHuOpJ6SkPBnezR1x%2Bv35Vsek812QDApNJTWpibAobz%2Fmh%2BNEUGU2CRrc549IfI0Y7T7Urm0VhvwxpkJBLhLj1lu841S7%2FydGeb1SFspgYOtcLX7qpdesbnWt%2BGb8RRwA9Mx59W8dgB7INdbTQmEpA0HjiFkYRPauyHLLxt8nQyaEvgfO8DqUk1%2FuIjjUoWFTRqC44l7OnGgbslqszORSJfU%2BsagxWh6diC2u2ZsEUMPYYgG46mCE0qdCWFkm1jLsLV2QyZFAqXlyORdrvt7qEGv3L%2Bff76F8aBl%2FWA%2BgTxzJ6gDlrZ57jxCjPO6GPjwCI8nsaYT%2FAV272i9oHahcNfIzfVPpQOH1C9bXe8S2qLZSaCxUW85fYlc7o73Zk5lkDtOt2NjmRKrLszkQz5H9c%2F0N8ePt%2BKhZ382nBMQWdjuf6Zssn%2BNxFF6a9HYepL%2FdTCDTwCZ3aX%2BvvYHxHF3ViPjH7JRcxGu5KqCZKimNSy%2FOuCSSZqM63heYwd%2FrYPx7ONfi1LPJ%2FpZ44GyvnMkzOcb8qj7hmKa%2Buriro%2F%2BBWQ4aAbCmhJ1PvFkr%2B98BdL8JdVjv%2BDJYXz%2BL2SE5XRS7w0kngsfSDL4CzUyDkSfjgYGw7Gh4L%2FBw%3D%3D&__VIEWSTATE=%2FwEPZmTmdbBv690e15MeCfmpE3bSYCtwezSVw%2Bwn6uRw94py3w%3D%3D&PageNotifications%24hfState=&floatingwindowTab_UploadAsset_espr_renderHost_PageStructureDisplayRenderer_esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_esctl_fee12737-3202-45c3-b815-0e49565e485f_InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f_esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce_ctlAssetManager_flwBrowseAssets_ctrlUploadAsset=0&floatingwindowVisible_UploadAsset_espr_renderHost_PageStructureDisplayRenderer_esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_esctl_fee12737-3202-45c3-b815-0e49565e485f_InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f_esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce_ctlAssetManager_flwBrowseAssets_ctrlUploadAsset=0&floatingwindowConfig_UploadAsset_espr_renderHost_PageStructureDisplayRenderer_esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_esctl_fee12737-3202-45c3-b815-0e49565e485f_InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f_esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce_ctlAssetManager_flwBrowseAssets_ctrlUploadAsset=%7B%22name%22%3A%22UploadAsset_espr_renderHost_PageStructureDisplayRenderer_esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_esctl_fee12737-3202-45c3-b815-0e49565e485f_InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f_esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce_ctlAssetManager_flwBrowseAssets_ctrlUploadAsset%22%2C%22title%22%3A%22New%22%2C%22width%22%3A800%2C%22height%22%3A650%2C%22collapsible%22%3Afalse%2C%22closable%22%3Atrue%2C%22modal%22%3Atrue%2C%22autoScroll%22%3Afalse%2C%22autoTabs%22%3Afalse%2C%22shadow%22%3Atrue%2C%22constrain%22%3Afalse%2C%22minWidth%22%3A650%2C%22maxWidth%22%3A1024%2C%22minHeight%22%3A500%2C%22maxHeight%22%3A1000%2C%22buttons%22%3A%5B%7B%22id%22%3A%22ok%22%2C%22text%22%3A%22Next%22%7D%2C%7B%22id%22%3A%22cancel%22%2C%22text%22%3A%22Cancel%22%7D%5D%2C%22showOnLoad%22%3Afalse%2C%22focusOnLoad%22%3Afalse%2C%22postbackOnClose%22%3Atrue%2C%22postbackOnResize%22%3Afalse%2C%22postbackOnCancel%22%3Atrue%2C%22maintainPostbackVisibility%22%3Atrue%2C%22controlId%22%3A%22espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce%24ctlAssetManager%24flwBrowseAssets%24ctrlUploadAsset%24flwUploadAsset%22%2C%22contentElementId%22%3A%22floatingwindowContent_UploadAsset_espr_renderHost_PageStructureDisplayRenderer_esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_esctl_fee12737-3202-45c3-b815-0e49565e485f_InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f_esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce_ctlAssetManager_flwBrowseAssets_ctrlUploadAsset%22%2C%22selectedTabHiddenFieldId%22%3A%22floatingwindowTab_UploadAsset_espr_renderHost_PageStructureDisplayRenderer_esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_esctl_fee12737-3202-45c3-b815-0e49565e485f_InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f_esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce_ctlAssetManager_flwBrowseAssets_ctrlUploadAsset%22%2C%22visibilityStateHiddenFieldId%22%3A%22floatingwindowVisible_UploadAsset_espr_renderHost_PageStructureDisplayRenderer_esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_esctl_fee12737-3202-45c3-b815-0e49565e485f_InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f_esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce_ctlAssetManager_flwBrowseAssets_ctrlUploadAsset%22%7D&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce%24ctlAssetManager%24flwBrowseAssets%24console%24consoleMessagePanel%24hfState=&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_0b2c3462-b8da-44cd-b9fd-2dfe792de087%24ctl00%24InnerRenderer_0b2c3462-b8da-44cd-b9fd-2dfe792de087%24esctl_130e05f8-ab28-44e4-ad59-27c1fdab1e7f%24ctl00%24InnerRenderer_130e05f8-ab28-44e4-ad59-27c1fdab1e7f%24esctl_2d2ec58c-3f3b-4810-b613-d64b98727ca1%24notificationPanel%24hfState=&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_0b2c3462-b8da-44cd-b9fd-2dfe792de087%24ctl00%24InnerRenderer_0b2c3462-b8da-44cd-b9fd-2dfe792de087%24esctl_130e05f8-ab28-44e4-ad59-27c1fdab1e7f%24ctl00%24InnerRenderer_130e05f8-ab28-44e4-ad59-27c1fdab1e7f%24esctl_2d2ec58c-3f3b-4810-b613-d64b98727ca1%24formBuilderUI%24fieldset%24formBuilderStructureInputRendererHost%24FormBuilderStructureInputRenderer%24esctl_eb40337a-4eb0-4ba4-ac7d-1df5eadca107%24cContainer%24TextArea=&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_0b2c3462-b8da-44cd-b9fd-2dfe792de087%24ctl00%24InnerRenderer_0b2c3462-b8da-44cd-b9fd-2dfe792de087%24esctl_130e05f8-ab28-44e4-ad59-27c1fdab1e7f%24ctl00%24InnerRenderer_130e05f8-ab28-44e4-ad59-27c1fdab1e7f%24esctl_2d2ec58c-3f3b-4810-b613-d64b98727ca1%24formBuilderUI%24fieldset%24formBuilderStructureInputRendererHost%24FormBuilderStructureInputRenderer%24esctl_432b366d-c2a4-4d8d-8375-d383c5354add%24cContainer%24txtText=&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_0b2c3462-b8da-44cd-b9fd-2dfe792de087%24ctl00%24InnerRenderer_0b2c3462-b8da-44cd-b9fd-2dfe792de087%24esctl_130e05f8-ab28-44e4-ad59-27c1fdab1e7f%24ctl00%24InnerRenderer_130e05f8-ab28-44e4-ad59-27c1fdab1e7f%24esctl_2d2ec58c-3f3b-4810-b613-d64b98727ca1%24formBuilderUI%24fieldset%24formBuilderStructureInputRendererHost%24FormBuilderStructureInputRenderer%24esctl_b2472b8b-a222-4e0a-a085-80e2baee8d14%24cContainer%24txtText=&__VIEWSTATEGENERATOR=9467529C&__EVENTVALIDATION=%2FwEdABq81V2eETe85SjYoW4XfYCVxJMl82VmbJPmhJ8RpBZ23tzfw%2Btuel%2FVjWDmellieobLQchhq4mAfXo89P92yErq3Ei%2BJuvpjAOTXbPxBuLdZkUSUmtoqqsXZDB3ZaOfxsVCd%2BmqY1SXK84wqVMU362Me%2FmrD1IBRRnHqkaeAATc8lGciEqE%2Bi23avvRnK3GDLFOW4KhMW7l9sL3cHMsbXNT3wGIiKCr86HlTZrP0oCd%2Bzz%2BbOfELNYfHfUgT5SfzeBHACred64Tvh4xU00grmQ67yCBGahfPQSqk%2FfrHqnKcHfeCZHzBGy9Ku8GwXo3mtRswNsLYw%2Bd5J61vzmd2yYNBLLsUWwNDNORCnXHifX%2FaNsWAehsmUWzZkHr4WanZrSJpbAddwRR5t%2FclHR4E48MHrP1NopRbBNKMTOnv2mnpux2SDk8bSOYhHJL1KdvAE61130jnSjq1ogwRrpLmC6JbW1ZiX4SxsTEDNhZsyiu5m%2BsHk%2Fq2KQvT29fmpy2aulonJozgE9ctZ48pffi6SIld6Sz3zPVLJ5qWoqvz0%2F9s%2Bt8a66m9ndwKa8k8lwGnac2wQBMyalK3s0lZ99z70%2F%2B&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce%24ctlAssetManager%24flwBrowseAssets%24ctrlUploadAsset%24flwUploadAsset%24ctnBinaryUpload%24tabBinaryUpload%24notificationPanel%24hfState=&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce%24ctlAssetManager%24flwBrowseAssets%24ctrlUploadAsset%24flwUploadAsset%24ctnBinaryUpload%24tabBinaryUpload%24pnlBinaryUpload%24sbumBinary%24state=&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce%24ctlAssetManager%24flwBrowseAssets%24ctrlUploadAsset%24flwUploadAsset%24ctnBinaryUpload%24tabContentUpload%24notificationPanel%24hfState=&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce%24ctlAssetManager%24flwBrowseAssets%24ctrlUploadAsset%24flwUploadAsset%24ctnBinaryUpload%24ctnBinaryUpload_SelectedTabHiddenField=0";
my $post_con="EasySitePostBack=&ScrollPos=1083&LinkState=&__EVENTTARGET=espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce%24ctlAssetManager%24flwBrowseAssets%24console%24consoleTable%24cell_1_24666&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATEZIP=7V1bc9vGFRaWWJISJUuxLcZOHBKxFcuKRIoX8GbJydjyJZraisdS3GYyGRYEliQiEGBx0aUP7Vs706c%2Bto%2FtpA%2F9JX1pf0ff%2Bt7OZNI9C4AEQV8kSxrHNqQ54LfY3cO9nD17PcsfudlLHM40mxuGbpuGZj0hv3JUkzw2LPuOJO%2F%2BjBw2m2mE%2F4GI1TcXTKIrxPyCei48ljpk2zYd2XZMcle1%2Bpp0%2BIR5E3OBWLKtNSvtSrvcKtZyYqXeyIlSq5CTKrKcaxXr9VKBVFtVIi5s6jox%2FYhHi%2BJybxNSLNXKtVy5VCjR8HI516oXK7kCERuVaoWI9Uo7xP1IUVzuNSLVqy2RBm2IhZzYKoo5qdqQcnKjUaq2KtVaVSYLNOBtyyL2I0mnpWEutLX9O6axbxH21lqQDd0yNLLQsvUHpqo8Vck%2B%2FmdUkicryZ2u02vpkqpBcc7iVK5Yq1eLVVGsibNK%2BlybPlB79uLyBFI6CuJGnWjUGaPOeDhMgjp5%2BtJ1g48yeDzrHX0MQ8fbs2mU4XfIgY3%2FyK0r6p4ga5Jl3braJ1rOcGxiCrKhOT3dCrlaxJaufjYzNTn4C8dWoQoG4ZlrNMJIDFkjUoC7oRxe%2FWxQAN6Db4%2BkOzuBvx9Jsxt7JKE5Set3pauCZR9q5NbVtmZI9k1BI217TWBfeVMw1U6XuvZVxe7eFBqNT9aCyXwG%2F%2FGsPC8jz8nHB8EKH9Y1IKiODzZ11ValERF7QixHsy10CaozKAKz6XhG8CJs25Jpq3pnQ7JJxzAPH5uGTCgTpZNJPSam1Seyre6R1NLvuXuSdbit2uTnpJX3lWneld58SLqXg44AmxUhwGRFeEp9VEO%2FVcwX4H9F2KAJpirilk4c25S0FeGx09JUmeroHWOX6Ld0R9O4THrDMal2sTfcJgOtZOewT9P4nc89f08jPRrCynth8qGww4QMU1E9VipoofJ%2BHUEN4PsgJ10lPe1VELxMeSndtkk%2FtfTbYAkGUzhSlF%2F1KSPFdbkYYp9GyU347Zr3pSGgBoaNv%2FR9bIKbmJj4kf7BJ%2FxNI%2Fr4%2FIRlxgO3P3Pbh5ZNejTfmgZSQSsm%2F4DQ5qHK%2BYeqZf%2By%2BM034%2FW4ZdhqW5UlN3zQ9YiKKxWzE9fot9%2FGaPLiTcqhZ%2BGmpf6aJJt7Lid%2BYqLyCmn65lsotmRyMjYx%2FEswh1%2BwfPkV%2BALXFOh2r%2Bb4qOberJqLg3JIPFUttaURr5dmzXFq2Byno0p9syoVjY6d3FGWP3gCnEkY%2Bi45dPp4S3d6UMKSBsOoO8bBvQObDTyb93SLlsyW772p9x37ht1VrRWhLWkWGXzQ4TR9QkktrWWShi53Jb1DTpuzN7z0MpBNnjb%2F7OTpp5gf1IOXeOxlYU6Zm0%2BkpX5f8%2Bpv9TtpT7JkU%2B3b88HXB127py0f9LTZkbc9bdKmSVwF3yRDsmVNMUBnNKqOv7S7xJxPcAWuyJW4MidyFa56cTmW6Lh%2Faa4NbX9UTGIjw2sFJZ%2FjP0d1AsSfGvFPBnJH%2FRHnju6H0Wap3CU3LGsDhpo43nJs29AzfPPj7TuIqhmUGq3fRyeoDbcaQtV7ugwVNPOGpTfqpN8GfR7SKMPn9HAo7TXiBKWY0unS%2Fw5MEmB6wGen8GVQFC3jQLC6hmkLCp1w5fqqvEvMbIq1RJhMxL0J35wbJ6mYRl8x9nUWZDZ9qY04RNs24hFGcZRASTSJplAKTaMZdA7Nojn0HjqPLqCLaB6l0fvpS3MYFQqYK3QAFDFXZKCEuRIDZcyVGRAxJzJQwVyFgSrmqgzUMFdjoI65OgMNzDUAFAtADBWBGCoBMVQGYkgEYqgCxFAViKEaEEN1IIYaQIBKBSCGikAMlYAYKgO5ipV7YdGtH7Po0CV0GX2APkRX0Ecog7JIQB%2Bjq%2BgaWkCfoOtoEd1AS%2BhTtIxWUA7l0SoqoCIqoTISUQVVUQ3VUQPdRGvp9XejCgCJQAxVgBiqAjFUA2KoDsRQAwhQuQDEUBGIoRIQQ2UghkQghipADFWBGKoBMVQHYqgBBEgsADFUBGKoBMRQGYghEYihChBDVSCGakAM1YEYagABqhSAGCoCMVQCYqgMxJAIxFAFiKEqEEM1IIbqQAw1gFxRD64NRfIeyfvbLu%2BxkV530Ml2aSfbjTrZqJONlE6kdKJONpL3SN5PIO84uF4ZTWSjPjbSOZHOOWOdE%2FWxkby%2Fc%2FIe2OxJRH3ua28AUZ%2F72qsg0kFRnxvJeyTvZyfvgT53MrhhO0elnWunuTmc2DJswSI2CB%2BLl4o2dl97Q4n65tdeBZGuivrmSN4jeT8TecfR6bg3%2FnScP7Qq%2FYsbqzpW6r85QqF7QTZ1u1xaEXqWbJiaGjB0EY9amq1aTarIlWqxURZJod54UckmoHySkyhQQLO%2BI5nyzGR4xDP7HmYOhhcNGretGfs3Jcc21roELMFulquF%2FsGaaw5WrgOGc74wtowxbZ2OzeHkvYO%2BxIwLaSOYch3CbU3DccIwfTsNJST1LcLeJ2XPFTgDMThOnkbZRFeBBaSoAb3hDWiw%2FqcMzfQChotKwIf3hgbpZAYz20F8rmXrAqWcRaA%2BMpPM0hBOqOP4BjMJyPBdk7Qxdw3MD2Rabrv4h9jCjcVrYKXbHFrpNl9kpds8upVu8%2FhWus2jW%2Bk2j2%2Bl2zy6lW4zZMfYDFnpUn9Tc43y2AvwDzplW2fgLrElVbOattR6RCGdEkosqu%2FYMPS22mm2LT8gjfjlPs1Y0%2Bm7nzQhhUJTo5JFJf6ebqv24WM2p2yCnXXwxeJSvkNfuTPOG4ua5Ohy1%2FNaEeB8%2BtKaYBJaqbp7Tn1tMKNKJ7MzeMaXIGaFmj2H8QYDs1RksnP4v5GsvLWyYpIe7c5oX7MJWnNxXE7QsLvJnIcE7Uidu6Stgu0w1W6lv5xlX%2FOnY%2FU1t5UeTZZFWULAvF96%2BbFUn3VXIx4%2FSUfpaUrHZ%2Bt2NLDgFY0R3uwxAkyU3BGfO%2BDD7uvUMubwFNMiO6qtkU43dGUDjTODE97VEsxWLDBw9BhODm6IYCuanjcXDtlhw2Hv5eh9Al4aXXWRpANVN7xnGJ%2Fc7NEMfWVq%2BNEqoSVh0ZLYJ60BXrWcft8w7baqEWtVhcDWqgR56rmKdXVPJftN0FtNd5BDlHxfh%2B%2FjB%2Bb32fP43km42%2BDtccUj6aec75%2BIM1zm4bOOBxN8IrayY9lGz2M76VYeS7biTVpGhpNDkUGX0SUlMJwEm0iEigVUEv3Pi8sxBLaKYEvIuXdmTF%2FXW1Z%2FzWgLJVEYdgth3rDSxHVh5dwXC09eOu3Bsrsv0LO%2BnMZ9Ob0ge3dQ5GyTEPfKDJaR9eUJOkcDQ8Zz211jf6NL5N07xgGxUp8ueqoGblT4ahM%2BBtcp7FAmW4ZC4LoHayIzs0VoU%2FfmYDQBSZYQeKR5msOP7hKZ9FrEFEqFoijkhC3aM%2FruSiY5jOifRo8NLDJdge%2By%2ByAGrYZncIKw6nH93dzHvcL6O0ot7QxUw5et76hkn8rVFAgsTZFXSQEz0OyUb%2F7p7Uu8B8217ZnIeklE3NwcM05npUwUmhVFcWUqHh3AjzZDosXhaHE42gyJ5D2S97Ozcou616h7jdRNpG7OWN1E3Wsk7%2B%2BSvA8vSvKP%2FM08lCxbeGQoalslCp7WqLPnufw2Mpi6B%2FfoYC2CU0v%2FwWezjIqBW3582fC%2BYfbyD0zD6cMyKizaD5dYIQpmK5uxP7zaAuw491NYcYXiYIujPDx%2BSil74Vrw9aMkYrD4y3vLvUAJ3l%2F8BcAvHIURKyRMHzMxDB9HisTYw3Dww%2FXbjm08NGRJI5%2FtNptwq7Wqd%2B6rRFMur297S6Zhn%2FT6U0lzxiLQhHMcBwmKx%2BkjdlvTKExAlgqpDNcp%2FXvqDGX%2B05dlfFTe4x14b8V%2BdxpiderC%2FlNJ1QsF%2FerLEnAUIRdexiQs4EeK4Gqz%2BfUtqTcmpjx3TPUIDFkqQKqZPMe3bcl2LBx%2Fu3RtEiok%2Bfbq2smgGE6%2Boq6dckVx6ieja1PwBQ%2FVPULxNOSpmFJCR75GjBaU9ITCzqWxLbzYbHCvxb8Bc%2BjGIXc85E6E3MmQezLkngq5UyH3dPgW%2BdHr815w3fzo1Zf880O78xWxL3VIjrgV513ibkD13XFUTaGONsXC8GZ5cFpsbuNvjTCeMTrvv7H0C6%2B9PHBU5VSPIC5IUo2URLGQK1VrrZzYqLZy9YrYyMlinZQrhYaotOvu2S5%2F3y5wi%2F9whxVxpb%2FFz7D3XRw0Bf%2FMTJ7daJqH0z6bOhQe26MG7fLh%2BnDHarwhuH6bYw3h%2FXXG6hkNhO1fh19%2FvH5Xssl91aQdv0mkZ7QsNwQM4%2F1RfDhEhoVgWba6495XWIq2nV5PMg%2FHehnaEGOxGHfhGdt0rjr6lSczW7QMpZZGAkdZ4Wt31B455dOs78I3st7%2FByrjzyp4pvj3QJ82mxNJCJoInL2Ygr%2FU2M9XeJvjg0u1O%2B6rDH4otYiGzz%2FWiGQRQSO24FjCrm7su1uhytx8LPY1sa5RWhFaji2ovb4JW8PQUwiy4WiK0CJCT1II2jLmY1yDzoxRsXhxORbrdDru3mnwK7OX8fUvjH0v6j7xGbKTeoI6bGWfsw1XmGnyT4x9C2F2BiObxFXbvZj2odqDI1%2Bh%2B2kfSQcPid6xu97VtKUKrYLFRXa37SvGdHe4M%2FM0gNpzehtdyZRoN2cyNuh%2F3OAYf2f0VCvL7OTXhmMKOh3DDU6STQ6%2BCSnKYB2Khb4wCC3cYAffzN7SYP%2F6ClLcDfVY%2BEvOs2ikJ6maICmKSSzLvyQYZXibSg3GzHf0266MRwt%2FLQ2c%2FWgQmQ%2Bcx3nG5bDIP4KBnn%2BahvM7bd49WnFtfVVR94a%2FDDLqdF0BGeK9XzH560t%2FxYT92srRf8SkeBa%2FYXKsPHqBl0KBx8IHogzPR4VOl%2BBRZ3zUmRhx%2Fh8%3D&__VIEWSTATE=%2FwEPZmSlcqOXjYqNMMSS0uwp07ekBAW9C58KrelZwXcvY5w%2FOQ%3D%3D&PageNotifications%24hfState=&floatingwindowTab_UploadAsset_espr_renderHost_PageStructureDisplayRenderer_esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_esctl_fee12737-3202-45c3-b815-0e49565e485f_InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f_esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce_ctlAssetManager_flwBrowseAssets_ctrlUploadAsset=0&floatingwindowVisible_UploadAsset_espr_renderHost_PageStructureDisplayRenderer_esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_esctl_fee12737-3202-45c3-b815-0e49565e485f_InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f_esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce_ctlAssetManager_flwBrowseAssets_ctrlUploadAsset=0&floatingwindowConfig_UploadAsset_espr_renderHost_PageStructureDisplayRenderer_esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_esctl_fee12737-3202-45c3-b815-0e49565e485f_InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f_esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce_ctlAssetManager_flwBrowseAssets_ctrlUploadAsset=%7B%22name%22%3A%22UploadAsset_espr_renderHost_PageStructureDisplayRenderer_esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_esctl_fee12737-3202-45c3-b815-0e49565e485f_InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f_esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce_ctlAssetManager_flwBrowseAssets_ctrlUploadAsset%22%2C%22title%22%3A%22New%22%2C%22width%22%3A800%2C%22height%22%3A650%2C%22collapsible%22%3Afalse%2C%22closable%22%3Atrue%2C%22modal%22%3Atrue%2C%22autoScroll%22%3Afalse%2C%22autoTabs%22%3Afalse%2C%22shadow%22%3Atrue%2C%22constrain%22%3Afalse%2C%22minWidth%22%3A650%2C%22maxWidth%22%3A1024%2C%22minHeight%22%3A500%2C%22maxHeight%22%3A1000%2C%22buttons%22%3A%5B%7B%22id%22%3A%22ok%22%2C%22text%22%3A%22Next%22%7D%2C%7B%22id%22%3A%22cancel%22%2C%22text%22%3A%22Cancel%22%7D%5D%2C%22showOnLoad%22%3Afalse%2C%22focusOnLoad%22%3Afalse%2C%22postbackOnClose%22%3Atrue%2C%22postbackOnResize%22%3Afalse%2C%22postbackOnCancel%22%3Atrue%2C%22maintainPostbackVisibility%22%3Atrue%2C%22controlId%22%3A%22espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce%24ctlAssetManager%24flwBrowseAssets%24ctrlUploadAsset%24flwUploadAsset%22%2C%22contentElementId%22%3A%22floatingwindowContent_UploadAsset_espr_renderHost_PageStructureDisplayRenderer_esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_esctl_fee12737-3202-45c3-b815-0e49565e485f_InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f_esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce_ctlAssetManager_flwBrowseAssets_ctrlUploadAsset%22%2C%22selectedTabHiddenFieldId%22%3A%22floatingwindowTab_UploadAsset_espr_renderHost_PageStructureDisplayRenderer_esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_esctl_fee12737-3202-45c3-b815-0e49565e485f_InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f_esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce_ctlAssetManager_flwBrowseAssets_ctrlUploadAsset%22%2C%22visibilityStateHiddenFieldId%22%3A%22floatingwindowVisible_UploadAsset_espr_renderHost_PageStructureDisplayRenderer_esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4_esctl_fee12737-3202-45c3-b815-0e49565e485f_InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f_esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce_ctlAssetManager_flwBrowseAssets_ctrlUploadAsset%22%7D&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce%24ctlAssetManager%24flwBrowseAssets%24console%24consoleMessagePanel%24hfState=&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce%24ctlAssetManager%24flwBrowseAssets%24console%24resultsPerPageControl%24ddlRecords=24&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_0b2c3462-b8da-44cd-b9fd-2dfe792de087%24ctl00%24InnerRenderer_0b2c3462-b8da-44cd-b9fd-2dfe792de087%24esctl_130e05f8-ab28-44e4-ad59-27c1fdab1e7f%24ctl00%24InnerRenderer_130e05f8-ab28-44e4-ad59-27c1fdab1e7f%24esctl_2d2ec58c-3f3b-4810-b613-d64b98727ca1%24notificationPanel%24hfState=&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_0b2c3462-b8da-44cd-b9fd-2dfe792de087%24ctl00%24InnerRenderer_0b2c3462-b8da-44cd-b9fd-2dfe792de087%24esctl_130e05f8-ab28-44e4-ad59-27c1fdab1e7f%24ctl00%24InnerRenderer_130e05f8-ab28-44e4-ad59-27c1fdab1e7f%24esctl_2d2ec58c-3f3b-4810-b613-d64b98727ca1%24formBuilderUI%24fieldset%24formBuilderStructureInputRendererHost%24FormBuilderStructureInputRenderer%24esctl_eb40337a-4eb0-4ba4-ac7d-1df5eadca107%24cContainer%24TextArea=&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_0b2c3462-b8da-44cd-b9fd-2dfe792de087%24ctl00%24InnerRenderer_0b2c3462-b8da-44cd-b9fd-2dfe792de087%24esctl_130e05f8-ab28-44e4-ad59-27c1fdab1e7f%24ctl00%24InnerRenderer_130e05f8-ab28-44e4-ad59-27c1fdab1e7f%24esctl_2d2ec58c-3f3b-4810-b613-d64b98727ca1%24formBuilderUI%24fieldset%24formBuilderStructureInputRendererHost%24FormBuilderStructureInputRenderer%24esctl_432b366d-c2a4-4d8d-8375-d383c5354add%24cContainer%24txtText=&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_0b2c3462-b8da-44cd-b9fd-2dfe792de087%24ctl00%24InnerRenderer_0b2c3462-b8da-44cd-b9fd-2dfe792de087%24esctl_130e05f8-ab28-44e4-ad59-27c1fdab1e7f%24ctl00%24InnerRenderer_130e05f8-ab28-44e4-ad59-27c1fdab1e7f%24esctl_2d2ec58c-3f3b-4810-b613-d64b98727ca1%24formBuilderUI%24fieldset%24formBuilderStructureInputRendererHost%24FormBuilderStructureInputRenderer%24esctl_b2472b8b-a222-4e0a-a085-80e2baee8d14%24cContainer%24txtText=&recaptcha_challenge_field=03AJzQf7OrK8-WhVgBI7EyBr0kn6RpSzFvouPsQJ_ftYgldjnw9LAeHi9ILpt0tJuOtr_F804-OAaYgB7ojJdJidU2dua_LyeEtLs9RyhMgC7Q9fBbUcZhlw-6g8ZYJaTW3A8I_t3r3UgWT8PBlHHAJXoXItsIzkaDYPo7y5sZmYOy0rwUEIpnoP6WEym-1kQO3NRbvM4qBwsL&recaptcha_response_field=&__VIEWSTATEGENERATOR=9467529C&__EVENTVALIDATION=%2FwEdAEnw4UkmIAh5oNlDMLkimNmZxJMl82VmbJPmhJ8RpBZ23tzfw%2Btuel%2FVjWDmellieobLQchhq4mAfXo89P92yErq3Ei%2BJuvpjAOTXbPxBuLdZkUSUmtoqqsXZDB3ZaOfxsVCd%2BmqY1SXK84wqVMU362Me%2FmrD1IBRRnHqkaeAATc8lGciEqE%2Bi23avvRnK3GDLFOW4KhMW7l9sL3cHMsbXNT3wGIiKCr86HlTZrP0oCd%2Bzz%2BbOfELNYfHfUgT5SfzeBHACred64Tvh4xU00grmQ67yCBGahfPQSqk%2FfrHqnKcMXprwNsFycTQ3%2B2KuV3n1zsd8u%2BLzz0NuCubOdhiME5kBt6axxAMXGbdGV66lEqYufsox90nJi5D%2BsKdW0mFiO1xmH9W1xLPaD17288wbrRv8aECW4onu9u0yk3Bg6rNSiJB2ausQt5okZqre9iNPsNcBIMZujbGgSfqo7UX0GsNt%2BRuYsddHkug6jHuSaGQfHHjKlVdGS40eVJ0b0cSrwG7w18VqbL6e5tZNCzxLuUaeK%2Ft5n%2FhKUzadPzTV5YuiMobxdEI9q6ddYRY9bIhh9BPMQ%2BTEr%2Fp8xjIq7GJkMK9SGjq1lWPyLDmIVHEj%2Bl%2FSHltdA0Ap9LaFoY6UuLnqro9syL5NqKxt8S6izThXiAPlL9DgyJgROaxdhyV6ZWGiQ6Sa1J70l6rzR%2FK9jmw3FFSAFLHI0UkadAO4EwOcYMFQ5fO%2Bu8DoV3GMLOKieekP8FayCpibaX6cx%2FdJyZdMCBUEuiDng4WI12tRo3JGZ5wUt7Y%2BOkdHgapdxf5P6tROFdjRSLkAql4JTxNfoBVN89HVT0k%2BfoUlFL%2FTC7ECynfyqLD%2BQ%2Blb95Z11GuSBRNMGeOZhbeczU9Rtc5LKRksDKH7DWqHDWlRgaYVLzxUXFJDDYmHWbwGwx6gnkhrH4EetPCI4sjZMCWIhT5Rsn%2B%2BG6QU2OGptnPZ573OKEop30KM6jwYJ8GgAXy8lIJQearu2AsIznMRHJQH8Nxq8DavqwYHEQJqo%2FntAQX3yjuZKSDOwYg1Sku01FEr4jdXQvdmSrMEprMRzliV6lMAYLADsniJFK6o6msmSVE0t%2B%2B0kdusDWAm63XxJBdK958F%2BZvpNp52Ot8rz0VyEyFneKvOCytLj0TdkU0pSFcDTbfk%2FhqF2krAO3vX8hd64unv7GcXxCB%2BQ9IWCL3%2B7k02J%2BJ2St5iOGFsJ7p4puWQBvoRp90jpTI%2Fc8ZEpcrm7XdJQtP8HNlmHhWvpAHz08DtOfTbPYLbQz9GoSB9lY4hNiNJGTjP5mF%2F0gxMfB2jFovnjRFQSy7FFsDQzTkQp1x4n1%2F2hkToQhSUvAcEboduj0qGNpZSas8M%2FRLjduzzJm1z7nbYmlsB13BFHm39yUdHgTjwwes%2FU2ilFsE0oxM6e%2Faaem7HZIOTxtI5iEckvUp28ATrXXfSOdKOrWiDBGukuYLoltbVmJfhLGxMQM2FmzKK7mb6weT%2BrYpC9Pb1%2BanLZq6WicmjOAT1y1njyl9%2BLpIiV3pLPfM9Usnmpaiq%2FPT%2F2z6f5sq0vdZ6nDrDnU7Rgt7y28GTHvXKBs9ge0KXd%2B62E%3D&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce%24ctlAssetManager%24flwBrowseAssets%24ctrlUploadAsset%24flwUploadAsset%24ctnBinaryUpload%24tabBinaryUpload%24notificationPanel%24hfState=&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce%24ctlAssetManager%24flwBrowseAssets%24ctrlUploadAsset%24flwUploadAsset%24ctnBinaryUpload%24tabBinaryUpload%24pnlBinaryUpload%24sbumBinary%24state=&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce%24ctlAssetManager%24flwBrowseAssets%24ctrlUploadAsset%24flwUploadAsset%24ctnBinaryUpload%24tabContentUpload%24notificationPanel%24hfState=&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24InnerRenderer_5f5f3b17-4589-4ab0-a5cc-b18820e6b6e4%24esctl_fee12737-3202-45c3-b815-0e49565e485f%24InnerRenderer_fee12737-3202-45c3-b815-0e49565e485f%24esctl_7ea86b47-3940-4b14-a69a-c9926b5676ce%24ctlAssetManager%24flwBrowseAssets%24ctrlUploadAsset%24flwUploadAsset%24ctnBinaryUpload%24ctnBinaryUpload_SelectedTabHiddenField=0";
my $Doc_Link="https://www.n-kesteven.gov.uk/_resources/assets/attachment/full/0/24666.xls";
my $host="www.n-kesteven.gov.uk";
my $ref="https://www.n-kesteven.gov.uk/your-council/facts-and-figures-about-the-council/council-spending/council-transparency/december-2014-november-2015/";

my ($Doc_content,$status_line,$File_Type,$File_Type_cont)=&Postcontent($Buyer_Url,$post_con,$host,$ref);
		
#$LinkInfo=&clean($LinkInfo);	
my $LinkInfo_Temp=$LinkInfo;		
my ($File_Date,$File_Month,$File_Year,$Doc_Name,$Doc_Type);		
print "\nhi: $LinkInfo\n";		
#next if($Doc_Link=~m/\.pdf/is);	

# while($Buyer_content=~m/(<tr[^<]*?>[\w\W]*?<\/tr>)/igs)
# {
	# my ($LinkInfo,$Doc_Link,$File_Month,$File_Year,$Doc_Name);
	# my $block = $1;
	
	
	
	# if ($block =~m/<[^<]*?id=[\"\']show([^<]*?)_0\s*/igs)
	# {
		# $Doc_Link="https://www.n-kesteven.gov.uk/_resources/assets/attachment/full/0/".$1.".xls";
	# }
	# if($block=~m/<span\s*[^<]*?\s*class=[\"\']text[\"\'][^<]*?>([\w]+\s*[\d]{4})[^<]*?</is)
	# if($block=~m/<span\s*[^<]*?\s*class=[\"\']text[\"\'][^<]*?>9.\s*([\w\W*?]+\s*[\d]{4})[^<]*?</is)
	# {
		# $LinkInfo=$1;
	# }
	
	print "LinkInfo	: <$LinkInfo>\n";
		if($LinkInfo ne '') 
	{	
		$LinkInfo=~s/\s\s+/ /igs;
		print "LinkInfo	: $LinkInfo\n";
		
		if($LinkInfo ~~ @Processed_LinkInfo || $LinkInfo =~ m/^\s*$/)
		{
			print "File Already Processed\n";
			next;
		}
		else
		{			
			print "\nThis is new file to download\n";
		}
	}
	
	# if($LinkInfo ~~ @Processed_LinkInfo || $LinkInfo =~ m/^\s*$/)
	# {
		# print "File Already Processed\n";
		# next;
	# }
	my $Doc_Link_Temp=$LinkInfo;
	#next if($Doc_Link eq '');

	$Doc_Link=~s/amp\;//igs;	
	$LinkInfo=~s/\<[^>]*?\>//igs;	
	$LinkInfo=~s/\s\s+/ /igs;
	
	my ($Doc_Link_contect,$Status_Line,$Content_Disposition)=&Getcontent($Doc_Link); # Ping the web link page
	print "\n Content_Disposition::$Content_Disposition";

	my ($File_Month,$File_Year,$Doc_Name);
	my ($File_Type,$File_Type_cont,$Doc_Type);
	
		if($File_Type=~m/\.pdf/is)
		{
			next;
		}
		elsif($File_Type_cont=~m/\.pdf/is)
		{
			next;
		}

		if($Content_Disposition=~m/(csv|xlsx|xls)/is)
		{
			$Doc_Type="$1";
			print "\nI:1\n";
		}
		elsif($File_Type_cont=~m/(csv)/is)
		{
			$Doc_Type="$1";
			print "\nI:2\n";	
		}
		elsif($File_Type_cont=~m/(excel)/is)
		{
			$Doc_Type="xls";
			print "\nI:2\n";	
		}
		elsif($Doc_Link_Temp=~m/^[^>]*?\.([\w]*?)\s*$/is)
		{
			$Doc_Type="$1";		
			print "\nI:5\n";	
		}		
		
		$Doc_Type=~s/\"//igs;
		$Doc_Type=~s/\'//igs;
	
	
	
	if ($Content_Disposition =~m/\.([\w]+)"/is )
	{
		$Doc_Type=$1;
	}
	
	print "Doc_Type	: $Doc_Type\n";	
	
	next if($Doc_Type=~m/pdf/is);
	
	# Get Date, Month, Year from the link info for file location Creation
	if($LinkInfo=~m/\s*([a-zA-Z]+)\s*(\d{4})\s*/is)
	{
		$File_Month=$1;
		$File_Year=$2;
		print "File_Month	: $File_Month\n";
		print "File_Year	: $File_Year\n";
	}
	my $Month_Of_Document = "$File_Month $File_Year";
	
	if($Doc_Link!~m/http/is)
	{
		my $u1=URI::URL->new($Doc_Link,$Buyer_Url);
		my $u2=$u1->abs;
		$Doc_Link=$u2;
	}
	print "Doc_Link	: $Doc_Link\n";
	
	# File name creation from Link Info
	my $File_Type = $Doc_Type if ($Doc_Type ne '');
	my $File_Type = 'xls' if($File_Type eq '');
	print "\n File_Type::$File_Type";

	$Doc_Name=$LinkInfo;
	$Doc_Name=~s/\W/\_/igs;
	$Doc_Name=~s/_+/\_/igs;
	$Doc_Name.='.xls';
	print "Doc_Name	: $Doc_Name\n"; 
	

	
	# File Path creation
	# my $hash_ref = &BIP_Column_Map_V1::ReadIniFile();	my %inihashvalue = %{$hash_ref};	my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source";  
	my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source";  
	
	# File location creation
	unless ( -d $Filestorepath ) 
	{
		$Filestorepath=~s/\//\\/igs;
		system("mkdir $Filestorepath");
	}
	my $Storefile = "$Filestorepath\\$Doc_Name";
	print "STOREPATH  : $Storefile \n";
	my $StartDate=DateTime->now();
	# Spend file download
	my ($Doc_content,$status_line)=&Getcontent($Doc_Link);
	my $fh=FileHandle->new("$Storefile",'w');
	binmode($fh);
	$fh->print($Doc_content);
	$fh->close();
	my ($Status,$Failed_Reason);
	if($status_line!~m/20/is)
	{
		$Failed_Reason=$status_line;
	}
	else
	{
		$Status='Downloaded';
	}
	print "\nStatus:::: $Status\n";
	<>;
	&BIP_Column_Map_V1::ReadSource($Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
	print "Completed \n";
  # }


sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;	
	my $Content_Disposition=$res->header("Content-Disposition");
	# my $get_cookie=$res->header('set-cookie');
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$Content_Disposition);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$Content_Disposition);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	if($Data eq '')
	{
		# $Data='Not provided';
	}
	return $Data;
}