#-*- coding: utf-8 -*-
import base64
import requests
import json
import pybase64
import io
from io import StringIO
import PyPDF2
import xlrd
import csv
from os import sys
import os
import ntpath
# import pandas as pd
import configparser

config = configparser.ConfigParser()
config.read('./config.ini')

'''
Usage :  python pdf2csv.py "F:/MuthubabuMasterFolder/POC/SolidFramework/Summary_Part_B_Activities_sept2018 (2).pdf"

'''

def stringToBase64(s):
    return base64.b64encode(s.encode('utf-8'))

def base64ToString(b):
    return base64.b64decode(b)

def fileConversion(pdfFile,fileName,path):

    with open(pdfFile, "rb") as pdf_file:
        encoded_string = base64.b64encode(pdf_file.read())
    # url = config.get("path", "url")
    url = 'http://172.27.139.76:83/api/conversion/pdftoexcel'

    postData = '''{
    "PdfFile": "%s",
    "PdfConversionProperties": {
        "NonTableContent": true,
        "Singlesheet": false,
        "T_seperator": "Comma",
        "D_symbol": "Comma",
        "Delimiters": true,
        "Textrecovery": "Automatic",
        "Optional_password": ""
        }
    }''' % encoded_string.decode("utf-8")



    header= {"Content-Type" : "application/json"}
    source = requests.post(url, data = postData, headers = header)
    print (source.text)
    postDataSource = json.loads(source.text)


    string = base64ToString(postDataSource["Data"]["ResponseBase64String"])

    excelfile = open(str(path)+"/"+str(fileName)+".xlsx","wb")
    excelfile.write(string)
    excelfile.close()
    return str(path)+"/"+str(fileName)+".xlsx"

# def merge_excel_sheets(excel_file,fileName,path):
#     try:
#         df = pd.read_excel(excel_file, sheet_name=None, ignore_index=True)
#         cdf = pd.concat(df.values())
#         writer = pd.ExcelWriter(str(path)+'/'+str(fileName)+'_merge.xlsx', engine='xlsxwriter')
#         cdf.to_excel(writer,'sheet1')
#         writer.save()
#         return str(path)+'/'+str(fileName)+'_merge.xlsx'
#     except Exception as error:
#         trace_back = sys.exc_info()[2]
#         line = trace_back.tb_lineno
#         print(error,line)

def csv_from_excel(excel_file,fileName,path):
    try:
        workbook = xlrd.open_workbook(excel_file)
        all_worksheets = workbook.sheet_names()
        your_csv_file = open(str(path)+'/'+str(fileName)+'.csv', 'w',newline='')
        # your_csv_file = open(str(path)+'/'+str(fileName)+'.csv', 'wb')
        for worksheet_name in all_worksheets:
            worksheet = workbook.sheet_by_name(worksheet_name)
            
            wr = csv.writer(your_csv_file, quoting=csv.QUOTE_ALL)
            for rownum in range(worksheet.nrows):
                wr.writerow([str(entry).encode("utf-8").decode("utf-8") for entry in worksheet.row_values(rownum)])
        return str(path)+'/'+str(fileName)+'.csv'
    except Exception as error:
        # pass
        trace_back = sys.exc_info()[2]
        line = trace_back.tb_lineno
        print(error,line)
def path_leaf(path):
    head, tail = ntpath.split(path)
    return tail,head

if __name__ == "__main__": 
    try:
        pdfFile = sys.argv[2]
        fileType = sys.argv[1]
    except:
        print('Required argument is missed.\npdf2csv.exe "csv" "c:\\filepath\\sample.pdf"')
        sys.exit()
    
    if str(fileType).lower() != 'excel' and str(fileType).lower() != 'csv':
        print('File type is invalid. Valid file type is excel or csv.')
        sys.exit()
    name, path = path_leaf(pdfFile)
    fileName = name.replace('.pdf','')
    excelFile = fileConversion(pdfFile,fileName,path)
    # if str(fileType).lower() == 'excel':
    #     merged_excel = merge_excel_sheets(excelFile,fileName,path)
    #     os.remove(excelFile)
    if str(fileType).lower() == 'csv':
        csvFile = csv_from_excel(excelFile,fileName,path)
        os.remove(excelFile)