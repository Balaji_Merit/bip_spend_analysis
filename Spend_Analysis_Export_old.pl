use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use Text::CSV;
use JSON;
use HTTP::Cookies;
require BIP_DB;

############Database Initialization########
my $dbh = &BIP_DB::DbConnection();
###########################################

my $csv = Text::CSV->new({ binary => 1, auto_diag => 1, eol => "\n"})
        or die "Cannot use CSV: " . Text::CSV->error_diag();
		
my $Field_Match	='Y';
my $import_flag='Y';
my $format_flag='Y';
my $Supplier_Match	= 'Y';
my $Export_Flag	= 'N';
my ($Input_Buyer_ID,$Batch_ID,$FileName) = &BIP_DB::Retrieve_Buyers_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Field_Match,$import_flag,$format_flag,$Supplier_Match,$Export_Flag);
my @Input_Buyer_ID		= @$Input_Buyer_ID;
my @Batch_ID			= @$Batch_ID;
my @FileName			= @$FileName;
for(my $i=0;$i<@Input_Buyer_ID;$i++)
{
	$Input_Buyer_ID=$Input_Buyer_ID[$i];
	$Batch_ID=$Batch_ID[$i];
	$FileName=$FileName[$i];
	my $Export_Flag='N';
	my ($Month_Of_Document) = &BIP_DB::Retrieve_Month_Document($dbh,$Input_Buyer_ID,$Batch_ID,"SUPPLIER_SOURCE_DATA");
	my ($Buyer_Name) = &BIP_DB::Retrieve_Buyer_Name($dbh,$Input_Buyer_ID,"Buyers");
	# File Location
	$Month_Of_Document=~s/[^>]*?\s*([\d]+)\s*[^>]*?$/$1/igs;
	my $Filestorepath='//172.27.137.180/str/BIP/Download/'.$Input_Buyer_ID."/".$Month_Of_Document."/Upload"; 
	# my $Filestorepath='D:/Arul/BIP/Demo/Upload'; 
	unless ( -d $Filestorepath ) 
	{
		$Filestorepath=~s/\//\\/igs;
		system("mkdir $Filestorepath");
	}

	print "Filestorepath	: $Filestorepath\n";
	print "Buyer_Name	: $Buyer_Name\n";

	$FileName=~s/\_\_+/\_/igs;
	my $output_csv = "$Filestorepath/$Input_Buyer_ID".'_'."$FileName";
	open(ts,">>File_Name_List.html");
	print ts "$Batch_ID\t$output_csv\n";
	close ts;	
	############Database Initialization########
	my $dbh = &BIP_DB::DbConnection();
	###########################################
	my $sort_query="select Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2 from SUPPLIER_FORMATTED_DATA where Buyer_ID=\'$Input_Buyer_ID\' and Batch_ID=\'$Batch_ID\'";
	
	my $sdata=$dbh->prepare($sort_query);
	$sdata->execute();

	open my $fh, ">", "$output_csv" or die "Failed to open file: $!";
	$csv->print($fh,["Transaction_ID","Requirement","Amount","Start_Date","End_Date","Supplier_Name","Department_Name","Requirement_2"]);
	close $fh;

	print "$Input_Buyer_ID:$Batch_ID:Exporting to CSV.....\n";
	while (my @results = $sdata->fetchrow()) {
		open my $fh, ">>", "$output_csv" or die "Failed to open file: $!";
		$csv->print($fh,\@results);
		close $fh;
	$Export_Flag='Y';	
	}
	&BIP_DB::Update_Export_Flag($dbh,"BUYERS_DOWNLOAD_DETAILS",$Input_Buyer_ID,$Batch_ID,$Export_Flag);
}
