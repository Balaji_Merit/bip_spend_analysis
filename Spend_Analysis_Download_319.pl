use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use BIP_DB_V1;
use BIP_Column_Map_V1;

my $Input_Buyer_ID = $0;
$Input_Buyer_ID =~ s/\.pl//igs;
$Input_Buyer_ID =$1 if($Input_Buyer_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

# $Input_Buyer_ID='689';
# my $ua  = LWP::UserAgent->new(
        # ssl_opts => { verify_hostname => 1 },
    # );
# my $ua=LWP::UserAgent->new(show_progress=>1);
my $ua=LWP::UserAgent->new(ssl_opts => {SSL_verify_mode => 'SSL_VERIFY_PEER',verify_hostname => 1 }, show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
# $ua->proxy('https', 'http://172.27.137.192:3128'); 
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my %Months_To_Num=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");
my @Months1=("January","February","March","April","May","June","July","August","September","October","November","December","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");

############Database Initialization########
my $dbh = &BIP_DB_V1::DbConnection();
###########################################

my $hash_ref = &BIP_Column_Map_V1::ReadIniFile();
my %inihashvalue = %{$hash_ref};
my $Input_Table='BUYERS';
############ Retrieve Spent Data weblink from Buyers table ###########
my ($ID,$Buyer_Name,$Spent_Data_weblink) = &BIP_DB_V1::RetrieveUrl($dbh,$Input_Buyer_ID,$Input_Table);
my $Buyer_ID 			= @$ID[0];
my $Buyer_Name 			= @$Buyer_Name[0];
my $Buyer_Url 			= @$Spent_Data_weblink[0];

###############
print "Buyer_ID	: $Buyer_ID\n";	
print "Buyer_Name	: $Buyer_Name\n";	
print "Buyer_Url	: $Buyer_Url\n";	
sleep(10);
my ($Buyer_content,$Status_Line)=&Getcontent($Buyer_Url); &BIP_Column_Map_V1::Link_Status_Update($dbh,$Buyer_ID,$Status_Line);
open(ts,">Buyer_content215.html");
print ts "$Buyer_content";
close ts;

my ($Processed_LinkInfo) = &BIP_DB_V1::LinkInfo_RetrieveUrl($dbh,$Buyer_ID,"bip_pdf_json_status");
my @Processed_LinkInfo=@$Processed_LinkInfo;
# print "Processed_LinkInfo :: @Processed_LinkInfo \n";exit;

while($Buyer_content=~m/href\=\"([^>]*?\s*\.pdf)\">\s*([^>]*?)\s*<\/a>/igs)
{
	my $Doc_Link=$1;
	my $LinkInfo=$2;	
	$LinkInfo=&clean($LinkInfo);		
	my $LinkInfo_Temp=$LinkInfo;
	my ($File_Date,$File_Month,$File_Year,$Doc_Name,$Doc_Type);
	# next if($Doc_Link=~m/\.pdf/is);		
	# Get Title of the file name
	print "LinkInfo :: $LinkInfo \n";
	if($LinkInfo ne '') 
	{			
		$LinkInfo=~s/\s\s+/ /igs;
		print "LinkInfo	: $LinkInfo\n";
		
		if($LinkInfo ~~ @Processed_LinkInfo || $LinkInfo =~ m/^\s*$/)
		{
			print "File Already Processed\n";
			next;
		}
		else
		{			
			print "\nThis is new file to download\n";
		}		
	}	
	####### Get file link 	#################################	
	if($Doc_Link!~m/^http/is)
	{	
		print "\nhi\n";
		my $u1=URI::URL->new($Doc_Link,$Buyer_Url);
		my $u2=$u1->abs;
		$Doc_Link=$u2;
		$Doc_Link=~s/\&amp\;/&/igs;
	}	
	
	my $Doc_Link_Temp=$Doc_Link;	
		
	foreach my $monthss(@Months1)	
	{		
		if($LinkInfo_Temp=~m/$monthss/is)
		{
			$File_Month=$monthss;
			if($LinkInfo_Temp=~m/(2\d{3})/is)
			{
				$File_Year=$1;
			}			
			else
			{
				while($LinkInfo_Temp=~m/(\d{2,4})/igs)
				{					
					$File_Year=$1;
				}
			}	
			last;
		}			
	}	
	###### print "Doc_Link :: $Doc_Link \n";exit;
	my ($Doc_content,$status_line,$File_Type,$File_Type_cont)=&Getcontent($Doc_Link);
	
	if($File_Type=~m/(csv|xlsx|xls|pdf)/is)
	{
		$Doc_Type="$1";
		print "\nI:1\n";
	}
	elsif($File_Type_cont=~m/(csv|pdf)/is)
	{
		$Doc_Type="$1";
		print "\nI:2\n";	
	}
	elsif($Doc_Link_Temp=~m/^[^>]*?\.([\w]*?)\s*$/is)
	{
		$Doc_Type="$1";		
		print "\nI:5\n";	
	}			

		
	$Doc_Type=~s/\"//igs;
	$Doc_Type=~s/\'//igs;
			
	# if($LinkInfo_Temp=~m/(Month\s*\d+)\s(\d+\s*\-\s*\d+)/is)
	# {
		# $File_Month=$1;
		# $File_Year=$2;		
	# }
	my $Month_Of_Document = "$File_Month $File_Year";	
	
	# File name creation from Link Info
	my $File_Type = $Doc_Type if ($Doc_Type ne '');
	$File_Year=~s/\W+//igs;
	$Doc_Name=$LinkInfo;
	$Doc_Name=~s/\W/\_/igs;
	$Doc_Name=~s/_+/\_/igs;
	$Doc_Name.='.'.$File_Type;
	print "#########################################\n";
	print "Doc_Name	: $Doc_Name\n"; 
	print "File_Month		: $File_Month\n";
	print "File_Year		: $File_Year\n";
	print "Doc_Type		: $Doc_Type\n";
	print "Doc_Link	: $Doc_Link\n";
	print "Month_Of_Document	: $Month_Of_Document\n";
	print "#########################################";
	# File Path creation
	# my $Filestorepath='D:/Arul/BIP/'.$Buyer_ID."/".$File_Year."/Source";
	my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source";  
	
	# File location creation
	unless ( -d $Filestorepath ) 
	{
		$Filestorepath=~s/\//\\/igs;
		system("mkdir $Filestorepath");
	}
	my $Storefile = "$Filestorepath/$Doc_Name";
	print "STOREPATH  : $Storefile \n";
	my $StartDate=DateTime->now();
	# Spend file download
	
	my $fh=FileHandle->new("$Storefile",'w');
	binmode($fh);
	$fh->print($Doc_content);
	$fh->close();
	my ($Status,$Failed_Reason);
	if($status_line!~m/20/is)
	{
		$Failed_Reason=$status_line;
	}
	else
	{
		$Status='Downloaded';
	}
	print "\nStatus:::: $Status\n";
	&BIP_DB_V1::Insert_Pdf_Json_Details($dbh,$Buyer_ID,$Storefile,$LinkInfo,$Month_Of_Document);
	# print "Storefile :: $Storefile \n";exit;
	# &BIP_Column_Map_V1::ReadSource($Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
	print "Completed \n";
}


sub Getcontent
{
	my $url = shift;

	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"www.northumbria-pcc.gov.uk");
	# $req->header("Referer"=>"");
	$req->header("Cookie"=> "__cfduid=d1a34cf4c19451a8e5aeb0c98279bce671553772075; cf_clearance=190450c9da2643ddc0b1643e5db6f269cebcb5cb-1553772079-31536000-150; __utma=1.462047561.1553772064.1557743433.1558960962.8; __utmz=1.1553772064.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); PHPSESSID=9iah10olna9cq20cptf2fkel46; __utmb=1.1.10.1558960962; __utmc=1; __utmt=1");
		sleep(50);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	print "\nFile_Type---> $File_Type\n";
	print "\nFile_Type_cont---> $File_Type_cont\n";
	# my $get_cookie=$res->header('set-cookie');
	print "\nCODE---> $code\n";
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$File_Type,$File_Type_cont);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	if($Data eq '')
	{
		# $Data='Not provided';
	}
	return $Data;
}

sub clean()
{
	my $C_Clean=shift;
	$C_Clean=~s/<[^>]*?>//igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/\&nbsp\;/ /igs;
	$C_Clean=~s/\&amp\;/ /igs;
	$C_Clean=~s/\&pound\;/ /igs;
	$C_Clean=~s/\&\#8211\;/ /igs;
	$C_Clean=~s/ndash\;/ /igs;
	$C_Clean=~s/[^[:print:]]+//igs;
	
	
	$C_Clean=~s/\&nbsp\;//igs;
	$C_Clean=~s/\#160\;//igs;
	$C_Clean=~s/\#163\;//igs;
	$C_Clean=~s/\&/ /igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	return $C_Clean;
}