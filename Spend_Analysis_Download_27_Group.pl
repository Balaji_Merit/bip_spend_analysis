# Script for Group Of url's 'http://data.gov.uk/dataset/financial-transactions-data-defra'
# Buyer ID 27,39,45,50,73,80,86,90,125,126,135,182,183,184,185
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use BIP_DB_V1;
use BIP_Column_Map_V1;

my $Input_Buyer_ID = $0;
$Input_Buyer_ID =~ s/\.pl//igs;
$Input_Buyer_ID =$1 if($Input_Buyer_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

# my $ua=LWP::UserAgent->new(ssl_opts=>{verify_hostname=>0},show_progress=>1);
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
# $ua->proxy('http', 'http://172.27.137.199:3128'); 
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/\.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my %Months_To_Num=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");

############Database Initialization########
my $dbh = &BIP_DB_V1::DbConnection();
###########################################

$Input_Buyer_ID='135';

my $Input_Table='BUYERS';
############ Retrieve Spent Data weblink from Buyers table ###########
my ($ID,$Buyer_Name,$Spent_Data_weblink) = &BIP_DB_V1::RetrieveUrl($dbh,$Input_Buyer_ID,$Input_Table);
my $Buyer_ID 			= @$ID[0];
my $Buyer_Name 			= @$Buyer_Name[0];
my $Buyer_Url 			= @$Spent_Data_weblink[0];
# $Buyer_Url 			= "https://data.gov.uk/dataset/91072f06-093a-41a2-b8b5-6f120ceafd62/spend-over-25-000-in-the-department-for-environment-food-and-rural-affairs";
###############
print "Buyer_ID	: $Buyer_ID\n";	
print "Buyer_Name	: $Buyer_Name\n";	
print "Buyer_Url	: $Buyer_Url\n";	

my ($Buyer_content,$Status_Line)=&Getcontent($Buyer_Url); # Ping the web link page
open(ts,">Buyer_content.html");
print ts "$Buyer_content";
close ts;

&BIP_Column_Map_V1::Link_Status_Update($dbh,$Buyer_ID,$Status_Line);

my ($Processed_LinkInfo) = &BIP_DB_V1::LinkInfo_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
my @Processed_LinkInfo=@$Processed_LinkInfo;
# To get the Doc_url from the Buyer_content
# while($Buyer_content=~m/<div\s*class\s*=\s*\"dataset\-resource\"\s*>([\w\W]*?)<\/div>\s*<[^>]*?col\-sm\-6[^>]*?>/igs)
# {
	# my $List_content=$1;
	# $List_content=~s/\&nbsp\;/ /igs;
	# open(LC,">>List_content.html");
	# print LC $List_content;
	# close LC;
	# my ($LinkInfo,$Doc_Link,$File_Month,$File_Year,$Doc_Name);
	
	# if($List_content =~m/<span\s*class\s*=\s*\"\s*inner[^>]*?>\s*([^>]*?)\s*<\/span>/is )
	# {
		# $LinkInfo=$1;
		# $LinkInfo=&Link_Info_Clean($LinkInfo);
	# }
	
	# if($List_content=~m/<a\s*[^>]*?\s*href\=(?:\"|\')([^<]*?)(?:\"|\')\s*[^>]*?>\s*<i\s*class\s*\=\s*\"icon\-download\-alt\"\s*>\s*<\/i>\s*Download/is )
	# if($List_content=~m/<a\s*[^>]*?\s*href\=(?:\"|\')([^<]*?)(?:\"|\')\s*[^>]*?>\s*<[^>]*?>\s*Download/is )
	# while($Buyer_content=~m/<a\s*[^>]*?\s*href\=(?:\"|\')([^<]*?)(?:\"|\')\s*[^>]*?>\s*<[^>]*?>\s*Download/igs)
	while($Buyer_content=~m/href="([^>]*?\.csv)">\s*<span class="visually-hidden">Download\s*<\/span>\s*([^>]*?)\s*</igs)
	{
		my ($LinkInfo,$Doc_Link,$File_Month,$File_Year,$Doc_Name);
		$Doc_Link=$1;
		$LinkInfo=$2;
		#print "Vinoth :: $Doc_Link";
		#exit;		
		my $Doc_Type;
		if ( $Doc_Link =~ m/^[^>]*?\.([\w]*?)\s*$/is )
		{
			$Doc_Type=$1;
		}
				# Get Date, Month, Year from the link info for file location Creation
		if($LinkInfo=~m/[^>]*?\s+([a-zA-Z]+)\s+(\d{4})\s*return$/is)
		{
			$File_Month=$1;
			$File_Year=$2;
			print "File_Month	: $File_Month\n";
			print "File_Year	: $File_Year\n";
		}
		my $Month_Of_Document = "$File_Month $File_Year";
		
		# print "LinkInfo	: <$LinkInfo>\n";
		# <stdin>;
		if($LinkInfo ~~ @Processed_LinkInfo || $LinkInfo =~ m/^\s*$/)
		{
			print "File Already Processed\n";
			next;
		}
		if($Doc_Link!~m/http/is)
		{
			my $u1=URI::URL->new($Doc_Link,$Buyer_Url);
			my $u2=$u1->abs;
			$Doc_Link=$u2;
		}
		print "Doc_Link	: $Doc_Link\n";
		
		# File name creation from Link Info
		my $File_Type = $Doc_Type if ($Doc_Type ne '');
		
		$Doc_Name=$LinkInfo;
		$Doc_Name=~s/\W/\_/igs;
		$Doc_Name=~s/_+/\_/igs;
		$Doc_Name.='.'.$File_Type;
		print "Doc_Name	: $Doc_Name\n"; 
		
		# File Path creation
		my $hash_ref = &BIP_Column_Map_V1::ReadIniFile();	my %inihashvalue = %{$hash_ref};	my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source";  
		
		# File location creation
		unless ( -d $Filestorepath ) 
		{
			$Filestorepath=~s/\//\\/igs;
			system("mkdir $Filestorepath");
		}
		my $Storefile = "$Filestorepath\\$Doc_Name";
		print "STOREPATH  : $Storefile \n";
		
		my $StartDate=DateTime->now();
		# Spend file download
		my ($Doc_content,$status_line)=&Getcontent($Doc_Link);
		my $fh=FileHandle->new("$Storefile",'w');
		binmode($fh);
		$fh->print($Doc_content);
		$fh->close();
		my ($Status,$Failed_Reason);
		if($status_line!~m/20/is)
		{
			$Failed_Reason=$status_line;
		}
		else
		{
			$Status='Downloaded';
		}
		print "\nStatus:::: $Status\n";
		<>;
		&BIP_Column_Map_V1::ReadSource($Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
		print "Completed \n";
		############ Retrieve Group ID from Buyers table ###########
		# my ($ID,$Group_ID) = &BIP_DB_V1::Retrieve_Group_ID($dbh,$Input_Buyer_ID,$Input_Table);
		# my $Group_ID_Count = @{$Group_ID};
		# for(my $reccnt = 0; $reccnt < $Group_ID_Count; $reccnt++ )
		# {
			# my $Group_Buyer_ID 	= &Trim(@$ID[$reccnt]);
			# my $Group_ID 	= &Trim(@$Group_ID[$reccnt]);		
			# &BIP_Column_Map_V1::ReadSource($Group_Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
			# print "$Group_Buyer_ID From Group ID $Group_ID Completed \n";
		# }	
	}
# }

sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	# my $get_cookie=$res->header('set-cookie');
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	if($Data eq '')
	{
		# $Data='Not provided';
	}
	return $Data;
}

sub Link_Info_Clean()
{
	my $Data=shift;
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\&pound\;//igs;
	$Data=~s/\&nbsp\;/ /igs;
	$Data=~s/\&amp\;/&/igs;
	$Data=~s/�//igs;
	$Data=~s/[^[:print:]]//igs;
	$Data=~s/\s*\([^>]*?\)\s*//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}




sub clean()
{
	my $C_Clean=shift;
	$C_Clean=~s/<[^>]*?>//igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/\&nbsp\;/ /igs;
	$C_Clean=~s/\&amp\;/ /igs;
	$C_Clean=~s/\&pound\;/ /igs;
	$C_Clean=~s/\&\#8211\;/ /igs;
	$C_Clean=~s/ndash\;/ /igs;
	$C_Clean=~s/[^[:print:]]+//igs;
	
	
	$C_Clean=~s/\&nbsp\;//igs;
	$C_Clean=~s/\#160\;//igs;
	$C_Clean=~s/\#163\;//igs;
	$C_Clean=~s/\&/ /igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	return $C_Clean;
}