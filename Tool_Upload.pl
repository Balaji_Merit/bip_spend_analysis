#!/usr/bin/perl -w

## Imports
use strict;
use LWP::UserAgent;
use HTTP::Cookies;
use JSON;
require BIP_DB;

my %Months=("Jan" => "01", "Jeb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");

my $Batch_ID_List=$ARGV[0];
my $Start_Mon_Year=$ARGV[1];
my $End_Mon_Year=$ARGV[2];
my ($Start_Month,$Start_Year,$End_Month,$End_Year);
if($Start_Mon_Year eq '')
{
	print "\nEnter File Month and Year\n";
	exit;
}
if($Start_Mon_Year=~m/\s*([a-z]+)\s*(?:-|\s+)\s*([\d]+)\s*/is)
{
	$Start_Month=$1;
	$Start_Year=$2;
	print "";
}
if($End_Mon_Year ne '')
{
	if($End_Mon_Year=~m/\s*([a-z]+)\s*(?:-|\s+)\s*([\d]+)\s*/is)
	{
		$End_Month=$1;
		$End_Year=$2;
	}
}
else
{
	$End_Month=$Start_Month;
	$End_Year=$Start_Year;
}	
$Start_Month=$Months{$Start_Month};
$End_Month=$Months{$End_Month};
print "$Batch_ID_List\n";
print "SM: $Start_Month\n";
print "SY: $Start_Year\n";
print "EM: $End_Month\n";
print "EY: $End_Year\n";

# Get Month(MM) and year(YYYY)
my @Current_Date=split(" ",localtime(time));
my $Current_Month_txt=$Current_Date[1];
my $Current_Year=$Current_Date[4];
my $Current_Date=$Current_Date[2];
my $Current_Month=$Months{$Current_Month_txt};

# print "Current_Month_txt-->$Current_Month_txt\n";
# print "Current_Year-->$Current_Year\n";
# print "Current_Date-->$Current_Date\n";
# print "Current_Month-->$Current_Month\n";

## User agent setup
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Merit BI OUToader");
$ua->timeout(50);
push @{ $ua->requests_redirectable }, 'POST';

my $cookiefile = $0;
$cookiefile =~s/\.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1);
$ua->cookie_jar($cookie);
my $Input_Table='BUYERS_DOWNLOAD_DETAILS';
############Database Initialization########
my $dbh = &BIP_DB::DbConnection();
###########################################
my ($Input_Buyer_ID,$Batch_ID,$FileName,$Filestorepath) = &BIP_DB::Retrieve_Buyers_Download_Details_For_Import($dbh,$Input_Table,$Batch_ID_List);
my @Input_Buyer_ID		= @$Input_Buyer_ID;
my @Batch_ID			= @$Batch_ID;
my @FileName			= @$FileName;
my @Filestorepath		= @$Filestorepath;
for(my $i=0;$i<@Input_Buyer_ID;$i++)
{
	$Input_Buyer_ID=$Input_Buyer_ID[$i];
	$Batch_ID=$Batch_ID[$i];
	$FileName=$FileName[$i];
	my $File_Location=$Filestorepath[$i];
	my ($Month_Of_Document) = &BIP_DB::Retrieve_Month_Document($dbh,$Input_Buyer_ID,$Batch_ID,"SUPPLIER_SOURCE_DATA");
	my ($Buyer_Name,$Upload_Buyer_Name) = &BIP_DB::Retrieve_Buyer_Name($dbh,$Input_Buyer_ID,"Buyers");
	print "$Batch_ID: Month_Of_Document: $Month_Of_Document\n";
	if($Upload_Buyer_Name ne '')
	{
		$Buyer_Name=$Upload_Buyer_Name;
	}
	my $File_Year=$Month_Of_Document;
	$File_Year=~s/[^>]*?\s*([\d]+)\s*[^>]*?$/$1/igs;
	$FileName=~s/\_\_+/\_/igs;
	$FileName=~s/\.xlsx?$/\.csv/igs;	
	print "MOD: $File_Year\n";
	# my $Upload_Filename=$File_Location."/".$Input_Buyer_ID."_".$FileName;
	my $Upload_Filename="C:/BIP/Export/Export_".$Input_Buyer_ID."_".$FileName;
	print "Location:: $Upload_Filename\n";
	my ($Upload_Status,$Warning_Msg,$Uploaded_Reference_Number,$Import_BITOOL_Status);
	if (! -e $Upload_Filename) {
		$Warning_Msg='File Not Exists';
		$Uploaded_Reference_Number=0;
		$Import_BITOOL_Status='N';
		print "$Warning_Msg\n";
		&Status_Update($dbh,$Input_Buyer_ID,$Batch_ID,$Import_BITOOL_Status,$Warning_Msg,$Uploaded_Reference_Number);
		next;
	} 

	## Login
	my $url='http://62.253.177.4:8080/businessIntelligence/j_security_check';

	my $response=$ua->post(
		$url,
		[
			'j_username' => 'gladys.christopher@meritgroup.co.uk',
			'j_password' => 'zOl3BoXy'
		]
	);

	if ($response->is_success) {
		if (!$response->request()->uri() =~ /http:\/\/62.253.177.4:8080\/businessIntelligence\/businessIntelligence.html/) {
			die "Login Failed";
		}
	}

	## Login is successful, cookies in the cookie jar.

	## Initialize the OUToad process
	$url='http://62.253.177.4:8080/businessIntelligence/data/loader/dataImport.html';
	$response = $ua->get($url);

	## Load the existing buyer list. Currently unused, but could be used to check if the appropriate flag
	## should be set
	$url='http://62.253.177.4:8080/businessIntelligence/buyer/data.json?action=findAllSpendBuyers&_=1432644269711';
	$response = $ua->get($url);
	my $buyer_json=$response->content;
	# my $buyerStructure=decode_json($buyer_json);


	## OUToad the file.

	my $re2=1;
	my $OUToadUrl='http://62.253.177.4:8080/businessIntelligence/data/loader/dataImport.html';

	Reping1:
	$response=$ua->post(
						$OUToadUrl,
						Content_Type => 'form-data',
						Content =>[
							'OUToad_form' => 'true',
							'buyerName' => $Buyer_Name,
							'fileType' => 'CSV',
							'data_file' => [
											"$Upload_Filename" => $Upload_Filename,
											"Content_Type" => 'application/octetstream'
										],
							'batchOUToad.batchName' => '',
							'_target1' => 'proceed'
						]
	);

	## At this stage, warnings will be presented if any exist. They are presented in Divs with a class of 'warning'
	## This should be handled in some fashion.
	my $Import_Reference_code=$response->code;	
	my $Upload_Content=$response->content;
	if($Import_Reference_code=~m/50/igs)
	{
		print "Code: $Import_Reference_code\n";
		if($re2<=3)
		{
			$re2++;
			goto Reping1;
		}
		else		
		{
			print "\n---------- IMPORT FAILED ----------\n";
		}
	}
	open(OUT,">Upload_Content.html");
	print OUT $Upload_Content;
	close OUT;
	print "Upload_Content\n";

	## Map appropriately

	my $mapUrl='http://62.253.177.4:8080/businessIntelligence/data/loader/dataImport.html';

	$response=$ua->post(
						$mapUrl,
						Content_Type => 'form-data',
						Content =>[
							'_target2' => 'proceed',
							'_update_mappings' => 'true',
							'processedEndMonth' => $Start_Month,
							'processedEndYear' => $Start_Year,
							'processedStartMonth' => $End_Month,
							'processedStartYear' => $End_Year,
							'property.amount.column' => 'Amount',
							'property.transactionId.column' => 'Transaction_ID',
							'property.buyerName.defaultValue.dataType' => 'STRING',
							'property.buyerName.defaultValue' => $Buyer_Name,
							'property.requirement.column'=>'Requirement',
							'property.startDate.column'=>'Start_Date',
							'property.startDate.defaultValue.dataType'=>'DATE',
							'property.startDate.defaultValue'=>'',
							'property.endDate.column'=>'End_Date',
							'property.endDate.defaultValue.dataType'=>'DATE',
							'property.endDate.defaultValue'=>'',
							'property.supplierName.column'=>'Supplier_Name',
							'property.departmentName.column'=>'Department_Name',
							'property.departmentName.defaultValue.dataType'=>'STRING',
							'property.departmentName.defaultValue'=>'Unknown',
							'property.requirementLevel2.column'=>'Requirement_2'
						]
	);
	my $OUToad_Content=$response->content;
	open(OUT,">OUToad_Responce1.html");
	print OUT $OUToad_Content;
	close OUT;
	if($OUToad_Content=~m/<div\s*class\s*=\s*\"\s*warning\s*\"\s*>\s*([\w\W]*?)\s*<\/div>/is)
	{
		$Warning_Msg=$1;
		$Warning_Msg=~s/<[^>]*?>/ /igs;
		$Warning_Msg=~s/\&nbsp\;/ /igs;
		$Warning_Msg=~s/\&pound\;/\�/igs;
		$Warning_Msg=~s/\s\s+/ /igs;
		print "Warning:\n--------\n$Warning_Msg\n";
	}
	## At this stage, warnings will be presented if any exist. They are presented in Divs with a class of 'warning'
	## This should be handled in some fashion. If they occur, the map step will be needed to be done again.


	my $finishUrl='http://62.253.177.4:8080/businessIntelligence/data/loader/dataImport.html';

	$response = $ua->post(
					$finishUrl,
					Content_Type => 'form-data',
					Content =>[
						'_finish'=>'continue'
					]
	);

	my $Uploaded_code=$response->code;
	my $OUToad_Content1=$response->content;
	open(OUT,">OUToad_Responce2.html");
	print OUT $OUToad_Content1;
	close OUT;
	print "OUToad_Content: OUToad_Content\n";
	## process complete. No notification is given on the results of the final page.
	if($Uploaded_code!=200)
	{
		print "\n------ FAILED TO UPLOAD -------\n";
		next;
	}
	print "Please wait for 10 Sec...\n";
	sleep(10);
	my $re1=1;
	my $Batch_OUToads_Url='http://62.253.177.4:8080/businessIntelligence/spend/uploads.html';
	Reping:
	$response = $ua->get($Batch_OUToads_Url);
	my $Uploaded_Reference_code=$response->code;	
	if($Uploaded_Reference_code=~m/50/igs)
	{
		print "Code: $Uploaded_Reference_code\n";
		if($re1<=3)
		{
			$re1++;
			goto Reping;
		}	
		else		
		{
			print "\n----------UNABLE TO GET REFERENCE----------\n";
		}
		
	}
	my $Batch_OUToads_Content=$response->content;
	open(OUT,">Batch_OUToads_Content.html");
	print OUT $Batch_OUToads_Content;
	close OUT;
	if($Batch_OUToads_Content=~m/>\s*ID\s*\:\s*<a\s*href\s*=\s*\"[^>]*?\"\s*>\s*([^>]*?)\s*<\/a>\s*<br\/>\s*Created\s*\:\s*$Current_Year-$Current_Month-0?$Current_Date/is)
	{
		$Uploaded_Reference_Number=$1;
		$Upload_Status='Uploaded';
		$Import_BITOOL_Status='Y';
		print "-----------------------------------------------------\n";
		print "Uploaded Reference Number: $Uploaded_Reference_Number\n";
		print "-----------------------------------------------------\n";
		# my $Rename_Uploaded_File='//172.27.137.180/str/BIP/Download/'.$Input_Buyer_ID."/".$File_Year."/Upload/".$Uploaded_Reference_Number."_".$Input_Buyer_ID."_".$FileName;  
		my $Rename_Uploaded_File='C:/BIP/Export/'.$Uploaded_Reference_Number."_Export_".$Input_Buyer_ID."_".$FileName;  
		rename "$Upload_Filename","$Rename_Uploaded_File";
	}
	
	$Warning_Msg=~s/\'/\'\'/igs;
	#Update the status of the tool uploading
	&Status_Update($dbh,$Input_Buyer_ID,$Batch_ID,$Import_BITOOL_Status,$Warning_Msg,$Uploaded_Reference_Number);
	print "Completed\n";

}

sub Status_Update()
{
	my $dbh = shift;
	my $Input_Buyer_ID = shift;
	my $Batch_ID = shift;
	my $Import_BITOOL_Status = shift;
	my $Warning_Msg = shift;
	my $Uploaded_Reference_Number = shift;
	
	my $Try_Again=0;
	Try_Again:
	my $query = "Update $Input_Table set Import_BITOOL=\'$Import_BITOOL_Status\', Tool_Warning=\'$Warning_Msg\', Upload_Reference=\'$Uploaded_Reference_Number\' where Buyer_ID=\'$Input_Buyer_ID\' and Batch_ID=\'$Batch_ID\'";
	my $sth = $dbh->prepare($query);
	if($sth->execute())
	{
		print "Updated\n";
		$sth->finish();	
	}
	else
	{
		$dbh=&DbConnection();
		if($Try_Again==0)
		{
			$Try_Again=1;
			goto Try_Again;
		}
		else
		{
			open(ERR,">>Upload_Failed_Query.txt");
			print ERR $query."\n";
			close ERR;
		}	
	}
}