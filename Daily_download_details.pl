
#!/usr/bin/perl -w

## Imports
use strict;
use LWP::UserAgent;
use HTTP::Cookies;
use JSON;
use MIME::Lite;
use Date::Parse;
use Date::Manip;
use POSIX 'strftime';
use MIME::Lite;
use Cwd;
require BIP_DB;

sub Retrieve_Link_Not_Working_Buyers()
{
	my $dbh 		= shift;
	my $Input_Table	= shift;
	
	my $fulldate = strftime "%d %m %Y %A", localtime;
my $day = (split(" ",$fulldate))[-1];
print "Day :: $day\n";
my $today = time;
	print "Today::$today\n";
my $yesterday = $today - 60 * 72 * 60;
my $previous_date = strftime "%Y%m%d", ( localtime($yesterday));
		# my $yesterday = $today;
		
		
my $today_date = strftime "%Y-%m-%d", (localtime($today) );
	my $query = "select a.Buyer_ID,a.batch_id,a.FileName,a.no_of_records,a.Month_Of_Doc,a.Source_File_Location,a.Exported_Location,b.Stake_holder_name from buyers_download_details a inner join buyers b on a.Buyer_ID = b.ID where date(startdate)>='".$previous_date."' and b.status='Active'";
	print $query;
	my $sth = $dbh->prepare($query);
	$sth->execute();
	sleep(10);
	my (@Buyer_ID,@batch_id,@FileName,@no_of_records,@@month_of_doc,@Source_File_Location,@Exported_Location,@Stake_holder_name);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@Buyer_ID,&Trim($record[0]));
		push(@batch_id,&Trim($record[1]));
		push(@FileName,&Trim($record[2]));
		push(@no_of_records,&Trim($record[3]));
		push(@@month_of_doc,&Trim($record[4]));
		push(@Source_File_Location,&Trim($record[5]));
		push(@Exported_Location,&Trim($record[6]));
		push(@Stake_holder_name,&Trim($record[7]));
	}
	$sth->finish();
	return (\@Buyer_ID,\@batch_id,\@FileName,\@no_of_records,\@@month_of_doc,\@Source_File_Location,\@Exported_Location,\@Stake_holder_name);
}

sub DbConnection()
{
	my $DBDETAILS 		= &ReadIniFile('DBDETAILS');
	my %Credentials		= %{$DBDETAILS};
	my $database_name 	= $Credentials{'database_name'};
	my $server_name 	= $Credentials{'server_name'};
	my $database_user 	= $Credentials{'database_user'};
	my $database_pass 	= $Credentials{'database_pass'};
	my $driver = "mysql"; 
	my $dsn = "DBI:$driver:database=$database_name;host=$server_name;";
	my $dbh = DBI->connect($dsn, $database_user, $database_pass,{mysql_enable_utf8 => 1}) or die $DBI::errstr;
	return $dbh;
}

my %Months=("Jan" => "01", "Jeb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");

# Get Month(MM) and year(YYYY)
my @Current_Date=split(" ",localtime(time));
my $Current_Month_txt=$Current_Date[1];
my $Current_Year=$Current_Date[4];
my $Current_Date=$Current_Date[2];
my $Current_Month=$Months{$Current_Month_txt};

print "Current_Month_txt-->$Current_Month_txt\n";
print "Current_Year-->$Current_Year\n";
print "Current_Date-->$Current_Date\n";
print "Current_Month-->$Current_Month\n";

## User agent setup
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Merit BI OUToader");
$ua->timeout(50);
push @{ $ua->requests_redirectable }, 'POST';

my $cookiefile = $0;
$cookiefile =~s/\.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1);
$ua->cookie_jar($cookie);
my $Input_Table='buyers_download_details';
############Database Initialization########
my $dbh = &DbConnection();
###########################################
my ($Buyer_ID,$batch_id,$FileName,$no_of_records,$month_of_doc,$Source_File_Location,$Exported_Location,$Stake_holder_name) = &Retrieve_Link_Not_Working_Buyers($dbh,$Input_Table);

# exit;
my @Buyer_ID=@$Buyer_ID;
my @batch_id=@$batch_id;
my @FileName=@$FileName;
my @no_of_records=@$no_of_records;
my @month_of_doc=@$month_of_doc;
my @Source_File_Location=@$Source_File_Location;
my @Exported_Location=@$Exported_Location;
my @Stake_holder_name=@$Stake_holder_name;
# print $Tender_link;
# exit;
open(DATA,">Batch_Details.csv");
print DATA "Buyer_ID,Batch_ID,File_Name,No_of_Records,Month_of_doc,Source_File_Location,Exported_Location,Stake_holder_name\n";
for(my $i=0;$i<@Buyer_ID;$i++)
{
	my $Buyer_ID=$Buyer_ID[$i];
	my $batch_id=$batch_id[$i];
	my $FileName=$FileName[$i];
	my $no_of_records=$no_of_records[$i];
	my $month_of_doc=$no_of_records[$i];
	my $Source_File_Location=$Source_File_Location[$i];
	my $Exported_Location=$Exported_Location[$i];
	my $Stake_holder_name=$Stake_holder_name[$i];
	print DATA "$Buyer_ID,$batch_id,$FileName,$no_of_records,$month_of_doc,$Source_File_Location,$Exported_Location,$Stake_holder_name\n";
}
close DATA;
my $fulldate = strftime "%b %d, %Y %A %H:%M:%S", localtime;
&send_mail("BiP Solutions_Daily_Files_Downloaded_details_$fulldate","Batch_Details.csv");

sub send_mail()
{
	my $subject = shift;
	my $File_Name = shift;
	my $dir=getcwd();
	$dir=~s/\//\\/igs;
	$dir=$dir.'\\'.$File_Name;	

	# my $host ='mail.meritgroup.co.uk'; 
	# my $from='autoemailsender@meritgroup.co.uk';
	# my $user='autoemailsender';
	# my $to ='arul.kumaran@meritgroup.co.uk';
	# my $pass='T!me#123456';	
	
	my $host ='74.80.234.196'; 
	my $from='autoemailsender@meritgroup.co.uk';
	my $user='meritgroup';
	my $to ='spend.reports@meritgroup.co.uk';
	# my $cc='balaji.ekambaram@meritgroup.co.uk,vinothkumar.rajendren@meritgroup.co.uk';
	my $pass='sXNdrc6JU';
	my $body = "Greetings from Software support..!<br><br>\t\tPlease find the attached BIP Spend Analysis - files downloaded details status file<br><br><br><br>Note : This is an automatically generated email, please don't reply.<br><br>Have a nice day..!<br><br>"."Thanks &amp; Regards,<br>Merit Software Support.";
	print "Mail Part";
	my $msg = MIME::Lite->new (
	  From => $from,
	  To => $to,
	  
	  Subject => $subject,
	  Data => $body,
	  Type =>'multipart/mixed'
	) or die "Error creating multipart container: $!\n";
	$msg->attach(
		Type=> "text/html",
		Data     => $body
	);
	$msg->attach (
	   Type => 'application/csv',
	   Path=> $dir,
	   Filename => $File_Name,
	   Disposition => 'attachment'
	) or die "Error adding $!\n";
	print 	$msg->attach (
	   Type => 'application/csv',
	   Path=> $dir,
	   Filename => $File_Name,
	   Disposition => 'attachment'
	) or die "Error adding $!\n";
    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	$msg->send;
}

sub ReadIniFile()
{
	my $INIVALUE = shift;
	my $cfg = new Config::IniFiles( -file => 'C:\Perl\lib\BIP_Config.ini', -nocase => 1);
	# my $cfg = new Config::IniFiles( -file => 'BIP_Config.ini', -nocase => 1);
	my %hash_ini1;
	my @FileExten = $cfg -> Parameters("$INIVALUE");
	foreach my $FileExten (@FileExten)
	{
		my $FileExt =  $cfg -> val("$INIVALUE", $FileExten);
		$hash_ini1{$FileExten}=$FileExt;
	}
	return(\%hash_ini1);
}
sub Trim()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}