use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use Text::CSV;
use JSON;
use HTTP::Cookies;
require BIP_DB;

############Database Initialization########
my $dbh = &BIP_DB::DbConnection();
###########################################

my $csv = Text::CSV->new({ binary => 1, auto_diag => 1, eol => "\n"})
        or die "Cannot use CSV: " . Text::CSV->error_diag();


	my $Month_Of_Document1=$Month_Of_Document;
	# File Location
	# $Month_Of_Document=~s/[^>]*?\s*([\-\s\d]+)\s*[^>]*?\s*$/$1/igs;
	if($Month_Of_Document=~m/^\s*[a-z]*?\s*([\d]{2,4})\s*[a-z]+?\s*([\d]{2,4})\s*[a-z]*?\s*$/is)
	{
		$Month_Of_Document="$1-$2";
	}
	elsif($Month_Of_Document=~m/^\s*[a-z]*?\s*([\d]{2,4})\s*[a-z]*?\s*$/is)
	{
		$Month_Of_Document=$1;
	}
	
	$Month_Of_Document=~s/^\s*|\s*$//igs;
	print "Month_Of_Document: [$Month_Of_Document]\n";
	open(MOD, ">>Month_Of_Document.html");
	print MOD "$Month_Of_Document1\t$Month_Of_Document\n";
	close MOD;
	
