use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use Time::Local qw(timelocal_nocheck);
use Spreadsheet::ParseExcel::Utility qw(ExcelLocaltime);
use Spreadsheet::WriteExcel::Utility; 
use Date::Parse;
use POSIX;
require BIP_DB;

# my $Input_Buyer_ID=$ARGV[0];
# my $Batch_ID=$ARGV[0];

my @Months=("","January","February","March","April","May","June","July","August","September","October","November","December");
my %Months1=("jan" => "01", "feb" => "02", "mar" => "03", "apr" => "04", "may" => "05", "jun" => "06", "jul" => "07", "aug" => "08", "sep" => "09", "oct" => "10", "nov" => "11", "dec" => "12");
my @lower_months = keys %Months1;
my %Months2=("january" => "01", "february" => "02", "march" => "03", "april" => "04", "may" => "05", "june" => "06", "july" => "07", "august" => "08", "september" => "09", "october" => "10", "november" => "11", "december" => "12");
my %Months3=("January" => "Jan", "February" => "Feb", "March" => "Mar", "April" => "Apr", "May" => "May", "June" => "Jun", "July" => "Jul", "August" => "Aug", "September" => "Sep", "October" => "Oct", "November" => "Nov", "December" => "Dec");
my %MonthHash =();
 %MonthHash = ('JAN' => '01','FEB' => '02','MAR' => '03','APR' => '04','MAY' => '05','JUN' => '06','JUL' => '07','AUG' => '08','SEP' => '09','OCT' => '10','NOV' => '11','DEC' => '12','JANUARY' => '01','FEBRUARY' => '02','MARCH' => '03','APRIL' => '04','MAY' => '05','JUNE' => '06','JULY' => '07','AUGUST' => '08','SEPTEMBER' => '09','OCTOBER' => '10','NOVEMBER' => '11','DECEMBER' => '12');
 
my ($Second, $Minute, $Hour, $Day, $Month, $Year, $WeekDay, $DayOfYear, $IsDST) = localtime();
 $Month = $Month+1;
 $Year = $Year+1900;

my $YY; # Year in last 2 digits
if($Year=~m/([\d]{2})$/is)
{
	$YY=$1;
}

my $Cencheck = substr($Year,2,2);
my $Cen  = substr($Year,0,2);
my $Cen1  = $Cen-1;
 
############Database Initialization########
my $dbh = &BIP_DB::DbConnection();
###########################################

my $Input_Table='SUPPLIER_SOURCE_DATA';
############ Retrieve Buyer_Name, ID, Spent Data weblink from Buyers table ###########
my $import_flag='Y';
my $format_flag='N';
my ($Input_Buyer_ID,$Batch_ID,$Linkinfo) = &BIP_DB::Retrieve_Buyers_Download_Details_For_Clean($dbh,"BUYERS_DOWNLOAD_DETAILS",$import_flag,$format_flag);
my @Input_Buyer_ID		= @$Input_Buyer_ID;
my @Batch_ID			= @$Batch_ID;
my @Linkinfo			= @$Linkinfo;

for(my $i=0;$i<@Input_Buyer_ID;$i++)
{
	$Input_Buyer_ID=$Input_Buyer_ID[$i];
	$Batch_ID=$Batch_ID[$i];
	$Linkinfo=$Linkinfo[$i];
	print "Batch_ID: $Batch_ID\n";
	print "Linkinfo: $Linkinfo\n";

	####### Identify Month OF Document ##########
	my ($MOD,@MOD,@YYYY);
	while ($Linkinfo=~m/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)/igs)
	{
		push(@MOD,$1);
	}
	while ($Linkinfo=~m/(20[\d]{2})/igs)
	{
		push(@YYYY,$1);
	}
	if(@YYYY == 0)
	{
		while($Linkinfo=~m/\b([\d]{2})\b/igs)
		{
			my $FY=$1;
			if($FY<=$YY)
			{
				push(@YYYY,$FY);
				last;
			}
		}
	}
	if(@YYYY>1)
	{
		if(@MOD>1)
		{
			$MOD="$MOD[0]-$YYYY[0] $MOD[1]-$YYYY[1]";
		}	
		else
		{
			$MOD="$MOD[0]-$YYYY[0]";
		}	
	}
	elsif(@MOD>1)
	{
		$MOD="$MOD[0]-$YYYY[0] $MOD[1]-$YYYY[0]";
	}
	else
	{
		$MOD="$MOD[0]-$YYYY[0]";
	}
	$MOD='' if($MOD!~m/\w/is);	
	$MOD=~s/\-//igs if($MOD!~m/[a-z]+-[\d]+/is);	

	my $query = "update BUYERS_DOWNLOAD_DETAILS set Month_Of_Doc=\'$MOD\' where Buyer_ID=\'$Input_Buyer_ID\' and Batch_ID=\'$Batch_ID\'";
	my $sth = $dbh->prepare($query);
	if($sth->execute())
	{
		print "Updated\n";
		$sth->finish();	
	}


	print "MOD:$MOD\n";
	# <stdin>;
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	# $Data=~s/Not\s*provided//igs;
	# if($Data eq '')
	# {
		# $Data='Not provided';
	# }
	return $Data;
}

sub Price_Clean()
{
	my $Data=shift;
	my $Format_Error;
	if($Data=~m/^\s*\([^>]*?\)\s*$/is)
	{
		$Data=~s/[\(\)]//igs;
		$Data='-'.$Data;
	}
	$Data=~s/^\s*-/minussymbol/igs;
	$Data=~s/^minussymbol\W+/minussymbol/igs;
	$Data=~s/^\W+|\W+$//igs;
	$Data=~s/\,//igs;
	$Data=~s/minussymbol/-/igs;
	$Data = sprintf("%.2f", $Data);	
	if(($Data=~m/^\s*$/is) or ($Data=~m/^0.00$/is) or ($Data=~m/^0$/is))
	{
		$Data='Not Provided'
	}	
	return ($Data);
}
sub Date_Format()
{
	my $Data=shift;
	my $Format_Error;
	my ($DD,$MM,$YY);

	if($Data=~m/^\s*(0*[1-9]|[12][0-9]|3[01])\s*[-\/. ]\s*(0*[1-9]|1[012])\s*[-\/. ]\s*((?:19|20)\d\d)\s*$/is) # DD/MM/YYYY - Format;
	{
		$DD=$1;
		$MM=$2;
		$YY=$3;
	}
	elsif($Data=~m/^\s*((?:19|20)\d\d)\s*[-\/. ]\s*(0*[1-9]|1[012])\s*[-\/. ]\s*(0*[1-9]|[12][0-9]|3[01])\s*$/is) # YYYY/MM/DD - Format
	{
		$YY=$1;
		$MM=$2;
		$DD=$3;
	}
	elsif($Data=~m/^\s*(0*[1-9]|[12][0-9]|3[01])\s*[-\/. ]\s*([a-z]+?)\s*[-\/. ]\s*((?:19|20)\d\d)\s*$/is) # YYYY/MM as Text/DD - Format
	{
		$DD=$1;
		$MM=lc($2);
		$YY=$3;
		my $mon=$Months1{$MM};
		if($mon eq '')
		{
			$mon=$Months2{$MM};
		}
	}
	else
	{
		if($Data ne '')
		{
			$Format_Error='Format Error at Date';
			open(DATA,">>Date_Format_Error.txt");
			print DATA $Format_Error;
			close DATA;
		}	
	}
	return ($Data);
}

sub formatdate
{
	my $value = shift;
	my $Month_Of_Document = shift;
	$value =~ s/^\s+|\s+$//g;
	my $value2; #End Date
	my ($day,$month,$year);
	if($value eq '')
	{
		if($Month_Of_Document=~m/^\s*([a-z]+)\s*([\d]+)$/is)
		{
			$month=$1;
			$year=$2
		}
		$value=$month.'/'.$year;
	}	
	print "V1: $value\n";
	# if($value!~m/^\s*$/is)
	# {
		# eval{
		# $value=xl_parse_date($value);
		# };
	# }
	# print "$value\n";
	if ($value=~m/^(\d+)\s*(?:[\-\/\.])\s*(\d+)\s*(?:[\-\/\.])\s*(\d+)$/)
	{
		my $v1=$1;
		my $v2=$2;
		my $v3=$3;
		$v1='0'.$v1 if(length($v1)==1);
		$v2='0'.$v2 if(length($v2)==1);
		$v3='0'.$v3 if(length($v3)==1);
		$value=$v1.'/'.$v2.'/'.$v3;
	}
	# print "V1: $value\n";
	if($value=~m/(\bjanuary\b|\bfebruary\b|\bmarch\b|\bapril\b|\bmay\b|\bjune\b|\bjuly\b|\baugust\b|\bseptember\b|\boctober\b|\bnovember\b|\bdecember\b|\bjan\b|\bfeb\b|\bmar\b|\bapr\b|\bmay\b|\bjun\b|\bjul\b|\baug\b|\bsep\b|\boct\b|\bnov\b|\bdec\b)/is)
	{
		my $txt=lc($1);
		my $month_t=$Months1{$txt};
		if($month_t=~m/^\s*$/is)
		{
			$month_t=$Months2{$txt};
		}
		$value=~s/$txt/$month_t/igs;
		$value=~s/\./\//igs;
		$value=='' if($value!~m/\d/is);
		print "IF1: $value";
	}
	# print "V2: $value\n";
	if($value=~m/^\s*([\d]{1,2})\s*(?:\/|\.|\-)\s*([\d]{4})\s*$/)
	{
		$day='01';
		$month=$1;
		$year=$2;
		$value = "$day/$month/$year"; #Start Date
		$month--;
		my @time_data = (0,0,0,$day,$month,$year,0,0,0);
		$time_data[4]++;   # Next month.
		$time_data[3] = 0; # Last day of previous month.

		@time_data = localtime(timelocal_nocheck(@time_data));

		my $year     = $time_data[5]+1900;
		my $month    = $time_data[4]+1;    # Make 1-based
		my $last_day = $time_data[3];
		$last_day = "0$last_day" if (length($last_day) == 1);
		$month = "0$month" if (length($month) == 1);
		$value2 = "$last_day/$month/$year";#End Date
		print "IF2: $value";
		print "IF2: $value2";
	}
	# print "V3: $value\n";
	if($value=~m/^\s*([\d]{1,2})\s*(?:\/|\.|\-)\s*([\d]{2})\s*$/)
	{
		$day='01';
		$month=$1;
		$year='20'.$2;
		$value = "$day/$month/$year"; #Start Date
		$month--;
		my @time_data = (0,0,0,$day,$month,$year,0,0,0);
		$time_data[4]++;   # Next month.
		$time_data[3] = 0; # Last day of previous month.

		@time_data = localtime(timelocal_nocheck(@time_data));

		my $year     = $time_data[5]+1900;
		my $month    = $time_data[4]+1;    # Make 1-based
		my $last_day = $time_data[3];
		$last_day = "0$last_day" if (length($last_day) == 1);
		$month = "0$month" if (length($month) == 1);
		$value2 = "$last_day/$month/$year";#End Date
		print "IF 10: $value";
		print "IF 10: $value2";
	}
	# print "V4: $value\n";
	# if($value=~m/^\s*(\d{5})\s*$/)
	# {
		# my ($iSec, $iMin, $iHour, $iDay, $iMon, $iYear, $iwDay, $iMSec) = ExcelLocaltime($value);
		# $iYear += 1900;
		# $iMon  +=1;
		
		# $iDay = "0$iDay" if (length($iDay) == 1);
		# $iMon = "0$iMon" if (length($iMon) == 1);
		# $value = "$iDay/$iMon/$iYear";
		# print "1: $value\n";
	# }	
	if($value=~m/^\s*([\d]{5})\s*$/)
	{
		if($value>=40000)
		{
			$value=&BIP_DB::dt_format($dbh,$value);
			print "IF3: $value\t";
		}
	}
	if($value=~m/^\s*([\d]{6,8})\s*$/)
	{
		if($value=~m/^([\d]{4})/is)
		{
			my $Year_Check=$1;
			if($Year_Check>2012)
			{
				$value = strftime("%d/%m/%Y", localtime(str2time($value, 'GMT')));				
				print "IF4: $value\t";
			}	
		}		
		# if($value=~m/^\s*([\d]{2})([\d]{2})([\d]{4})\s*$/is)
		# {
			# $value = "$1/$2/$3";
			# print "IF4: $value\t";
		# }	
	}
	# elsif(length($value)==8 and ($value =~m/^\s*([\d]+)\s*$/))
	# {
		# if($value=~m/([\d]){4}([\d]){2}([\d]){2}/is)
		# {
			# $year = $1;
			# $month =$2;
			# $day  = $3;

			# $day = "0$day" if (length($day) == 1);
			# $month = $MonthHash{$month};
			# if ((length($year) == 2) && ($year le $Cencheck) )
			# {
				# $year = $Cen.$year;
			# }
			# elsif (length($year) == 2)
			# {
				# $year = $Cen1.$year;
			# }
			# $value = "$day/$month/$year";
			# print "IF4: $value";
		# }	
	# }
	# elsif ($value=~ m/^(\d+)\s*(?:[\-\/\.])\s*(\d+)\s*(?:[\-\/\.])\s*(\d+)$/ )
	elsif ($value=~ m/^((?:19|20)\d\d)[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$/igs)
	{
		$year = $1;
		$month = $2;
		$day  = $3;
		# if ((length($year) == 2) && ($year le $Cencheck) )
		# {
			# $year = $Cen.$year;
		# }
		# elsif (length($year) == 2)
		# {
			# $year = $Cen1.$year;
		# }
		$value = "$day/$month/$year";
		print "IF5: $value\t";
	}
	elsif($value=~ m/^(0[1-9]|[12][0-9]|3[01])[-\/.](0[1-9]|1[012])[- \/.]((?:19|20)\d\d)$/)
	{
		$day  = $1;
		$month = $2;
		$year = $3;
		# if ((length($year) == 2) && ($year le $Cencheck) )
		# {
			# $year = $Cen.$year;
		# }
		# elsif (length($year) == 2)
		# {
			# $year = $Cen1.$year;
		# }
		$value = "$day/$month/$year";
		print "IF6: $value\t";
	}
	elsif($value=~ m/^(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[-\/.]((?:19|20)\d\d)$/)
	{
		$month = $1;
		$day  = $2;
		$year = $3;
		# if ((length($year) == 2) && ($year le $Cencheck) )
		# {
			# $year = $Cen.$year;
		# }
		# elsif (length($year) == 2)
		# {
			# $year = $Cen1.$year;
		# }
		$value = "$day/$month/$year";
		print "IF7: $value\t";
	}
	elsif ($value =~ m/^(\d+)\s*(?:[\-\/\.])\s*(\d+)\s*(?:[\-\/\.])\s*((?:\d+)\s*(?:[0-9\:\.]+))/)
	{
		if (length($1) == 4)
		{
			$day  = $3;
			$month = $2;
			$year = $1;
		}
		else
		{
			$day  = $1;
			$month = $2;
			$year = $3;
		}

		$day = "0$day" if (length($day) == 1);
		$month = "0$month" if (length($month) == 1);
		if ((length($year) == 2) && ($year le $Cencheck) )
		{
			$year = $Cen.$year;
		}
		elsif (length($year) == 2)
		{
			$year = $Cen1.$year;
		}

		$value = "$day/$month/$year";
		print "IF8: $value\t";
	}
	elsif ( $value =~ m/^(\d+)\/?\-?\s*([a-zA-Z]+)\/?\-?\s*(\d+)/ )
	{
		$day  = $1;
		$month = uc($2);
		$year = $3;

		$day = "0$day" if (length($day) == 1);
		$month = $MonthHash{$month};
		if ((length($year) == 2) && ($year le $Cencheck) )
		{
			$year = $Cen.$year;
		}
		elsif (length($year) == 2)
		{
			$year = $Cen1.$year;
		}
		$value = "$day/$month/$year";
		print "IF9: $value";
	}
	elsif($value=~m/^\s*(0*[1-9]|[12][0-9]|3[01])\s*[-\/. ]\s*(0*[1-9]|1[012])\s*[-\/. ]\s*((?:19|20)\d\d)\s*$/is) # DD/MM/YYYY - Format;
	{
		$day=$1;
		$month=$2;
		$year=$3;
		$value = "$day/$month/$year";
		print "IF10: $value\t";
	}
	elsif($value=~m/^\s*((?:19|20)\d\d)\s*[-\/. ]\s*(0*[1-9]|1[012])\s*[-\/. ]\s*(0*[1-9]|[12][0-9]|3[01])\s*$/is) # YYYY/MM/DD - Format
	{
		$year=$1;
		$month=$2;
		$day=$3;
		$value = "$day/$month/$year";
		print "IF11: $value\t";
	}
	elsif($value=~m/^\s*(0*[1-9]|[12][0-9]|3[01])\s*[-\/. ]\s*([a-z]+?)\s*[-\/. ]\s*((?:19|20)\d\d)\s*$/is) # YYYY/MM as Text/DD - Format
	{
		$day=$1;
		$month=lc($2);
		$year=$3;
		my $mon=$Months1{$month};
		if($mon eq '')
		{
			$mon=$Months2{$month};
		}
		$value = "$day/$mon/$year";
		print "IF12: $value\t";
	}	

	
	$value=~s/\./\//igs;
	my @match_month = grep { $Month_Of_Document =~/$_/is } @lower_months;
	my ($doc_mon,$doc_mon_str);
	$doc_mon_str = $match_month[0] if($match_month[0]);
	$doc_mon = $Months1{$doc_mon_str};
	unless(@match_month)
	{
		if($Month_Of_Document =~m/month\s*\b(\d{1,2})\b/is)
		{
			my $mon = $1;
			$mon='0'.$mon if(length($mon) == 1);
			$doc_mon = $mon if($mon <= 12);
		}
	}
	if($value =~ m/(\d+)\/(\d+)\/(\d+)/igs)
	{
		my $day = $1;
		my $month = $2;
		my $year = $3;
		if($month eq $doc_mon)
		{
			$value = $value;
		}
		elsif($day eq $doc_mon)
		{
			$value = $month.'/'.$day.'/'.$year;
		}
	}
	return ($value,$value2);
}
