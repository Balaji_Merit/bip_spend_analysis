#!/usr/bin/perl -w

## Imports
use strict;
use LWP::UserAgent;
use HTTP::Cookies;
use JSON;
use MIME::Lite;
use MIME::Base64;
# use Authen::SASL;
# my $err_stmt;
# my $err_warn;
# $SIG{__DIE__}  = \&die_handler;
# $SIG{__WARN__} = \&warn_handler;
our $Buyer_ID1;
require BIP_DB;

# my %Months=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");
my %Months=("Jan" => "0", "Feb" => "1", "Mar" => "2", "Apr" => "3", "May" => "4", "Jun" => "5", "Jul" => "6", "Aug" => "7", "Sep" => "8", "Oct" => "9", "Nov" => "10", "Dec" => "11");
my %End_Months=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");

my $Sleep_Seconds=180;

# Get Month(MM) and year(YYYY)
my @Current_Date=split(" ",localtime(time));
my $Current_Month_txt=$Current_Date[1];
my $Current_Year=$Current_Date[4];
my $Current_Date=$Current_Date[2];
my $Current_Month=$Months{$Current_Month_txt};

# print "Current_Month_txt-->$Current_Month_txt\n";
# print "Current_Year-->$Current_Year\n";
# print "Current_Date-->$Current_Date\n";
# print "Current_Month-->$Current_Month\n";

## User agent setup
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Merit BI OUToader");
# $ua->timeout(50);
push @{ $ua->requests_redirectable }, 'POST';

my $cookiefile = $0;
$cookiefile =~s/\.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1);
$ua->cookie_jar($cookie);
my $Input_Table='BUYERS_DOWNLOAD_DETAILS';
############Database Initialization########
my $dbh = &BIP_DB::DbConnection();

###########################################
# open(AL,">>Upload_Selected_Files_Details.txt");
# print AL "Input_Buyer_ID\tBatch_ID\tFileName\tFile_Location\tImport_BITOOL_Status\tMonth_Of_Doc\tLocal_time\n";
# close AL;	

my ($Input_Buyer_ID,$Batch_ID,$FileName,$Filestorepath,$Import_BITOOL,$Month_Of_Doc) = &BIP_DB::Retrieve_Buyers_Download_Details_For_Import($dbh,$Input_Table);
my @Input_Buyer_ID		= @$Input_Buyer_ID;
my @Batch_ID			= @$Batch_ID;
my @FileName			= @$FileName;
my @Filestorepath		= @$Filestorepath;
my @Import_BITOOL		= @$Import_BITOOL;
my @Month_Of_Doc		= @$Month_Of_Doc;
for(my $i=0;$i<@Input_Buyer_ID;$i++)
{
	my $Input_Buyer_ID=$Input_Buyer_ID[$i];
	my $Batch_ID=$Batch_ID[$i];
	my $FileName=$FileName[$i];
	my $File_Location=$Filestorepath[$i];
	my $Import_BITOOL_Status=$Import_BITOOL[$i];
	my $Month_Of_Doc=$Month_Of_Doc[$i];
	print "Buyer_ID: $Input_Buyer_ID\n";
	print "Batch_ID: $Batch_ID\n";
	print "FileName: $FileName\n";
	print "Location: $File_Location\n";
	print "BITOOL_Status: $Import_BITOOL_Status\n";
	print "Month_Of_Doc: $Month_Of_Doc\n";

	my $Processing_Time;
	my ($Month_Of_Document) = &BIP_DB::Retrieve_Month_Document($dbh,$Input_Buyer_ID,$Batch_ID,"SUPPLIER_SOURCE_DATA");
	my ($Buyer_Name,$Upload_Buyer_Name) = &BIP_DB::Retrieve_Buyer_Name($dbh,$Input_Buyer_ID,"Buyers");
	print "$Batch_ID:=> Month_Of_Document: $Month_Of_Document\n";
	print "$Batch_ID:=> Buyer_Name: $Upload_Buyer_Name\n";
	if($Upload_Buyer_Name ne '')
	{
		$Buyer_Name=$Upload_Buyer_Name;
	}
	my $File_Year=$Month_Of_Document;
	$File_Year=~s/[^>]*?\s*([\d]+)\s*[^>]*?$/$1/igs;
	$FileName=~s/\_\_+/\_/igs;
	$FileName=~s/\.xlsx?$|\.xml$|\.ods$/\.csv/igs;	
	print "MOD: $File_Year\n";

	my $Upload_Filename=$File_Location."/Export_".$Input_Buyer_ID."_".$FileName;
	my ($Start_Mon_Year,$End_Mon_Year);
	if($Month_Of_Doc=~m/^\s*([^>]*?)\s+([^>]*?)\s*$/is)
	{
		$Start_Mon_Year=$1;
		$End_Mon_Year=$2;
	}
	else
	{
		$Start_Mon_Year=$Month_Of_Doc;
		# $End_Mon_Year=$2;
	}
	my $L_time=localtime();
	open(AL,">>Upload_Selected_Files_Details.txt");
	print AL "$Input_Buyer_ID\t$Batch_ID\t$FileName\t$File_Location\t$Import_BITOOL_Status\t$Month_Of_Doc\t$L_time\n";
	close AL;	
	my ($Start_Month,$Start_Year,$End_Month,$End_Year);
	if($Start_Mon_Year eq '')
	{
		my $Warning_Msg='Check File Month and Year';
		print "\n$Warning_Msg\n";
		if($Import_BITOOL_Status eq 'N')
		{
			$Import_BITOOL_Status=1;
			&send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
		}	
		# elsif($Import_BITOOL_Status == 1)
		# {
			# $Import_BITOOL_Status=2;
			# &send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
		# }	
		my $Uploaded_Reference_Number='';
		&Status_Update($dbh,$Input_Buyer_ID,$Batch_ID,$Import_BITOOL_Status,$Warning_Msg,$Uploaded_Reference_Number);	
		exit;
	}
	open(AL,">>Upload_Selected_Files_Details.txt");
	print AL "Step 2\n";
	close AL;	
	if($Start_Mon_Year=~m/\s*([a-z]+)\s*(?:-|\s+)\s*([\d]+)\s*/is)
	{
		$Start_Month=$1;
		$Start_Year=$2;
		print "$Start_Month\n";
	}
	if($End_Mon_Year ne '')
	{
		if($End_Mon_Year=~m/\s*([a-z]+)\s*(?:-|\s+)\s*([\d]+)\s*/is)
		{
			$End_Month=$1;
			$End_Year=$2;
		}
	}
	else
	{
		$End_Month=$Start_Month;
		$End_Year=$Start_Year;
	}	
	my $End_Month_Txt=$End_Month;
	my $in = lc($End_Month);
	$End_Month = ucfirst($in);
	my $End_Month_last_File_taken=$End_Months{$End_Month}; # This is only to update last file taken details. Here Jan=>01, Feb=>02 like wise.
	my $intmp = lc($Start_Month);
	$Start_Month = ucfirst($intmp);
	$Start_Month=$Months{$Start_Month};
	$End_Month=$Months{$End_Month};
	# print "$Batch_ID_List\n";
	print "SM: $Start_Month\n";
	print "SY: $Start_Year\n";
	print "EM: $End_Month\n";
	print "EY: $End_Year\n";
	print "Last File: $End_Month_last_File_taken\n";
	open(AL,">>Upload_Selected_Files_Details.txt");
	print AL "Step 3\n";
	close AL;	
	my ($Upload_Status,$Warning_Msg,$Uploaded_Reference_Number);
	if (($Start_Month eq '') or ($End_Month eq '') )
	{
		$Warning_Msg='Check Month Of Document';
		$Uploaded_Reference_Number='';
		if($Import_BITOOL_Status eq 'N')
		{
			$Import_BITOOL_Status=1;
			&send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
		}	
		# elsif($Import_BITOOL_Status == 1)
		# {
			# $Import_BITOOL_Status=2;
			# &send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
		# }	
		print "$Warning_Msg\n";
		&Status_Update($dbh,$Input_Buyer_ID,$Batch_ID,$Import_BITOOL_Status,$Warning_Msg,$Uploaded_Reference_Number);
		exit;
	} 
	open(AL,">>Upload_Selected_Files_Details.txt");
	print AL "Step 4\n";
	close AL;	

	print "Location:: $Upload_Filename\n";
	if (! -e $Upload_Filename) {
		$Warning_Msg='File Does Not Exists';
		$Uploaded_Reference_Number='';
		if($Import_BITOOL_Status eq 'N')
		{
			$Import_BITOOL_Status=1;
			&send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
		}	
		# elsif($Import_BITOOL_Status == 1)
		# {
			# $Import_BITOOL_Status=2;
			# &send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
		# }	
		print "$Warning_Msg\n";
		&Status_Update($dbh,$Input_Buyer_ID,$Batch_ID,$Import_BITOOL_Status,$Warning_Msg,$Uploaded_Reference_Number);
		exit;
	} 
	open(AL,">>Upload_Selected_Files_Details.txt");
	print AL "Step 5\n";
	close AL;	

	## Login
	my $url='http://62.253.177.4:8080/businessIntelligence/j_security_check';

	my $response=$ua->post(
		$url,
		[
			# 'j_username' => 'gladys.christopher@meritgroup.co.uk',
			# 'j_password' => 'zOl3BoXy'
			'j_username' => 'arul.kumaran@meritgroup.co.uk',
			'j_password' => 'tomcat'
		]
	);

	if ($response->is_success) {
		if (!$response->request()->uri() =~ /http:\/\/62.253.177.4:8080\/businessIntelligence\/businessIntelligence.html/) {
			die "Login Failed";
		}
		else{
			print "LoggedIn\n";
		}
	}
	open(AL,">>Upload_Selected_Files_Details.txt");
	print AL "Step 6\n";
	close AL;	

	## Login is successful, cookies in the cookie jar.

	## Initialize the OUToad process
	$url='http://62.253.177.4:8080/businessIntelligence/data/loader/dataImport.html';
	$response = $ua->get($url);
	open(AL,">>Upload_Selected_Files_Details.txt");
	print AL "Step 7\n";
	close AL;	

	## Load the existing buyer list. Currently unused, but could be used to check if the appropriate flag
	## should be set
	$url='http://62.253.177.4:8080/businessIntelligence/buyer/data.json?action=findAllSpendBuyers&_=1432644269711';
	$response = $ua->get($url);
	my $buyer_json=$response->content;
	# my $buyerStructure=decode_json($buyer_json);
	open(AL,">>Upload_Selected_Files_Details.txt");
	print AL "Step 8\n";
	close AL;	


	## OUToad the file.

	my $re2=1;
	my $OUToadUrl='http://62.253.177.4:8080/businessIntelligence/data/loader/dataImport.html';

	Re_ping1:
	$response=$ua->post(
						$OUToadUrl,
						Content_Type => 'form-data',
						Content =>[
							'OUToad_form' => 'true',
							'buyerName' => $Buyer_Name,
							'fileType' => 'CSV',
							'data_file' => [
											"$Upload_Filename" => $Upload_Filename,
											"Content_Type" => 'application/octetstream'
										],
							'batchOUToad.batchName' => '',
							'_target1' => 'proceed'
						]
	);
	open(AL,">>Upload_Selected_Files_Details.txt");
	print AL "Step 9\n";
	close AL;	

	## At this stage, warnings will be presented if any exist. They are presented in Divs with a class of 'warning'
	## This should be handled in some fashion.
	my $Import_Reference_code=$response->code;	
	my $Import_status_line=$response->status_line;	
	my $Upload_Content=$response->content;
	if($Import_Reference_code=~m/50/igs)
	{
		print "Code: $Import_status_line\n";
		# if($re2<=3)
		# {
			# $re2++;
			# goto Re_ping1;
		# }
		# else		
		# {
			print "\n---------- IMPORT FAILED ----------\n";
			$Warning_Msg="IMPORT FAILED - $Import_status_line";
			$Uploaded_Reference_Number=0;
			if($Import_BITOOL_Status eq 'N')
			{
				$Import_BITOOL_Status=1;
				&send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
			}	
			# elsif($Import_BITOOL_Status == 1)
			# {
				# $Import_BITOOL_Status=2;
				# &send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
			# }	
			print "\n$Warning_Msg\n";
			&Status_Update($dbh,$Input_Buyer_ID,$Batch_ID,$Import_BITOOL_Status,$Warning_Msg,$Uploaded_Reference_Number);
			exit;
		# }
	}
	open(OUT,">Upload_Content.html");
	print OUT $Upload_Content;
	close OUT;
	print "Upload_Content\n";
	
	open(AL,">>Upload_Selected_Files_Details.txt");
	print AL "Step 10\n";
	close AL;	

	## Map appropriately

	print "MOD:  $Start_Month-$Start_Year $End_Month-$End_Year\n";
	my $mapUrl='http://62.253.177.4:8080/businessIntelligence/data/loader/dataImport.html';
	my $re3=0;
	my $Buyer_Name_no_Space=$Buyer_Name;
	# $Buyer_Name_no_Space=~s/\s+/\+/igs;
	# print "No Space: $Buyer_Name_no_Space\n";
	Reping3:
	$response=$ua->post(
						$mapUrl,
						Content_Type => 'form-data',
						Content =>[
							'_target2' => 'proceed',
							'_update_mappings' => 'true',
							'processedEndMonth' => $End_Month,
							'processedEndYear' => $End_Year,
							'processedStartMonth' => $Start_Month,
							'processedStartYear' => $Start_Year,
							'property.amount.column' => 'Amount',
							'property.transactionId.column' => 'Transaction_ID',
							'property.buyerName.defaultValue.dataType' => 'STRING',
							'property.buyerName.defaultValue' => $Buyer_Name,
							'property.requirement.column'=>'Requirement',
							'property.startDate.column'=>'Start_Date',
							'property.startDate.defaultValue.dataType'=>'DATE',
							'property.startDate.defaultValue'=>'',
							'property.endDate.column'=>'End_Date',
							'property.endDate.defaultValue.dataType'=>'DATE',
							'property.endDate.defaultValue'=>'',
							'property.supplierName.column'=>'Supplier_Name',
							'property.departmentName.column'=>'Department_Name',
							'property.departmentName.defaultValue.dataType'=>'STRING',
							'property.departmentName.defaultValue'=>'Unknown',
							'property.requirementLevel2.column'=>'Requirement_2'
						]
	);
	my $Mapping_code=$response->code;
	my $Mapping_status_line=$response->status_line;	
	my $OUToad_Content=$response->content;
	open(AL,">>Upload_Selected_Files_Details.txt");
	print AL "Step 11\n";
	close AL;	
	if($Mapping_code=~m/50/igs)
	{
		print "Code: $Mapping_status_line\n";
		# if($re3<=3)
		# {
			# $re3++;
			# goto Reping3;
		# }	
		# else		
		# {
			print "\n---------- $Mapping_code ----------\n";
			$Warning_Msg="DATA LOADER FAILED - $Mapping_status_line";
			$Uploaded_Reference_Number=0;
			if($Import_BITOOL_Status eq 'N')
			{
				$Import_BITOOL_Status=1;
				&send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
			}	
			# elsif($Import_BITOOL_Status == 1)
			# {
				# $Import_BITOOL_Status=2;
				# &send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
			# }	
			print "\n$Warning_Msg\n";
			&Status_Update($dbh,$Input_Buyer_ID,$Batch_ID,$Import_BITOOL_Status,$Warning_Msg,$Uploaded_Reference_Number);
			exit;
		# }
	}
	$Processing_Time=localtime();
	open(OUT,">OUToad_Responce1.html");
	print OUT $OUToad_Content;
	close OUT;
	open(AL,">>Upload_Selected_Files_Details.txt");
	print AL "Step 12\n";
	close AL;	
	
	if($OUToad_Content=~m/<div\s*class\s*=\s*\"\s*warning\s*\"\s*>\s*([\w\W]*?)\s*<\/div>/is)
	{
		$Warning_Msg=$1;
		$Warning_Msg=~s/<[^>]*?>/ /igs;
		$Warning_Msg=~s/\&nbsp\;/ /igs;
		$Warning_Msg=~s/\&pound\;/\�/igs;
		$Warning_Msg=~s/\s\s+/ /igs;
		print "Warning:\n--------\n$Warning_Msg\n--------\n";
		if($Warning_Msg=~m/java\.lang\./is)
		{
			print "\nPLEASE CHECK THE UPLOAD\n";
			open(AL,">>Upload_Selected_Files_Details.txt");
			print AL "$Input_Buyer_ID\t$Batch_ID\t$FileName\t$File_Location\t$Import_BITOOL_Status\t$Month_Of_Doc\t$L_time\t$Warning_Msg\n";
			close AL;	
			$Import_BITOOL_Status=1;
			&Status_Update($dbh,$Input_Buyer_ID,$Batch_ID,$Import_BITOOL_Status,$Warning_Msg,$Uploaded_Reference_Number);
			$Warning_Msg=$Warning_Msg." Kindly check later. May be those upload displayed later.";
			&send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
		}
		elsif($Warning_Msg=~m/An\s*exception\s*occurred\s*while\s*processing\s*the\s*data/is)
		{
			print "\nPLEASE CHECK THE UPLOAD\n";
			open(AL,">>Upload_Selected_Files_Details.txt");
			print AL "$Input_Buyer_ID\t$Batch_ID\t$FileName\t$File_Location\t$Import_BITOOL_Status\t$Month_Of_Doc\t$L_time\t$Warning_Msg\n";
			close AL;	
			$Import_BITOOL_Status=1;
			&Status_Update($dbh,$Input_Buyer_ID,$Batch_ID,$Import_BITOOL_Status,$Warning_Msg,$Uploaded_Reference_Number);
			$Warning_Msg=$Warning_Msg." Kindly check later. May be those upload displayed later.";
			&send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
		}
		open(AL,">>Upload_Selected_Files_Details.txt");
		print AL "Step 13\n";
		close AL;	
		
	}
	## At this stage, warnings will be presented if any exist. They are presented in Divs with a class of 'warning'
	## This should be handled in some fashion. If they occur, the map step will be needed to be done again.

	my $re4=0;
	Reping4:
	my $finishUrl='http://62.253.177.4:8080/businessIntelligence/data/loader/dataImport.html';

	$response = $ua->post(
					$finishUrl,
					Content_Type => 'form-data',
					Content =>[
						'_finish'=>'continue'
					]
	);

	my $Uploaded_code=$response->code;
	my $Uploaded_status_line=$response->status_line;	
	my $OUToad_Content1=$response->content;
	open(OUT,">OUToad_Responce2.html");
	print OUT $OUToad_Content1;
	close OUT;
	open(AL,">>Upload_Selected_Files_Details.txt");
	print AL "Step 14\n";
	close AL;	
	## process complete. No notification is given on the results of the final page.
	if($Uploaded_code!=200)
	{
		print "\n------ UPLOAD FINISHING URL FAILED -------\n";
		$Warning_Msg='Finishing URL - '.$Uploaded_status_line;
		if($re4<=1)
		{
			$re4++;
			goto Reping4;
		}	
		else		
		{		
			if($Import_BITOOL_Status eq 'N')
			{
				$Import_BITOOL_Status=1;
				&send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
			}	
			# elsif($Import_BITOOL_Status == 1)
			# {
				# $Import_BITOOL_Status=2;
				# &send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
			# }	
			$Uploaded_Reference_Number='';
			&Status_Update($dbh,$Input_Buyer_ID,$Batch_ID,$Import_BITOOL_Status,$Warning_Msg,$Uploaded_Reference_Number);
			exit;
		}	
	}
	open(AL,">>Upload_Selected_Files_Details.txt");
	print AL "Step 15\n";
	close AL;		
	print "\n-------------------------------------------------------\n";
	print "Please wait for $Sleep_Seconds Seconds... Dont Colse it Please....\n";
	print "-------------------------------------------------------\n";
	sleep($Sleep_Seconds);
	my $re1=1;
	my $Batch_OUToads_Url='http://62.253.177.4:8080/businessIntelligence/spend/uploads.html';
	Reping:
	$response = $ua->get($Batch_OUToads_Url);
	my $Uploaded_Reference_code=$response->code;	
	open(AL,">>Upload_Selected_Files_Details.txt");
	print AL "Step 16\n";
	close AL;	
	if($Uploaded_Reference_code!~m/20/igs)
	{
		print "Code: $Uploaded_Reference_code\n";
		if($re1<=3)
		{
			$re1++;
			goto Reping;
		}	
		else		
		{
			print "\n----------UNABLE TO GET REFERENCE----------\n";
		}
		
	}
	my $Batch_OUToads_Content=$response->content;
	open(OUT,">Batch_OUToads_Content.html");
	print OUT $Batch_OUToads_Content;
	close OUT;
	open(AL,">>Upload_Selected_Files_Details.txt");
	print AL "Step 17\n";
	close AL;		
	
	my $Uploaded_Time;
	if($Batch_OUToads_Content=~m/>\s*ID\s*\:\s*<a\s*href\s*=\s*\"[^>]*?\"\s*>\s*([^>]*?)\s*<\/a>\s*<br\/>\s*Created\s*\:\s*$Current_Year-$Current_Month-0?$Current_Date\s*([^>]*?)\s*<br\/>\s*Uploader\s*\:\s*arul\.kumaran\@meritgroup\.co\.uk/is)
	{
		$Uploaded_Reference_Number=$1;
		$Uploaded_Time=$2;
		my $query = "select Upload_Reference from BUYERS_DOWNLOAD_DETAILS where Upload_Reference=\'$Uploaded_Reference_Number\'";
		print "\n$query\n";
		my $sth = $dbh->prepare($query);
		$sth->execute();
		my (@Upload_Reference_tmp);
		my $reccount;
		while(my @record = $sth->fetchrow)
		{
			push(@Upload_Reference_tmp,&Trim($record[0]));
		}
		$sth->finish();		
		open(AL,">>Upload_Selected_Files_Details.txt");
		print AL "Step 18\n";
		close AL;		
		
		print "------------------\nUpload_Reference_tmp\n--------------------\n";
		if(@Upload_Reference_tmp == 0)
		{
			$Upload_Status='Uploaded';
			$Import_BITOOL_Status='Y';
			my $Last_File="$End_Month_last_File_taken-$End_Year";
			print "Last file Update: $Last_File\n";
			print "-----------------------------------------------------\n";
			print "Uploaded Reference Number: $Uploaded_Reference_Number\n";
			print "-----------------------------------------------------\n";
			open(AL,">>Upload_Selected_Files_Details.txt");
			print AL "Step 19\n";
			close AL;		
			my $Uploaded_By_Automation='Y';
			my $FileTakenDate=&BIP_DB::dt_format($dbh,$Last_File);
			print "File Taken Date: $FileTakenDate\n";
			&Status_Update($dbh,$Input_Buyer_ID,$Batch_ID,$Import_BITOOL_Status,$Warning_Msg,$Uploaded_Reference_Number,$FileTakenDate,$Uploaded_Time,$Uploaded_By_Automation);
			&Update_Last_File_Taken($dbh,$Input_Buyer_ID);
			my $Rename_Uploaded_File=$File_Location."/".$Uploaded_Reference_Number."_Export_".$Input_Buyer_ID."_".$FileName;  
			my $Rename_Status=rename "$Upload_Filename","$Rename_Uploaded_File";
			if($Rename_Status==0)
			{
				$Warning_Msg='Rename Failed '.$Rename_Uploaded_File;
				&send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
			}
		}
		else
		{
			$Warning_Msg='Unable To Get Reference';
			open(AL,">>Upload_Selected_Files_Details.txt");
			print AL "Step 20\n";
			close AL;		
			&send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
			if($Import_BITOOL_Status eq 'N')
			{
				$Import_BITOOL_Status=1;
				&send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
			}	
			# elsif($Import_BITOOL_Status == 1)
			# {
				# $Import_BITOOL_Status=2;
				# &send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
			# }	
			$Uploaded_Reference_Number='';
			&Status_Update($dbh,$Input_Buyer_ID,$Batch_ID,$Import_BITOOL_Status,$Warning_Msg,$Uploaded_Reference_Number);
			exit;
		}
		open(AL,">>Upload_Selected_Files_Details.txt");
		print AL "Step 21\n";
		close AL;		
		
	}
	elsif($Batch_OUToads_Content=~m/>\s*ID\s*\:\s*<a\s*href\s*=\s*\"[^>]*?\"\s*>\s*([^>]*?)\s*<\/a>\s*<br\/>\s*Created\:\s*[\d]+\-[\d]+\-[\d]+\s+([^>]*?)\s*<[^>]*?>Uploader\s*\:\s*arul\.kumaran\@meritgroup\.co\.uk/is)
	{
		$Uploaded_Reference_Number=$1;
		$Uploaded_Time=$2;
		my $query = "select Upload_Reference from BUYERS_DOWNLOAD_DETAILS where Upload_Reference=\'$Uploaded_Reference_Number\'";
		print "\n$query\n";
		my $sth = $dbh->prepare($query);
		$sth->execute();
		my (@Upload_Reference_tmp);
		my $reccount;
		while(my @record = $sth->fetchrow)
		{
			push(@Upload_Reference_tmp,&Trim($record[0]));
		}
		$sth->finish();		
		open(AL,">>Upload_Selected_Files_Details.txt");
		print AL "Step 22\n";
		close AL;		
		
		print "------------------\nUpload_Reference_tmp\n--------------------\n";
		if(@Upload_Reference_tmp == 0)
		{
			$Upload_Status='Uploaded';
			$Import_BITOOL_Status='Y';
			my $Last_File="$End_Month_last_File_taken-$End_Year";
			print "-----------------------------------------------------\n";
			print "Uploaded Reference Number: $Uploaded_Reference_Number\n";
			print "-----------------------------------------------------\n";
			open(AL,">>Upload_Selected_Files_Details.txt");
			print AL "Step 23\n";
			close AL;	
			my $Uploaded_By_Automation='Y';			
			my $FileTakenDate=&BIP_DB::dt_format($dbh,$Last_File);
			&Status_Update($dbh,$Input_Buyer_ID,$Batch_ID,$Import_BITOOL_Status,$Warning_Msg,$Uploaded_Reference_Number,$FileTakenDate,$Uploaded_Time,$Uploaded_By_Automation);
			&Update_Last_File_Taken($dbh,$Input_Buyer_ID);
			my $Rename_Uploaded_File=$File_Location."/".$Uploaded_Reference_Number."_Export_".$Input_Buyer_ID."_".$FileName;  
			my $Rename_Status=rename "$Upload_Filename","$Rename_Uploaded_File";
			if($Rename_Status==0)
			{
				$Warning_Msg='Rename Failed '.$Rename_Uploaded_File;
				&send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
			}
		}
		else
		{
			$Warning_Msg='Unable To Get Reference';
			open(AL,">>Upload_Selected_Files_Details.txt");
			print AL "Step 24\n";
			close AL;		
			&send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
			if($Import_BITOOL_Status eq 'N')
			{
				$Import_BITOOL_Status=1;
				&send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
			}	
			# elsif($Import_BITOOL_Status == 1)
			# {
				# $Import_BITOOL_Status=2;
				# &send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
			# }	
			$Uploaded_Reference_Number='';
			&Status_Update($dbh,$Input_Buyer_ID,$Batch_ID,$Import_BITOOL_Status,$Warning_Msg,$Uploaded_Reference_Number);
			exit;
		}
		open(AL,">>Upload_Selected_Files_Details.txt");
		print AL "Step 25\n";
		close AL;		
	}
	else
	{
		$Warning_Msg='Regex not Matched - Unable To Get Reference';
		open(AL,">>Upload_Selected_Files_Details.txt");
		print AL "Step 26\n";
		close AL;		
		open(OUT,">Regex_Not_Matched.html");
		print OUT $Batch_OUToads_Content;
		close OUT;		
		&send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
		if($Import_BITOOL_Status eq 'N')
		{
			$Import_BITOOL_Status=1;
			&send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
		}	
		# elsif($Import_BITOOL_Status == 1)
		# {
			# $Import_BITOOL_Status=2;
			# &send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);
		# }	
		$Uploaded_Reference_Number='';
		&Status_Update($dbh,$Input_Buyer_ID,$Batch_ID,$Import_BITOOL_Status,$Warning_Msg,$Uploaded_Reference_Number);
		exit;
	}
	
	# $Warning_Msg=~s/\'/\'\'/igs;
	# Update the status of the tool uploading
	# &Update_Last_File_Taken($dbh,$Input_Buyer_ID,$End_Month_Txt,$End_Year);
	open(AL,">>Upload_Selected_Files_Details.txt");
	print AL "Step 27\n";
	close AL;
	print "Completed\n";
}

sub Status_Update()
{
	my $dbh = shift;
	my $Input_Buyer_ID = shift;
	my $Batch_ID = shift;
	my $Import_BITOOL_Status = shift;
	my $Warning_Msg = shift;
	my $Uploaded_Reference_Number = shift;
	my $FileTakenDate = shift;
	my $Uploaded_Time = shift;
	my $Processing_Time=localtime();
	my $Uploaded_By_Automation=shift;

	$Warning_Msg =~s/\'/\'\'/igs;
	
	my $Try_Again=0;
	Try_Again:
	my $query;
	if($FileTakenDate ne '')
	{
		$query = "Update BUYERS_DOWNLOAD_DETAILS set Import_BITOOL=\'$Import_BITOOL_Status\', Tool_Warning=\'$Warning_Msg\', Upload_Reference=\'$Uploaded_Reference_Number\',FileTakenDate=\'$FileTakenDate\',EndDate=now(),Uploaded_By_Automation=\'$Uploaded_By_Automation\' where Buyer_ID=\'$Input_Buyer_ID\' and Batch_ID=\'$Batch_ID\'";
	}	
	else
	{
		$query = "Update BUYERS_DOWNLOAD_DETAILS set Import_BITOOL=\'$Import_BITOOL_Status\', Tool_Warning=\'$Warning_Msg\', Upload_Reference=\'$Uploaded_Reference_Number\' where Buyer_ID=\'$Input_Buyer_ID\' and Batch_ID=\'$Batch_ID\'";
	}	
	my $sth = $dbh->prepare($query);
	if($sth->execute())
	{
		print "Updated\n";
		$sth->finish();	
	}
	else
	{
		$dbh=&DbConnection();
		if($Try_Again==0)
		{
			$Try_Again=1;
			goto Try_Again;
		}
		else
		{
			$Warning_Msg='Update Failed Please see the following Query.......'.$query;
			&send_mail($Warning_Msg,$Input_Buyer_ID,$Batch_ID);		
			open(ERR,">>Upload_Failed_Query.txt");
			print ERR $query."\n";
			close ERR;
		}
	}
	open(DATA,">>Uploaded_Status.txt");
	print DATA "$Input_Buyer_ID\t$Batch_ID\t$Uploaded_Reference_Number\t$Processing_Time\t$Uploaded_Time\t$Warning_Msg\n";
	close DATA;
}
sub Update_Last_File_Taken()
{
	my $dbh = shift;
	my $Input_Buyer_ID = shift;
	my $Try_Again=0;
	my $Try_Again_To_Select=0;
	Try_Again_To_Select:
	my $Select_query = "select Concat(substring(Monthname(filetakendate),1,3),'-',year(FileTakenDate)) as Date from BUYERS_DOWNLOAD_DETAILS where Buyer_ID=\'$Input_Buyer_ID\' order by filetakendate desc limit 1";
	my $sth = $dbh->prepare($Select_query);
	$sth->execute();
	my ($Month_Of_LastFile);
	while(my @record = $sth->fetchrow)
	{
		$Month_Of_LastFile=&Trim($record[0]);
	}
	print "Last_File_Taken :=: $Month_Of_LastFile\n";
	$sth->finish();
	
	if($Month_Of_LastFile ne '')
	{
		Try_Again:
		my $query = "update spendanalysisreportfile set last_file_taken_merit=\'$Month_Of_LastFile\' where ID=\'$Input_Buyer_ID\'";
		my $sth = $dbh->prepare($query);
		if($sth->execute())
		{
			print "Updated\n";
			$sth->finish();	
		}
		else
		{
			$dbh=&DbConnection();
			if($Try_Again==0)
			{
				$Try_Again=1;
				goto Try_Again;
			}
			else
			{
				open(ERR,">>Last_File_Taken_Failed_Query.txt");
				print ERR $query."\n";
				close ERR;
			}	
		}
	}
	else
	{
		$dbh=&DbConnection();
		if($Try_Again_To_Select==0)
		{
			$Try_Again_To_Select=1;
			goto Try_Again_To_Select;
		}
		else
		{
			open(ERR,">>Last_File_Taken_Failed_Query.txt");
			print ERR $Select_query."\n";
			close ERR;
		}	
	}
}


sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "application/json;charset=UTF-8");
	$req->header("Host" => "62.253.177.4:8080");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$redir_url);
}
	
sub send_mail()
{
	my $error	 = shift;
	my $Buyer_ID = shift;
	my $Batch_ID = shift;
	
	my $subject="Upload Failed - $Buyer_ID - $Batch_ID";
	my $host ='74.80.234.196'; 
	my $from='autoemailsender@meritgroup.co.uk';
	my $user='meritgroup';
	my $to ='arul.kumaran@meritgroup.co.uk';
	my $pass='11meritgroup11';
	my $body = "Hi Arul, <br><br>\t\t$error<br><br>Regards<br>BIP TEAM";
	
	my $msg = MIME::Lite->new (
	  From => $from,
	  To => $to,
	  # Cc => $cc,
	  Subject => $subject,
	  Data => $body,
	  Type =>'text/html'
	) or die "Error creating multipart container: $!\n";
    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	$msg->send;
}

sub Trim()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}

# sub die_handler()
# {
	# my $err_stmt = shift;	
	# my $Buyer_ID = shift;
	# $err_stmt = $err_stmt . $_ foreach (@_);
	# print "Error	:: $err_stmt\n"; 
	# $err_stmt =~s/\n/ /igs;
	# if ($err_stmt=~m/Can\'t\s*locate\s*Authen\/SASL\//is)
	# {
		# next;
	# }
	# my $TYPE = 'Error';
	# &send_mail($err_stmt,$TYPE,$Buyer_ID);
	# print "Err: $err_stmt\n";
	# open FH,">>Error.txt";
	# print FH $err_stmt;
	# close FH;
# }
# sub warn_handler()
# {
	# my $err_warn = shift;	
	# my $Buyer_ID = shift;
	# $err_warn = $err_warn . $_ foreach (@_);
	# print "Error	:: $err_warn\n"; 
	# $err_warn =~s/\n/ /igs;
	# if ($err_warn=~m/Can\'t\s*locate\s*Authen\/SASL\//is)
	# {
		# next;
	# }
	# my $TYPE = 'Warning';
	# &send_mail($err_warn,$TYPE,$Buyer_ID);
	# print "Err: $err_warn\n";
	# open FH,">>Error.txt";
	# print FH "$err_warn\n";
	# close FH;
# }

