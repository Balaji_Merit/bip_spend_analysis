use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use BIP_DB_V1;
use BIP_Column_Map_V1;

my $Input_Buyer_ID = $0;
$Input_Buyer_ID =~ s/\.pl//igs;
$Input_Buyer_ID =$1 if($Input_Buyer_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

# my $ua=LWP::UserAgent->new(show_progress=>1);
my $ua=LWP::UserAgent->new(ssl_opts => {SSL_verify_mode => 'SSL_VERIFY_PEER',verify_hostname => 0 }, show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/\.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my %Months_To_Num=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");
my @Months1=("January","February","March","April","May","June","July","August","September","October","November","December","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");

############Database Initialization########
my $dbh = &BIP_DB_V1::DbConnection();
###########################################

my $Input_Table='BUYERS';
############ Retrieve Spent Data weblink from Buyers table ###########
my ($ID,$Buyer_Name,$Spent_Data_weblink) = &BIP_DB_V1::RetrieveUrl($dbh,$Input_Buyer_ID,$Input_Table);
my $Buyer_ID 			= @$ID[0];
my $Buyer_Name 			= @$Buyer_Name[0];
my $Buyer_Url 			= @$Spent_Data_weblink[0];
# $Buyer_Url 			="https://www.miltonkeynesccg.nhs.uk/modules/downloads/list_ajax.php";
# $Buyer_Url 			="http://www.miltonkeynesccg.nhs.uk/search_results/expenditure/";
# my $post_content = "doc_order=1&doc_disp=100&template=0&default_cnt=-1&catclause=0&key_search=&url_doc=157";
# my $referer 	 = "http://www.miltonkeynesccg.nhs.uk/documentlist/157/";
# my $host= "www.miltonkeynesccg.nhs.uk";
# my ($Buyer_content,$Status_Line)=&Postcontent($Buyer_Url,$post_content,$host,$referer); &BIP_Column_Map_V1::Link_Status_Update($dbh,$Buyer_ID,$Status_Line);
###############
print "Buyer_ID	: $Buyer_ID\n";	
print "Buyer_Name	: $Buyer_Name\n";	
print "Buyer_Url	: $Buyer_Url\n";	

my ($Buyer_content,$Status_Line)=&Getcontent($Buyer_Url);
# my ($Buyer_content,$Status_Line)=&Postcontent($Buyer_Url); &BIP_Column_Map_V1::Link_Status_Update($dbh,$Buyer_ID,$Status_Line);
# my ($Buyer_content,$Status_Line)=&
open(ts,">Buyer_content.html");
print ts "$Buyer_content";
close ts;

# $Buyer_Url 			="http://www.miltonkeynesccg.nhs.uk/modules/downloads/downloads_list.ajax.php";
# my $post_content = "doc_order=1&doc_disp=100&template=0&default_cnt=-1&catclause=0&key_search=&url_doc=157";
# my $referer = "http://www.miltonkeynesccg.nhs.uk/documentlist/157/";
# my $host= "www.miltonkeynesccg.nhs.uk";
# my ($Buyer_content,$Status_Line)=&Postcontent($Buyer_Url,$post_content,$host,$referer); &BIP_Column_Map_V1::Link_Status_Update($dbh,$Buyer_ID,$Status_Line);

my ($Processed_LinkInfo) = &BIP_DB_V1::LinkInfo_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
my @Processed_LinkInfo=@$Processed_LinkInfo;

# if($Buyer_content=~m/<h\d+>\s*Document\s*Pages\s*\(\d+\)\s*<\/h\d+>\s*<\/td>([\w\W]*?)\s*<\/table>/is)
# {
	# my $table_content = $1;
	# while($Buyer_content=~m/<a[^<]*?href\=\"([^<]*?)\"[^>]*?>\s*([^<]*?)\s*</igs)
    # while($Buyer_content=~m/<h3><a\s*href="([^>]*?)" class="list_title ">([^>]*?csv)<\/a><\/h3>/igs)
    # while($Buyer_content=~m/<h3><a\s*[^>]*?href="([^>]*?)"\s*class="list_title\s*">([^>]*?)<\/a><\/h3>/igs)
    # while($Buyer_content=~m/<td><strong><a href="([^>]*?)" class="search ">([^>]*?)<\/a><\/strong>/igs)
    while($Buyer_content=~m/<a[^>]*?href="([^>]*?)"[^>]*?>([^>]*?ver 25[^>]*?)<\/a>/igs)
	
	{
		my $Doc_Link=$1;	
		my $LinkInfo=$2;
		
		$LinkInfo=&clean($LinkInfo);	
		# print "LinkInfo	:: $LinkInfo\n";
		# print "Doc_Link	:: $Doc_Link\n";		
		if($Doc_Link!~m/^http/is)
		{	
			print "\nhi\n";
			my $u1=URI::URL->new($Doc_Link,$Buyer_Url);
			my $u2=$u1->abs;
			$Doc_Link=$u2;
			$Doc_Link=~s/\&amp\;/&/igs;
		}
		$LinkInfo=&clean($LinkInfo);	
		my ($File_Date,$File_Month,$File_Year,$Doc_Name,$Doc_Type);
		my $LinkInfo_Temp=$LinkInfo;
		print "\nhi: $LinkInfo\n";	
		my $Doc_Link_Temp=$Doc_Link;
		
		if($LinkInfo =~ m/\.([a-z]{3,4})\s*$/is)
		{
			$Doc_Type = $1;
		}
		
		# Get Title of the file name
		if($LinkInfo ne '') 
		{	
			$LinkInfo=~s/\s\s+/ /igs;
			print "LinkInfo	: $LinkInfo\n";
			
			if($LinkInfo ~~ @Processed_LinkInfo || $LinkInfo =~ m/^\s*$/)
			{
				print "File Already Processed\n";
				next;
			}
			else
			{			
				print "\nThis is new file to download\n";
			}
		}	
		print "Doc_Link	: $Doc_Link\n";
		print "\nLinkInfo_Temp***********	: $LinkInfo_Temp\n";	
		foreach my $monthss(@Months1)	
		{		
			if($LinkInfo_Temp=~m/$monthss/is)
			{
				$File_Month=$monthss;				
				if($LinkInfo_Temp=~m/(2\d{3})/is)
				{
					$File_Year=$1;
				}			
				else
				{
					while($LinkInfo_Temp=~m/(\d{2,4})/igs)
					{					
						$File_Year=$1;
					}
				}	
				last;
			}			
		}	
		if($File_Month eq '')
		{		
			if($LinkInfo=~m/\s+([a-z]*?)\s+([\d]*?)\.([a-z]{3,4})\s*$/is)
			{
				$File_Month = $1;		
				$File_Year  = $2;		
				$Doc_Type   = $3;		
			}
		}
		if($File_Month eq '')
		{		
			if($LinkInfo=~m/(M\d+)/is)
			{
				$File_Year=$1;
			}	
		}	
		my ($Doc_content,$status_line,$File_Type,$File_Type_cont)=&Getcontent($Doc_Link);
		print "File_Month		: $File_Month\n";
		print "File_Year		: $File_Year\n";
		print "File_Type		: $File_Type\n";
		my $Month_Of_Document = "$File_Month $File_Year";	
		if($File_Type =~ m/\.([a-z]{3,4})\s*\"*"$/is)
		{
			$Doc_Type = $1;
		}
		if($File_Type=~m/\.pdf/is)
		{
			next;
		}
		elsif($File_Type_cont=~m/\.pdf/is)
		{
			next;
		}
		my $File_Type = $Doc_Type if ($Doc_Type ne '');
		$Doc_Name=$LinkInfo;
		$Doc_Name=~s/\W/\_/igs;
		$Doc_Name=~s/_+/\_/igs;
		$Doc_Name.='.'.$File_Type;
		print "Doc_Name	: $Doc_Name\n"; 
		
		# File Path creation
		my $hash_ref = &BIP_Column_Map_V1::ReadIniFile();	my %inihashvalue = %{$hash_ref};	my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source";   
		
		# File location creation
		unless ( -d $Filestorepath ) 
		{
			$Filestorepath=~s/\//\\/igs;
			system("mkdir $Filestorepath");
		}
		my $Storefile = "$Filestorepath/$Doc_Name";
		print "Doc_Type		: $Doc_Type\n";
		print "STOREPATH  : $Storefile \n";
		
		my $StartDate=DateTime->now();
		# Spend file download
		# $Doc_Name = $Doc_Name. 'x';
		my $fh=FileHandle->new("$Storefile",'w');
		binmode($fh);
		$fh->print($Doc_content);
		$fh->close();
		my ($Status,$Failed_Reason);
		if($status_line!~m/20/is)
		{
			$Failed_Reason=$status_line;
		}
		else
		{
			$Status='Downloaded';
		}
		print "\nStatus:::: $Status\n";
		&BIP_Column_Map_V1::ReadSource($Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
		print "Completed \n";
		
	}
# }
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	print "\nFile_Type---> $File_Type\n";
	print "\nFile_Type_cont---> $File_Type_cont\n";
	# my $get_cookie=$res->header('set-cookie');
	print "\nCODE---> $code\n";
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$File_Type,$File_Type_cont);
}

sub Postcontent()
{
	my $post_url=shift;
	# my $Post_Content=shift;
	# my $Host=shift;
	# my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "www.miltonkeynesccg.nhs.uk");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("Referer"=> "https://www.miltonkeynesccg.nhs.uk/documentlist/expenditure-over-25000/");
	$req->header("X-Requested-With"=> "XMLHttpRequest");
    $req->header("Cookie"=> "__atuvc=1%7C14%2C0%7C15%2C0%7C16%2C0%7C17%2C8%7C18; __utma=124346006.1811880155.1554550273.1556809077.1556829380.4; __utmz=124346006.1554550273.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); PHPSESSID=hlr9odv3q5ne4d0qelda55k617; __utmc=124346006; __atuvs=5ccb54c360c3ea84002; __utmb=124346006.3.10.1556829380; __utmt=1");
	$req->content("doc_order=1&doc_disp=100&template=0&default_cnt=-1&catclause=0&key_search=&url_doc=183");
	# $req->content("url_doc=165");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}


sub clean()
{
	my $C_Clean=shift;
	$C_Clean=~s/<[^>]*?>//igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/\&nbsp\;/ /igs;
	$C_Clean=~s/\&amp\;/ /igs;
	$C_Clean=~s/\&pound\;/ /igs;
	$C_Clean=~s/\&\#8211\;/ /igs;
	$C_Clean=~s/ndash\;/ /igs;
	$C_Clean=~s/[^[:print:]]+//igs;
	
	
	$C_Clean=~s/\&nbsp\;//igs;
	$C_Clean=~s/\#160\;//igs;
	$C_Clean=~s/\#163\;//igs;
	$C_Clean=~s/\&/ /igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	return $C_Clean;
}