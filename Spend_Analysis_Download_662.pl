use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use BIP_DB_V1;
use BIP_Column_Map_V1;

my $Input_Buyer_ID = $0;
$Input_Buyer_ID =~ s/\.pl//igs;
$Input_Buyer_ID =$1 if($Input_Buyer_ID =~ m/[^>]*?\_(\d+?)\s*$/is);


# my $ua=LWP::UserAgent->new(show_progress=>1);
my $ua=LWP::UserAgent->new(ssl_opts => {SSL_verify_mode => 'SSL_VERIFY_PEER',verify_hostname => 0 }, show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
# $ua->ssl_opts( verify_hostnames => 0 ,SSL_verify_mode => 0x00);
$ua->timeout(100); 
# BEGIN { $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0 };
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my %Months_To_Num=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");

############Database Initialization########
my $dbh = &BIP_DB_V1::DbConnection();
###########################################
my $hash_ref = &BIP_Column_Map_V1::ReadIniFile;
my %inihashvalue = %{$hash_ref};

my $Input_Table='BUYERS';
############ Retrieve Spent Data weblink from Buyers table ###########
my ($ID,$Buyer_Name,$Spent_Data_weblink) = &BIP_DB_V1::RetrieveUrl($dbh,$Input_Buyer_ID,$Input_Table);
my $Buyer_ID 			= @$ID[0];
my $Buyer_Name 			= @$Buyer_Name[0];
my $Buyer_Url 			= @$Spent_Data_weblink[0];
$Buyer_Url="https://www.stoke.gov.uk/site/scripts/google_results.php?q=transparency";
$Buyer_Url="https://www.stoke.gov.uk/directory/27/a_to_z/T";
$Buyer_Url="https://www.stoke.gov.uk/directory/27/data_directory/category/292";
$Buyer_Url="https://www.stoke.gov.uk/site/scripts/directory_search.php?directoryID=27&keywords=Transparency&categoryID=-1&search=";
# $Buyer_Url="https://www.stoke.gov.uk/site/scripts/google_results.php?q=transparency&startNum=10";
# $Buyer_Url="https://www.stoke.gov.uk/directory/27/data_directory/category/292";
# $Buyer_Url="https://www.stoke.gov.uk/site/scripts/google_results.php?q=Transparency&sites%5B%5D=&startNum=20";
###############
print "Buyer_ID	: $Buyer_ID\n";	
print "Buyer_Name	: $Buyer_Name\n";	
print "Buyer_Url	: $Buyer_Url\n";	

my ($Buyer_content,$Status_Line)=&Getcontent($Buyer_Url); &BIP_Column_Map_V1::Link_Status_Update($dbh,$Buyer_ID,$Status_Line);
# open(ts,">Buyer_content.html");
# print ts "$Buyer_content";
# close ts;

my ($Processed_LinkInfo) = &BIP_DB_V1::LinkInfo_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
my @Processed_LinkInfo=@$Processed_LinkInfo;
top1:

while($Buyer_content=~m/<a href="([^>]*?)">([^>]*?ransparency[^>]*?)\s*<\/a>/igs)
{
	my $folderurl=$1;
	my $LinkInfo=$2;
	if($folderurl!~m/http/is)
	{
		my $u1=URI::URL->new($folderurl,$Buyer_Url);
		my $u2=$u1->abs;
		$folderurl=$u2;
	}
	my ($folder_content,$Status_Line,$Content_Disposition)=&Getcontent($folderurl); # Ping the web link page

	# if($folder_content=~m/>Transparency\s*[\d]+\s*<\/a>\s*<ul>([\w\W]*?)\s*<\/ul>/is)
	# {
		# my $folder_sub=$1;
		# To get the Doc_url from the Buyer_content
		# while($folder_sub=~m/<a\s*href\=\"([^>]*?)\">\s*([^>]*?)\s*<\/a>\s*<\/li>/igs)
		# {
			# my $fileurl=$1;
			# my $filename=$2;
			
			# if($fileurl!~m/http/is)
			# {
				# my $u1=URI::URL->new($fileurl,$Buyer_Url);
				# my $u2=$u1->abs;
				# $fileurl=$u2;
			# }
			# my ($file_content,$status_line,$Content_Disposition)=&Getcontent($fileurl);
			my ($Doc_Link,$File_Month,$File_Year,$Doc_Name);
			if($folder_content =~ m/href="([^>]*?\.(?:csv|xlsx?))"[^>]*?>/is)
			{
				$Doc_Link=$1;
				my $LinkInfo_Temp=$LinkInfo;
				$LinkInfo=&clean($LinkInfo);	
				if($LinkInfo ne '') 
				{	
					$LinkInfo=~s/\s\s+/ /igs;
					print "LinkInfo	: $LinkInfo\n";
					
					if($LinkInfo ~~ @Processed_LinkInfo || $LinkInfo =~ m/^\s*$/)
					{
						print "File Already Processed\n";
						next;
					}
					else
					{			
						print "\nThis is new file to download\n";
					}
				}	
				
				if($Doc_Link!~m/http/is)
				{
					my $u1=URI::URL->new($Doc_Link,$Buyer_Url);
					my $u2=$u1->abs;
					$Doc_Link=$u2;
				}
				
				# Spend file download
				my ($Doc_content,$status_line,$Content_Disposition)=&Getcontent($Doc_Link);		
				my $Doc_Type='CSV';
				
				# $LinkInfo=~s/\<[^>]*?\>//igs;
				# $LinkInfo=~s/\s\s+/ /igs;
				
				# Get Date, Month, Year from the link info for file location Creation
				if($LinkInfo_Temp=~m/\s*([a-zA-Z]+)\s*(\d{4})[^>]*?$/is)
				{
					$File_Month=$1;
					$File_Year=$2;
					print "File_Month	: $File_Month\n";
					print "File_Year	: $File_Year\n";
				}
				
				my $Month_Of_Document = "$File_Month $File_Year";
				print "LinkInfo	: <$LinkInfo>\n";
				if($LinkInfo ~~ @Processed_LinkInfo || $LinkInfo =~ m/^\s*$/)
				{
					print "File Already Processed\n";
					next;
				}
				if($Doc_Link!~m/http/is)
				{
					my $u1=URI::URL->new($Doc_Link,$Buyer_Url);
					my $u2=$u1->abs;
					$Doc_Link=$u2;
				}
				
				# File name creation from Link Info
				my $File_Type = $Doc_Type if ($Doc_Type ne '');
				
				$Doc_Name=$LinkInfo;
				$Doc_Name=~s/\.[\w+]$//igs;
				$Doc_Name=~s/\W/\_/igs;
				$Doc_Name=~s/\%20/\_/igs;
				$Doc_Name=~s/_+/\_/igs;
				$Doc_Name.='.'.$File_Type;
				print "Doc_Name	: $Doc_Name\n";
				
				# File Path creation
				my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source";   
				
				# File location creation
				unless ( -d $Filestorepath ) 
				{
					$Filestorepath=~s/\//\\/igs;
					system("mkdir $Filestorepath");
				}
				my $Storefile = "$Filestorepath\\$Doc_Name";
				print "STOREPATH  : $Storefile \n";
				my $StartDate=DateTime->now();
				# Spend file download
				#my ($Doc_content,$status_line)=&Getcontent($Doc_Link);
				my $fh=FileHandle->new("$Storefile",'w');
				binmode($fh);
				$fh->print($Doc_content);
				$fh->close();
				my ($Status,$Failed_Reason);
				if($status_line!~m/20/is)
				{
					$Failed_Reason=$status_line;
				}
				else
				{
					$Status='Downloaded';
				}
				print "\nStatus:::: $Status\n";
				<>;
				&BIP_Column_Map_V1::ReadSource($Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
				print "Completed \n";
			}
		}
	# }
# }
if($Buyer_content=~m/<a class="nav__link" href="([^>]*?)"><span class="nav__label">\s*Next\s*<\/span>/is)
{
my $Buyer_Url1=$1;
$Buyer_Url1=~s/amp;//igs;
my ($Buyer_content1,$Status_Line)=&Getcontent($Buyer_Url1);
$Buyer_content=$Buyer_content1;
goto top1;
}
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	
	#$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME}=0;	
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	# my $get_cookie=$res->header('set-cookie');
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	my $Content_Disposition=$res->header("Content-Disposition");
	return ($content,$status_line,$Content_Disposition);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	if($Data eq '')
	{
		# $Data='Not provided';
	}
	return $Data;
}
sub clean()
{
	my $C_Clean=shift;
	$C_Clean=~s/<[^>]*?>//igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/\&nbsp\;/ /igs;
	$C_Clean=~s/\&amp\;/ /igs;
	$C_Clean=~s/\&pound\;/ /igs;
	$C_Clean=~s/\&\#8211\;/ /igs;
	$C_Clean=~s/ndash\;/ /igs;
	$C_Clean=~s/[^[:print:]]+//igs;
	
	
	$C_Clean=~s/\&nbsp\;//igs;
	$C_Clean=~s/\#160\;//igs;
	$C_Clean=~s/\#163\;//igs;
	$C_Clean=~s/\&/ /igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	return $C_Clean;
}