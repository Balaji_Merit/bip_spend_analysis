use Date::Parse;
use Date::Manip;
use POSIX 'strftime';

my $fulldate = strftime "%b %m, %Y %A %H:%M:%S", localtime;
print $fulldate;