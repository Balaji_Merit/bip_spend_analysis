use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use BIP_DB_V1;
use BIP_Column_Map_V1;
# require "D:/Suresh/2015/July/Scraping/BIP_Analysis/Arul_Allotment/BIP_DB.pm";
# require "D:/Suresh/2015/July/Scraping/BIP_Analysis/Arul_Allotment/BIP_Column_Map.pm";


my $robotname = $0;
my $Input_Buyer_ID=$1 if($robotname =~ m/_([\d+]*?)\.pl/is);
print"Input_Buyer_ID::$Input_Buyer_ID\n";
print"$robotname\n";
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/\.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my @Months=("January","February","March","April","May","June","July","August","September","October","November","December");
my %Months_To_Num=("Jan" => "1", "Feb" => "2", "Mar" => "3", "Apr" => "4", "May" => "5", "Jun" => "6", "Jul" => "7", "Aug" => "8", "Sep" => "9", "Oct" => "10", "Nov" => "11", "Dec" => "12");
my %Months_To_FullMonth=("Jan" => "January", "Feb" => "February", "Mar" => "March", "Apr" => "April", "May" => "May", "Jun" => "June", "Jul" => "July", "Aug" => "August", "Sep" => "September", "Oct" => "October", "Nov" => "November", "Dec" => "December");
my %year_To_Fullyear=("10" => "2010", "11" => "2011", "12" => "2012", "13" => "2013", "14" => "2014", "15" => "2015");
my %Months_To_Text=("01" => "Jan", "02" => "Feb", "03" => "Mar", "04" => "Apr", "05" => "May", "06" => "Jun", "07" => "Jul", "08" => "Aug", "09" => "Sep", "10" => "Oct", "11" => "Nov", "12" => "Dec");

############Database Initialization########
my $dbh = &BIP_DB_V1::DbConnection();
###########################################

my $Input_Table='Buyers';
############ Retrieve Spent Data weblink from Buyers table ###########
my ($ID,$Buyer_Name,$Spent_Data_weblink) = &BIP_DB_V1::RetrieveUrl($dbh,$Input_Buyer_ID,$Input_Table);
my $Buyer_ID 			= @$ID[0];
my $Buyer_Name 			= @$Buyer_Name[0];
my $Buyer_Url 			= @$Spent_Data_weblink[0];
$Buyer_Url 			= "https://www.ons.gov.uk/aboutus/transparencyandgovernance/organisationdeclarations/paymentstosuppliersover25000";
################ Retrived Column mapping details from Field_Map_Details ###########################
my ($File_Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2) = &BIP_DB_V1::Column_Name_RetrieveUrl($dbh,$Buyer_ID);

#To change
# For number of columns available in field mapping table. This is used to update the Field_Match status
my @DB_Column_Headers=($Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2);
my @Header_Data_Avail_Column = grep(/[A-Za-z]/, @DB_Column_Headers);
my $Header_Data_Avail_Column=@Header_Data_Avail_Column;
###############
print "Buyer_ID	: $Buyer_ID\n";	
print "Buyer_Name	: $Buyer_Name\n";	
print "Buyer_Url	: $Buyer_Url\n";	

my $StartDate=DateTime->now();
my ($Buyer_content,$Status_Line)=&Getcontent($Buyer_Url); &BIP_Column_Map_V1::Link_Status_Update($dbh,$Buyer_ID,$Status_Line);
open(ts,">Buyer_content.html");
print ts "$Buyer_content";
close ts;

my ($Processed_LinkInfo) = &BIP_DB_V1::LinkInfo_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
my @Processed_LinkInfo=@$Processed_LinkInfo;
my ($LinkInfo,$Doc_Link,$File_Date,$File_Month,$File_Year,$Doc_Name);

# To get block content for getting downloand link.
$Buyer_content=~s/\&nbsp\;/ /igs;
$Buyer_content=decode_entities($Buyer_content);
# my $Block_content=$1 if($Buyer_content=~m/<table\s*class\=\"downloads\">([\w\W]*?)<\/table>/is);
#while($Buyer_content=~m/<a\s*href\=\"([^>]*?csv)\"[^>]*?>\s*([^>]*?)\s*</igs)
while($Buyer_content=~m/<a[^>]*?href="([^>]*?\.csv)">([^>]*?)<\/a>/igs)

{
	$Doc_Link=$1;
	$LinkInfo=$2;
	$LinkInfo=~s/<[^>]*?>//igs;
	$LinkInfo=~s/\s\s+/ /igs;
	print "LinkInfo	: $LinkInfo\n";
	# Get Title of the file name
	if($LinkInfo ~~ @Processed_LinkInfo)
	{
		print "File Already Processed\n";
		next;
	}
	# Get file link 
	if($Doc_Link!~m/http/is)
	{
		my $u1=URI::URL->new($Doc_Link,$Buyer_Url);
		my $u2=$u1->abs;
		$Doc_Link=$u2;
	}
	
	# Get Date, Month, Year from the link info for file location Creation
	if ($LinkInfo=~m/([a-zA-Z]+)\s([\d]+)/is)
	{
		$File_Month=$1;
		$File_Year=$2;
		# $File_Month=$Months_To_FullMonth{$File_Month};
		# $File_Year=$year_To_Fullyear{$File_Year};
		# print "File_Month	: $File_Month\n";
		# print "File_Year	: $File_Year\n";
	}
	
	
	print "Doc_Link	: $Doc_Link\n";
	# File name creation from Link Info
	$Doc_Name=$LinkInfo;
	$Doc_Name=~s/\W/\_/igs;
	$Doc_Name.='.'.$File_Type;
	print "Doc_Name	: $Doc_Name\n"; 
	my $Month_Of_Document=$File_Month." ".$File_Year;
	# File Path creation
	my $hash_ref = &BIP_Column_Map_V1::ReadIniFile();	my %inihashvalue = %{$hash_ref};	my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source"; 
	# my $Filestorepath='D:/Suresh/2015/July/Scraping/BIP_Analysis/Arul_Allotment/'.$Buyer_ID."/".$File_Year."/Source";  
	print "Filestorepath	: $Filestorepath\n";
	
	# File location creation
	unless ( -d $Filestorepath ) 
	{
		$Filestorepath=~s/\//\\/igs;
		system("mkdir $Filestorepath");
	}
	
	# Spend file download
	my ($Doc_content,$status_line)=&Getcontent($Doc_Link);
	my $fh=FileHandle->new("$Filestorepath\\$Doc_Name",'w');
	binmode($fh);
	$fh->print($Doc_content);
	$fh->close();
	my ($Status,$Failed_Reason);
	if($status_line!~m/20/is)
	{
		$Failed_Reason=$status_line;
	}
	else
	{
		$Status='Downloaded';
	}
	print "\nStatus:::: $Status\n";
	
	&BIP_Column_Map_V1::ReadSource($Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
	print "Completed \n";
}

sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	# my $get_cookie=$res->header('set-cookie');
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}