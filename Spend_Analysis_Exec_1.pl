use strict;
use DateTime;
require BIP_DB;
my $err_stmt;
$SIG{__DIE__} = \&die_handler;

my %Day_Hash=("mon" => "Monday","tue" => "Tuesday","wed" => "Wednesday","thu" => "Thursday","fri" => "Friday","sat" => "Saturday","sun" => "Sunday");
############Database Initialization########
my $dbh = &BIP_DB::DbConnection();
###########################################

my $robotname = $0;
my $Priority_ID=$1 if($robotname =~ m/_([\d+]*?)\.pl/is);
print "Priority_ID: $Priority_ID\n";
# my $Day=lc((split(" ",localtime()))[0]);
# my $Today=$Day_Hash{$Day};
my ($Today) = &BIP_DB::Today($dbh);
print "Today: $Today\n";
my $Input_Table='BUYERS';
############ Retrieve Buyer_Name, ID, Spent Data weblink from Buyers table ###########
my ($Buyer_ID,$Buyer_Name,$Schedule_Frquency,$File_Path) = &BIP_DB::Retrieve_Schedule_Frquency($dbh,$Input_Table,$Today,$Priority_ID);
my @Buyer_ID		= @$Buyer_ID;
my @Buyer_Name		= @$Buyer_Name;
my @Schedule_Frquency= @$Schedule_Frquency;
my @File_Path		= @$File_Path;

for(my $i=0;$i<@Buyer_ID;$i++)
{
	$Buyer_ID=$Buyer_ID[$i];
	$Buyer_Name=$Buyer_Name[$i];
	$Schedule_Frquency=$Schedule_Frquency[$i];
	$File_Path=$File_Path[$i];
	my $Execution_Status;
	my $Try_Again=0;
	my $Script_Name='Spend_Analysis_Download_'.$Buyer_ID.'.pl';
	print "Buyer_ID: $Buyer_ID\n";
	my $Return_Code;
	open(TIME,">>Time_Log.txt");
	print TIME "Start:\t$Buyer_ID\t".localtime()."\n";
	eval{
		$Return_Code=system("PERL","$File_Path/$Script_Name");
		print "Return: $Return_Code\n";
	};
	open(ERR,">>Failed_Log.txt");
	if($Return_Code!=0)
	{
		$Execution_Status="Failed to Execute";
		print "Status: $Buyer_ID: $Execution_Status : $!\n";
		print ERR "$Buyer_ID\t$Execution_Status\t$!\n";
	}
	else
	{
		$Execution_Status="Success";
		print "Status: $Execution_Status : $!\n";
		print ERR "$Buyer_ID\t$Execution_Status: $!\n";
	}
	close ERR;
	print TIME "End:\t$Buyer_ID\t".localtime()."\n";
	close TIME;
	Try_Again:
	my $query = "Update $Input_Table set Execution_Status=\'$Execution_Status\',Date_Researched=now() where ID=\'$Buyer_ID\'";
	my $sth = $dbh->prepare($query);
	if($sth->execute())
	{
		print "Updated\n";
		$sth->finish();	
	}
	else
	{
		$dbh=&BIP_DB::DbConnection();
		if($Try_Again==0)
		{
			$Try_Again=1;
			goto Try_Again;
		}
		else
		{
			open(ERR,">>Exec_Failed_Query.txt");
			print ERR $query."\n";
			close ERR;
		}	
	}
}
sub die_handler()
{
  my $err_stmt = $err_stmt . $_ foreach (@_);
  $err_stmt =~s/\n/ /igs;
  print "Err: $err_stmt\n";
  open FH,">>Error.txt";
  print FH $err_stmt;
  close FH;
}
