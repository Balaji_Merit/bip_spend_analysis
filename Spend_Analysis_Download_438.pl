use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use BIP_DB_V1;
use BIP_Column_Map_V1;

my $Input_Buyer_ID = $0;
$Input_Buyer_ID =~ s/\.pl//igs;
$Input_Buyer_ID =$1 if($Input_Buyer_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

# my $ua  = LWP::UserAgent->new(
        # ssl_opts => { verify_hostname => 1 },
    # );
# my $ua=LWP::UserAgent->new(show_progress=>1);
my $ua=LWP::UserAgent->new(ssl_opts => {SSL_verify_mode => 'SSL_VERIFY_PEER',verify_hostname => 0 }, show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:46.0) Gecko/20100101 Firefox/46.0");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my %Months_To_Num=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");
my @Months1=("January","February","March","April","May","June","July","August","September","October","November","December","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");

############Database Initialization########
my $dbh = &BIP_DB_V1::DbConnection();
###########################################
my $hash_ref = &BIP_Column_Map_V1::ReadIniFile;
my %inihashvalue = %{$hash_ref};

my $Input_Table='BUYERS';
############ Retrieve Spent Data weblink from Buyers table ###########
my ($ID,$Buyer_Name,$Spent_Data_weblink) = &BIP_DB_V1::RetrieveUrl($dbh,$Input_Buyer_ID,$Input_Table);
my $Buyer_ID 			= @$ID[0];
my $Buyer_Name 			= @$Buyer_Name[0];
my $Buyer_Url 			= @$Spent_Data_weblink[0];

###############
print "Buyer_ID	: $Buyer_ID\n";	
print "Buyer_Name	: $Buyer_Name\n";	
print "Buyer_Url	: $Buyer_Url\n";	


# $Buyer_Url="http://www.dudley.gov.uk/resident/your-council/local-transparency/council-expenditure-over-500/";
my ($Buyer_content,$Status_Line)=&Getcontent($Buyer_Url); &BIP_Column_Map_V1::Link_Status_Update($dbh,$Buyer_ID,$Status_Line);
open(ts,">Buyer_content.html");
print ts "$Buyer_content";
close ts;

my ($Processed_LinkInfo) = &BIP_DB_V1::LinkInfo_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
my @Processed_LinkInfo=@$Processed_LinkInfo;

# To get the Doc_url from the Buyer_content
# while($Buyer_content=~m/<strong><span\s*([\w\W]*?)\s*<div>\s*<\/div>\s*<\/div>/igs)
# {
	# my $S_Content=$1;

while($Buyer_content=~m/href\s*\=\s*\"\s*([^>]*?)\s*\"\s*[^>]*?\s*>\s*(?:<s[^>]*?\s*>|\s*)(Dudley\s*Spend\s*[^>]*?)\s*<\//igs)
{
	my $s_source_link=$1;
	if($s_source_link!~m/^http/is)
	{	
	$s_source_link="https://www.dudley.gov.uk".$s_source_link;
		# print "\nhi\n";
		# my $u1=URI::URL->new($s_source_link,$Buyer_Url);
		# my $u2=$u1->abs;
		# $s_source_link=$u2;
		# $s_source_link=~s/\&amp\;/&/igs;
	}
	# print "\ns_source_link-->$s_source_link\n";
	my ($Doc_content1)=&Getcontent($s_source_link);	
	# while($Doc_content1=~m/<li\s*class\s*\=\s*\"\s*icon\s*\"\s*>\s*<a\s*href\s*\=\s*\"\s*([^>]*?)\s*\"\s*title\s*\=\s*\"([^>]*?csv[^>]*?)\s*\"\s*>/igs)
	while($Doc_content1=~m/href\s*\=\s*\"\s*([^>]*?.csv)\s*\"\s*[^>]*?\s*>\s*<span\s*[^>]*?\s*>\s*(Payment[^>]*?)\s*<\/span>/igs)
	{
		my $Doc_Link=$1;	
		my $LinkInfo=$2;	
		$LinkInfo=&clean($LinkInfo);	
		my ($File_Date,$File_Month,$File_Year,$Doc_Name,$Doc_Type);
		my $LinkInfo_Temp=$LinkInfo;
		print "\nhi: $LinkInfo\n";	
		
		next if($Doc_Link=~m/\.pdf/is);		
		
		# Get Title of the file name
		if($LinkInfo ne '') 
		{	
			$LinkInfo=~s/\s\s+/ /igs;
			print "LinkInfo	: $LinkInfo\n";
			
			if($LinkInfo ~~ @Processed_LinkInfo || $LinkInfo =~ m/^\s*$/)
			{
				print "File Already Processed\n";
				next;
			}
			else
			{			
				print "\nThis is new file to download\n";
			}
		}	
		# Get file link 		
		if($Doc_Link!~m/^http/is)
		{	
			print "\nhi\n";
			my $u1=URI::URL->new($Doc_Link,$Buyer_Url);
			my $u2=$u1->abs;
			$Doc_Link=$u2;
			$Doc_Link=~s/\&amp\;/&/igs;
		}
		
		# my ($Doc_content,$status_line,$File_Type,$File_Type_cont)=&Getcontent($Doc_Link);
		
		# if($Doc_content=~m/href\s*\=\s*\"\s*([^>]*?\.csv)\s*\"/is)
		# {
			# $Doc_Link=$1;
		# }
		my $Doc_Link_Temp=$Doc_Link;
		# print "Doc_Link	: $Doc_Link\n";
		# print "\nLinkInfo_Temp	: $LinkInfo_Temp\n";
			
		
		foreach my $monthss(@Months1)	
		{		
			if($LinkInfo_Temp=~m/$monthss/is)
			{
				$File_Month=$monthss;
				if($LinkInfo_Temp=~m/(2\d{3})/is)
				{
					$File_Year=$1;
				}			
				else
				{
					while($LinkInfo_Temp=~m/(\d{2,4})/igs)
					{					
						$File_Year=$1;
					}
				}	
				last;
			}			
		}
		
		# if($File_Year eq '')
		# {
			# if($LinkInfo_Temp=~m/(q\d+)\s*(\d+\/\d+)/is)
			# {
				# $File_Month=$1;
				# $File_Year=$2;
			# }
		# }
		
		my ($Doc_content,$status_line,$File_Type,$File_Type_cont)=&Getcontent($Doc_Link);
		
		if($File_Type=~m/\.pdf/is)
		{
			next;
		}
		elsif($File_Type_cont=~m/\.pdf/is)
		{
			next;
		}

		if($File_Type=~m/(csv|xlsx|xls)/is)
		{
			$Doc_Type="$1";
			print "\nI:1\n";
		}
		elsif($File_Type_cont=~m/(csv)/is)
		{
			$Doc_Type="$1";
			print "\nI:2\n";	
		}
		elsif($Doc_Link_Temp=~m/^[^>]*?\.([\w]*?)\s*$/is)
		{
			$Doc_Type="$1";		
			print "\nI:5\n";	
		}			

		
		$Doc_Type=~s/\"//igs;
		$Doc_Type=~s/\'//igs;
			
		# if($LinkInfo=~m/\s*([a-zA-Z]+)\s*(\d{4})\s*$/is)
		# {
			# $File_Month=$1;
			# $File_Year=$2;
			
		# }
		my $Month_Of_Document = "$File_Month $File_Year";	
		
		# File name creation from Link Info
		my $File_Type = $Doc_Type if ($Doc_Type ne '');
		$File_Year=~s/\W+//igs;
		$Doc_Name=$LinkInfo;
		$Doc_Name=~s/\W/\_/igs;
		$Doc_Name=~s/_+/\_/igs;
		$Doc_Name.='.'.$File_Type;
		print "#########################################\n";
		print "Doc_Name	: $Doc_Name\n"; 
		print "File_Month		: $File_Month\n";
		print "File_Year		: $File_Year\n";
		print "Doc_Type		: $Doc_Type\n";
		print "Doc_Link	: $Doc_Link\n";
		print "Month_Of_Document	: $Month_Of_Document\n";
		print "#########################################";
		# File Path creation
		# my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source"; 
		my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source";  
		# my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source"; 
		
		# File location creation
		unless ( -d $Filestorepath ) 
		{
			$Filestorepath=~s/\//\\/igs;
			system("mkdir $Filestorepath");
		}
		my $Storefile = "$Filestorepath\\$Doc_Name";
		print "STOREPATH  : $Storefile \n";
		my $StartDate=DateTime->now();
		# Spend file download
		
		my $fh=FileHandle->new("$Storefile",'w');
		binmode($fh);
		$fh->print($Doc_content);
		$fh->close();
		my ($Status,$Failed_Reason);
		if($status_line!~m/20/is)
		{
			$Failed_Reason=$status_line;
		}
		else
		{
			$Status='Downloaded';
		}
		print "\nStatus:::: $Status\n";
		&BIP_Column_Map_V1::ReadSource($Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
		print "Completed \n";
	}	
}

	while($Buyer_content=~m/<li\s*class\s*\=\s*\"\s*icon\s*\"\s*>\s*<a\s*href\s*\=\s*\"\s*([^>]*?)\s*\"\s*title\s*\=\s*\"([^>]*?csv[^>]*?)\s*\"\s*>/igs)
	#while($Buyer_content=~m/<li\s*class\s*\=\s*\"\s*icon\s*\"\s*>\s*<a\s*href\s*\=\s*\"\s*([^>]*?)\s*\"\s*title\s*\=\s*\"([^>]*?csv[^>]*?)\s*\"\s*>/igs)
	{
		my $Doc_Link=$1;	
		my $LinkInfo=$2;	
		$LinkInfo=&clean($LinkInfo);	
		my ($File_Date,$File_Month,$File_Year,$Doc_Name,$Doc_Type);
		my $LinkInfo_Temp=$LinkInfo;
		print "\nhi: $LinkInfo\n";	
		
		next if($Doc_Link=~m/\.pdf/is);		
		
		# Get Title of the file name
		if($LinkInfo ne '') 
		{	
			$LinkInfo=~s/\s\s+/ /igs;
			print "LinkInfo	: $LinkInfo\n";
			
			if($LinkInfo ~~ @Processed_LinkInfo || $LinkInfo =~ m/^\s*$/)
			{
				print "File Already Processed\n";
				next;
			}
			else
			{			
				print "\nThis is new file to download\n";
			}
		}	
		# Get file link 		
		if($Doc_Link!~m/^http/is)
		{	
			print "\nhi\n";
			my $u1=URI::URL->new($Doc_Link,$Buyer_Url);
			my $u2=$u1->abs;
			$Doc_Link=$u2;
			$Doc_Link=~s/\&amp\;/&/igs;
		}
		
		# my ($Doc_content,$status_line,$File_Type,$File_Type_cont)=&Getcontent($Doc_Link);
		
		# if($Doc_content=~m/href\s*\=\s*\"\s*([^>]*?\.csv)\s*\"/is)
		# {
			# $Doc_Link=$1;
		# }
		my $Doc_Link_Temp=$Doc_Link;
		# print "Doc_Link	: $Doc_Link\n";
		# print "\nLinkInfo_Temp	: $LinkInfo_Temp\n";
			
		
		foreach my $monthss(@Months1)	
		{		
			if($LinkInfo_Temp=~m/$monthss/is)
			{
				$File_Month=$monthss;
				if($LinkInfo_Temp=~m/(2\d{3})/is)
				{
					$File_Year=$1;
				}			
				else
				{
					while($LinkInfo_Temp=~m/(\d{2,4})/igs)
					{					
						$File_Year=$1;
					}
				}	
				last;
			}			
		}
		
		# if($File_Year eq '')
		# {
			# if($LinkInfo_Temp=~m/(q\d+)\s*(\d+\/\d+)/is)
			# {
				# $File_Month=$1;
				# $File_Year=$2;
			# }
		# }
		
		my ($Doc_content,$status_line,$File_Type,$File_Type_cont)=&Getcontent($Doc_Link);
		
		if($File_Type=~m/\.pdf/is)
		{
			next;
		}
		elsif($File_Type_cont=~m/\.pdf/is)
		{
			next;
		}

		if($File_Type=~m/(csv|xlsx|xls)/is)
		{
			$Doc_Type="$1";
			print "\nI:1\n";
		}
		elsif($File_Type_cont=~m/(csv)/is)
		{
			$Doc_Type="$1";
			print "\nI:2\n";	
		}
		elsif($Doc_Link_Temp=~m/^[^>]*?\.([\w]*?)\s*$/is)
		{
			$Doc_Type="$1";		
			print "\nI:5\n";	
		}			

		
		$Doc_Type=~s/\"//igs;
		$Doc_Type=~s/\'//igs;
			
		# if($LinkInfo=~m/\s*([a-zA-Z]+)\s*(\d{4})\s*$/is)
		# {
			# $File_Month=$1;
			# $File_Year=$2;
			
		# }
		my $Month_Of_Document = "$File_Month $File_Year";	
		
		# File name creation from Link Info
		my $File_Type = $Doc_Type if ($Doc_Type ne '');
		$File_Year=~s/\W+//igs;
		$Doc_Name=$LinkInfo;
		$Doc_Name=~s/\W/\_/igs;
		$Doc_Name=~s/_+/\_/igs;
		$Doc_Name.='.'.$File_Type;
		print "#########################################\n";
		print "Doc_Name	: $Doc_Name\n"; 
		print "File_Month		: $File_Month\n";
		print "File_Year		: $File_Year\n";
		print "Doc_Type		: $Doc_Type\n";
		print "Doc_Link	: $Doc_Link\n";
		print "Month_Of_Document	: $Month_Of_Document\n";
		print "#########################################";
		# File Path creation
		# my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source"; 
		my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source";  
		# my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source"; 
		
		# File location creation
		unless ( -d $Filestorepath ) 
		{
			$Filestorepath=~s/\//\\/igs;
			system("mkdir $Filestorepath");
		}
		my $Storefile = "$Filestorepath\\$Doc_Name";
		print "STOREPATH  : $Storefile \n";
		my $StartDate=DateTime->now();
		# Spend file download
		
		my $fh=FileHandle->new("$Storefile",'w');
		binmode($fh);
		$fh->print($Doc_content);
		$fh->close();
		my ($Status,$Failed_Reason);
		if($status_line!~m/20/is)
		{
			$Failed_Reason=$status_line;
		}
		else
		{
			$Status='Downloaded';
		}
		print "\nStatus:::: $Status\n";
		&BIP_Column_Map_V1::ReadSource($Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
		print "Completed \n";
	}	
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"www.dudley.gov.uk");
	$req->header("Accept"=>"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	# $req->header("Accept"=>"application/json, text/javascript, */*");
	$req->header("X-Powered-By"=>"ASP.NET");
	# $req->header("Pragma"=> "no-cache");
	$req->header("Content-Type"=> "text/html; charset=utf-8");
	# $req->header("Cookie"=>"TS9b7226c7_75=TS9b7226c7_rc=1&TS9b7226c7_id=5&TS9b7226c7_cr=0886bb8f5dab2800c5028b20f13c5d957eff0d3f0424f282538ba94e6afc7218d6c34ff08f6d1391ffab955bd5a7da98:08bcfa0947047000e78fc5a0dcbf019772218383fff435a928f5bbbf05cd2a8f55b1683352d9ca15c0d4997f2d44345c34236e669512fd1541e281cf8d3cb7e9d45e749ececb56526a795efbcb6130fd9da0ca3233d69aedf4f2262b62207418b1e280de9fd8c8b5052446b1c8c2cbf4d15860decedd7b0d&TS9b7226c7_ct=0&TS9b7226c7_rf=0; _ga=GA1.3.1188748078.1461751893; nmstat=1461751904356; TSPD_101=0886bb8f5dab2800accfab642f444765de5855e02463aa543cc61d485bf0af8c95381f0d7ec228b78be2f2a5e0d29ca3085f307e65051000e94acfc32c59cde035d66e8a4d24f668; ASP.NET_SessionId=3iw24bsaixrfwzcr1duhjsot; TS012f1a86=0104e7c06223cd50e906ca48055c3f5d6be31ef5eaa9bb4532c4d943205d617f82181efa3a1e5e0981835c33ee12c5431c11f6592f");
	$req->header("Cookie"=>"TS9b7226c7_75=TS9b7226c7_rc=1&TS9b7226c7_id=5&TS9b7226c7_cr=0886bb8f5dab28005b404b04edc745d5b2647dc655fe9af87601940c9c147c44c814a3a7726192c928976b94a7435811:0810ca779b047000da35d6881ece481f72218383fff435a928f5bbbf05cd2a8f55b1683352d9ca15c0d4997f2d44345c34236e669512fd1541e281cf8d3cb7e9d45e749ececb56526a795efbcb6130fd9da0ca3233d69aedf4f2262b62207418b1e280de9fd8c8b5052446b1c8c2cbf4d15860decedd7b0d&TS9b7226c7_ct=0&TS9b7226c7_rf=0; _ga=GA1.3.1188748078.1461751893; nmstat=1461751904356; TSPD_101=0886bb8f5dab28008f4b881b06f282d73369383c83c1c0c0c0478987c9089e9f37d2bee40dcb59063bd7497c6e8ee4f608d6ed173f051000bcbb5039b7d0228435d66e8a4d24f668; ASP.NET_SessionId=3iw24bsaixrfwzcr1duhjsot; TS012f1a86=0104e7c062b411e8a66927a5ceeaa306b0af4713f723d8c14dcb5e34d5ddf4c83642b2b5e9adb9d0346616a442b9488da022ad7c07");
	
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	my $File_Type_cont=$res->header("Cookie");
	print "\nFile_Type---> $File_Type\n";
	print "\nFile_Type_cont---> $File_Type_cont\n";
	# my $get_cookie=$res->header('set-cookie');
	print "\nCODE---> $code\n";
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$File_Type,$File_Type_cont);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("Referer"=> "$Referer");
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	if($Data eq '')
	{
		# $Data='Not provided';
	}
	return $Data;
}

sub clean()
{
	my $C_Clean=shift;
	$C_Clean=~s/<[^>]*?>//igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/\&nbsp\;/ /igs;
	$C_Clean=~s/\&amp\;/ /igs;
	$C_Clean=~s/\&pound\;/ /igs;
	$C_Clean=~s/\&\#8211\;/ /igs;
	$C_Clean=~s/ndash\;/ /igs;
	$C_Clean=~s/[^[:print:]]+//igs;
	
	
	$C_Clean=~s/\&nbsp\;//igs;
	$C_Clean=~s/\#160\;//igs;
	$C_Clean=~s/\#163\;//igs;
	$C_Clean=~s/\&/ /igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	return $C_Clean;
}