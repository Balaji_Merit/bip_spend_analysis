use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use BIP_DB_V1;
use BIP_Column_Map_V1;

my $Input_Buyer_ID = $0;
$Input_Buyer_ID =~ s/\.pl//igs;
$Input_Buyer_ID =$1 if($Input_Buyer_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

# $Input_Buyer_ID='689';
# my $ua  = LWP::UserAgent->new(
        # ssl_opts => { verify_hostname => 1 },
    # );
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my %Months_To_Num=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");
my @Months1=("January","February","March","April","May","June","July","August","September","October","November","December","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");

############Database Initialization########
my $dbh = &BIP_DB_V1::DbConnection();
###########################################
my $hash_ref = &BIP_Column_Map_V1::ReadIniFile;
my %inihashvalue = %{$hash_ref};

my $Input_Table='BUYERS';
############ Retrieve Spent Data weblink from Buyers table ###########
my ($ID,$Buyer_Name,$Spent_Data_weblink) = &BIP_DB_V1::RetrieveUrl($dbh,$Input_Buyer_ID,$Input_Table);
my $Buyer_ID 			= @$ID[0];
my $Buyer_Name 			= @$Buyer_Name[0];
my $Buyer_Url 			= @$Spent_Data_weblink[0];
$Buyer_Url 			="https://www.warwickshirenorthccg.nhs.uk/About-Us/Our-spending";
###############
print "Buyer_ID	: $Buyer_ID\n";	
print "Buyer_Name	: $Buyer_Name\n";	
print "Buyer_Url	: $Buyer_Url\n";	

my ($Buyer_content,$Status_Line)=&Getcontent($Buyer_Url); &BIP_Column_Map_V1::Link_Status_Update($dbh,$Buyer_ID,$Status_Line);
open(ts,">Buyer_content215.html");
print ts "$Buyer_content";
close ts;

# my ($Processed_LinkInfo) = &BIP_DB_V1::LinkInfo_RetrieveUrl($dbh,$Buyer_ID,"bip_pdf_json_status");
# my @Processed_LinkInfo=@$Processed_LinkInfo;
my ($Processed_LinkInfo) = &BIP_DB_V1::LinkInfo_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
my @Processed_LinkInfo=@$Processed_LinkInfo;
$Buyer_content=decode_entities($Buyer_content);

# print "Count	:: $count\n";<>;


while($Buyer_content=~m/href="javascript:__doPostBack\(\'([^>]*?)\'[^>]*?>\s*[\d]+/igs)
{
	my $EVENTTARGET=$1;
	$EVENTTARGET=uri_escape($EVENTTARGET);

	my ($VIEWSTATE);
	if ($Buyer_content=~m/__VIEWSTATE\"\s*value\=\"([^>]*?)\"/is)
	{
		$VIEWSTATE = $1;
	}
	$VIEWSTATE=uri_escape($VIEWSTATE);

	my $host = 'www.warwickshirenorthccg.nhs.uk';
	# my $post_content='ctl01%24ToolkitScriptManager=ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24pnlGrid%7Cctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl02%24lnkItemTitle&ctl01_ToolkitScriptManager_HiddenField=&__EVENTTARGET='.$EVENTTARGET.'&__EVENTARGUMENT=&__VIEWSTATE='.$VIEWSTATE.'&__SCROLLPOSITIONX=0&__SCROLLPOSITIONY=0&ctl01%24txtSearch=Search%20this%20site...&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24hdnLibNav_Folder=&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24txtHiddenRefreshID=&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl02%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl02%24hdnUniqueID=078ea28a-a968-44f7-a225-93742669e4a2&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl03%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl03%24hdnUniqueID=bad310e5-6540-411a-83a7-3f77bf4fa174&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl04%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl04%24hdnUniqueID=8d866da6-ad80-4e43-8b7a-03ac3de6dd86&__ASYNCPOST=true&';
	# my $post_content='ctl01%24ToolkitScriptManager=ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24pnlGrid%7Cctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl02%24lnkItemTitle&ctl01_ToolkitScriptManager_HiddenField=&__EVENTTARGET='.$EVENTTARGET.'&__EVENTARGUMENT=&__VIEWSTATE='.$VIEWSTATE.'&__VIEWSTATEGENERATOR=52ADDA99&__SCROLLPOSITIONX=0&__SCROLLPOSITIONY=0&ctl01%24txtSearch=Search%20this%20site...&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24hdnLibNav_Folder=&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24txtHiddenRefreshID=&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl02%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl02%24hdnUniqueID=b049ffe6-ee7e-4ede-8726-02f6091dc3a9&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl03%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl03%24hdnUniqueID=8280cc60-da64-4abf-b9a4-f745281c5ad4&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl04%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl04%24hdnUniqueID=007a4f28-5844-4575-8bdd-38a413bf1cc5&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl05%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl05%24hdnUniqueID=078ea28a-a968-44f7-a225-93742669e4a2&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl06%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl06%24hdnUniqueID=bad310e5-6540-411a-83a7-3f77bf4fa174&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl07%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl07%24hdnUniqueID=8d866da6-ad80-4e43-8b7a-03ac3de6dd86&__ASYNCPOST=true&';
	# my $post_content='ctl01%24ToolkitScriptManager=ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24pnlGrid%7Cctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl02%24lnkItemTitle&ctl01_ToolkitScriptManager_HiddenField=&ctl01%24txtSearch=Search%20this%20site...&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24hdnLibNav_Folder=&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24txtHiddenRefreshID=&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl02%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl02%24hdnUniqueID=b049ffe6-ee7e-4ede-8726-02f6091dc3a9&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl03%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl03%24hdnUniqueID=8280cc60-da64-4abf-b9a4-f745281c5ad4&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl04%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl04%24hdnUniqueID=007a4f28-5844-4575-8bdd-38a413bf1cc5&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl05%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl05%24hdnUniqueID=078ea28a-a968-44f7-a225-93742669e4a2&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl06%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl06%24hdnUniqueID=bad310e5-6540-411a-83a7-3f77bf4fa174&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl07%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl07%24hdnUniqueID=8d866da6-ad80-4e43-8b7a-03ac3de6dd86&__EVENTTARGET='.$EVENTTARGET.'&__EVENTARGUMENT=&__VIEWSTATE='.$VIEWSTATE.'&__VIEWSTATEGENERATOR=52ADDA99&__SCROLLPOSITIONX=0&__SCROLLPOSITIONY=0&__ASYNCPOST=true&';
	my $post_content="ctl01%24ToolkitScriptManager=ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24pnlGrid%7Cctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl03%24lnkItemTitle&ctl01_ToolkitScriptManager_HiddenField=&__EVENTTARGET=ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl03%24lnkItemTitle&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUKLTc2NDIxMDk0NA9kFgJmD2QWAgIDD2QWAgIBD2QWBAIDD2QWBmYPFgIeB1Zpc2libGVoFgICAQ8WAh4EVGV4dAUzTkhTIFdhcndpY2tzaGlyZSBOb3J0aCBDbGluaWNhbCBDb21taXNzaW9uaW5nIEdyb3VwZAICDxYCHwBoFgYCBQ8PFgQeC05hdmlnYXRlVXJsBQYvQWRtaW4eCEltYWdlVXJsZWRkAgkPDxYEHwJlHwNlZGQCDQ8PFgQfAgULL015LVByb2ZpbGUfA2VkZAIEDxYCHwBoFgICBQ8PFgQfAmUfA2VkZAIFD2QWCAIBD2QWAgIBDxYCHwBnZAIFD2QWAgIBDw9kFgIeCm9ua2V5cHJlc3MFNnJldHVybiBGdXNpb25HcmlkS2V5UHJlc3MoZXZlbnQsJ2N0bDAxJGxua0RvU2VhcmNoJyk7IGQCCw9kFgICAQ9kFgICBg9kFgJmDw8WCB4NQ3VycmVudFBhZ2VJRCgpWFN5c3RlbS5HdWlkLCBtc2NvcmxpYiwgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWI3N2E1YzU2MTkzNGUwODkkNjFmNGZhOTUtYzMyYy00Mzg3LTlhMWUtNjIyNWJhMDNmOTBmHg9DdXJyZW50Rm9sZGVySUQoKwQkNzJiNGM5NDUtMmJiZi00YWM1LWExMjYtNjk0ZGQ5YzMwMTJjHgxSb290Rm9sZGVySUQoKwQkNzJiNGM5NDUtMmJiZi00YWM1LWExMjYtNjk0ZGQ5YzMwMTJjHgtEZWZhdWx0U29ydAspX0dQLkFQSS5FRG9jTGliV2lkZ2V0X0RlZmF1bHRTb3J0LCBHUC5BUEksIFZlcnNpb249MS4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1udWxsAWQWAmYPZBYCZg9kFgQCAQ9kFgICAQ8PFgQfAmUfA2VkZAIDD2QWBAIFDw8WAh4JR3JpZFRpdGxlBVc8YT48aSBjbGFzcz0nZmEgZmEtaG9tZScgdGl0bGU9J0V4cGVuZGl0dXJlIG92ZXIgwqMyNWsnPjwvaT4gRXhwZW5kaXR1cmUgb3ZlciDCozI1azwvYT5kFgJmD2QWBGYPZBYCAgEPDxYGHwEFC1Nob3cgU2VhcmNoHghDc3NDbGFzcwUUQnV0dG9uU21hbGwgQ29sbGFwc2UeBF8hU0ICAmRkAgEPFgIfAGgWAmYPZBYCAgEPDxYIHhFWYWxpZGF0aW9uTWVzc2FnZWUeC0NhcHR1cmVEYXRlaB4PQ2FwdHVyZVRpbWVPbmx5aB4OQ2FwdHVyZU51bWVyaWNoZGQCBg88KwANAQAPFgQeC18hRGF0YUJvdW5kZx4LXyFJdGVtQ291bnQCBmQWAmYPZBYQAgEPZBYKZg9kFgICAQ8PFgIfAwUaL2xpYi9pbWcvaWNvLzE2L2ZvbGRlci5wbmdkZAIBD2QWBAIBDw8WAh4PQ29tbWFuZEFyZ3VtZW50BSQwMDdhNGYyOC01ODQ0LTQ1NzUtOGJkZC0zOGE0MTNiZjFjYzVkFgJmDxYCHwEFBzIwMTYtMTdkAgMPFgIfAQUnPGJyIC8%2BPGk%2BMCBGb2xkZXJzLCAxMiBNZWRpYSBGaWxlcy48L2k%2BZAICD2QWAgIBDxYCHwFlZAIDD2QWAgIBDw8WAh8AaGQWAmYPDxYCHwMFGC9saWIvaW1nL2ljby8xNi9kaXNrLnBuZ2RkAgQPZBYCAgEPDxYGHwMFJi9saWIvaW1nL2ljby8xNi9pbmZvcm1hdGlvbi1idXR0b24ucG5nHxIFJDAwN2E0ZjI4LTU4NDQtNDU3NS04YmRkLTM4YTQxM2JmMWNjNR8AaGRkAgIPZBYKZg9kFgICAQ8PFgIfAwUaL2xpYi9pbWcvaWNvLzE2L2ZvbGRlci5wbmdkZAIBD2QWBAIBDw8WAh8SBSRiMDQ5ZmZlNi1lZTdlLTRlZGUtODcyNi0wMmY2MDkxZGMzYTlkFgJmDxYCHwEFBzIwMTgtMTlkAgMPFgIfAQUnPGJyIC8%2BPGk%2BMCBGb2xkZXJzLCAxMyBNZWRpYSBGaWxlcy48L2k%2BZAICD2QWAgIBDxYCHwFlZAIDD2QWAgIBDw8WAh8AaGQWAmYPDxYCHwMFGC9saWIvaW1nL2ljby8xNi9kaXNrLnBuZ2RkAgQPZBYCAgEPDxYGHwMFJi9saWIvaW1nL2ljby8xNi9pbmZvcm1hdGlvbi1idXR0b24ucG5nHxIFJGIwNDlmZmU2LWVlN2UtNGVkZS04NzI2LTAyZjYwOTFkYzNhOR8AaGRkAgMPZBYKZg9kFgICAQ8PFgIfAwUaL2xpYi9pbWcvaWNvLzE2L2ZvbGRlci5wbmdkZAIBD2QWBAIBDw8WAh8SBSQ4MjgwY2M2MC1kYTY0LTRhYmYtYjlhNC1mNzQ1MjgxYzVhZDRkFgJmDxYCHwEFBzIwMTctMThkAgMPFgIfAQUmPGJyIC8%2BPGk%2BMCBGb2xkZXJzLCAzIE1lZGlhIEZpbGVzLjwvaT5kAgIPZBYCAgEPFgIfAWVkAgMPZBYCAgEPDxYCHwBoZBYCZg8PFgIfAwUYL2xpYi9pbWcvaWNvLzE2L2Rpc2sucG5nZGQCBA9kFgICAQ8PFgYfAwUmL2xpYi9pbWcvaWNvLzE2L2luZm9ybWF0aW9uLWJ1dHRvbi5wbmcfEgUkODI4MGNjNjAtZGE2NC00YWJmLWI5YTQtZjc0NTI4MWM1YWQ0HwBoZGQCBA9kFgpmD2QWAgIBDw8WAh8DBRovbGliL2ltZy9pY28vMTYvZm9sZGVyLnBuZ2RkAgEPZBYEAgEPDxYCHxIFJDA3OGVhMjhhLWE5NjgtNDRmNy1hMjI1LTkzNzQyNjY5ZTRhMmQWAmYPFgIfAQUHMjAxNS0xNmQCAw8WAh8BBSc8YnIgLz48aT4wIEZvbGRlcnMsIDEyIE1lZGlhIEZpbGVzLjwvaT5kAgIPZBYCAgEPFgIfAWVkAgMPZBYCAgEPDxYCHwBoZBYCZg8PFgIfAwUYL2xpYi9pbWcvaWNvLzE2L2Rpc2sucG5nZGQCBA9kFgICAQ8PFgYfAwUmL2xpYi9pbWcvaWNvLzE2L2luZm9ybWF0aW9uLWJ1dHRvbi5wbmcfEgUkMDc4ZWEyOGEtYTk2OC00NGY3LWEyMjUtOTM3NDI2NjllNGEyHwBoZGQCBQ9kFgpmD2QWAgIBDw8WAh8DBRovbGliL2ltZy9pY28vMTYvZm9sZGVyLnBuZ2RkAgEPZBYEAgEPDxYCHxIFJGJhZDMxMGU1LTY1NDAtNDExYS04M2E3LTNmNzdiZjRmYTE3NGQWAmYPFgIfAQUHMjAxNC0xNWQCAw8WAh8BBSc8YnIgLz48aT4wIEZvbGRlcnMsIDEyIE1lZGlhIEZpbGVzLjwvaT5kAgIPZBYCAgEPFgIfAWVkAgMPZBYCAgEPDxYCHwBoZBYCZg8PFgIfAwUYL2xpYi9pbWcvaWNvLzE2L2Rpc2sucG5nZGQCBA9kFgICAQ8PFgYfAwUmL2xpYi9pbWcvaWNvLzE2L2luZm9ybWF0aW9uLWJ1dHRvbi5wbmcfEgUkYmFkMzEwZTUtNjU0MC00MTFhLTgzYTctM2Y3N2JmNGZhMTc0HwBoZGQCBg9kFgpmD2QWAgIBDw8WAh8DBRovbGliL2ltZy9pY28vMTYvZm9sZGVyLnBuZ2RkAgEPZBYEAgEPDxYCHxIFJDhkODY2ZGE2LWFkODAtNGU0My04YjdhLTAzYWMzZGU2ZGQ4NmQWAmYPFgIfAQUHMjAxMy0xNGQCAw8WAh8BBSc8YnIgLz48aT4wIEZvbGRlcnMsIDEyIE1lZGlhIEZpbGVzLjwvaT5kAgIPZBYCAgEPFgIfAWVkAgMPZBYCAgEPDxYCHwBoZBYCZg8PFgIfAwUYL2xpYi9pbWcvaWNvLzE2L2Rpc2sucG5nZGQCBA9kFgICAQ8PFgYfAwUmL2xpYi9pbWcvaWNvLzE2L2luZm9ybWF0aW9uLWJ1dHRvbi5wbmcfEgUkOGQ4NjZkYTYtYWQ4MC00ZTQzLThiN2EtMDNhYzNkZTZkZDg2HwBoZGQCBw8PFgIfAGhkZAIIDw8WAh8AaGRkAg0PDxYCHhJFbnN1cmVTaWduSW5IaWRkZW5oZGQYAQU7Y3RsMDEkQ29udGVudFBsYWNlSG9sZGVyMSREeW5hbWljUGFnZUNvbnRlbnQxJGN0bDI1JGd2d0RhdGEPPCsACgEIAgFkA9IxH4QfokIlLvCAj%2FPESP2Jkng%3D&__VIEWSTATEGENERATOR=52ADDA99&__SCROLLPOSITIONX=0&__SCROLLPOSITIONY=0&ctl01%24txtSearch=Search%20this%20site...&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24hdnLibNav_Folder=&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24txtHiddenRefreshID=&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl02%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl02%24hdnUniqueID=007a4f28-5844-4575-8bdd-38a413bf1cc5&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl03%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl03%24hdnUniqueID=b049ffe6-ee7e-4ede-8726-02f6091dc3a9&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl04%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl04%24hdnUniqueID=8280cc60-da64-4abf-b9a4-f745281c5ad4&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl05%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl05%24hdnUniqueID=078ea28a-a968-44f7-a225-93742669e4a2&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl06%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl06%24hdnUniqueID=bad310e5-6540-411a-83a7-3f77bf4fa174&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl07%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl25%24gvwData%24ctl07%24hdnUniqueID=8d866da6-ad80-4e43-8b7a-03ac3de6dd86&__ASYNCPOST=true&";
	my ($content,$status)=&Postcontent($Buyer_Url,$post_content,$host,$Buyer_Url);
	open(ts,">Buyer_content_Post.html");
	print ts "$content";
	close ts;
	top:
	$content=decode_entities($content);
	while ($content=~m/<a href="([^>]*?.xlsx?)">([^>]*?)<\/a>/igs)
	{
		my $Doc_Link="http://www.warwickshirenorthccg.nhs.uk".$1;	
		my $LinkInfo=$2;	
		$LinkInfo=&clean($LinkInfo);
		next if ($LinkInfo!~m/(?:pdf)/is);
		my $LinkInfo_Temp=$LinkInfo;
		my ($File_Date,$File_Month,$File_Year,$Doc_Name,$Doc_Type);
		
		
		# Get Title of the file name
		if($LinkInfo ne '') 
		{	
			$LinkInfo=~s/\s\s+/ /igs;
			print "LinkInfo	: $LinkInfo\n";
			
			if($LinkInfo ~~ @Processed_LinkInfo || $LinkInfo =~ m/^\s*$/)
			{
				print "File Already Processed\n";
				next;
			}
			else
			{			
				print "\nThis is new file to download\n";
			}
		}	
		
		foreach my $monthss(@Months1)	
		{		
			if($LinkInfo_Temp=~m/$monthss/is)
			{
				$File_Month=$monthss;
				if($LinkInfo_Temp=~m/(2\d{3})/is)
				{
					$File_Year=$1;
				}			
				else
				{
					while($LinkInfo_Temp=~m/(\d{2,4})/igs)
					{					
						$File_Year=$1;
					}
				}	
				last;
			}			
		}	
		
		my ($Doc_content,$status_line,$File_Type,$File_Type_cont)=&Getcontent($Doc_Link);
		
		
		if($File_Type=~m/(csv|xlsx|xls|pdf)/is)
		{
			$Doc_Type="$1";
			print "\nI:1\n";
		}
		elsif($File_Type_cont=~m/(csv|pdf)/is)
		{
			$Doc_Type="$1";
			print "\nI:2\n";	
		}
		

		
		$Doc_Type=~s/\"//igs;
		$Doc_Type=~s/\'//igs;
			
		# if($LinkInfo_Temp=~m/(Month\s*\d+)\s(\d+\s*\-\s*\d+)/is)
		# {
			# $File_Month=$1;
			# $File_Year=$2;		
		# }
		my $Month_Of_Document = "$File_Month $File_Year";	
		
		# File name creation from Link Info
		my $File_Type = $Doc_Type if ($Doc_Type ne '');
		$File_Year=~s/\W+//igs;
		$Doc_Name=$LinkInfo;
		$Doc_Name=~s/\W/\_/igs;
		$Doc_Name=~s/_+/\_/igs;
		$Doc_Name.='.'.$File_Type;
		print "#########################################\n";
		print "Doc_Name	: $Doc_Name\n"; 
		print "File_Month		: $File_Month\n";
		print "File_Year		: $File_Year\n";
		print "Doc_Type		: $Doc_Type\n";
		print "Doc_Link	: $Doc_Link\n";
		print "Month_Of_Document	: $Month_Of_Document\n";
		print "#########################################";
		# File Path creation
		# my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source"; 
		my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source";  
		
		# File location creation
		unless ( -d $Filestorepath ) 
		{
			$Filestorepath=~s/\//\\/igs;
			system("mkdir $Filestorepath");
		}
		my $Storefile = "$Filestorepath\\$Doc_Name";
		print "STOREPATH  : $Storefile \n";
		my $StartDate=DateTime->now();
		# Spend file download
		
		my $fh=FileHandle->new("$Storefile",'w');
		binmode($fh);
		$fh->print($Doc_content);
		$fh->close();
		my ($Status,$Failed_Reason);
		if($status_line!~m/20/is)
		{
			$Failed_Reason=$status_line;
		}
		else
		{
			$Status='Downloaded';
		}
		print "\nStatus:::: $Status\n";
		# &BIP_DB_V1::Insert_Pdf_Json_Details($dbh,$Buyer_ID,$Storefile,$LinkInfo,$Month_Of_Document);
		&BIP_Column_Map_V1::ReadSource($Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
		print "Completed \n";
	}
	# if($content=~m/href="javascript:__doPostBack\(\'([^>]*?)\'[^>]*?>\s*[\d]+/is) 	
	# {
		# my $EVENTTARGET=$1;
		# $EVENTTARGET=uri_escape($EVENTTARGET);
		# my ($VIEWSTATE);
		# if ($content=~m/VIEWSTATE\|([^>]*?)\|/is)
		# {
			# $VIEWSTATE = $1;
		# }
		# $VIEWSTATE=uri_escape($VIEWSTATE);
		# my $post_Content='%24ToolkitScriptManager=ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24pnlGrid%7Cctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData&ctl01_ToolkitScriptManager_HiddenField=&ctl01%24txtSearch=Search%20this%20site...&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24hdnLibNav_Folder=&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24txtHiddenRefreshID=&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl02%24hdnItemType=3&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl02%24hdnUniqueID=00000000-0000-0000-0000-000000000000&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl03%24hdnItemType=1&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl03%24hdnUniqueID=10a84513-1271-445a-8a13-cbaa16b7d5a5&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl04%24hdnItemType=1&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl04%24hdnUniqueID=6c3b332f-720c-4a68-9f62-a2137f6c3877&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl05%24hdnItemType=1&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl05%24hdnUniqueID=83b792e7-340d-43c7-b6ce-8742ee6ad1c2&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl06%24hdnItemType=1&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl06%24hdnUniqueID=9a75d58e-8019-4bbf-b2f4-a1cc7c856f20&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl07%24hdnItemType=1&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl07%24hdnUniqueID=74bf4e17-e0c1-4ce1-8020-2759fa2fb0e8&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl08%24hdnItemType=1&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl08%24hdnUniqueID=e367cc17-b707-44d0-ba22-c25fb8c8336f&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl09%24hdnItemType=1&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl09%24hdnUniqueID=3bd7ddbe-8946-4bd6-80c2-9139be29fe79&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl10%24hdnItemType=1&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl10%24hdnUniqueID=f612d5ca-732d-4116-beaa-30e1b6ccd89b&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl11%24hdnItemType=1&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl11%24hdnUniqueID=10558430-d3fc-4522-aa2d-9b608e3fa0fa&__EVENTTARGET='.$EVENTTARGET.'&__EVENTARGUMENT=Page%242&__VIEWSTATE='.$VIEWSTATE.'&__SCROLLPOSITIONX=0&__SCROLLPOSITIONY=0&__ASYNCPOST=true&';
		# ($content,$status)=&Postcontent($Buyer_Url,$post_Content,$host,$Buyer_Url);
		# open(ts,">Buyer_content_nextPost.html");
		# print ts "$content";
		# close ts;
		# print "NExt PAge Here\n";<>;
		# goto top;
	# }
}



sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"");
	# $req->header("Referer"=>"");
	# $req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	print "\nFile_Type---> $File_Type\n";
	print "\nFile_Type_cont---> $File_Type_cont\n";
	# my $get_cookie=$res->header('set-cookie');
	print "\nCODE---> $code\n";
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$File_Type,$File_Type_cont);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("X-MicrosoftAjax"=>"Delta=true");
	$req->header("Referer"=> "https://www.warwickshirenorthccg.nhs.uk/About-Us/Our-spending");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	# print "\nFile_Type---> $File_Type\n";
	# print "\nFile_Type_cont---> $File_Type_cont\n";
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$File_Type,$File_Type_cont);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	
	return $Data;
}

sub clean()
{
	my $C_Clean=shift;
	$C_Clean=~s/<[^>]*?>//igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/\&nbsp\;/ /igs;
	$C_Clean=~s/\&amp\;/ /igs;
	$C_Clean=~s/\&pound\;/ /igs;
	$C_Clean=~s/\&\#8211\;/ /igs;
	$C_Clean=~s/ndash\;/ /igs;
	$C_Clean=~s/[^[:print:]]+//igs;
	
	
	$C_Clean=~s/\&nbsp\;//igs;
	$C_Clean=~s/\#160\;//igs;
	$C_Clean=~s/\#163\;//igs;
	$C_Clean=~s/\&/ /igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	return $C_Clean;
}