package BIP_Column_Map_V1;
### Package Name ####
use BIP_DB_V1;
use DateTime;
use Win32::OLE qw(in with in);
# use Win32::OLE::Const 'Microsoft Excel';
use Win32::OLE::Const;
use Win32::OLE::Variant;
use Win32::OLE::NLS qw(:LOCALE :DATE);
use Text::CSV;
# use Text::CSV_XS;
use Spreadsheet::Read;
use Unicode::String qw(utf8);
use Spreadsheet::ReadSXC qw(read_sxc);
use MIME::Lite;
use MIME::Base64;
use Authen::SASL;
my $err_stmt;
my $err_warn;
$SIG{__DIE__}  = \&die_handler;
$SIG{__WARN__} = \&warn_handler;
our $Buyer_ID1;

###########Database Initialization########
my $dbh = &BIP_DB_V1::DbConnection();
##########################################
sub ReadIniFile()
{
	my $cfg = new Config::IniFiles( -file => 'C:\Perl\lib\BIP_Config.ini', -nocase => 1);
	# my $cfg = new Config::IniFiles( -file => 'BIP_Config.ini', -nocase => 1);
	my %hash_ini;
	my @FileExten = $cfg -> Parameters('FILEPATH');
	foreach my $FileExten (@FileExten)
	{
		my $FileExt =  $cfg -> val(FILEPATH, $FileExten);
		$hash_ini{$FileExten}=$FileExt;
	}
	return(\%hash_ini);
}
sub ReadSource()
{
	my $Buyer_ID		= shift;
	my $Filestorepath	= shift;
	my $Doc_Name		= shift;
	my $LinkInfo		= shift;
	my $Month_Of_Document = shift;
	my $StartDate		= shift;
	my $Status			= shift;
	my $Failed_Reason	= shift;
	$Failed_Reason	= ~s/\'//igs;
	$Buyer_ID1=$Buyer_ID;
	# print "2  Buyer_ID1	:: $Buyer_ID1\n";<STDIN>;
	my ($EndDate,$Number_Of_Records,$Batch_ID,$Mapped_Header_Data_Avail_Column);
	
	$EndDate="";
	$Number_Of_Records=0;
	my $PDF_to_Json_flag=0;
	################ Retrived Column mapping details from Field_Map_Details ###########################
	my ($File_Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field,$Entity_Code) = &BIP_DB_V1::Column_Name_RetrieveUrl($dbh,$Buyer_ID);
	
	my @Headerarray;
	push(@Headerarray,($Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field));
	@Headerarray = grep { $_ ne '' } @Headerarray;
	print "########################## Headerarray ::@Headerarray";

	my $Header_Data_Avail_Column = $#Headerarray+1;
	if($Entity_Field)
	{
		&BIP_DB_V1::Insert_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Buyer_ID,$LinkInfo,$Doc_Name,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);
		#Get Batch ID from BUYERS_DOWNLOAD_DETAILS
		$Batch_ID = &BIP_DB_V1::BatchID_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
		# print "Batch_ID :: $Batch_ID \n";
		my $Source_File_Type=$1 if($Doc_Name=~m/\.(xlsx?|csv|pdf|ods)\s*$/is);
		if($Doc_Name=~m/\.xlsx?\s*$/is)
		{
			($Number_Of_Records,$Mapped_Header_Data_Avail_Column,$Ref_Hash_Buyer,$Ref_Hash_Batch,$Ref_Hash_Count,$Ref_Hash_Source_Count) = &Read_Excel_File($Buyer_ID,$Batch_ID,$Filestorepath,$Doc_Name,$Month_Of_Document,\@Headerarray,$LinkInfo,$Doc_Name,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);
		}
		elsif($Doc_Name=~m/\.csv\s*$/is)
		{
			($Number_Of_Records,$Mapped_Header_Data_Avail_Column,$Ref_Hash_Buyer,$Ref_Hash_Batch,$Ref_Hash_Count,$Ref_Hash_Source_Count) = &Read_Csv_File($Buyer_ID,$Batch_ID,$Filestorepath,$Doc_Name,$Month_Of_Document,\@Headerarray,$LinkInfo,$Doc_Name,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);
		}
		my %Hash_Buyer = %$Ref_Hash_Buyer;
		my %Hash_Batch = %$Ref_Hash_Batch;
		my %Hash_Count = %$Ref_Hash_Count;
		my %Hash_Source_Count = %$Ref_Hash_Source_Count;
		@Hash_Buyer_Value = keys %Hash_Buyer;
		@Hash_Buyer_Value = grep { $_ ne '' } @Hash_Buyer_Value;
		# print "Hash_Buyer_Value :: @Hash_Buyer_Value \n";# <STDIN>;
		foreach my $Key_Entity_Code (@Hash_Buyer_Value)
		{
			my $Buyer_ID = $Hash_Buyer{$Key_Entity_Code};
			my $Batch_ID = $Hash_Batch{$Key_Entity_Code};
			my $Number_Of_Records = $Hash_Source_Count{$Key_Entity_Code};
			my @values_batch_id = values(%Hash_Batch);
			my @keys_batch_id = keys(%Hash_Batch);
			my @matched_batch_id = grep { $Batch_ID =~ /$_/is } @values_batch_id;
			if($#matched_batch_id  > 0)
			{
				$Number_Of_Records = 0;
				foreach $batch_entity_code (@keys_batch_id)
				{
					if($Hash_Batch{$batch_entity_code} == $Batch_ID )
					{
						$Number_Of_Records = $Number_Of_Records + $Hash_Source_Count{$batch_entity_code};
					}
				}
			}
			my $Field_Match='N';
			if($Mapped_Header_Data_Avail_Column==$Header_Data_Avail_Column)
			{
				$Field_Match='Y';
			}
			
			my ($Records_Count_DB)=&BIP_DB_V1::Retrieve_Count_Details($dbh,"SUPPLIER_SOURCE_DATA",$Buyer_ID,$Batch_ID);
			my $Import_Flag='N';
			if($Records_Count_DB==$Number_Of_Records)
			{
				$Import_Flag='Y';
			}
			print "Import_Flag: $Import_Flag\n";
			print "Field_Match: $Field_Match\n";
			$EndDate=DateTime->now();
			$Number_Of_Records = 0 if($Number_Of_Records eq "");
			# To update Supplier_Match, No_of_Suppliers_Matched, No_of_Suppliers_Not_Matched, Formatted_Flag 
			&BIP_DB_V1::Update_Import_Flag($dbh,"BUYERS_DOWNLOAD_DETAILS",$Buyer_ID,$Batch_ID,$Import_Flag,$Field_Match,$EndDate,$Number_Of_Records,$Filestorepath,$Source_File_Type);
			#############
			
		}
		
	}
	else
	{
		&BIP_DB_V1::Insert_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Buyer_ID,$LinkInfo,$Doc_Name,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);
		#Get Batch ID from BUYERS_DOWNLOAD_DETAILS
		$Batch_ID = &BIP_DB_V1::BatchID_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
		print "Batch_ID :: $Batch_ID \n";
		my $Source_File_Type=$1 if($Doc_Name=~m/\.(xlsx?|csv|pdf|ods)\s*$/is);
		if($Doc_Name=~m/\.xlsx?\s*$/is)
		{
			($Number_Of_Records,$Mapped_Header_Data_Avail_Column) = &Read_Excel_File($Buyer_ID,$Batch_ID,$Filestorepath,$Doc_Name,$Month_Of_Document,\@Headerarray);
		}
		elsif($Doc_Name=~m/\.csv\s*$/is)
		{
			($Number_Of_Records,$Mapped_Header_Data_Avail_Column) = &Read_Csv_File($Buyer_ID,$Batch_ID,$Filestorepath,$Doc_Name,$Month_Of_Document,\@Headerarray);
		}
		elsif($Doc_Name=~m/\.pdf\s*$/is)
		{			
			($Number_Of_Records,$Mapped_Header_Data_Avail_Column) = &Read_Json_File($Buyer_ID,$Batch_ID,$Filestorepath,\@Headerarray,$Month_Of_Document);
			$PDF_to_Json_flag=1;
		}
		elsif($Doc_Name=~m/\.ods\s*$/is)
		{			
			($Number_Of_Records,$Mapped_Header_Data_Avail_Column) = &Read_Ods_File($Buyer_ID,$Batch_ID,$Filestorepath,$Doc_Name,$Month_Of_Document,\@Headerarray);
		}
		my $Field_Match='N';
		if($Mapped_Header_Data_Avail_Column==$Header_Data_Avail_Column)
		{
			$Field_Match='Y';
		}
		
		my ($Records_Count_DB)=&BIP_DB_V1::Retrieve_Count_Details($dbh,"SUPPLIER_SOURCE_DATA",$Buyer_ID,$Batch_ID);
		my $Import_Flag='N';
		if($Records_Count_DB==$Number_Of_Records)
		{
			$Import_Flag='Y';
		}
		print "Import_Flag: $Import_Flag\n";
		print "Field_Match: $Field_Match\n";
		$EndDate=DateTime->now();
		# To update Supplier_Match, No_of_Suppliers_Matched, No_of_Suppliers_Not_Matched, Formatted_Flag 
		&BIP_DB_V1::Update_Import_Flag($dbh,"BUYERS_DOWNLOAD_DETAILS",$Buyer_ID,$Batch_ID,$Import_Flag,$Field_Match,$EndDate,$Number_Of_Records,$Filestorepath,$Source_File_Type);
		#############		
	}
	if($PDF_to_Json_flag eq '1')
	{
		return $Number_Of_Records;
	}
}

sub Read_Excel_File
{
	my $Buyer_ID 			= shift;
	my $Batch_ID 			= shift;
	my $Filepath 			= shift;
	my $Docname				= shift;
	my $Month_Of_Document	= shift;
	my $Header_array		= shift;
	my $LinkInfo			= shift;
	my $Doc_Name			= shift;
	my $StartDate			= shift;
	my $EndDate				= shift;
	my $Number_Of_Records	= shift;
	my $Status				= shift;
	my $Failed_Reason		= shift;
	my @Headerarray = @{$Header_array};
	# @Headerarray = grep { $_ ne '' } @Headerarray;
	my $Outfile = "$Filepath\\$Docname";
	print "Excel LOADFILE : $Outfile\n";
	my ($File_Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field,$Entity_Code) = &BIP_DB_V1::Column_Name_RetrieveUrl($dbh,$Buyer_ID);
	my $insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
	my $Book = Win32::OLE->GetObject($Outfile) or die "$!";
	$Book->{'Visible'} = 1;
	$Book->{DisplayAlerts} = 0;
	 # Win32::OLE->LastError(0);
	my $Sheets;
	eval{$Sheets = $Book->Worksheets->Count;};
	
	my $Headercheck = 0;
	my $Sno = 0;
	my %Header_col_position;
	my $insert_collect = 0;
	my (%Hash_buyer,%Hash_batch,%Hash_count,%Hash_Source_Count);
	if ($Entity_Code)
	{
		my $Entity_Code_Temp = $Entity_Code;
		$Entity_Code_Temp =~ s/^\s*\|//igs;
		while($Entity_Code_Temp =~ m/([^<]*?)\|/igs)
		{
			my $Entity_Values = $1;
			$Hash_buyer{$Entity_Values} = $Buyer_ID;
			$Hash_batch{$Entity_Values} = $Batch_ID;
		}
		
	}
	# print "Headerarray :: @Headerarray\n";
	foreach my $Sheet (1 .. $Sheets)
	{
		my $eSheet = $Book->Worksheets($Sheet);
		my $Rowcount = $eSheet->UsedRange->Rows->{'Count'}; 

		my $Colcount = $eSheet->UsedRange->Columns->{'Count'}; 
		foreach my $Row (1 .. $Rowcount)
		{
			my ($Transaction_ID_Value,$Requirement_Value,$Amount_Value,$Start_Date_Value,$End_Date_Value,$Supplier_Name_Value,$Department_Name_Value,$Requirement_2_Value,$Entity_Field_Value);
			# print "HEADER : $Headercheck\n";
			if ( $Headercheck == 0 )
			{
				foreach my $Col (1 .. $Colcount)
				{
					my $Value = &Trim($eSheet->Cells($Row,$Col)->{'Value'});
					my @Header = grep { $Value =~ /$_/is } @Headerarray;
					# print "HEADER :: $Value ::  @Header \n";
					$Header_col_position{$Header[0]} = $Col if (@Header);
				}
				
				if ($Header_col_position{$Transaction_ID}||$Header_col_position{$Requirement}||$Header_col_position{$Amount}||$Header_col_position{$Start_Date}||$Header_col_position{$End_Date}||$Header_col_position{$Supplier_Name}||$Header_col_position{$Department_Name}||$Header_col_position{$Requirement_2})
				{
					$Headercheck = 1;
				}
				
			}
			else
			{
				# exit;
				$Transaction_ID_Value 	= &Trim($eSheet->Cells($Row,$Header_col_position{$Transaction_ID})->{'Value'}) if ($Header_col_position{$Transaction_ID} ne '');
				$Requirement_Value 		= &Trim($eSheet->Cells($Row,$Header_col_position{$Requirement})->{'Value'}) if ($Header_col_position{$Requirement} ne '');
				$Amount_Value 			= &Trim($eSheet->Cells($Row,$Header_col_position{$Amount})->{'Value'}) if ($Header_col_position{$Amount} ne '');
				$Start_Date_Value 		= &Trim($eSheet->Cells($Row,$Header_col_position{$Start_Date})->{'Text'}) if ($Header_col_position{$Start_Date} ne '');
				$End_Date_Value 		= &Trim($eSheet->Cells($Row,$Header_col_position{$End_Date})->{'Text'}) if ($Header_col_position{$End_Date} ne '');
				$Supplier_Name_Value 	= &Trim($eSheet->Cells($Row,$Header_col_position{$Supplier_Name})->{'Value'}) if ($Header_col_position{$Supplier_Name} ne '');
				$Department_Name_Value 	= &Trim($eSheet->Cells($Row,$Header_col_position{$Department_Name})->{'Value'}) if ($Header_col_position{$Department_Name} ne '');
				$Requirement_2_Value	= &Trim($eSheet->Cells($Row,$Header_col_position{$Requirement_2})->{'Value'}) if ($Header_col_position{$Requirement_2} ne '');
				$Entity_Field_Value  	= &Trim($eSheet->Cells($Row,$Header_col_position{$Entity_Field})->{'Value'}) if ($Header_col_position{$Entity_Field} ne '');
				if($Start_Date_Value =~ m/\#/is)
				{
					$Start_Date_Value   = &Trim($eSheet->Cells($Row,$Header_col_position{$Start_Date})->{'Value'}) if ($Header_col_position{$Start_Date} ne '');
				}
				if($End_Date_Value =~ m/\#/is)
				{
					$End_Date_Value     = &Trim($eSheet->Cells($Row,$Header_col_position{$End_Date})->{'Value'}) if ($Header_col_position{$End_Date} ne '');
				}
				if($Entity_Field)
				{
					unless($Hash_buyer{$Entity_Field_Value})
					{
						my ($Ret_Buyer_ID,$Ret_Entity_Code) = &BIP_DB_V1::RetrieveBuyerID($dbh,"field_map_details_automation",$Buyer_ID,$Entity_Field_Value);
						# print "Ret_Buyer_ID :: $Ret_Buyer_ID \n";
						if($Ret_Buyer_ID)
						{
							&BIP_DB_V1::Insert_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Ret_Buyer_ID,$LinkInfo,$Doc_Name,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);
							$Batch_ID = &BIP_DB_V1::BatchID_RetrieveUrl($dbh,$Ret_Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
							# print "Batch_ID :: $Entity_Field_Value ::  $Batch_ID \n";# <STDIN>;
							if ($Ret_Entity_Code)
							{
								my $Entity_Code_Temp = $Ret_Entity_Code;
								$Entity_Code_Temp =~ s/^\s*\|//igs;
								while($Entity_Code_Temp =~ m/([^<]*?)\|/igs)
								{
									my $Entity_Values = $1;
									$Hash_buyer{$Entity_Values} = $Ret_Buyer_ID;
									$Hash_batch{$Entity_Values} = $Batch_ID;
								}	
							}
						}
						else
						{
							$Hash_buyer{$Entity_Field_Value} = '-';
							$Hash_batch{$Entity_Field_Value} = '-';
						}
					}
					next if($Hash_buyer{$Entity_Field_Value} eq '-');
					if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
					{
						$insert_collect++;
						$Transaction_ID_Value=~s/\'/\'\'/igs;
						$Supplier_Name_Value=~s/\'/\'\'/igs;
						$Department_Name_Value=~s/\'/\'\'/igs;
						$Requirement_Value=~s/\'/\'\'/igs;
						$Requirement_2_Value=~s/\'/\'\'/igs;
						$Hash_count{$Entity_Field_Value}++;
						$Hash_Source_Count{$Entity_Field_Value}++;
						if($insert_collect <= 990)
						{
						$insert_query .= "(\'$Hash_buyer{$Entity_Field_Value}\',\'$Hash_batch{$Entity_Field_Value}\',\'$Month_Of_Document\',date(now()),\'$Hash_count{$Entity_Field_Value}\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
						}
						else
						{
							$insert_query .= "(\'$Hash_buyer{$Entity_Field_Value}\',\'$Hash_batch{$Entity_Field_Value}\',\'$Month_Of_Document\',date(now()),\'$Hash_count{$Entity_Field_Value}\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
							$insert_query =~ s/\,\s*$//igs;
							$insert_query .= ';';
							&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
							$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
							$insert_collect = 0;
						}
					}
				}
				else
				{
					if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
					{
						$Sno++;
						$insert_collect++;
						$Transaction_ID_Value=~s/\'/\'\'/igs;
						$Supplier_Name_Value=~s/\'/\'\'/igs;
						$Department_Name_Value=~s/\'/\'\'/igs;
						$Requirement_Value=~s/\'/\'\'/igs;
						$Requirement_2_Value=~s/\'/\'\'/igs;
						if($insert_collect <= 990)
						{
						$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
						}
						else
						{
							$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
							$insert_query =~ s/\,\s*$//igs;
							$insert_query .= ';';
							&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
							$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
							$insert_collect = 0;
						}
					}
				}
			}
		}
	}
	$Book->quit();
	if($insert_collect >= 1)
	{
		$insert_query =~ s/\,\s*$//igs;
		$insert_query .= ';';
		&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
	}
	@Value = values %Header_col_position;
	@Value = grep { $_ ne '' } @Value;
	$Mapped_Col = $#Value+1;
	return ($Sno,$Mapped_Col,\%Hash_buyer,\%Hash_batch,\%Hash_count,\%Hash_Source_Count);
}

sub Read_Csv_File
{
	my $Buyer_ID 			= shift;
	my $Batch_ID 			= shift;
	my $Filepath 			= shift;
	my $Docname				= shift;
	my $Month_Of_Document	= shift;
	my $Header_array		= shift;
	my $LinkInfo			= shift;
	my $Doc_Name			= shift;
	my $StartDate			= shift;
	my $EndDate				= shift;
	my $Number_Of_Records	= shift;
	my $Status				= shift;
	my $Failed_Reason		= shift;
	my @Headerarray = @{$Header_array};
	
	my $Outfile = "$Filepath\\$Docname";
	my ($File_Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field,$Entity_Code) = &BIP_DB_V1::Column_Name_RetrieveUrl($dbh,$Buyer_ID);
	my $insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
	my $Headercheck = 0;
	my $Sno = 0;
	my $insert_collect = 0;
	my %Header_col_position;
	my (%Hash_buyer,%Hash_batch,%Hash_count,%Hash_Source_Count);
	print "Entity_Code	:: $Entity_Code\n";
	if ($Entity_Code)
	{
		my $Entity_Code_Temp = $Entity_Code;
		$Entity_Code_Temp =~ s/^\s*\|//igs;
		while($Entity_Code_Temp =~ m/([^<]*?)\|/igs)
		{
			my $Entity_Values = $1;
			print "EntityMAjor	:: $Entity_Values\n";
			$Hash_buyer{$Entity_Values} = $Buyer_ID;
			$Hash_batch{$Entity_Values} = $Batch_ID;
		}
	}
	
	# my $csv = Text::CSV_XS->new ({
	my $csv = Text::CSV->new ({
	binary => 1,
	allow_loose_quotes  => 1,
	allow_loose_escapes => 1,
	auto_diag => 1,
	escape_char => "\"",
	sep_char => ',',
	verbatim => 1
	});           
	$csv->verbatim (0);
	my $sum = 0;
	my $Valid_Records=0;
	open(my $data, '<', "$Outfile") or die "Could not open '$Doc_Name' $!\n";
	while (my $fields = $csv->getline($data)) 
	{
		my @fieldsarray = @{$fields};
		my ($Transaction_ID_Value,$Requirement_Value,$Amount_Value,$Start_Date_Value,$End_Date_Value,$Supplier_Name_Value,$Department_Name_Value,$Requirement_2_Value,$Entity_Field_Value);
		if ( $Headercheck == 0 )
		{
			foreach my $Col (0 .. $#fieldsarray)
			{
				my @Header = grep { $fieldsarray[$Col] =~ /$_/is } @Headerarray;
				print "HEADER :: @Header :: $Col \n";# 
				$Header_col_position{$Header[0]} = $Col if (@Header);
			}
			
			if ($Header_col_position{$Transaction_ID}||$Header_col_position{$Requirement}||$Header_col_position{$Amount}||$Header_col_position{$Start_Date}||$Header_col_position{$End_Date}||$Header_col_position{$Supplier_Name}||$Header_col_position{$Department_Name}||$Header_col_position{$Requirement_2})
			{
				$Headercheck = 1;
			}
		}
		else
		{
			$Transaction_ID_Value 	= &Trim($fieldsarray[$Header_col_position{$Transaction_ID}]) if ($Header_col_position{$Transaction_ID} ne '');
			$Requirement_Value 		= &Trim($fieldsarray[$Header_col_position{$Requirement}]) if ($Header_col_position{$Requirement} ne '');
			$Amount_Value 			= &Trim($fieldsarray[$Header_col_position{$Amount}]) if ($Header_col_position{$Amount} ne '');
			$Start_Date_Value 		= &Trim($fieldsarray[$Header_col_position{$Start_Date}]) if ($Header_col_position{$Start_Date} ne '');
			$End_Date_Value 		= &Trim($fieldsarray[$Header_col_position{$End_Date}]) if ($Header_col_position{$End_Date} ne '');
			$Supplier_Name_Value 	= &Trim($fieldsarray[$Header_col_position{$Supplier_Name}]) if ($Header_col_position{$Supplier_Name} ne '');
			$Department_Name_Value 	= &Trim($fieldsarray[$Header_col_position{$Department_Name}]) if ($Header_col_position{$Department_Name} ne '');
			$Requirement_2_Value	= &Trim($fieldsarray[$Header_col_position{$Requirement_2}]) if ($Header_col_position{$Requirement_2} ne '');
			$Entity_Field_Value		= &Trim($fieldsarray[$Header_col_position{$Entity_Field}]) if ($Header_col_position{$Entity_Field} ne '');
			
			if($Entity_Field)
			{
				unless($Hash_buyer{$Entity_Field_Value})
				{
					my ($Ret_Buyer_ID,$Ret_Entity_Code) = &BIP_DB_V1::RetrieveBuyerID($dbh,"field_map_details_automation",$Buyer_ID,$Entity_Field_Value);
					# print "Ret_Buyer_ID :: $Ret_Buyer_ID :: $Ret_Entity_Code\n";
					if($Ret_Buyer_ID)
					{
						&BIP_DB_V1::Insert_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Ret_Buyer_ID,$LinkInfo,$Doc_Name,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);
						$Batch_ID = &BIP_DB_V1::BatchID_RetrieveUrl($dbh,$Ret_Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
						# print "Batch_ID :: $Entity_Field_Value ::  $Batch_ID \n";
						if ($Ret_Entity_Code)
						{
							my $Entity_Code_Temp = $Ret_Entity_Code;
							$Entity_Code_Temp =~ s/^\s*\|//igs;
							while($Entity_Code_Temp =~ m/([^<]*?)\|/igs)
							{
								my $Entity_Values = $1;
								$Hash_buyer{$Entity_Values} = $Ret_Buyer_ID;
								$Hash_batch{$Entity_Values} = $Batch_ID;
								# print "Batch_ID :: $Entity_Values ::  $Batch_ID ::  $Ret_Buyer_ID \n";
							}	
						}
					}
					else
					{
						$Hash_buyer{$Entity_Field_Value} = '-';
						$Hash_batch{$Entity_Field_Value} = '-';
					}
				}
				# print "Entity_Code###### $Hash_batch{$Entity_Field_Value}\n";
				next if($Hash_buyer{$Entity_Field_Value} eq '-');
				if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
				{
					$insert_collect++;
					$Transaction_ID_Value=~s/\'/\'\'/igs;
					$Supplier_Name_Value=~s/\\\'/\'/igs;
					$Supplier_Name_Value=~s/\'/\'\'/igs;
					$Department_Name_Value=~s/\'/\'\'/igs;
					$Requirement_Value=~s/\'/\'\'/igs;
					$Requirement_2_Value=~s/\'/\'\'/igs;
					$Hash_count{$Entity_Field_Value}++;
					$Hash_Source_Count{$Entity_Field_Value}++;
					# print "Hash_count: $Entity_Field_Value:  $Hash_count{$Entity_Field_Value}\n";
					# print "Hash_Source_Count: $Entity_Field_Value:  $Hash_Source_Count{$Entity_Field_Value}\n";
					if($insert_collect <= 990)
					{
						$insert_query .= "(\'$Hash_buyer{$Entity_Field_Value}\',\'$Hash_batch{$Entity_Field_Value}\',\'$Month_Of_Document\',date(now()),\'$Hash_count{$Entity_Field_Value}\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
					}
					else
					{
						$insert_query .= "(\'$Hash_buyer{$Entity_Field_Value}\',\'$Hash_batch{$Entity_Field_Value}\',\'$Month_Of_Document\',date(now()),\'$Hash_count{$Entity_Field_Value}\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
						$insert_query =~ s/\,\s*$//igs;
						$insert_query .= ';';
						&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
						$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
						$insert_collect = 0;
					}
				}
			}
			else
			{
				# print "ELSE Ret_Buyer_ID :: $Ret_Buyer_ID \n";
				if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
				{
					$Sno++;
					$insert_collect++;
					$Transaction_ID_Value=~s/\\\'/\'/igs;
					$Transaction_ID_Value=~s/\'/\'\'/igs;
					$Supplier_Name_Value=~s/\\\'/\'/igs;
					$Supplier_Name_Value=~s/\'/\'\'/igs;
					$Department_Name_Value=~s/\\\'/\'/igs;
					$Department_Name_Value=~s/\'/\'\'/igs;
					$Requirement_Value=~s/\\\'/\'/igs;
					$Requirement_Value=~s/\'/\'\'/igs;
					$Requirement_2_Value=~s/\\\'/\'/igs;
					$Requirement_2_Value=~s/\'/\'\'/igs;
					if($insert_collect <= 990)
					{
					$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
					}
					else
					{
						$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
						$insert_query =~ s/\,\s*$//igs;
						$insert_query .= ';';
						&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
						$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
						$insert_collect = 0;
					}
				}
			}
		}
	}
	if(not $csv->eof)
	{
		$csv->error_diag();
	}
	close $data;
	
	if($insert_collect >= 1)
	{
		$insert_query =~ s/\,\s*$//igs;
		$insert_query .= ';';
		&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
	}
	@Value = values %Header_col_position;
	print "###################################Value :: @Value\n";
	@Value = grep { $_ ne '' } @Value;
	$Mapped_Col = $#Value+1;
	# return ($Sno,$Mapped_Col);
	return ($Sno,$Mapped_Col,\%Hash_buyer,\%Hash_batch,\%Hash_count,\%Hash_Source_Count);
}

sub Read_Json_File()
{
	my $Buyer_ID 			= shift;
	my $Batch_ID 			= shift;
	my $JSON_NAME 			= shift;
	my $Header_array		= shift;
	my $Month_Of_Document	= shift;
	my @Headerarray = @{$Header_array};
	print "JSON LOADFILE : $JSON_NAME\n";
	
	my $Outfile=$JSON_NAME;
	if($Outfile eq '' && $Outfile!~m/\.json/igs && !(-e $Outfile))
	{
		print "\nPlease select correct file !!! file extenstion ofr file type is incorrect please check\n";
		exit;
	}
	my ($File_Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field,$Entity_Code) = &BIP_DB_V1::Column_Name_RetrieveUrl($dbh,$Buyer_ID);
	my $insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
	
	my $Headercheck = 0;
	my $Sno = 0;
	my $insert_collect = 0;
	my %Header_col_position;
	my (%Hash_buyer,%Hash_batch,%Hash_count,%Hash_Source_Count);
	my $sum = 0;
	my $Valid_Records=0;
	
	my $json;
	{
	  local $/; #Enable 'slurp' mode
	  open my $fh, "<", "$Outfile";
	  $json = <$fh>;
	  close $fh;
	}
	my $data=$json;
	my $Remove_Same_header_Check;
	while($data=~m/\[(\"[^>]*?\")\](?:\,|\])/igs)
	{
		my $first=$1;
		next if($first=~m/$Remove_Same_header_Check/is);
		next if($first!~m/\"\,\"/is);
		print "\nfirst mani data ---->$first \n";
		my @Field_Header=split("\"\,\"",$first);
		# my @fieldsarray = map { $_ =~ s/(\"|\/|\(|\)|\(|\))//igs; $_ } @Field_Header;
		my @fieldsarray = map { $_ =~ s/\"//igs; $_ } @Field_Header;
		# print join("\n",@Field_Header);# <STDIN>;
		
		my ($Transaction_ID_Value,$Requirement_Value,$Amount_Value,$Start_Date_Value,$End_Date_Value,$Supplier_Name_Value,$Department_Name_Value,$Requirement_2_Value,$Entity_Field_Value);
		if ($Headercheck == 0)
		{
			foreach my $Col (0 .. $#fieldsarray)
			{
				my @Header = grep { $fieldsarray[$Col] =~ /$_/is } @Headerarray;
				# print "HEADER :: @Header :: $Col \n";# <STDIN>;
				$Header_col_position{$Header[0]} = $Col if (@Header);
			}
			# print "\nsuccess\n";exit;
			if ($Header_col_position{$Transaction_ID}||$Header_col_position{$Requirement}||$Header_col_position{$Amount}||$Header_col_position{$Start_Date}||$Header_col_position{$End_Date}||$Header_col_position{$Supplier_Name}||$Header_col_position{$Department_Name}||$Header_col_position{$Requirement_2})
			{
				$Headercheck = 1;
				$Remove_Same_header_Check=$first;
				# print "\nsuccess\n";exit;
			}
		}	
		else
		{
			# print "Amount Before 	:: $fieldsarray[$Header_col_position{$Amount}]\n";
			$Transaction_ID_Value 	= &Trim($fieldsarray[$Header_col_position{$Transaction_ID}]) if ($Header_col_position{$Transaction_ID} ne '');
			$Requirement_Value 		= &Trim($fieldsarray[$Header_col_position{$Requirement}]) if ($Header_col_position{$Requirement} ne '');
			$Amount_Value 			= &Trim($fieldsarray[$Header_col_position{$Amount}]) if ($Header_col_position{$Amount} ne '');
			$Start_Date_Value 		= &Trim($fieldsarray[$Header_col_position{$Start_Date}]) if ($Header_col_position{$Start_Date} ne '');
			$End_Date_Value 		= &Trim($fieldsarray[$Header_col_position{$End_Date}]) if ($Header_col_position{$End_Date} ne '');
			$Supplier_Name_Value 	= &Trim($fieldsarray[$Header_col_position{$Supplier_Name}]) if ($Header_col_position{$Supplier_Name} ne '');
			$Department_Name_Value 	= &Trim($fieldsarray[$Header_col_position{$Department_Name}]) if ($Header_col_position{$Department_Name} ne '');
			$Requirement_2_Value	= &Trim($fieldsarray[$Header_col_position{$Requirement_2}]) if ($Header_col_position{$Requirement_2} ne '');
			$Entity_Field_Value	= &Trim($fieldsarray[$Header_col_position{$Entity_Field}]) if ($Header_col_position{$Entity_Field} ne '');	
			print			"\nTransaction_ID_Value--->$Transaction_ID_Value\n";
			print			"\nAmount_Value--->$Amount_Value\n";
			
			if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
			{
				$Sno++;
				$insert_collect++;
				$Transaction_ID_Value=~s/\'/\'\'/igs;
				$Supplier_Name_Value=~s/\\\'/\'/igs;
				$Supplier_Name_Value=~s/\'/\'\'/igs;
				$Department_Name_Value=~s/\'/\'\'/igs;
				$Requirement_Value=~s/\'/\'\'/igs;
				$Requirement_2_Value=~s/\'/\'\'/igs;
				if($insert_collect <= 990)
				{
				$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
				}
				else
				{
					$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
					$insert_query =~ s/\,\s*$//igs;
					$insert_query .= ';';
					&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
					$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values";
					$insert_collect = 0;
				}
			}					
		}		
	}
	if($insert_collect >= 1)
	{
		$insert_query =~ s/\,\s*$//igs;
		$insert_query .= ';';
		&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
	}
	print "\nI am in last\n";
	my @Value = values %Header_col_position;
	print "###################################Value :: @Value\n";
	@Value = grep { $_ ne '' } @Value;
	my $Mapped_Col = $#Value+1;
	# return ($Sno,$Mapped_Col);
	return ($Sno,$Mapped_Col,\%Hash_buyer,\%Hash_batch,\%Hash_count,\%Hash_Source_Count);
}
sub Read_Ods_File()
{
	my $Buyer_ID 			= shift;
	my $Batch_ID 			= shift;
	my $Filepath 			= shift;
	my $Docname				= shift;
	my $Month_Of_Document	= shift;
	my $Header_array		= shift;
	my $LinkInfo			= shift;
	my $Doc_Name			= shift;
	my $StartDate			= shift;
	my $EndDate				= shift;
	my $Number_Of_Records	= shift;
	my $Status				= shift;
	my $Failed_Reason		= shift;
	my @Headerarray = @{$Header_array};
	
	my $Outfile = "$Filepath\\$Docname";
	print "ODS LOADFILE : $Outfile\n";
	my ($File_Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field,$Entity_Code) = &BIP_DB_V1::Column_Name_RetrieveUrl($dbh,$Buyer_ID);
	my $insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
	my $Headercheck = 0;
	my $Sno = 0;
	my $insert_collect = 0;
	my %Header_col_position;
	my (%Hash_buyer,%Hash_batch,%Hash_count,%Hash_Source_Count);
	
	my %options = (
        ReplaceNewlineWith      => "",
        IncludeCoveredCells     => 0,
        DropHiddenRows          => 0,
        DropHiddenColumns       => 0,
        NoTruncate              => 0,
        StandardDate            => 0,
        StandardTime            => 0,
        OrderBySheet            => 0,
	);
	my $Workbook = read_sxc("$Outfile", \%options );
	foreach ( sort keys %$Workbook ) 
	{
		$#{$$Workbook{$_}} + 1;
		my $RowCount = $#{$$Workbook{$_}} + 1;
		# print "RowCount	:: $RowCount";
		foreach ( @{$$Workbook{$_}} ) 
		{
			my @fieldsarray=map { defined $_ ? $_ : '' } @{$_};
			my ($Transaction_ID_Value,$Requirement_Value,$Amount_Value,$Start_Date_Value,$End_Date_Value,$Supplier_Name_Value,$Department_Name_Value,$Requirement_2_Value,$Entity_Field_Value);
			if ($Headercheck == 0)
			{
				foreach my $Col (0 .. $#fieldsarray)
				{
					my @Header = grep { $fieldsarray[$Col] =~ /$_/is } @Headerarray;
					# print "HEADER :: @Header :: $Col \n";# <STDIN>;
					$Header_col_position{$Header[0]} = $Col if (@Header);
				}
				if ($Header_col_position{$Transaction_ID}||$Header_col_position{$Requirement}||$Header_col_position{$Amount}||$Header_col_position{$Start_Date}||$Header_col_position{$End_Date}||$Header_col_position{$Supplier_Name}||$Header_col_position{$Department_Name}||$Header_col_position{$Requirement_2})
				{
					$Headercheck = 1;
				}
			}	
			else
			{
				$Transaction_ID_Value 	= &Trim($fieldsarray[$Header_col_position{$Transaction_ID}]) if ($Header_col_position{$Transaction_ID} ne '');
				$Requirement_Value 		= &Trim($fieldsarray[$Header_col_position{$Requirement}]) if ($Header_col_position{$Requirement} ne '');
				$Amount_Value 			= &Trim($fieldsarray[$Header_col_position{$Amount}]) if ($Header_col_position{$Amount} ne '');
				$Start_Date_Value 		= &Trim($fieldsarray[$Header_col_position{$Start_Date}]) if ($Header_col_position{$Start_Date} ne '');
				$End_Date_Value 		= &Trim($fieldsarray[$Header_col_position{$End_Date}]) if ($Header_col_position{$End_Date} ne '');
				$Supplier_Name_Value 	= &Trim($fieldsarray[$Header_col_position{$Supplier_Name}]) if ($Header_col_position{$Supplier_Name} ne '');
				$Department_Name_Value 	= &Trim($fieldsarray[$Header_col_position{$Department_Name}]) if ($Header_col_position{$Department_Name} ne '');
				$Requirement_2_Value	= &Trim($fieldsarray[$Header_col_position{$Requirement_2}]) if ($Header_col_position{$Requirement_2} ne '');
				$Entity_Field_Value	= &Trim($fieldsarray[$Header_col_position{$Entity_Field}]) if ($Header_col_position{$Entity_Field} ne '');		
				if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
				{
					$Sno++;
					$insert_collect++;
					$Transaction_ID_Value=~s/\'/\'\'/igs;
					$Supplier_Name_Value=~s/\'/\'\'/igs;
					$Department_Name_Value=~s/\'/\'\'/igs;
					$Requirement_Value=~s/\'/\'\'/igs;
					$Requirement_2_Value=~s/\'/\'\'/igs;
					if($insert_collect <= 990)
					{
					$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
					}
					else
					{
						$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
						$insert_query =~ s/\,\s*$//igs;
						$insert_query .= ';';
						&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
						$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values";
						$insert_collect = 0;
					}
				}					
			}
		}	
	}
	if($insert_collect >= 1)
	{
		$insert_query =~ s/\,\s*$//igs;
		$insert_query .= ';';
		&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
	}
	my @Value = values %Header_col_position;
	print "###################################Value :: @Value\n";
	@Value = grep { $_ ne '' } @Value;
	my $Mapped_Col = $#Value+1;
	return ($Sno,$Mapped_Col);
	return ($Sno,$Mapped_Col,\%Hash_buyer,\%Hash_batch,\%Hash_count,\%Hash_Source_Count);
}

sub Trim()
{
	$txt = shift;
	my $Tab=chr(9);
	my $Linefeed=chr(10);
	my $Carr_Return=chr(13);
	$txt =~s/^\s+|\s+$//g;
	$txt =~s/$Tab/ /igs;
	$txt =~s/$Linefeed/ /igs;
	$txt =~s/$Carr_Return/ /igs;
	$txt =~s/\s\s+/ /igs;
	return $txt;
}

sub die_handler()
{
	my $err_stmt = shift;	
	my $Buyer_ID = shift;
	$err_stmt = $err_stmt . $_ foreach (@_);
	print "Error	:: $err_stmt\n"; 
	$err_stmt =~s/\n/ /igs;
	if ($err_stmt=~m/Can\'t\s*locate\s*Authen\/SASL\//is)
	{
		next;
	}
	my $TYPE = 'Error';
	&send_mail($err_stmt,$TYPE,$Buyer_ID);
	print "Err: $err_stmt\n";
	open FH,">>Error.txt";
	print FH $err_stmt;
	close FH;
}
sub warn_handler()
{
	my $err_warn = shift;	
	my $Buyer_ID = shift;
	$err_warn = $err_warn . $_ foreach (@_);
	print "Error	:: $err_warn\n"; 
	$err_warn =~s/\n/ /igs;
	if ($err_warn=~m/Can\'t\s*locate\s*Authen\/SASL\//is)
	{
		next;
	}
	my $TYPE = 'Warning';
	&send_mail($err_warn,$TYPE,$Buyer_ID);
	print "Err: $err_warn\n";
	open FH,">>Error.txt";
	print FH "$err_warn\n";
	close FH;
}

sub send_mail()
{
	my $error	 = shift;
	my $type 	 = shift;	
	my $Buyer_ID = shift;
	
	my $hash_ref1 = &ReadIniFile_Mail();
	my %inihashvalue1 = %{$hash_ref1};
	
	my $subject="Failed Buyer - $Buyer_ID";
	# my $host =$inihashvalue1{'host'}; 
	# my $from=$inihashvalue1{'from'};
	# my $user=$inihashvalue1{'user'};
	# my $to =$inihashvalue1{'to'};
	# my $pass=$inihashvalue1{'password'};
	my $host ='74.80.234.196'; 
	my $from='autoemailsender@meritgroup.co.uk';
	my $user='meritgroup';
	my $to ='balaji.ekambaram@meritgroup.co.uk';
	my $pass='sXNdrc6JU';
	my $body = "Hi Balaji, <br><br>BUYER ID: <b>$Buyer_ID<\/b>\t\tPlease find the below Issue<br><br>"."<b>$type<\/b> : $error"."<br><br>Regards<br>BIP TEAM";
	
	my $msg = MIME::Lite->new (
	  From => $from,
	  To => $to,
	  # Cc => $cc,
	  Subject => $subject,
	  Data => $body,
	  Type =>'text/html'
	) or die "Error creating multipart container: $!\n";
    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	$msg->send;
}

sub Link_Status_send_mail()
{
	my $error	 = shift;
	my $type 	 = shift;	
	my $Buyer_ID = shift;
	
	my $hash_ref1 = &ReadIniFile_Mail();
	my %inihashvalue1 = %{$hash_ref1};
	
	my $subject="Link Not Working - $Buyer_ID";
	# my $host =$inihashvalue1{'host'}; 
	# my $from=$inihashvalue1{'from'};
	# my $user=$inihashvalue1{'user'};
	# my $to =$inihashvalue1{'to'};
	# my $pass=$inihashvalue1{'password'};
	my $host ='74.80.234.196'; 
	my $from='autoemailsender@meritgroup.co.uk';
	my $user='meritgroup';
	my $to ='balaji.ekambaram@meritgroup.co.uk';
	my $pass='sXNdrc6JU';
	my $body = "Hi Balaji, <br><br>BUYER ID: <b>$Buyer_ID<\/b>\t\tPlease find the below Issue<br><br>"."<b>$type<\/b> : $error"."<br><br>Regards<br>BIP TEAM";
	
	my $msg = MIME::Lite->new (
	  From => $from,
	  To => $to,
	  # Cc => $cc,
	  Subject => $subject,
	  Data => $body,
	  Type =>'text/html'
	) or die "Error creating multipart container: $!\n";
    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	$msg->send;
}

sub ReadIniFile_Mail()
{
	my $cfg = new Config::IniFiles( -file => 'C:\Perl\lib\BIP_Config.ini', -nocase => 1);
	# my $cfg = new Config::IniFiles( -file => 'BIP_Config.ini', -nocase => 1);
	my %hash_ini1;
	my @FileExten = $cfg -> Parameters('MAILDETAILS');
	foreach my $FileExten (@FileExten)
	{
		my $FileExt =  $cfg -> val("MAILDETAILS", $FileExten);
		$hash_ini1{$FileExten}=$FileExt;
	}
	return(\%hash_ini1);
}

sub Link_Status_Update()
{
	my $dbh   		= shift;
	my $Buyer_ID   	= shift;
	my $Status_Line	= shift;
	$Status_Line	=~s/\'//igs;
	my $query = "update buyers set Link_Status=\'$Status_Line\' where ID=\'$Buyer_ID\'";	
	my $sth = $dbh->prepare($query);
	$sth->execute();	
	$sth->finish();	
	if($Status_Line!~m/200/is)
	{

		&Link_Status_send_mail($Status_Line,"Link Not working",$Buyer_ID);
		
}
	
}
