use strict;
use LWP::UserAgent;
use Time::Local qw(timelocal_nocheck);
use DateTime;
use LWP::Simple;

my $ua=LWP::UserAgent->new(show_progress=>0);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:42.0) Gecko/20100101 Firefox/42.0");

my $url='https://www.cafcass.gov.uk/about-cafcass/procurement/transparency-of-contracts/spend-information.aspx';
my $req = HTTP::Request->new(GET=>$url);
$req->header("Content-Type"=> "application/x-www-form-urlencoded; charset=UTF-8");
# $req->header("Host"=> "mantova.bakeca.it");
# $req->header("Referer"=> "http://mantova.bakeca.it/dettaglio/auto/audi-a3-spb-16-9nqx117116052");
# $req->content("&idpub=9nqx117116052&idvetrina=10034&annuncicliente=1&categoria=auto&numeroannunci=6&layout=sidebar&categoria=auto");
my $res = $ua->request($req);
my $code=$res->code;
print "Code: $code\n";
my $status_line=$res->status_line;
print "Code: $status_line\n";
my $content = $res->content;
open(FI,">cafcass.html");
print FI $content;
close FI;