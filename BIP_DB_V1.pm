package BIP_DB_V1;
### Package Name ####
use strict;
use DBI;
use DBD::ODBC;
require Exporter;
use Config::IniFiles;
my @ISA = qw(Exporter);
my @EXPORT = qw(ImageDownload);

###### DB Connection ####
sub DbConnection()
{
	my $cfg = new Config::IniFiles( -file => 'C:\Perl\lib\BIP_Config.ini', -nocase => 1);
	my %hash_ini;
	my @FileExten = $cfg -> Parameters('DBDETAILS');
	foreach my $FileExten (@FileExten)
	{
		my $FileExt =  $cfg -> val('DBDETAILS', $FileExten);
		$hash_ini{$FileExten}=$FileExt;
	}
	my $database_name = $hash_ini{'database_name'};
	my $server_name = $hash_ini{'server_name'};
	my $database_user = $hash_ini{'database_user'};
	my $database_pass = $hash_ini{'database_pass'};
	my $driver = "mysql"; 
	my $dsn = "DBI:$driver:database=$database_name;host=$server_name;";
	my $dbh = DBI->connect($dsn, $database_user, $database_pass ) or die $DBI::errstr;
	return $dbh;
}

###### Retrieve DocumentURL ####
sub RetrieveUrl()
{
	my $dbh 		= shift;
	my $Buyer_ID 	= shift;
	my $Input_Table	= shift;
	
	my $query = "select ID,Buyer_Name,Spent_Data_weblink from $Input_Table  where ID=\'$Buyer_ID\'";
	
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@ID,@Buyer_Name,@Spent_Data_weblink,@End_Date,@Type_of_Government,@Expressions,@Request_method,@Host,@Referer,@Hidden_Content,@File_Type,@Robot_Name);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@ID,Trim($record[0]));
		push(@Buyer_Name,Trim($record[1]));
		push(@Spent_Data_weblink,Trim($record[2]));
		push(@End_Date,Trim($record[3]));
		push(@Type_of_Government,Trim($record[4]));
		push(@Expressions,Trim($record[5]));
		push(@Request_method,Trim($record[6]));
		push(@Host,Trim($record[7]));
		push(@Referer,Trim($record[8]));
		push(@Hidden_Content,Trim($record[9]));
		push(@File_Type,Trim($record[10]));
		push(@Robot_Name,Trim($record[11]));
	}
	$sth->finish();
	return (\@ID,\@Buyer_Name,\@Spent_Data_weblink,\@End_Date,\@Type_of_Government,\@Expressions,\@Request_method,\@Host,\@Referer,\@Hidden_Content,\@File_Type,\@Robot_Name);
}

sub Retrieve_Buyers_Download_Details()
{
	my $dbh 		= shift;
	my $Input_Table	= shift;
	
	my $query = "select Buyer_ID,Batch_ID from $Input_Table  where Formatted_Flag=\'N\'";
	
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@Buyer_ID,@Batch_ID);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@Buyer_ID,Trim($record[0]));
		push(@Batch_ID,Trim($record[1]));
	}
	$sth->finish();
	return (\@Buyer_ID,\@Batch_ID);
}

sub Retrieve_Source_Data()
{
	my $dbh 		= shift;
	my $Buyer_ID 	= shift;
	my $Batch_ID 	= shift;
	my $Input_Table	= shift;
	
	my $query = "select Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2 from $Input_Table   where Buyer_ID=\'$Buyer_ID\' and Batch_ID=\'$Batch_ID\'";
	
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@Month_Of_Document,@Processed_Date,@Sno,@Transaction_ID,@Requirement,@Amount,@Start_Date,@End_Date,@Supplier_Name,@Department_Name,@Requirement_2);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@Month_Of_Document,Trim($record[0]));
		push(@Processed_Date,Trim($record[1]));
		push(@Sno,Trim($record[2]));
		push(@Transaction_ID,Trim($record[3]));
		push(@Requirement,Trim($record[4]));
		push(@Amount,Trim($record[5]));
		push(@Start_Date,Trim($record[6]));
		push(@End_Date,Trim($record[7]));
		push(@Supplier_Name,Trim($record[8]));
		push(@Department_Name,Trim($record[9]));
		push(@Requirement_2,Trim($record[10]));
	}
	$sth->finish();
	return (\@Month_Of_Document,\@Processed_Date,\@Sno,\@Transaction_ID,\@Requirement,\@Amount,\@Start_Date,\@End_Date,\@Supplier_Name,\@Department_Name,\@Requirement_2);
}

sub Column_Name_RetrieveUrl()
{
	my $dbh 		= shift;
	my $Buyer_ID 	= shift;
	
	my $query = "select Type,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2,Entity_Field,Entity_Code from field_map_details_Automation where Buyer_ID=\'$Buyer_ID\'";
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my ($Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field,$Entity_Code);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		$Type=Trim($record[0]);
		$Transaction_ID=Trim($record[1]);
		$Requirement=Trim($record[2]);
		$Amount=Trim($record[3]);
		$Start_Date=Trim($record[4]);
		$End_Date=Trim($record[5]);
		$Supplier_Name=Trim($record[6]);
		$Department_Name=Trim($record[7]);
		$Requirement_2=Trim($record[8]);
		$Entity_Field=Trim($record[9]);
		$Entity_Code=Trim($record[10]);
	}
	$sth->finish();
	return ($Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field,$Entity_Code);
}


sub Supplier_Name_Matching()
{
	my $dbh 		= shift;
	my $Supplier_Name_withoutspace 	= shift;
	
	my $query = "select Supplier_Name from SUPPLIER_LIST where ID=(select Matching_ID from SUPPLIER_LIST where Supplier_Name_withoutspace=\'$Supplier_Name_withoutspace\')";
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my ($Supplier_Name);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		$Supplier_Name=Trim($record[0]);
	}
	$sth->finish();
	return ($Supplier_Name);
}


sub BatchID_RetrieveUrl()
{
	my $dbh 		= shift;
	my $Buyer_ID 	= shift;
	my $Input_Table 	= shift;
	
	my $query = "select max(Batch_ID) from $Input_Table  where Buyer_ID=\'$Buyer_ID\'";
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my ($Batch_ID);
	while(my @record = $sth->fetchrow)
	{
		$Batch_ID=Trim($record[0]);
	}
	$sth->finish();
	return ($Batch_ID);
}

sub RetrieveBuyerID()
{
	my $dbh 		 = shift;
	my $Input_Table  = shift;
	my $Buyer_ID 	 = shift;
	my $Entity_Code  = shift;
	
	# my $query = "select b.id from field_map_details_automation f,buyers b where b.id=f.buyer_id and b.Group_ID=$Buyer_ID and f.Entity_Code = \'$Entity_Code\'";
	my $query = "select b.id from field_map_details_automation f,buyers b where b.id=f.buyer_id and b.Group_ID=$Buyer_ID and f.Entity_Code regexp ('\\\\|$Entity_Code\\\\|')";
	
	# print "query :: $query \n";
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my ($id);
	while(my @record = $sth->fetchrow)
	{
		$id=Trim($record[0]);
	}
	$sth->finish();
	return ($id);
}



sub LinkInfo_RetrieveUrl()
{
	my $dbh 		= shift;
	my $Buyer_ID 	= shift;
	my $Input_Table 	= shift;
	
	my $query = "select LinkInfo from $Input_Table  where Buyer_ID=\'$Buyer_ID\'";
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my (@LinkInfo);
	while(my @record = $sth->fetchrow)
	{
		push(@LinkInfo,Trim($record[0]));
	}
	$sth->finish();
	return (\@LinkInfo);
}

# ###### Pdf Download Path ####
# sub DownloadPath()
# {
	# my $dbh 			= shift;
	# my $downloadpath;
	# my $query = "Select Picklistvalue from PickList where PicklistCategory='ONE_APP' and PickListField='PDF_DOWNLOAD_PATH' AND ID=436";
	# my $sth = $dbh->prepare($query);
	# $sth->execute();
	# while(my @record = $sth->fetchrow)
	# {
		# $downloadpath = Trim($record[0]);
	# }
	# $sth->finish();
	# return $downloadpath;
# }

sub Update_Download_Details()
{
	my $dbh 		= shift;
	my $Table_Name	= shift;
	my $Buyer_ID	= shift;
	my $Batch_ID 	= shift;
	my $Supplier_Name_Matched_Count 	= shift;
	my $Supplier_Name_NotMatched_Count	= shift;
	my $Formatted_Flag	= shift;
	my $Supplier_Match	= shift;
	
	my $query = "Update $Table_Name set No_of_Suppliers_Matched=\'$Supplier_Name_Matched_Count\',No_of_Suppliers_Not_Matched=\'$Supplier_Name_NotMatched_Count\',Formatted_Flag=\'$Formatted_Flag\',Supplier_Match=\'$Supplier_Match\' where Buyer_ID=\'$Buyer_ID\' and Batch_ID=\'$Batch_ID\'";
	my $sth = $dbh->prepare($query);
	if($sth->execute())
	{
		# print "Updated\n";
		$sth->finish();	
	}
	else
	{
		print "QUERY:: $query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

sub Update_Import_Flag()
{
	my $dbh 		= shift;
	my $Table_Name	= shift;
	my $Buyer_ID	= shift;
	my $Batch_ID 	= shift;
	my $Import_Flag	= shift;
	my $Field_Match	= shift;
	my $EndDate		= shift;
	my $Number_Of_Records	= shift;
	
	my $query = "Update $Table_Name set Import_Flag=\'$Import_Flag\', Field_Match=\'$Field_Match\',EndDate=\'$EndDate\',No_of_Records=\'$Number_Of_Records\' where Buyer_ID=\'$Buyer_ID\' and Batch_ID=\'$Batch_ID\'";
	my $sth = $dbh->prepare($query);
	if($sth->execute())
	{
		# print "Updated\n";
		$sth->finish();	
	}
	else
	{
		print "QUERY:: $query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

###### Update DB ####
sub Update_Buyers_Details
{
	my $dbh			= shift;
	my $Buyer_ID 	= shift;
	my $Date_Researched 	= shift;
	my $No_of_Document_Researched 	= shift;
	my $Document_Month_Researched 	= shift;

	my $query = "Update Buyers_Tmp  set Date_Researched=\'$Date_Researched\', No_of_Document_Researched=\'$No_of_Document_Researched\', Document_Month_Researched=\'$Document_Month_Researched\' where ID=\'$Buyer_ID\'";

	my $sth = $dbh->prepare($query);
	if($sth->execute())
	{
		# print "Updated\n";
		$sth->finish();	
	}
	else
	{
		print "QUERY:: $query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

###### Select Count Details ####
sub Retrieve_Count_Details
{
	my $dbh			= shift;
	my $Table_Name 	= shift;
	my $Buyer_ID 	= shift;
	my $Batch_ID 	= shift;

	my $query = "select count(*) from $Table_Name where Buyer_ID=\'$Buyer_ID\' and Batch_ID=\'$Batch_ID\'";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my ($Count);
	while(my @record = $sth->fetchrow)
	{
		$Count=Trim($record[0]);
	}
	$sth->finish();
	return ($Count);
}


sub Insert_Spend_Data()
{
	my $dbh 			= shift;
	my $insert_query		= shift;
	 # print "insert_query :: $insert_query\n";exit;
	my $sth = $dbh->prepare($insert_query);
	if($sth->execute())
	{
		# print "Executed\n";
	}
	else
	{
		print "QUERY:: $insert_query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $insert_query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

sub Insert_Cleaned_Spend_Data()
{
	my $dbh 			= shift;
	my $Table_Name		= shift;
	my $Buyer_ID		= shift;
	my $Batch_ID		= shift;
	my $Month_Of_Document= shift;
	my $Processed_Date	= shift;
	my $S_No			= shift;
	my $Transaction_ID	= shift;
	my $Requirement		= shift;
	my $Amount			= shift;
	my $Start_Date		= shift;
	my $End_Date		= shift;
	my $Supplier_Name	= shift;
	my $Department_Name	= shift;
	my $Requirement_2	= shift;
	my $Supplier_Match_Flag	= shift;

	$Supplier_Name=~s/\'/\'\'/igs;
	$Department_Name=~s/\'/\'\'/igs;
	$Requirement=~s/\'/\'\'/igs;
	$Requirement_2=~s/\'/\'\'/igs;
	
	my $insert_query = "insert into $Table_Name   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2,Supplier_Match_Flag) values (\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',\'$Processed_Date\',\'$S_No\',\'$Transaction_ID\',\'$Requirement\',\'$Amount\',\'$Start_Date\',\'$End_Date\',\'$Supplier_Name\',\'$Department_Name\',\'$Requirement_2\',\'$Supplier_Match_Flag\') ";
	# print "$insert_query\n";
	my $sth = $dbh->prepare($insert_query);
	if($sth->execute())
	{
		# print "Executed\n";
	}
	else
	{
		print "QUERY:: $insert_query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $insert_query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

sub Insert_Download_Details()
{
	my $dbh 			= shift;
	my $Table_Name		= shift;
	my $Buyer_ID		= shift;
	my $LinkInfo		= shift;
	my $FileName		= shift;
	my $StartDate		= shift;
	my $EndDate			= shift;
	my $No_Of_Records	= shift;
	my $Status			= shift;
	my $Failed_Reason	= shift;
	
	$LinkInfo=~s/\'/\'\'/igs;
	
	my $insert_query = "insert into $Table_Name  (Buyer_ID,LinkInfo,FileName,StartDate,No_Of_Records,Status,Failed_Reason) values (\'$Buyer_ID\',\'$LinkInfo\',\'$FileName\',\'$StartDate\',\'$No_Of_Records\',\'$Status\',\'$Failed_Reason\') ";
	my $sth = $dbh->prepare($insert_query);
	if($sth->execute())
	{
		# print "Executed\n";
	}
	else
	{
		print "QUERY:: $insert_query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $insert_query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

sub Insert_Errors()
{
	my $dbh 	= shift;
	my $Buyer_ID= shift;
	my $Batch_ID	= shift;
	# my $Date	= shift;
	my $Error	= shift;
	my $S_no	= shift;
	
	
	my $insert_query = "insert into Logs  (Buyer_ID,Batch_ID,Researched_Date,Error,Sno) values (\'$Buyer_ID\',\'$Batch_ID\',now(),\'$Error\',\'$S_no\') ";
	# print "$insert_query\n";
	# <stdin>;
	my $sth = $dbh->prepare($insert_query);
	if($sth->execute())
	{
		# print "Executed\n";
	}
	else
	{
		print "QUERY:: $insert_query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $insert_query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

sub Insert_Batch_Details()
{
	my $dbh 	= shift;
	my $Buyer_ID= shift;
	my $Batch_ID	= shift;
	my $No_of_Records	= shift;
	
	
	my $insert_query = "insert into Batch_Details  (Buyer_ID,Batch_ID,No_of_Records) values (\'$Buyer_ID\',\'$Batch_ID\','$No_of_Records\') ";
	# print "$insert_query\n";
	# <stdin>;
	my $sth = $dbh->prepare($insert_query);
	if($sth->execute())
	{
		# print "Executed\n";
	}
	else
	{
		print "QUERY:: $insert_query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $insert_query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

sub Insert_Pdf_Json_Details()
{
	my $dbh              = shift;
	my $Buyer_ID= shift;
	my $pdf_name = shift;
	my $LinkInfo      = shift;
	my $Month_Of_Document      = shift;
	my $Manual_convertion_Flag = shift;
	$pdf_name =~ s/\'/\'\'/igs;
	$pdf_name =~ s/\\/\\\\/igs;
	$Month_Of_Document =~ s/\\/\\\\/igs;
	$Month_Of_Document =~ s/\'/\'\'/igs;
	
	my $insert_query = "insert into bip_pdf_json_status (Buyer_ID,pdf_name,pdf_created_date,LinkInfo,MONTH_OF_DOC,manual_conversion) values (\'$Buyer_ID\',\'$pdf_name\',now(),\'$LinkInfo\',\'$Month_Of_Document\',\'$Manual_convertion_Flag\') ";

	# print "$insert_query\n";
	# <stdin>;

	my $sth = $dbh->prepare($insert_query);

	if($sth->execute())

	{
		print "Executed\n";
	}
	else
	{

		print "QUERY:: $insert_query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $insert_query."\n";
		close ERR;
		$dbh=&DbConnection();
	}

}

sub RetrieveJSONDATA()
{
	my $dbh   		= shift;
	my $Buyer_ID   	= shift;
	my $Input_Table = shift;
	my $query = "select ID,BUYER_ID,JSON_NAME,LinkInfo,MONTH_OF_DOC from $Input_Table where Buyer_ID=\'$Buyer_ID\' and IMPORTDB_FLAG='N' and JSON_NAME is not null";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	
	my (@ID,@BUYER_ID,@JSON_NAME,@LinkInfo,@MONTH_OF_DOC);
	while(my @record = $sth->fetchrow)
	{
		push(@ID,&Trim($record[0]));
		push(@BUYER_ID,&Trim($record[1]));
		push(@JSON_NAME,&Trim($record[2]));
		push(@LinkInfo,&Trim($record[3]));
		push(@MONTH_OF_DOC,&Trim($record[4]));
	}
	$sth->finish();
	return (\@BUYER_ID,\@JSON_NAME,\@ID,\@LinkInfo,\@MONTH_OF_DOC);
}

sub Json_Flag_Update()
{
	my $dbh   		= shift;
	my $Buyer_ID   	= shift;
	my $ID   	= shift;
	my $Input_Table = shift;
	my $Json_Imported_Flag = shift;
	
	my $query = "update $Input_Table set importDB_Flag=\'$Json_Imported_Flag\' where Buyer_ID=\'$Buyer_ID\' and ID=\'$ID\'";	
	my $sth = $dbh->prepare($query);
	$sth->execute();	
	$sth->finish();	
}

sub Manual_convertion_Update()
{
	my $dbh   		= shift;
	my $Buyer_ID   	= shift;
	my $ID   	= shift;
	my $Input_Table = shift;
	my $Manual_convertion_Flag = shift;
	
	my $query = "update $Input_Table set manual_conversion=\'$Manual_convertion_Flag\' where Buyer_ID=\'$Buyer_ID\' and ID=\'$ID\'";	
	my $sth = $dbh->prepare($query);
	$sth->execute();	
	$sth->finish();	
}

sub Trim()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}








