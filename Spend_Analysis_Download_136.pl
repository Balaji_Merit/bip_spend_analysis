use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use BIP_DB_V1;
use BIP_Column_Map_V1;

my $Input_Buyer_ID = $0;
$Input_Buyer_ID =~ s/\.pl//igs;
$Input_Buyer_ID =$1 if($Input_Buyer_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

# my $ua  = LWP::UserAgent->new(
        # ssl_opts => { verify_hostname => 1 },
    # );
# my $ua=LWP::UserAgent->new(show_progress=>1);
my $ua=LWP::UserAgent->new(ssl_opts => {SSL_verify_mode => 'SSL_VERIFY_PEER',verify_hostname => 0 }, show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->proxy('https', 'http://172.27.137.199:3128'); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my %Months_To_Num=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");
my @Months1=("January","February","March","April","May","June","July","August","September","October","November","December","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");

############Database Initialization########
my $dbh = &BIP_DB_V1::DbConnection();
###########################################
my $hash_ref = &BIP_Column_Map_V1::ReadIniFile;
my %inihashvalue = %{$hash_ref};

my $Input_Table='BUYERS';
############ Retrieve Spent Data weblink from Buyers table ###########
my ($ID,$Buyer_Name,$Spent_Data_weblink) = &BIP_DB_V1::RetrieveUrl($dbh,$Input_Buyer_ID,$Input_Table);
my $Buyer_ID 			= @$ID[0];
my $Buyer_Name 			= @$Buyer_Name[0];
my $Buyer_Url 			= @$Spent_Data_weblink[0];

###############
print "Buyer_ID	: $Buyer_ID\n";	
print "Buyer_Name	: $Buyer_Name\n";	
print "Buyer_Url	: $Buyer_Url\n";	
# $Buyer_Url="http://www.valeroyalccg.nhs.uk/publication/categories/116";



my ($Buyer_content,$Status_Line)=&Getcontent($Buyer_Url); &BIP_Column_Map_V1::Link_Status_Update($dbh,$Buyer_ID,$Status_Line);
my ($VIEWSTATE,$EVENTVALIDATION,$VIEWSTATEGENERATOR,$dnnVariable);
if ($Buyer_content=~m/VIEWSTATE\"\s*value\=\"([^>]*?)\"/is)
{
	$VIEWSTATE = $1;
}
if ($Buyer_content=~m/EVENTVALIDATION\"\s*value\=\"([^>]*?)\"/is)
{
	$EVENTVALIDATION = $1;
}
if ($Buyer_content=~m/VIEWSTATEGENERATOR\"\s*value\=\"([^>]*?)\"/is)
{
	$VIEWSTATEGENERATOR = $1;
}
if ($Buyer_content=~m/dnnVariable\"\s*value\=\"([^>]*?)\"/is)
{
	$dnnVariable = $1;
}
$VIEWSTATE=uri_escape($VIEWSTATE);
$EVENTVALIDATION=uri_escape($EVENTVALIDATION);

#my $postcont='-----------------------------78911751013654 Content-Disposition: form-data; name="__EVENTTARGET" dnn$ctr1286$Repository$ddlCategories -----------------------------78911751013654 Content-Disposition: form-data; name="__EVENTARGUMENT" -----------------------------78911751013654 Content-Disposition: form-data; name="__LASTFOCUS" -----------------------------78911751013654 Content-Disposition: form-data; name="__VIEWSTATE" '.$VIEWSTATE.' -----------------------------78911751013654 Content-Disposition: form-data; name="__VIEWSTATEGENERATOR" '.$VIEWSTATEGENERATOR.' -----------------------------78911751013654 Content-Disposition: form-data; name="dnn$ctr1286$Repository$ddlCategories" 5 -----------------------------78911751013654 Content-Disposition: form-data; name="ScrollTop" -----------------------------78911751013654 Content-Disposition: form-data; name="__dnnVariable" '.$dnnVariable.' -----------------------------78911751013654--';
my $postcont='-----------------------------78911751013654 Content-Disposition: form-data; name="__EVENTTARGET" dnn$ctr1286$Repository$lstObjects$ctl05$hypDownload -----------------------------78911751013654 Content-Disposition: form-data; name="__EVENTARGUMENT" -----------------------------78911751013654 Content-Disposition: form-data; name="__LASTFOCUS" -----------------------------78911751013654 Content-Disposition: form-data; name="__VIEWSTATE" /wEPDwULLTE2NDIyOTEzMjkPZBYGZg8WAh4EVGV4dAV5PCFET0NUWVBFIGh0bWwgUFVCTElDICItLy9XM0MvL0RURCBYSFRNTCAxLjAgVHJhbnNpdGlvbmFsLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL1RSL3hodG1sMS9EVEQveGh0bWwxLXRyYW5zaXRpb25hbC5kdGQiPmQCAQ9kFg4CBA8WAh4HVmlzaWJsZWhkAgUPFgIeB2NvbnRlbnQFNENvbWJpbmVkIEF1dGhvcml0eSBQYXltZW50cyB0byBzdXBwbGllcnMgYWJvdmUgwqMyNTBkAgYPFgQfAmQfAWhkAgcPFgIfAgURQ29weXJpZ2h0IGJ5IFNZSlNkAggPFgQfAmQfAWhkAgkPFgIfAgUEU1lKU2QCDA8WAh8CBQ1JTkRFWCwgRk9MTE9XZAICD2QWAgIBD2QWBAIED2QWAmYPZBYUAgIPZBYCZg9kFgJmDxYCHwFoFgJmD2QWAmYPZBYGAgMPEGRkFgBkAg8PZBYCZg8PFgYeCEltYWdlVXJsBRQvaW1hZ2VzL2NvbGxhcHNlLmdpZh4NQWx0ZXJuYXRlVGV4dAUITWluaW1pemUeB1Rvb2xUaXAFCE1pbmltaXplFgoeB3VzZXJjdHIFCVVzYWJpbGl0eR4HdXNlcmtleQUUQ29udHJvbFBhbmVsVmlzaWJsZTAeB29uY2xpY2sFUWlmIChfX2Rubl9TZWN0aW9uTWF4TWluKHRoaXMsICAnZG5uX0ljb25CYXIuYXNjeF9yb3dDb250cm9sUGFuZWwnKSkgcmV0dXJuIGZhbHNlOx4IbWF4X2ljb24FEi9pbWFnZXMvZXhwYW5kLmdpZh4IbWluX2ljb24FFC9pbWFnZXMvY29sbGFwc2UuZ2lmZAIRD2QWAgIBD2QWAgIBD2QWEAIBDxBkZBYAZAIFDxBkZBYAZAIHDxBkZBYAZAILDxBkZBYAZAIPDxBkZBYAZAIVDxBkZBYAZAIZDxBkZBYBZmQCHQ8QZGQWAGQCBA8WAh4FY2xhc3MFIEN1c3RvbUhlYWRlclBhbmVUb3AgRE5ORW1wdHlQYW5lZAIMDxYCHwsFFUxlZnRQYW5lIEROTkVtcHR5UGFuZWQCDg8WAh8LBRZSaWdodFBhbmUgRE5ORW1wdHlQYW5lZAIQDxYCHwsFGExlZnRDb250ZW50IEROTkVtcHR5UGFuZWQCEg8WAh8LBRlSaWdodENvbnRlbnQgRE5ORW1wdHlQYW5lZAIUD2QWAgICD2QWAmYPZBYCAgIPD2QWAh8LBQ9ETk5fSFRNTENvbnRlbnQWAgIBD2QWAgICDxYCHwFoZAIWD2QWBAICD2QWFAIBDw8WAh8BaGRkAgMPDxYCHwFoZGQCBQ9kFgJmDw8WBB4HRW5hYmxlZGgfAWhkZAIHD2QWAgICDxYCHwFoFgZmDxYCHwUFBEVkaXRkAgEPFgIfBQUGVXBkYXRlZAICDxYCHwUFBkNhbmNlbGQCCQ9kFgICAQ8PZBYCHwsFD0ROTl9IVE1MQ29udGVudBYCAgEPZBYCAgIPFgIfAWhkAgsPZBYCZg8PFgIfAWhkZAIND2QWAmYPDxYCHwFoZGQCDw9kFgJmDw8WAh8BaGRkAhEPZBYCZg8PFgIfAWhkZAITD2QWAmYPDxYCHwFoZGQCBA9kFgJmD2QWAgIBDw9kFgIfCwURUmVwb3NpdG9yeUNvbnRlbnQWAgIBDw8WCh4HbUZpbHRlcmUeB21JdGVtSURlHgptU29ydE9yZGVyBQtDcmVhdGVkRGF0ZR4LbUF0dHJpYnV0ZXNlHgVtUGFnZQUBMGQWBmYPDxYCHwAFDTxwPiYjMTYwOzwvcD5kZAIBD2QWAmYPZBYCZg9kFgJmD2QWAgIFDxBkZBYBZmQCAg9kFgJmD2QWAmYPZBYCZg88KwALAQAPFgweCVBhZ2VDb3VudAIBHghQYWdlU2l6ZQIYHghEYXRhS2V5cxYAHhBDdXJyZW50UGFnZUluZGV4Zh4LXyFJdGVtQ291bnQCGB4VXyFEYXRhU291cmNlSXRlbUNvdW50AhhkFgJmD2QWMAICD2QWAmYPZBYEAgEPZBYCZg8VAQpGaWxlSUQ9MTMzZAIDDxYCHwFnZAIDD2QWAmYPZBYEAgEPZBYCZg8VAQtGaWxlSUQ9MTQ2MWQCAw8WAh8BZ2QCBA9kFgJmD2QWBAIBD2QWAmYPFQELRmlsZUlEPTE0NjBkAgMPFgIfAWdkAgUPZBYCZg9kFgQCAQ9kFgJmDxUBC0ZpbGVJRD0xNDU5ZAIDDxYCHwFnZAIGD2QWAmYPZBYEAgEPZBYCZg8VAQtGaWxlSUQ9MTQ0NmQCAw8WAh8BZ2QCBw9kFgJmD2QWBAIBD2QWAmYPFQELRmlsZUlEPTE0NDVkAgMPFgIfAWdkAggPZBYCZg9kFgQCAQ9kFgJmDxUBC0ZpbGVJRD0xNDI0ZAIDDxYCHwFnZAIJD2QWAmYPZBYEAgEPZBYCZg8VAQtGaWxlSUQ9MTQ0M2QCAw8WAh8BZ2QCCg9kFgJmD2QWBAIBD2QWAmYPFQELRmlsZUlEPTE0MjVkAgMPFgIfAWdkAgsPZBYCZg9kFgQCAQ9kFgJmDxUBC0ZpbGVJRD0xNDI2ZAIDDxYCHwFnZAIMD2QWAmYPZBYEAgEPZBYCZg8VAQtGaWxlSUQ9MTQyN2QCAw8WAh8BZ2QCDQ9kFgJmD2QWBAIBD2QWAmYPFQELRmlsZUlEPTE0MjhkAgMPFgIfAWdkAg4PZBYCZg9kFgQCAQ9kFgJmDxUBC0ZpbGVJRD0xNDI5ZAIDDxYCHwFnZAIPD2QWAmYPZBYEAgEPZBYCZg8VAQtGaWxlSUQ9MTQzMGQCAw8WAh8BZ2QCEA9kFgJmD2QWBAIBD2QWAmYPFQELRmlsZUlEPTE0MzFkAgMPFgIfAWdkAhEPZBYCZg9kFgQCAQ9kFgJmDxUBC0ZpbGVJRD0xNDMyZAIDDxYCHwFnZAISD2QWAmYPZBYEAgEPZBYCZg8VAQtGaWxlSUQ9MTQzM2QCAw8WAh8BZ2QCEw9kFgJmD2QWBAIBD2QWAmYPFQELRmlsZUlEPTE0MzRkAgMPFgIfAWdkAhQPZBYCZg9kFgQCAQ9kFgJmDxUBC0ZpbGVJRD0xNDM1ZAIDDxYCHwFnZAIVD2QWAmYPZBYEAgEPZBYCZg8VAQtGaWxlSUQ9MTQzNmQCAw8WAh8BZ2QCFg9kFgJmD2QWBAIBD2QWAmYPFQELRmlsZUlEPTE0MzdkAgMPFgIfAWdkAhcPZBYCZg9kFgQCAQ9kFgJmDxUBC0ZpbGVJRD0xNDM4ZAIDDxYCHwFnZAIYD2QWAmYPZBYEAgEPZBYCZg8VAQtGaWxlSUQ9MTM4MmQCAw8WAh8BZ2QCGQ9kFgJmD2QWBAIBD2QWAmYPFQELRmlsZUlEPTEzODFkAgMPFgIfAWdkAhgPFgIfCwUZUGFuZV8yX1JpZ2h0IEROTkVtcHR5UGFuZWQCGg9kFgQCAQ9kFgJmD2QWAgIBDw9kFgIfCwUPRE5OX0hUTUxDb250ZW50FgICAQ9kFgICAg8WAh8BaGQCAw9kFgJmD2QWAgIBDw9kFgIfCwUPRE5OX0hUTUxDb250ZW50FgICAQ9kFgICAg8WAh8BaGQCCg8UKwACZGRkZHB33a7dtVjy5+Zf4E7mRlxIwFR1 -----------------------------78911751013654 Content-Disposition: form-data; name="__VIEWSTATEGENERATOR" CA0B0334 -----------------------------78911751013654 Content-Disposition: form-data; name="dnn$ctr1286$Repository$ddlCategories" 19 -----------------------------78911751013654 Content-Disposition: form-data; name="ScrollTop" 169 -----------------------------78911751013654 Content-Disposition: form-data; name="__dnnVariable" {"__scdoff":"1"} -----------------------------78911751013654--';
# my $postcont='-----------------------------889144795133
# Content-Disposition: form-data; name="__EVENTTARGET"

# dnn$ctr1286$Repository$ddlCategories
# -----------------------------889144795133
# Content-Disposition: form-data; name="__EVENTARGUMENT"


# -----------------------------889144795133
# Content-Disposition: form-data; name="__LASTFOCUS"


# -----------------------------889144795133
# Content-Disposition: form-data; name="__VIEWSTATE"

# '.$VIEWSTATE.'
# -----------------------------889144795133
# Content-Disposition: form-data; name="__VIEWSTATEGENERATOR"

# '.$VIEWSTATEGENERATOR.'
# -----------------------------889144795133
# Content-Disposition: form-data; name="dnn$ctr1286$Repository$ddlCategories"

# 5
# -----------------------------889144795133
# Content-Disposition: form-data; name="ScrollTop"


# -----------------------------889144795133
# Content-Disposition: form-data; name="__dnnVariable"

# '.$dnnVariable.'
# -----------------------------889144795133--';


my $host='www.southyorks.gov.uk';

my ($Buyer_content12,$Content_Disposition)=&Postcontent($Buyer_Url,$postcont,$host,$Buyer_Url);
open(ts,">Buyer_content12.html");
print ts "$Buyer_content12";
close ts;
print "Content-Disposition	: $Content_Disposition\n";
exit;
my ($Processed_LinkInfo) = &BIP_DB_V1::LinkInfo_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
my @Processed_LinkInfo=@$Processed_LinkInfo;
my @link_duplicate;
# To get the Doc_url from the Buyer_content
while($Buyer_content=~m/(?:<\/div>\s*<[^>]*?fluid\">\s*<div\s*class\=\"span3\">([\w\W]*?)<\/div>\s*[\w\W]*?)?<[^>]*?href\=\"([^>]*?((?:csv|xls|xlsx)))\"[^>]*?>\s*([^>]*?)\s*</igs)
{
	my $LinkInfo1=$1;
	my $Doc_Link =$2;	
	my $File_Type=$3;
	my $LinkInfo2=$4;
	my $LinkInfo;
	if ($LinkInfo2 eq 'Download data')
	{
		$LinkInfo=$LinkInfo1;
	}
	elsif($LinkInfo1 eq '')
	{
		$LinkInfo=$LinkInfo2;
	}
	
	if ($Doc_Link ~~ @link_duplicate)
	{
		print "File Already Exists\n";
		next;
	}
	push (@link_duplicate,$Doc_Link);
	$LinkInfo=&clean($LinkInfo);	
	my ($File_Date,$File_Month,$File_Year,$Doc_Name,$Doc_Type);
	my $LinkInfo_Temp=$LinkInfo;
	print "\nhi: $LinkInfo\n";	
	
	next if($Doc_Link=~m/\.pdf/is);		
	
	# Get Title of the file name
	if($LinkInfo ne '') 
	{	
		$LinkInfo=~s/\s\s+/ /igs;
		print "LinkInfo	: $LinkInfo\n";
		
		if($LinkInfo ~~ @Processed_LinkInfo || $LinkInfo =~ m/^\s*$/)
		{
			print "File Already Processed\n";
			next;
		}
		else
		{			
			print "\nThis is new file to download\n";
		}
	}	
	# Get file link 		
	if($Doc_Link!~m/^http/is)
	{	
		print "\nhi\n";
		my $u1=URI::URL->new($Doc_Link,$Buyer_Url);
		my $u2=$u1->abs;
		$Doc_Link=$u2;
		$Doc_Link=~s/\&amp\;/&/igs;
	}

	my $Doc_Link_Temp=$Doc_Link;					
	
	foreach my $monthss(@Months1)	
	{		
		if($LinkInfo_Temp=~m/$monthss/is)
		{
			$File_Month=$monthss;
			if($LinkInfo_Temp=~m/(2\d{3})/is)
			{
				$File_Year=$1;
			}			
			else
			{
				while($LinkInfo_Temp=~m/(\d{2,4})/igs)
				{					
					$File_Year=$1;
				}
			}	
			last;
		}			
	}
	
	# if($File_Year eq '')
	# {
		# if($LinkInfo_Temp=~m/(q\d+)\s*(\d+\/\d+)/is)
		# {
			# $File_Month=$1;
			# $File_Year=$2;
		# }
	# }
	
	my ($Doc_content,$status_line,$File_Type,$File_Type_cont)=&Getcontent($Doc_Link);
	
	if($File_Type=~m/\.pdf/is)
	{
		next;
	}
	elsif($File_Type_cont=~m/\.pdf/is)
	{
		next;
	}
	# elsif($File_Type_cont=~m/xlsx|xls/is)
	# {
		# next;
	# }

	if($File_Type=~m/(csv|xlsx|xls)/is)
	{
		$Doc_Type="$1";
		print "\nI:1\n";
	}
	elsif($File_Type_cont=~m/(csv)/is)
	{
		$Doc_Type="$1";
		print "\nI:2\n";	
	}
	elsif($Doc_Link_Temp=~m/^[^>]*?\.([\w]*?)\s*$/is)
	{
		$Doc_Type="$1";		
		print "\nI:5\n";	
	}			

	
	$Doc_Type=~s/\"//igs;
	$Doc_Type=~s/\'//igs;
		
	# if($LinkInfo=~m/\s*([a-zA-Z]+)\s*(\d{4})\s*$/is)
	# {
		# $File_Month=$1;
		# $File_Year=$2;
		
	# }
	my $Month_Of_Document = "$File_Month $File_Year";	
	
	# File name creation from Link Info
	my $File_Type = $Doc_Type if ($Doc_Type ne '');
	$File_Year=~s/\W+//igs;
	$Doc_Name=$LinkInfo;
	$Doc_Name=~s/\W/\_/igs;
	$Doc_Name=~s/_+/\_/igs;
	$Doc_Name.='.'.$File_Type;
	print "#########################################\n";
	print "Doc_Name	: $Doc_Name\n"; 
	print "File_Month		: $File_Month\n";
	print "File_Year		: $File_Year\n";
	print "Doc_Type		: $Doc_Type\n";
	print "Doc_Link	: $Doc_Link\n";
	print "Month_Of_Document	: $Month_Of_Document\n";
	print "#########################################";
	# File Path creation
	# my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source"; 
	my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source";  
	# my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source"; 
	
	# File location creation
	unless ( -d $Filestorepath ) 
	{
		$Filestorepath=~s/\//\\/igs;
		system("mkdir $Filestorepath");
	}
	my $Storefile = "$Filestorepath\\$Doc_Name";
	print "STOREPATH  : $Storefile \n";
	my $StartDate=DateTime->now();
	# Spend file download
	
	my $fh=FileHandle->new("$Storefile",'w');
	binmode($fh);
	$fh->print($Doc_content);
	$fh->close();
	my ($Status,$Failed_Reason);
	if($status_line!~m/20/is)
	{
		$Failed_Reason=$status_line;
	}
	else
	{
		$Status='Downloaded';
	}
	print "\nStatus:::: $Status\n";
	&BIP_Column_Map_V1::ReadSource($Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
	print "Completed \n";
}

sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	print "\nFile_Type---> $File_Type\n";
	print "\nFile_Type_cont---> $File_Type_cont\n";
	# my $get_cookie=$res->header('set-cookie');
	print "\nCODE---> $code\n";
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$File_Type,$File_Type_cont);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	# $req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("Content-Type"=>"multipart/form-data; boundary=---------------------------78911751013654"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$Content_Disposition);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	if($Data eq '')
	{
		# $Data='Not provided';
	}
	return $Data;
}

sub clean()
{
	my $C_Clean=shift;
	$C_Clean=~s/<[^>]*?>//igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/\&nbsp\;/ /igs;
	$C_Clean=~s/\&amp\;/ /igs;
	$C_Clean=~s/\&pound\;/ /igs;
	$C_Clean=~s/\&\#8211\;/ /igs;
	$C_Clean=~s/ndash\;/ /igs;
	$C_Clean=~s/[^[:print:]]+//igs;
	
	
	$C_Clean=~s/\&nbsp\;//igs;
	$C_Clean=~s/\#160\;//igs;
	$C_Clean=~s/\#163\;//igs;
	$C_Clean=~s/\&/ /igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	return $C_Clean;
}