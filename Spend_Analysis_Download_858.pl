use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use BIP_DB_V1;
use BIP_Column_Map_V1;

my $Input_Buyer_ID = $0;
$Input_Buyer_ID =~ s/\.pl//igs;
$Input_Buyer_ID =$1 if($Input_Buyer_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my %Months_To_Num=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");
my @Months1=("January","February","March","April","May","June","July","August","September","October","November","December","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");

############Database Initialization########
my $dbh = &BIP_DB_V1::DbConnection();
###########################################
my $hash_ref = &BIP_Column_Map_V1::ReadIniFile();
my %inihashvalue = %{$hash_ref};	

my $Input_Table='BUYERS';
############ Retrieve Spent Data weblink from Buyers table ###########
my ($ID,$Buyer_Name,$Spent_Data_weblink) = &BIP_DB_V1::RetrieveUrl($dbh,$Input_Buyer_ID,$Input_Table);
my $Buyer_ID 			= @$ID[0];
my $Buyer_Name 			= @$Buyer_Name[0];
my $Buyer_Url 			= @$Spent_Data_weblink[0];

###############
print "Buyer_ID	: $Buyer_ID\n";	
print "Buyer_Name	: $Buyer_Name\n";	
print "Buyer_Url	: $Buyer_Url\n";	

my ($Buyer_content,$Status_Line)=&Getcontent($Buyer_Url); &BIP_Column_Map_V1::Link_Status_Update($dbh,$Buyer_ID,$Status_Line);
open(ts,">Buyer_content.html");
print ts "$Buyer_content";
close ts;

my ($Processed_LinkInfo) = &BIP_DB_V1::LinkInfo_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
my @Processed_LinkInfo=@$Processed_LinkInfo;

while($Buyer_content=~m/<h3>\d{4}<\/h3>([\w\W]*?)<\/ul>/igs)
{	
	my $block=$1;
	$block=~s/<\!\-\-[\w\W]*?\-\->//igs;
	while($block=~m/class\=\"(?:csv|xlsx?)\"\s*href\=\"([^\"]*?)\"[\w\W]*?>\s*([^<]*?)\s*</igs)
	{
		my $Doc_Link=$1;
		my $LinkInfo=$2;
			
		$LinkInfo=&clean($LinkInfo);	
		my ($File_Date,$File_Month,$File_Year,$Doc_Name,$Doc_Type);
		my $LinkInfo_Temp=$LinkInfo;
		print "\nhi: $LinkInfo\n";		
		my $Doc_Link_Temp=$Doc_Link;
		next if($Doc_Link=~m/\.pdf/is);	
		
		# $LinkInfo=$File_Month." ".$File_Year;
		# Get Title of the file name
		if($LinkInfo ne '') 
		{	
			$LinkInfo=~s/\s\s+/ /igs;
			print "LinkInfo	: $LinkInfo\n";
			
			if($LinkInfo ~~ @Processed_LinkInfo || $LinkInfo =~ m/^\s*$/)
			{
				print "File Already Processed\n";
				next;
			}
			else
			{			
				print "\nThis is new file to download\n";
			}
		}	
		# Get file link 		
		if($Doc_Link!~m/^http/is)
		{	
			print "\nhi\n";
			my $u1=URI::URL->new($Doc_Link,$Buyer_Url);
			my $u2=$u1->abs;
			$Doc_Link=$u2;
			$Doc_Link=~s/\&amp\;/&/igs;
		}
		# foreach my $monthss(@Months1)	
		# {		
			# if($LinkInfo_Temp=~m/$monthss/is)
			# {
				# $File_Month=$monthss;
				# if($LinkInfo_Temp=~m/(2\d{3})/is)
				# {
					# $File_Year=$1;
				# }			
				# else
				# {
					# while($LinkInfo_Temp=~m/(\d{2,4})/igs)
					# {					
						# $File_Year=$1;
					# }
				# }	
				# last;
			# }			
		# }
		if($LinkInfo=~m/[^>]*?\s*([\w]*?)\s+(\d{4})/is)
		{
			$File_Month=$1; 
			$File_Year=$2;
			print "File_Month	: $File_Month\n";
			print "File_Year	: $File_Year\n";
		}
		print "Doc_Link	: $Doc_Link\n";
		print "\nLinkInfo_Temp	: $LinkInfo_Temp\n";	
		
		my ($Doc_content,$status_line,$File_Type,$File_Type_cont)=&Getcontent($Doc_Link);
		
		
		if($File_Type=~m/\.pdf/is)
		{
			next;
		}
		elsif($File_Type_cont=~m/\.pdf/is)
		{
			next;
		}

		if($File_Type=~m/(csv|xlsx|xls)/is)
		{
			$Doc_Type="$1";
			print "\nI:1\n";
		}
		elsif($File_Type_cont=~m/(csv)/is)
		{
			$Doc_Type="$1";
			print "\nI:2\n";	
		}
		elsif($File_Type_cont=~m/(excel)/is)
		{
			$Doc_Type="xls";
			print "\nI:2\n";	
		}
		elsif($Doc_Link_Temp=~m/^[^>]*?\.([\w]*?)\s*$/is)
		{
			$Doc_Type="$1";		
			print "\nI:5\n";	
		}		
		
		$Doc_Type=~s/\"//igs;
		$Doc_Type=~s/\'//igs;
			
		# if($LinkInfo=~m/\s*([a-zA-Z]+)\s*(\d{4})\s*$/is)
		# {
			# $File_Month=$1;
			# $File_Year=$2;
			
		# }
		my $Month_Of_Document = "$File_Month $File_Year";	
		
		# File name creation from Link Info
		my $File_Type = $Doc_Type if ($Doc_Type ne '');
		
		$Doc_Name=$LinkInfo;
		$Doc_Name=~s/\W/\_/igs;
		$Doc_Name=~s/_+/\_/igs;
		$Doc_Name.='.'.$File_Type;
		$File_Year=~s/\W//igs;
		print "#########################################\n";
		print "Doc_Name	: $Doc_Name\n"; 
		print "File_Month		: $File_Month\n";
		print "File_Year		: $File_Year\n";
		print "Doc_Type		: $Doc_Type\n";
		print "Doc_Link	: $Doc_Link\n";
		print "Month_Of_Document	: $Month_Of_Document\n";
		print "#########################################";
		# File Path creation
		my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source"; 
		
		# File location creation
		unless ( -d $Filestorepath ) 
		{
			$Filestorepath=~s/\//\\/igs;
			system("mkdir $Filestorepath");
		}
		my $Storefile = "$Filestorepath\\$Doc_Name";
		print "STOREPATH  : $Storefile \n";
		my $StartDate=DateTime->now();
		# Spend file download
		
		my $fh=FileHandle->new("$Storefile",'w');
		binmode($fh);
		$fh->print($Doc_content);
		$fh->close();
		my ($Status,$Failed_Reason);
		if($status_line!~m/20/is)
		{
			$Failed_Reason=$status_line;
		}
		else
		{
			$Status='Downloaded';
		}
		print "\nStatus:::: $Status\n";
		<>;
		&ReadSource($Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
		print "Completed \n";
		# exit;
	}
}

sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	print "\nFile_Type---> $File_Type\n";
	print "\nFile_Type_cont---> $File_Type_cont\n";
	# my $get_cookie=$res->header('set-cookie');
	print "\nCODE---> $code\n";
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$File_Type,$File_Type_cont);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub clean()
{
	my $C_Clean=shift;
	$C_Clean=~s/<[^>]*?>//igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/\&nbsp\;/ /igs;
	$C_Clean=~s/\&amp\;/ /igs;
	$C_Clean=~s/\&pound\;/ /igs;
	$C_Clean=~s/\&\#8211\;/ /igs;
	$C_Clean=~s/ndash\;/ /igs;
	$C_Clean=~s/[^[:print:]]+//igs;
	
	
	$C_Clean=~s/\&nbsp\;//igs;
	$C_Clean=~s/\#160\;//igs;
	$C_Clean=~s/\#163\;//igs;
	$C_Clean=~s/\&/ /igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	return $C_Clean;
}
sub ReadSource()
{
	my $Buyer_ID		= shift;
	my $Filestorepath	= shift;
	my $Doc_Name		= shift;
	my $LinkInfo		= shift;
	my $Month_Of_Document = shift;
	my $StartDate		= shift;
	my $Status			= shift;
	my $Failed_Reason	= shift;
	my ($EndDate,$Number_Of_Records,$Batch_ID,$Mapped_Header_Data_Avail_Column);
	$EndDate="";
	$Number_Of_Records=0;
	my $PDF_to_Json_flag=0;
	################ Retrived Column mapping details from Field_Map_Details ###########################
	my ($File_Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field,$Entity_Code) = &BIP_DB_V1::Column_Name_RetrieveUrl($dbh,$Buyer_ID);
	
	my @Headerarray;
	push(@Headerarray,($Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field));
	@Headerarray = grep { $_ ne '' } @Headerarray;
	print "########################## Headerarray ::@Headerarray";
	my $Header_Data_Avail_Column = $#Headerarray+1;
	if($Entity_Field)
	{
		&BIP_DB_V1::Insert_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Buyer_ID,$LinkInfo,$Doc_Name,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);
		#Get Batch ID from BUYERS_DOWNLOAD_DETAILS
		$Batch_ID = &BIP_DB_V1::BatchID_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
		# print "Batch_ID :: $Batch_ID \n";
		my ($Ref_Hash_Buyer,$Ref_Hash_Batch,$Ref_Hash_Count,$Ref_Hash_Source_Count);
		if($Doc_Name=~m/\.xlsx?\s*$/is)
		{
			($Number_Of_Records,$Mapped_Header_Data_Avail_Column,$Ref_Hash_Buyer,$Ref_Hash_Batch,$Ref_Hash_Count,$Ref_Hash_Source_Count) = &Read_Excel_File($Buyer_ID,$Batch_ID,$Filestorepath,$Doc_Name,$Month_Of_Document,\@Headerarray,$LinkInfo,$Doc_Name,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);
		}
		elsif($Doc_Name=~m/\.csv\s*$/is)
		{
			($Number_Of_Records,$Mapped_Header_Data_Avail_Column,$Ref_Hash_Buyer,$Ref_Hash_Batch,$Ref_Hash_Count,$Ref_Hash_Source_Count) = &Read_Csv_File($Buyer_ID,$Batch_ID,$Filestorepath,$Doc_Name,$Month_Of_Document,\@Headerarray,$LinkInfo,$Doc_Name,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);
		}
		my %Hash_Buyer = %$Ref_Hash_Buyer;
		my %Hash_Batch = %$Ref_Hash_Batch;
		my %Hash_Count = %$Ref_Hash_Count;
		my %Hash_Source_Count = %$Ref_Hash_Source_Count;
		my @Hash_Buyer_Value = keys %Hash_Buyer;
		@Hash_Buyer_Value = grep { $_ ne '' } @Hash_Buyer_Value;
		# print "Hash_Buyer_Value :: @Hash_Buyer_Value \n";<STDIN>;
		foreach my $Key_Entity_Code (@Hash_Buyer_Value)
		{
			my $Buyer_ID = $Hash_Buyer{$Key_Entity_Code};
			my $Batch_ID = $Hash_Batch{$Key_Entity_Code};
			my $Number_Of_Records = $Hash_Source_Count{$Key_Entity_Code};
			my @values_batch_id = values(%Hash_Batch);
			my @keys_batch_id = keys(%Hash_Batch);
			my @matched_batch_id = grep { $Batch_ID =~ /$_/is } @values_batch_id;
			if($#matched_batch_id  > 0)
			{
				$Number_Of_Records = 0;
				foreach my $batch_entity_code (@keys_batch_id)
				{
					if($Hash_Batch{$batch_entity_code} == $Batch_ID )
					{
						$Number_Of_Records = $Number_Of_Records + $Hash_Source_Count{$batch_entity_code};
					}
				}
			}
			my $Field_Match='N';
			if($Mapped_Header_Data_Avail_Column==$Header_Data_Avail_Column)
			{
				$Field_Match='Y';
			}
			
			my ($Records_Count_DB)=&BIP_DB_V1::Retrieve_Count_Details($dbh,"SUPPLIER_SOURCE_DATA",$Buyer_ID,$Batch_ID);
			my $Import_Flag='N';
			if($Records_Count_DB==$Number_Of_Records)
			{
				$Import_Flag='Y';
			}
			print "Import_Flag: $Import_Flag\n";
			print "Field_Match: $Field_Match\n";
			$EndDate=DateTime->now();
			$Number_Of_Records = 0 if($Number_Of_Records eq "");
			# To update Supplier_Match, No_of_Suppliers_Matched, No_of_Suppliers_Not_Matched, Formatted_Flag 
			&BIP_DB_V1::Update_Import_Flag($dbh,"BUYERS_DOWNLOAD_DETAILS",$Buyer_ID,$Batch_ID,$Import_Flag,$Field_Match,$EndDate,$Number_Of_Records);
			#############
			
		}
		
	}
	else
	{
		&BIP_DB_V1::Insert_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Buyer_ID,$LinkInfo,$Doc_Name,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);
		#Get Batch ID from BUYERS_DOWNLOAD_DETAILS
		$Batch_ID = &BIP_DB_V1::BatchID_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
		print "Batch_ID :: $Batch_ID \n";
		
		if($Doc_Name=~m/\.xlsx?\s*$|\.xlsb\s*$/is)
		{
			($Number_Of_Records,$Mapped_Header_Data_Avail_Column) = &Read_Excel_File($Buyer_ID,$Batch_ID,$Filestorepath,$Doc_Name,$Month_Of_Document,\@Headerarray);
		}
		# elsif($Doc_Name=~m/\.csv\s*$/is)
		# {
			# ($Number_Of_Records,$Mapped_Header_Data_Avail_Column) = &Read_Csv_File($Buyer_ID,$Batch_ID,$Filestorepath,$Doc_Name,$Month_Of_Document,\@Headerarray);
		# }
		elsif($Doc_Name=~m/\.pdf\s*$/is)
		{			
			($Number_Of_Records,$Mapped_Header_Data_Avail_Column) = &Read_Json_File($Buyer_ID,$Batch_ID,$Filestorepath,\@Headerarray,$Month_Of_Document);
			$PDF_to_Json_flag=1;
		}
		elsif($Doc_Name=~m/\.ods\s*$/is)
		{			
			($Number_Of_Records,$Mapped_Header_Data_Avail_Column) = &Read_Ods_File($Buyer_ID,$Batch_ID,$Filestorepath,$Doc_Name,$Month_Of_Document,\@Headerarray);
		}
		elsif($Doc_Name=~m/\.(csv)\s*$/is)
		{			
			($Number_Of_Records,$Mapped_Header_Data_Avail_Column) = &Read_txt_File($Buyer_ID,$Batch_ID,$Filestorepath,$Doc_Name,$Month_Of_Document,\@Headerarray);
		}
		my $Field_Match='N';
		if($Mapped_Header_Data_Avail_Column==$Header_Data_Avail_Column)
		{
			$Field_Match='Y';
		}
		
		my ($Records_Count_DB)=&BIP_DB_V1::Retrieve_Count_Details($dbh,"SUPPLIER_SOURCE_DATA",$Buyer_ID,$Batch_ID);
		my $Import_Flag='N';
		if($Records_Count_DB==$Number_Of_Records)
		{
			$Import_Flag='Y';
		}
		print "Import_Flag: $Import_Flag\n";
		print "Field_Match: $Field_Match\n";
		$EndDate=DateTime->now();
		# To update Supplier_Match, No_of_Suppliers_Matched, No_of_Suppliers_Not_Matched, Formatted_Flag 
		&BIP_DB_V1::Update_Import_Flag($dbh,"BUYERS_DOWNLOAD_DETAILS",$Buyer_ID,$Batch_ID,$Import_Flag,$Field_Match,$EndDate,$Number_Of_Records);
		#############		
	}
	if($PDF_to_Json_flag eq '1')
	{
		return $Number_Of_Records;
	}
}

sub Read_Excel_File
{
	my $Buyer_ID 			= shift;
	my $Batch_ID 			= shift;
	my $Filepath 			= shift;
	my $Docname				= shift;
	my $Month_Of_Document	= shift;
	my $Header_array		= shift;
	my $LinkInfo			= shift;
	my $Doc_Name			= shift;
	my $StartDate			= shift;
	my $EndDate				= shift;
	my $Number_Of_Records	= shift;
	my $Status				= shift;
	my $Failed_Reason		= shift;
	my @Headerarray = @{$Header_array};
	# @Headerarray = grep { $_ ne '' } @Headerarray;
	my $Outfile = "$Filepath/$Docname";
	print "LOADFILE : $Outfile\n";
	my ($File_Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field,$Entity_Code) = &BIP_DB_V1::Column_Name_RetrieveUrl($dbh,$Buyer_ID);
	my $insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
	my $Book = Win32::OLE->GetObject($Outfile) or die "$!";
	$Book->{'Visible'} = 1;
	$Book->{DisplayAlerts} = 0;
	# Win32::OLE->LastError(0);
	my $Sheets;
	eval{$Sheets = $Book->Worksheets->Count;};
	my $StartDate=DateTime->now();
	my $EndDate='';
	$Number_Of_Records=0;
	
	# Win32::OLE->LastError(0);
	my $Sheets;
	eval{$Sheets = $Book->Worksheets->Count;};
	
	my $Headercheck = 0;
	my $Sno = 0;
	my %Header_col_position;
	my $insert_collect = 0;
	my (%Hash_buyer,%Hash_batch,%Hash_count,%Hash_Source_Count);
	if ($Entity_Code)
	{
		my $Entity_Code_Temp = $Entity_Code;
		$Entity_Code_Temp =~ s/^\s*\|//igs;
		while($Entity_Code_Temp =~ m/([^<]*?)\|/igs)
		{
			my $Entity_Values = $1;
			$Hash_buyer{$Entity_Values} = $Buyer_ID;
			$Hash_batch{$Entity_Values} = $Batch_ID;
		}
		
	}
	# print "Headerarray :: @Headerarray\n";
	foreach my $Sheet (1 .. $Sheets)
	{
		my $eSheet = $Book->Worksheets($Sheet);
		my $Rowcount = $eSheet->UsedRange->Rows->{'Count'}; 

		my $Colcount = $eSheet->UsedRange->Columns->{'Count'}; 
		foreach my $Row (1 .. $Rowcount)
		{
			my ($Transaction_ID_Value,$Requirement_Value,$Amount_Value,$Start_Date_Value,$End_Date_Value,$Supplier_Name_Value,$Department_Name_Value,$Requirement_2_Value,$Entity_Field_Value);
			# print "HEADER : $Headercheck\n";
			if ( $Headercheck == 0 )
			{
				foreach my $Col (1 .. $Colcount)
				{
					my $Value 						= &Trim($eSheet->Cells($Row,$Col)->{'Value'});
					my @Header = grep { $Value =~ /$_/is } @Headerarray;
					# print "HEADER :: $Value ::  @Header \n";
					$Header_col_position{$Header[0]} = $Col if (@Header);
				}
				
				if ($Header_col_position{$Transaction_ID}||$Header_col_position{$Requirement}||$Header_col_position{$Amount}||$Header_col_position{$Start_Date}||$Header_col_position{$End_Date}||$Header_col_position{$Supplier_Name}||$Header_col_position{$Department_Name}||$Header_col_position{$Requirement_2})
				{
					$Headercheck = 1;
				}
				
			}
			else
			{
				# exit;
				$Transaction_ID_Value 	= &Trim($eSheet->Cells($Row,$Header_col_position{$Transaction_ID})->{'Value'}) if ($Header_col_position{$Transaction_ID} ne '');
				$Requirement_Value 		= &Trim($eSheet->Cells($Row,$Header_col_position{$Requirement})->{'Value'}) if ($Header_col_position{$Requirement} ne '');
				$Amount_Value 			= &Trim($eSheet->Cells($Row,$Header_col_position{$Amount})->{'Value'}) if ($Header_col_position{$Amount} ne '');
				$Start_Date_Value 		= &Trim($eSheet->Cells($Row,$Header_col_position{$Start_Date})->{'Text'}) if ($Header_col_position{$Start_Date} ne '');
				$End_Date_Value 		= &Trim($eSheet->Cells($Row,$Header_col_position{$End_Date})->{'Text'}) if ($Header_col_position{$End_Date} ne '');
				$Supplier_Name_Value 	= &Trim($eSheet->Cells($Row,$Header_col_position{$Supplier_Name})->{'Value'}) if ($Header_col_position{$Supplier_Name} ne '');
				$Department_Name_Value 	= &Trim($eSheet->Cells($Row,$Header_col_position{$Department_Name})->{'Value'}) if ($Header_col_position{$Department_Name} ne '');
				$Requirement_2_Value	= &Trim($eSheet->Cells($Row,$Header_col_position{$Requirement_2})->{'Value'}) if ($Header_col_position{$Requirement_2} ne '');
				$Entity_Field_Value  	= &Trim($eSheet->Cells($Row,$Header_col_position{$Entity_Field})->{'Value'}) if ($Header_col_position{$Entity_Field} ne '');
				
				if($Start_Date_Value =~ m/\#/is)
				{
					$Start_Date_Value   = &Trim($eSheet->Cells($Row,$Header_col_position{$Start_Date})->{'Value'}) if ($Header_col_position{$Start_Date} ne '');
				}
				if($End_Date_Value =~ m/\#/is)
				{
					$End_Date_Value     = &Trim($eSheet->Cells($Row,$Header_col_position{$End_Date})->{'Value'}) if ($Header_col_position{$End_Date} ne '');
				}
				if($Entity_Field)
				{
					unless($Hash_buyer{$Entity_Field_Value})
					{
						my ($Ret_Buyer_ID,$Ret_Entity_Code) = &BIP_DB_V1::RetrieveBuyerID($dbh,"field_map_details_automation",$Buyer_ID,$Entity_Field_Value);
						# print "Ret_Buyer_ID :: $Ret_Buyer_ID \n";
						if($Ret_Buyer_ID)
						{
							&BIP_DB_V1::Insert_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Ret_Buyer_ID,$LinkInfo,$Doc_Name,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);
							$Batch_ID = &BIP_DB_V1::BatchID_RetrieveUrl($dbh,$Ret_Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
							# print "Batch_ID :: $Entity_Field_Value ::  $Batch_ID \n";<STDIN>;
							if ($Ret_Entity_Code)
							{
								my $Entity_Code_Temp = $Ret_Entity_Code;
								$Entity_Code_Temp =~ s/^\s*\|//igs;
								while($Entity_Code_Temp =~ m/([^<]*?)\|/igs)
								{
									my $Entity_Values = $1;
									$Hash_buyer{$Entity_Values} = $Ret_Buyer_ID;
									$Hash_batch{$Entity_Values} = $Batch_ID;
								}	
							}
						}
						else
						{
							$Hash_buyer{$Entity_Field_Value} = '-';
							$Hash_batch{$Entity_Field_Value} = '-';
						}
					}
					next if($Hash_buyer{$Entity_Field_Value} eq '-');
					if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
					{
						$insert_collect++;
						$Supplier_Name_Value=~s/\'/\'\'/igs;
						$Department_Name_Value=~s/\'/\'\'/igs;
						$Requirement_Value=~s/\'/\'\'/igs;
						$Requirement_2_Value=~s/\'/\'\'/igs;
						$Hash_count{$Entity_Field_Value}++;
						$Hash_Source_Count{$Entity_Field_Value}++;
						if($insert_collect <= 990)
						{
						$insert_query .= "(\'$Hash_buyer{$Entity_Field_Value}\',\'$Hash_batch{$Entity_Field_Value}\',\'$Month_Of_Document\',date(now()),\'$Hash_count{$Entity_Field_Value}\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
						}
						else
						{
							$insert_query .= "(\'$Hash_buyer{$Entity_Field_Value}\',\'$Hash_batch{$Entity_Field_Value}\',\'$Month_Of_Document\',date(now()),\'$Hash_count{$Entity_Field_Value}\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
							$insert_query =~ s/\,\s*$//igs;
							$insert_query .= ';';
							&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
							$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
							$insert_collect = 0;
						}
					}
				}
				else
				{
					if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
					{
						$Sno++;
						$insert_collect++;
						$Supplier_Name_Value=~s/\'/\'\'/igs;
						$Department_Name_Value=~s/\'/\'\'/igs;
						$Requirement_Value=~s/\'/\'\'/igs;
						$Requirement_2_Value=~s/\'/\'\'/igs;
						if($insert_collect <= 990)
						{
						$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
						}
						else
						{
							$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
							$insert_query =~ s/\,\s*$//igs;
							$insert_query .= ';';
							&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
							$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
							$insert_collect = 0;
						}
					}
				}
			}
		}
	}
	$Book->quit();
	if($insert_collect >= 1)
	{
		$insert_query =~ s/\,\s*$//igs;
		$insert_query .= ';';
		&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
	}
	my @Value = values %Header_col_position;
	@Value = grep { $_ ne '' } @Value;
	my $Mapped_Col = $#Value+1;
	return ($Sno,$Mapped_Col,\%Hash_buyer,\%Hash_batch,\%Hash_count,\%Hash_Source_Count);
}

sub Read_Csv_File
{
	my $Buyer_ID 			= shift;
	my $Batch_ID 			= shift;
	my $Filepath 			= shift;
	my $Docname				= shift;
	my $Month_Of_Document	= shift;
	my $Header_array		= shift;
	my $LinkInfo			= shift;
	my $Doc_Name			= shift;
	my $StartDate			= shift;
	my $EndDate				= shift;
	my $Number_Of_Records	= shift;
	my $Status				= shift;
	my $Failed_Reason		= shift;
	my @Headerarray = @{$Header_array};
	
	my $Outfile = "$Filepath\\$Docname";
	print "LOADFILE : $Outfile\n";
	my ($File_Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field,$Entity_Code) = &BIP_DB_V1::Column_Name_RetrieveUrl($dbh,$Buyer_ID);
	my $insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
	my $Headercheck = 0;
	my $Sno = 0;
	my $insert_collect = 0;
	my %Header_col_position;
	my (%Hash_buyer,%Hash_batch,%Hash_count,%Hash_Source_Count);
	print "Entity_Code	:: $Entity_Code\n";
	if ($Entity_Code)
	{
		my $Entity_Code_Temp = $Entity_Code;
		$Entity_Code_Temp =~ s/^\s*\|//igs;
		while($Entity_Code_Temp =~ m/([^<]*?)\|/igs)
		{
			my $Entity_Values = $1;
			print "EntityMAjor	:: $Entity_Values\n";
			$Hash_buyer{$Entity_Values} = $Buyer_ID;
			$Hash_batch{$Entity_Values} = $Batch_ID;
		}
	}
	
	my $csv = Text::CSV->new ({
	binary => 1,
	allow_loose_quotes  => 1,
	allow_loose_escapes => 1,
	auto_diag => 1,
	escape_char => "\\",
	sep_char => ','
	});  
	
	my $sum = 0;
	my $Valid_Records=0;
	open(my $data, '<', "$Outfile") or die "Could not open '$Doc_Name' $!\n";
	
	my $fields = $csv->getline($data);
	print "Fields=>$fields\n";
	if ($fields eq '')
	{
		$Outfile=~s/csv$/txt/igs;
		# print "File name :: $Outfile\n";
		open(my $fh, '<', $Outfile) or die "Can't read file '$Outfile' [$!]\n";
		print "Inside the TEXT Loop\n";
		while (<$fh>) 
		{
			my $line=$_;
			chomp($line);
			$line=~s/[^[:print:]]/\,/igs;
			$line=~s/,,,/|/igs;
			$line=~s/\,\,/|/igs;
			# $line=~s/,\s,/ /igs;
			$line=~s/,//igs;
			# print "line	:: $line\n";
			my @fieldsarray = split('\|', $line);
			my @fieldsarray = map { $_ =~ s/\"//igs; $_ } @fieldsarray;
			# print "fieldsarray	:: @fieldsarray\n";
			&DataCollect(\@fieldsarray);
			my ($Transaction_ID_Value,$Requirement_Value,$Amount_Value,$Start_Date_Value,$End_Date_Value,$Supplier_Name_Value,$Department_Name_Value,$Requirement_2_Value,$Entity_Field_Value);
			
			if ( $Headercheck == 0 )
			{
				foreach my $Col (0 .. $#fieldsarray)
				{
					# print "Value	:: $fieldsarray[$Col]\n";
					my @Header = grep { $fieldsarray[$Col] =~ /$_/is } @Headerarray;
					# print "HEADER :: @Header :: $Col \n";<STDIN>;
					$Header_col_position{$Header[0]} = $Col if (@Header);
				}
				
				if ($Header_col_position{$Transaction_ID}||$Header_col_position{$Requirement}||$Header_col_position{$Amount}||$Header_col_position{$Start_Date}||$Header_col_position{$End_Date}||$Header_col_position{$Supplier_Name}||$Header_col_position{$Department_Name}||$Header_col_position{$Requirement_2})
				{
					$Headercheck = 1;
				}
			}
			else
			{
				$Transaction_ID_Value 	= &Trim($fieldsarray[$Header_col_position{$Transaction_ID}]) if ($Header_col_position{$Transaction_ID} ne '');
				$Requirement_Value 		= &Trim($fieldsarray[$Header_col_position{$Requirement}]) if ($Header_col_position{$Requirement} ne '');
				$Amount_Value 			= &Trim($fieldsarray[$Header_col_position{$Amount}]) if ($Header_col_position{$Amount} ne '');
				$Start_Date_Value 		= &Trim($fieldsarray[$Header_col_position{$Start_Date}]) if ($Header_col_position{$Start_Date} ne '');
				$End_Date_Value 		= &Trim($fieldsarray[$Header_col_position{$End_Date}]) if ($Header_col_position{$End_Date} ne '');
				$Supplier_Name_Value 	= &Trim($fieldsarray[$Header_col_position{$Supplier_Name}]) if ($Header_col_position{$Supplier_Name} ne '');
				$Department_Name_Value 	= &Trim($fieldsarray[$Header_col_position{$Department_Name}]) if ($Header_col_position{$Department_Name} ne '');
				$Requirement_2_Value	= &Trim($fieldsarray[$Header_col_position{$Requirement_2}]) if ($Header_col_position{$Requirement_2} ne '');
				$Entity_Field_Value	= &Trim($fieldsarray[$Header_col_position{$Entity_Field}]) if ($Header_col_position{$Entity_Field} ne '');
							
				# print "Entity_Field	:: $Entity_Field\n";
				if($Entity_Field)
				{
					unless($Hash_buyer{$Entity_Field_Value})
					{
						my ($Ret_Buyer_ID,$Ret_Entity_Code) = &BIP_DB_V1::RetrieveBuyerID($dbh,"field_map_details_automation",$Buyer_ID,$Entity_Field_Value);
						# print "Ret_Buyer_ID :: $Ret_Buyer_ID \n";
						if($Ret_Buyer_ID)
						{
							&BIP_DB_V1::Insert_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Ret_Buyer_ID,$LinkInfo,$Doc_Name,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);
							$Batch_ID = &BIP_DB_V1::BatchID_RetrieveUrl($dbh,$Ret_Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
							# print "Batch_ID :: $Entity_Field_Value ::  $Batch_ID \n";
							if ($Ret_Entity_Code)
							{
								my $Entity_Code_Temp = $Ret_Entity_Code;
								$Entity_Code_Temp =~ s/^\s*\|//igs;
								while($Entity_Code_Temp =~ m/([^<]*?)\|/igs)
								{
									my $Entity_Values = $1;
									$Hash_buyer{$Entity_Values} = $Ret_Buyer_ID;
									$Hash_batch{$Entity_Values} = $Batch_ID;
								}	
							}
						}
						else
						{
							$Hash_buyer{$Entity_Field_Value} = '-';
							$Hash_batch{$Entity_Field_Value} = '-';
						}
					}
					# print "Entity_Code###### $Hash_batch{$Entity_Field_Value}\n";
					next if($Hash_buyer{$Entity_Field_Value} eq '-');
					if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
					{
						$insert_collect++;
						$Supplier_Name_Value=~s/\'/\'\'/igs;
						$Department_Name_Value=~s/\'/\'\'/igs;
						$Requirement_Value=~s/\'/\'\'/igs;
						$Requirement_2_Value=~s/\'/\'\'/igs;
						$Hash_count{$Entity_Field_Value}++;
						$Hash_Source_Count{$Entity_Field_Value}++;
						if($insert_collect <= 990)
						{
						$insert_query .= "(\'$Hash_buyer{$Entity_Field_Value}\',\'$Hash_batch{$Entity_Field_Value}\',\'$Month_Of_Document\',date(now()),\'$Hash_count{$Entity_Field_Value}\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
						}
						else
						{
							$insert_query .= "(\'$Hash_buyer{$Entity_Field_Value}\',\'$Hash_batch{$Entity_Field_Value}\',\'$Month_Of_Document\',date(now()),\'$Hash_count{$Entity_Field_Value}\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
							$insert_query =~ s/\,\s*$//igs;
							$insert_query .= ';';
							&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
							$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
							$insert_collect = 0;
						}
					}
				}
				else
				{
					# print "ELSE Ret_Buyer_ID :: $Ret_Buyer_ID \n";
					if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
					{
						$Sno++;
						$insert_collect++;
						$Supplier_Name_Value=~s/\'/\'\'/igs;
						$Department_Name_Value=~s/\'/\'\'/igs;
						$Requirement_Value=~s/\'/\'\'/igs;
						$Requirement_2_Value=~s/\'/\'\'/igs;
						if($insert_collect <= 990)
						{
						$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
						}
						else
						{
							$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
							$insert_query =~ s/\,\s*$//igs;
							$insert_query .= ';';
							&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
							$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
							$insert_collect = 0;
						}
					}
				}
			}
		}
		if(not $csv->eof)
		{
			$csv->error_diag();
		}
		close $data;
		
		if($insert_collect >= 1)
		{
			$insert_query =~ s/\,\s*$//igs;
			$insert_query .= ';';
			&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
		}
		my @Value = values %Header_col_position;
		print "###################################Value :: @Value\n";
		@Value = grep { $_ ne '' } @Value;
		my $Mapped_Col = $#Value+1;
		# return ($Sno,$Mapped_Col);
		return ($Sno,$Mapped_Col,\%Hash_buyer,\%Hash_batch,\%Hash_count,\%Hash_Source_Count);		
	}
	else
	{
		while ($fields = $csv->getline($data)) 
		{
			$fields=~s/[^[:print:]]/\,/igs;
			my @fieldsarray = @{$fields};
			&DataCollect(\@fieldsarray);
			my ($Transaction_ID_Value,$Requirement_Value,$Amount_Value,$Start_Date_Value,$End_Date_Value,$Supplier_Name_Value,$Department_Name_Value,$Requirement_2_Value,$Entity_Field_Value);
			if ( $Headercheck == 0 )
			{
				foreach my $Col (0 .. $#fieldsarray)
				{
					# print "Value	:: $fieldsarray[$Col]\n";
					my @Header = grep { $fieldsarray[$Col] =~ /$_/is } @Headerarray;
					# print "HEADER :: @Header :: $Col \n";<STDIN>;
					$Header_col_position{$Header[0]} = $Col if (@Header);
				}
				
				if ($Header_col_position{$Transaction_ID}||$Header_col_position{$Requirement}||$Header_col_position{$Amount}||$Header_col_position{$Start_Date}||$Header_col_position{$End_Date}||$Header_col_position{$Supplier_Name}||$Header_col_position{$Department_Name}||$Header_col_position{$Requirement_2})
				{
					$Headercheck = 1;
				}
			}
			else
			{
				$Transaction_ID_Value 	= &Trim($fieldsarray[$Header_col_position{$Transaction_ID}]) if ($Header_col_position{$Transaction_ID} ne '');
				$Requirement_Value 		= &Trim($fieldsarray[$Header_col_position{$Requirement}]) if ($Header_col_position{$Requirement} ne '');
				$Amount_Value 			= &Trim($fieldsarray[$Header_col_position{$Amount}]) if ($Header_col_position{$Amount} ne '');
				$Start_Date_Value 		= &Trim($fieldsarray[$Header_col_position{$Start_Date}]) if ($Header_col_position{$Start_Date} ne '');
				$End_Date_Value 		= &Trim($fieldsarray[$Header_col_position{$End_Date}]) if ($Header_col_position{$End_Date} ne '');
				$Supplier_Name_Value 	= &Trim($fieldsarray[$Header_col_position{$Supplier_Name}]) if ($Header_col_position{$Supplier_Name} ne '');
				$Department_Name_Value 	= &Trim($fieldsarray[$Header_col_position{$Department_Name}]) if ($Header_col_position{$Department_Name} ne '');
				$Requirement_2_Value	= &Trim($fieldsarray[$Header_col_position{$Requirement_2}]) if ($Header_col_position{$Requirement_2} ne '');
				$Entity_Field_Value	= &Trim($fieldsarray[$Header_col_position{$Entity_Field}]) if ($Header_col_position{$Entity_Field} ne '');
							
				# print "Entity_Field	:: $Entity_Field\n";
				if($Entity_Field)
				{
					unless($Hash_buyer{$Entity_Field_Value})
					{
						my ($Ret_Buyer_ID,$Ret_Entity_Code) = &BIP_DB_V1::RetrieveBuyerID($dbh,"field_map_details_automation",$Buyer_ID,$Entity_Field_Value);
						# print "Ret_Buyer_ID :: $Ret_Buyer_ID \n";
						if($Ret_Buyer_ID)
						{
							&BIP_DB_V1::Insert_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Ret_Buyer_ID,$LinkInfo,$Doc_Name,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);
							$Batch_ID = &BIP_DB_V1::BatchID_RetrieveUrl($dbh,$Ret_Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
							# print "Batch_ID :: $Entity_Field_Value ::  $Batch_ID \n";
							if ($Ret_Entity_Code)
							{
								my $Entity_Code_Temp = $Ret_Entity_Code;
								$Entity_Code_Temp =~ s/^\s*\|//igs;
								while($Entity_Code_Temp =~ m/([^<]*?)\|/igs)
								{
									my $Entity_Values = $1;
									$Hash_buyer{$Entity_Values} = $Ret_Buyer_ID;
									$Hash_batch{$Entity_Values} = $Batch_ID;
								}	
							}
						}
						else
						{
							$Hash_buyer{$Entity_Field_Value} = '-';
							$Hash_batch{$Entity_Field_Value} = '-';
						}
					}
					# print "Entity_Code###### $Hash_batch{$Entity_Field_Value}\n";
					next if($Hash_buyer{$Entity_Field_Value} eq '-');
					if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
					{
						$insert_collect++;
						$Supplier_Name_Value=~s/\'/\'\'/igs;
						$Department_Name_Value=~s/\'/\'\'/igs;
						$Requirement_Value=~s/\'/\'\'/igs;
						$Requirement_2_Value=~s/\'/\'\'/igs;
						$Hash_count{$Entity_Field_Value}++;
						$Hash_Source_Count{$Entity_Field_Value}++;
						if($insert_collect <= 990)
						{
						$insert_query .= "(\'$Hash_buyer{$Entity_Field_Value}\',\'$Hash_batch{$Entity_Field_Value}\',\'$Month_Of_Document\',date(now()),\'$Hash_count{$Entity_Field_Value}\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
						}
						else
						{
							$insert_query .= "(\'$Hash_buyer{$Entity_Field_Value}\',\'$Hash_batch{$Entity_Field_Value}\',\'$Month_Of_Document\',date(now()),\'$Hash_count{$Entity_Field_Value}\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
							$insert_query =~ s/\,\s*$//igs;
							$insert_query .= ';';
							&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
							$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
							$insert_collect = 0;
						}
					}
				}
				else
				{
					# print "ELSE Ret_Buyer_ID :: $Ret_Buyer_ID \n";
					if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
					{
						$Sno++;
						$insert_collect++;
						$Supplier_Name_Value=~s/\'/\'\'/igs;
						$Department_Name_Value=~s/\'/\'\'/igs;
						$Requirement_Value=~s/\'/\'\'/igs;
						$Requirement_2_Value=~s/\'/\'\'/igs;
						if($insert_collect <= 990)
						{
						$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
						}
						else
						{
							$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
							$insert_query =~ s/\,\s*$//igs;
							$insert_query .= ';';
							&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
							$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
							$insert_collect = 0;
						}
					}
				}
			}
		}
		if(not $csv->eof)
		{
			$csv->error_diag();
		}
		close $data;
		
		if($insert_collect >= 1)
		{
			$insert_query =~ s/\,\s*$//igs;
			$insert_query .= ';';
			&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
		}
		my @Value = values %Header_col_position;
		print "###################################Value :: @Value\n";
		@Value = grep { $_ ne '' } @Value;
		my $Mapped_Col = $#Value+1;
		# return ($Sno,$Mapped_Col);
		return ($Sno,$Mapped_Col,\%Hash_buyer,\%Hash_batch,\%Hash_count,\%Hash_Source_Count);
	}	
	
}

sub Read_Json_File()
{
	my $Buyer_ID 			= shift;
	my $Batch_ID 			= shift;
	my $JSON_NAME 			= shift;
	my $Header_array		= shift;
	my $Month_Of_Document	= shift;
	my @Headerarray = @{$Header_array};
	
	
	my $Outfile = $JSON_NAME;
	print "LOADFILE : $Outfile\n";
	if($Outfile eq '' && $Outfile!~m/\.json/igs && !(-e $Outfile))
	{
		print "\nPlease select correct file !!! file extenstion ofr file type is incorrect please check\n";
		exit;
	}
	my ($File_Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field,$Entity_Code) = &BIP_DB_V1::Column_Name_RetrieveUrl($dbh,$Buyer_ID);
	my $insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
	
	my $Headercheck = 0;
	my $Sno = 0;
	my $insert_collect = 0;
	my %Header_col_position;
	my (%Hash_buyer,%Hash_batch,%Hash_count,%Hash_Source_Count);
	my $sum = 0;
	my $Valid_Records=0;
	
	my $json;
	{
	  local $/; #Enable 'slurp' mode
	  open my $fh, "<", "$Outfile";
	  $json = <$fh>;
	  close $fh;
	}
	my $data=$json;
	my $Remove_Same_header_Check;
	while($data=~m/\[(\"[^>]*?\")\](?:\,|\])/igs)
	{
		my $first=$1;
		next if($first=~m/$Remove_Same_header_Check/is);
		next if($first!~m/\"\,\"/is);
		print "\nfirst mani data ---->$first \n";
		my @Field_Header=split("\"\,\"",$first);
		# my @fieldsarray = map { $_ =~ s/(\"|\/|\(|\)|\(|\))//igs; $_ } @Field_Header;
		my @fieldsarray = map { $_ =~ s/\"//igs; $_ } @Field_Header;
		# print join("\n",@Field_Header);<STDIN>;
		
		my ($Transaction_ID_Value,$Requirement_Value,$Amount_Value,$Start_Date_Value,$End_Date_Value,$Supplier_Name_Value,$Department_Name_Value,$Requirement_2_Value,$Entity_Field_Value);
		if ($Headercheck == 0)
		{
			foreach my $Col (0 .. $#fieldsarray)
			{
				my @Header = grep { $fieldsarray[$Col] =~ /$_/is } @Headerarray;
				# print "HEADER :: @Header :: $Col \n";<STDIN>;
				$Header_col_position{$Header[0]} = $Col if (@Header);
			}
			# print "\nsuccess\n";exit;
			if ($Header_col_position{$Transaction_ID}||$Header_col_position{$Requirement}||$Header_col_position{$Amount}||$Header_col_position{$Start_Date}||$Header_col_position{$End_Date}||$Header_col_position{$Supplier_Name}||$Header_col_position{$Department_Name}||$Header_col_position{$Requirement_2})
			{
				$Headercheck = 1;
				$Remove_Same_header_Check=$first;
				# print "\nsuccess\n";exit;
			}
		}	
		else
		{
			# print "Amount Before 	:: $fieldsarray[$Header_col_position{$Amount}]\n";
			$Transaction_ID_Value 	= &Trim($fieldsarray[$Header_col_position{$Transaction_ID}]) if ($Header_col_position{$Transaction_ID} ne '');
			$Requirement_Value 		= &Trim($fieldsarray[$Header_col_position{$Requirement}]) if ($Header_col_position{$Requirement} ne '');
			$Amount_Value 			= &Trim($fieldsarray[$Header_col_position{$Amount}]) if ($Header_col_position{$Amount} ne '');
			$Start_Date_Value 		= &Trim($fieldsarray[$Header_col_position{$Start_Date}]) if ($Header_col_position{$Start_Date} ne '');
			$End_Date_Value 		= &Trim($fieldsarray[$Header_col_position{$End_Date}]) if ($Header_col_position{$End_Date} ne '');
			$Supplier_Name_Value 	= &Trim($fieldsarray[$Header_col_position{$Supplier_Name}]) if ($Header_col_position{$Supplier_Name} ne '');
			$Department_Name_Value 	= &Trim($fieldsarray[$Header_col_position{$Department_Name}]) if ($Header_col_position{$Department_Name} ne '');
			$Requirement_2_Value	= &Trim($fieldsarray[$Header_col_position{$Requirement_2}]) if ($Header_col_position{$Requirement_2} ne '');
			$Entity_Field_Value	= &Trim($fieldsarray[$Header_col_position{$Entity_Field}]) if ($Header_col_position{$Entity_Field} ne '');	
			print			"\nTransaction_ID_Value--->$Transaction_ID_Value\n";
			print			"\nAmount_Value--->$Amount_Value\n";
			
			if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
			{
				$Sno++;
				$insert_collect++;
				$Supplier_Name_Value=~s/\'/\'\'/igs;
				$Department_Name_Value=~s/\'/\'\'/igs;
				$Requirement_Value=~s/\'/\'\'/igs;
				$Requirement_2_Value=~s/\'/\'\'/igs;
				if($insert_collect <= 990)
				{
				$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
				}
				else
				{
					$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
					$insert_query =~ s/\,\s*$//igs;
					$insert_query .= ';';
					&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
					$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values";
					$insert_collect = 0;
				}
			}					
		}		
	}
	if($insert_collect >= 1)
	{
		$insert_query =~ s/\,\s*$//igs;
		$insert_query .= ';';
		&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
	}
	print "\nI am in last\n";
	my @Value = values %Header_col_position;
	print "###################################Value :: @Value\n";
	@Value = grep { $_ ne '' } @Value;
	my $Mapped_Col = $#Value+1;
	# return ($Sno,$Mapped_Col);
	return ($Sno,$Mapped_Col,\%Hash_buyer,\%Hash_batch,\%Hash_count,\%Hash_Source_Count);
}
sub Read_txt_File()
{
	my $Buyer_ID 			= shift;
	my $Batch_ID 			= shift;
	my $Filepath 			= shift;
	my $Docname				= shift;
	my $Month_Of_Document	= shift;
	my $Header_array		= shift;
	my $LinkInfo			= shift;
	my $Doc_Name			= shift;
	my $StartDate			= shift;
	my $EndDate				= shift;
	my $Number_Of_Records	= shift;
	my $Status				= shift;
	my $Failed_Reason		= shift;
	my @Headerarray = @{$Header_array};
	
	my $Outfile = "$Filepath\\$Docname";
	print "LOADFILE : $Outfile\n";
	my ($File_Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field,$Entity_Code) = &BIP_DB_V1::Column_Name_RetrieveUrl($dbh,$Buyer_ID);
	my $insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
	
	my $Headercheck = 0;
	my $Sno = 0;
	my $insert_collect = 0;
	my %Header_col_position;
	my (%Hash_buyer,%Hash_batch,%Hash_count,%Hash_Source_Count);
	my $sum = 0;
	my $Valid_Records=0;
	
	$Outfile=~s/\s//igs;
	
	
	open(my $fh, '<', $Outfile) or die "Can't read file '$Outfile' [$!]\n";
	# print "FH	:: $fh\n";
	my $flag = 'N';
	while (<$fh>) 
	{
		my $line=$_;
		chomp($line);
		my @fieldsarray;
		# print "Line ::$line\n";
		if ($line=~m/	/is)
		{
			# print "IFFFFFFFFFFF\n";
			$line=~s/[^[:print:]]/\`/igs;
			$line=~s/\`\`\`/|/igs;
			$line=~s/\`\`/|/igs;
			# $line=~s/,\s,/ /igs;
			$line=~s/\`//igs;
			# print "line	:: $line\n";
			@fieldsarray = split('\|', $line);
			@fieldsarray = map { $_ =~ s/\"//igs; $_ } @fieldsarray;
			my ($Transaction_ID_Value,$Requirement_Value,$Amount_Value,$Start_Date_Value,$End_Date_Value,$Supplier_Name_Value,$Department_Name_Value,$Requirement_2_Value,$Entity_Field_Value);
			if ( $Headercheck == 0 )
			{
				foreach my $Col (0 .. $#fieldsarray)
				{
					# print "Value	:: $fieldsarray[$Col]\n";
					my @Header = grep { $fieldsarray[$Col] =~ /$_/is } @Headerarray;
					# print "HEADER :: @Header :: $Col \n";<STDIN>;
					$Header_col_position{$Header[0]} = $Col-1 if (@Header);
				}
				
				if ($Header_col_position{$Transaction_ID}||$Header_col_position{$Requirement}||$Header_col_position{$Amount}||$Header_col_position{$Start_Date}||$Header_col_position{$End_Date}||$Header_col_position{$Supplier_Name}||$Header_col_position{$Department_Name}||$Header_col_position{$Requirement_2})
				{
					$Headercheck = 1;
				}
			}
			else
			{
				$Transaction_ID_Value 	= &Trim($fieldsarray[$Header_col_position{$Transaction_ID}]) if ($Header_col_position{$Transaction_ID} ne '');
				$Requirement_Value 		= &Trim($fieldsarray[$Header_col_position{$Requirement}]) if ($Header_col_position{$Requirement} ne '');
				$Amount_Value 			= &Trim($fieldsarray[$Header_col_position{$Amount}]) if ($Header_col_position{$Amount} ne '');
				$Start_Date_Value 		= &Trim($fieldsarray[$Header_col_position{$Start_Date}]) if ($Header_col_position{$Start_Date} ne '');
				$End_Date_Value 		= &Trim($fieldsarray[$Header_col_position{$End_Date}]) if ($Header_col_position{$End_Date} ne '');
				$Supplier_Name_Value 	= &Trim($fieldsarray[$Header_col_position{$Supplier_Name}]) if ($Header_col_position{$Supplier_Name} ne '');
				$Department_Name_Value 	= &Trim($fieldsarray[$Header_col_position{$Department_Name}]) if ($Header_col_position{$Department_Name} ne '');
				$Requirement_2_Value	= &Trim($fieldsarray[$Header_col_position{$Requirement_2}]) if ($Header_col_position{$Requirement_2} ne '');
				$Entity_Field_Value	= &Trim($fieldsarray[$Header_col_position{$Entity_Field}]) if ($Header_col_position{$Entity_Field} ne '');
				# print "Entity_Field	:: $Entity_Field\n";
				if($Entity_Field)
				{
					unless($Hash_buyer{$Entity_Field_Value})
					{
						my ($Ret_Buyer_ID,$Ret_Entity_Code) = &BIP_DB_V1::RetrieveBuyerID($dbh,"field_map_details_automation",$Buyer_ID,$Entity_Field_Value);
						# print "Ret_Buyer_ID :: $Ret_Buyer_ID \n";
						if($Ret_Buyer_ID)
						{
							&BIP_DB_V1::Insert_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Ret_Buyer_ID,$LinkInfo,$Doc_Name,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);
							$Batch_ID = &BIP_DB_V1::BatchID_RetrieveUrl($dbh,$Ret_Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
							# print "Batch_ID :: $Entity_Field_Value ::  $Batch_ID \n";
							if ($Ret_Entity_Code)
							{
								my $Entity_Code_Temp = $Ret_Entity_Code;
								$Entity_Code_Temp =~ s/^\s*\|//igs;
								while($Entity_Code_Temp =~ m/([^<]*?)\|/igs)
								{
									my $Entity_Values = $1;
									$Hash_buyer{$Entity_Values} = $Ret_Buyer_ID;
									$Hash_batch{$Entity_Values} = $Batch_ID;
								}	
							}
						}
						else
						{
							$Hash_buyer{$Entity_Field_Value} = '-';
							$Hash_batch{$Entity_Field_Value} = '-';
						}
					}
					# print "Entity_Code###### $Hash_batch{$Entity_Field_Value}\n";
					next if($Hash_buyer{$Entity_Field_Value} eq '-');
					if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
					{
						$insert_collect++;
						$Supplier_Name_Value=~s/\'/\'\'/igs;
						$Department_Name_Value=~s/\'/\'\'/igs;
						$Requirement_Value=~s/\'/\'\'/igs;
						$Requirement_2_Value=~s/\'/\'\'/igs;
						$Hash_count{$Entity_Field_Value}++;
						$Hash_Source_Count{$Entity_Field_Value}++;
						if($insert_collect <= 990)
						{
						$insert_query .= "(\'$Hash_buyer{$Entity_Field_Value}\',\'$Hash_batch{$Entity_Field_Value}\',\'$Month_Of_Document\',date(now()),\'$Hash_count{$Entity_Field_Value}\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
						}
						else
						{
							$insert_query .= "(\'$Hash_buyer{$Entity_Field_Value}\',\'$Hash_batch{$Entity_Field_Value}\',\'$Month_Of_Document\',date(now()),\'$Hash_count{$Entity_Field_Value}\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
							$insert_query =~ s/\,\s*$//igs;
							$insert_query .= ';';
							&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
							$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
							$insert_collect = 0;
						}
					}
				}
				else
				{
					# print "ELSE Ret_Buyer_ID :: $Ret_Buyer_ID \n";
					if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
					{
						$Sno++;
						$insert_collect++;
						$Supplier_Name_Value=~s/\'/\'\'/igs;
						$Department_Name_Value=~s/\'/\'\'/igs;
						$Requirement_Value=~s/\'/\'\'/igs;
						$Requirement_2_Value=~s/\'/\'\'/igs;
						if($insert_collect <= 990)
						{
						$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
						}
						else
						{
							$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
							$insert_query =~ s/\,\s*$//igs;
							$insert_query .= ';';
							&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
							$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
							$insert_collect = 0;
						}
					}
				}
			}
		}
		else
		{
			$flag='Y';
			last;
		}
	}	
	print "Flag	:: $flag\n";
	if ($flag eq 'Y')
	{
		my $csv = Text::CSV->new ({
		binary => 1,
		allow_loose_quotes  => 1,
		allow_loose_escapes => 1,
		auto_diag => 1,
		escape_char => "\\",
		sep_char => ','
		});  
		open(my $data, '<', "$Outfile") or die "Could not open '$Doc_Name' $!\n";
		while (my $fields = $csv->getline($data)) 
		{
			my @fieldsarray = @{$fields};
			my ($Transaction_ID_Value,$Requirement_Value,$Amount_Value,$Start_Date_Value,$End_Date_Value,$Supplier_Name_Value,$Department_Name_Value,$Requirement_2_Value,$Entity_Field_Value);
			if ( $Headercheck == 0 )
			{
				foreach my $Col (0 .. $#fieldsarray)
				{
					# print "Value	:: $fieldsarray[$Col]\n";
					my @Header = grep { $fieldsarray[$Col] =~ /$_/is } @Headerarray;
					# print "HEADER :: @Header :: $Col \n";<STDIN>;
					$Header_col_position{$Header[0]} = $Col if (@Header);
				}
				
				if ($Header_col_position{$Transaction_ID}||$Header_col_position{$Requirement}||$Header_col_position{$Amount}||$Header_col_position{$Start_Date}||$Header_col_position{$End_Date}||$Header_col_position{$Supplier_Name}||$Header_col_position{$Department_Name}||$Header_col_position{$Requirement_2})
				{
					$Headercheck = 1;
				}
			}
			else
			{
				$Transaction_ID_Value 	= &Trim($fieldsarray[$Header_col_position{$Transaction_ID}]) if ($Header_col_position{$Transaction_ID} ne '');
				$Requirement_Value 		= &Trim($fieldsarray[$Header_col_position{$Requirement}]) if ($Header_col_position{$Requirement} ne '');
				$Amount_Value 			= &Trim($fieldsarray[$Header_col_position{$Amount}]) if ($Header_col_position{$Amount} ne '');
				$Start_Date_Value 		= &Trim($fieldsarray[$Header_col_position{$Start_Date}]) if ($Header_col_position{$Start_Date} ne '');
				$End_Date_Value 		= &Trim($fieldsarray[$Header_col_position{$End_Date}]) if ($Header_col_position{$End_Date} ne '');
				$Supplier_Name_Value 	= &Trim($fieldsarray[$Header_col_position{$Supplier_Name}]) if ($Header_col_position{$Supplier_Name} ne '');
				$Department_Name_Value 	= &Trim($fieldsarray[$Header_col_position{$Department_Name}]) if ($Header_col_position{$Department_Name} ne '');
				$Requirement_2_Value	= &Trim($fieldsarray[$Header_col_position{$Requirement_2}]) if ($Header_col_position{$Requirement_2} ne '');
				$Entity_Field_Value	= &Trim($fieldsarray[$Header_col_position{$Entity_Field}]) if ($Header_col_position{$Entity_Field} ne '');
				# print "Entity_Field	:: $Entity_Field\n";
				if($Entity_Field)
				{
					unless($Hash_buyer{$Entity_Field_Value})
					{
						my ($Ret_Buyer_ID,$Ret_Entity_Code) = &BIP_DB_V1::RetrieveBuyerID($dbh,"field_map_details_automation",$Buyer_ID,$Entity_Field_Value);
						# print "Ret_Buyer_ID :: $Ret_Buyer_ID \n";
						if($Ret_Buyer_ID)
						{
							&BIP_DB_V1::Insert_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Ret_Buyer_ID,$LinkInfo,$Doc_Name,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);
							$Batch_ID = &BIP_DB_V1::BatchID_RetrieveUrl($dbh,$Ret_Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
							# print "Batch_ID :: $Entity_Field_Value ::  $Batch_ID \n";
							if ($Ret_Entity_Code)
							{
								my $Entity_Code_Temp = $Ret_Entity_Code;
								$Entity_Code_Temp =~ s/^\s*\|//igs;
								while($Entity_Code_Temp =~ m/([^<]*?)\|/igs)
								{
									my $Entity_Values = $1;
									$Hash_buyer{$Entity_Values} = $Ret_Buyer_ID;
									$Hash_batch{$Entity_Values} = $Batch_ID;
								}	
							}
						}
						else
						{
							$Hash_buyer{$Entity_Field_Value} = '-';
							$Hash_batch{$Entity_Field_Value} = '-';
						}
					}
					# print "Entity_Code###### $Hash_batch{$Entity_Field_Value}\n";
					next if($Hash_buyer{$Entity_Field_Value} eq '-');
					if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
					{
						$insert_collect++;
						$Supplier_Name_Value=~s/\'/\'\'/igs;
						$Department_Name_Value=~s/\'/\'\'/igs;
						$Requirement_Value=~s/\'/\'\'/igs;
						$Requirement_2_Value=~s/\'/\'\'/igs;
						$Hash_count{$Entity_Field_Value}++;
						$Hash_Source_Count{$Entity_Field_Value}++;
						if($insert_collect <= 990)
						{
						$insert_query .= "(\'$Hash_buyer{$Entity_Field_Value}\',\'$Hash_batch{$Entity_Field_Value}\',\'$Month_Of_Document\',date(now()),\'$Hash_count{$Entity_Field_Value}\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
						}
						else
						{
							$insert_query .= "(\'$Hash_buyer{$Entity_Field_Value}\',\'$Hash_batch{$Entity_Field_Value}\',\'$Month_Of_Document\',date(now()),\'$Hash_count{$Entity_Field_Value}\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
							$insert_query =~ s/\,\s*$//igs;
							$insert_query .= ';';
							&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
							$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
							$insert_collect = 0;
						}
					}
				}
				else
				{
					# print "ELSE Ret_Buyer_ID :: $Ret_Buyer_ID \n";
					if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
					{
						$Sno++;
						$insert_collect++;
						$Supplier_Name_Value=~s/\'/\'\'/igs;
						$Department_Name_Value=~s/\'/\'\'/igs;
						$Requirement_Value=~s/\'/\'\'/igs;
						$Requirement_2_Value=~s/\'/\'\'/igs;
						if($insert_collect <= 990)
						{
						$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
						}
						else
						{
							$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
							$insert_query =~ s/\,\s*$//igs;
							$insert_query .= ';';
							&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
							$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
							$insert_collect = 0;
						}
					}
				}
			}
			
		}
		if(not $csv->eof)
		{
			$csv->error_diag();
		}
		close $data;
	}
	if($insert_collect >= 1)
	{
		$insert_query =~ s/\,\s*$//igs;
		$insert_query .= ';';
		&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
	}
	
	my @Value = values %Header_col_position;
	print "###################################Value :: @Value\n";
	@Value = grep { $_ ne '' } @Value;
	my $Mapped_Col = $#Value+1;
	# return ($Sno,$Mapped_Col);
	return ($Sno,$Mapped_Col,\%Hash_buyer,\%Hash_batch,\%Hash_count,\%Hash_Source_Count);
}

sub Read_Ods_File()
{
	my $Buyer_ID 			= shift;
	my $Batch_ID 			= shift;
	my $Filepath 			= shift;
	my $Docname				= shift;
	my $Month_Of_Document	= shift;
	my $Header_array		= shift;
	my $LinkInfo			= shift;
	my $Doc_Name			= shift;
	my $StartDate			= shift;
	my $EndDate				= shift;
	my $Number_Of_Records	= shift;
	my $Status				= shift;
	my $Failed_Reason		= shift;
	my @Headerarray = @{$Header_array};
	
	my $Outfile = "$Filepath\\$Docname";
	
	print "LOADFILE ODS: $Outfile\n";
	my ($File_Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field,$Entity_Code) = &BIP_DB_V1::Column_Name_RetrieveUrl($dbh,$Buyer_ID);
	my $insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
	my $Headercheck = 0;
	my $Sno = 0;
	my $insert_collect = 0;
	my %Header_col_position;
	my (%Hash_buyer,%Hash_batch,%Hash_count,%Hash_Source_Count);
	
	my %options = (
        ReplaceNewlineWith      => "",
        IncludeCoveredCells     => 0,
        DropHiddenRows          => 0,
        DropHiddenColumns       => 0,
        NoTruncate              => 0,
        StandardDate            => 0,
        StandardTime            => 0,
        OrderBySheet            => 0,
	);
	my $Workbook = read_sxc("$Outfile", \%options );
	foreach ( sort keys %$Workbook ) 
	{
		$#{$$Workbook{$_}} + 1;
		my $RowCount = $#{$$Workbook{$_}} + 1;
		# print "RowCount	:: $RowCount";
		foreach ( @{$$Workbook{$_}} ) 
		{
			my @fieldsarray=map { defined $_ ? $_ : '' } @{$_};
			my ($Transaction_ID_Value,$Requirement_Value,$Amount_Value,$Start_Date_Value,$End_Date_Value,$Supplier_Name_Value,$Department_Name_Value,$Requirement_2_Value,$Entity_Field_Value);
			if ($Headercheck == 0)
			{
				foreach my $Col (0 .. $#fieldsarray)
				{
					my @Header = grep { $fieldsarray[$Col] =~ /$_/is } @Headerarray;
					# print "HEADER :: @Header :: $Col \n";<STDIN>;
					$Header_col_position{$Header[0]} = $Col if (@Header);
				}
				if ($Header_col_position{$Transaction_ID}||$Header_col_position{$Requirement}||$Header_col_position{$Amount}||$Header_col_position{$Start_Date}||$Header_col_position{$End_Date}||$Header_col_position{$Supplier_Name}||$Header_col_position{$Department_Name}||$Header_col_position{$Requirement_2})
				{
					$Headercheck = 1;
				}
			}	
			else
			{
				$Transaction_ID_Value 	= &Trim($fieldsarray[$Header_col_position{$Transaction_ID}]) if ($Header_col_position{$Transaction_ID} ne '');
				$Requirement_Value 		= &Trim($fieldsarray[$Header_col_position{$Requirement}]) if ($Header_col_position{$Requirement} ne '');
				$Amount_Value 			= &Trim($fieldsarray[$Header_col_position{$Amount}]) if ($Header_col_position{$Amount} ne '');
				$Start_Date_Value 		= &Trim($fieldsarray[$Header_col_position{$Start_Date}]) if ($Header_col_position{$Start_Date} ne '');
				$End_Date_Value 		= &Trim($fieldsarray[$Header_col_position{$End_Date}]) if ($Header_col_position{$End_Date} ne '');
				$Supplier_Name_Value 	= &Trim($fieldsarray[$Header_col_position{$Supplier_Name}]) if ($Header_col_position{$Supplier_Name} ne '');
				$Department_Name_Value 	= &Trim($fieldsarray[$Header_col_position{$Department_Name}]) if ($Header_col_position{$Department_Name} ne '');
				$Requirement_2_Value	= &Trim($fieldsarray[$Header_col_position{$Requirement_2}]) if ($Header_col_position{$Requirement_2} ne '');
				$Entity_Field_Value	= &Trim($fieldsarray[$Header_col_position{$Entity_Field}]) if ($Header_col_position{$Entity_Field} ne '');		
				if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
				{
					$Sno++;
					$insert_collect++;
					$Supplier_Name_Value=~s/\'/\'\'/igs;
					$Department_Name_Value=~s/\'/\'\'/igs;
					$Requirement_Value=~s/\'/\'\'/igs;
					$Requirement_2_Value=~s/\'/\'\'/igs;
					if($insert_collect <= 990)
					{
					$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
					}
					else
					{
						$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
						$insert_query =~ s/\,\s*$//igs;
						$insert_query .= ';';
						&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
						$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values";
						$insert_collect = 0;
					}
				}					
			}
		}	
	}
	if($insert_collect >= 1)
	{
		$insert_query =~ s/\,\s*$//igs;
		$insert_query .= ';';
		&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
	}
	my @Value = values %Header_col_position;
	print "###################################Value :: @Value\n";
	@Value = grep { $_ ne '' } @Value;
	my $Mapped_Col = $#Value+1;
	return ($Sno,$Mapped_Col);
	return ($Sno,$Mapped_Col,\%Hash_buyer,\%Hash_batch,\%Hash_count,\%Hash_Source_Count);
}

sub Trim()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	
	return $txt;
}


