package BIP_DB_V2;
### Package Name ####
use strict;
use DBI;
use DBD::ODBC;
require Exporter;
use Config::IniFiles;
my @ISA = qw(Exporter);
my @EXPORT = qw(ImageDownload);

###### DB Connection ####
sub DbConnection()
{
	my $cfg = new Config::IniFiles( -file => 'C:\Perl\lib\BIP_Config.ini', -nocase => 1);
	my %hash_ini;
	my @FileExten = $cfg -> Parameters('DBDETAILS');
	foreach my $FileExten (@FileExten)
	{
		my $FileExt =  $cfg -> val('DBDETAILS', $FileExten);
		$hash_ini{$FileExten}=$FileExt;
	}
	my $database_name = $hash_ini{'database_name'};
	my $server_name = $hash_ini{'server_name'};
	my $database_user = $hash_ini{'database_user'};
	my $database_pass = $hash_ini{'database_pass'};
	my $driver = "mysql"; 
	my $dsn = "DBI:$driver:database=$database_name;host=$server_name;";
	my $dbh = DBI->connect($dsn, $database_user, $database_pass ) or die $DBI::errstr;
	return $dbh;
}

###### Retrieve DocumentURL ####
sub RetrieveUrl()
{
	my $dbh 		= shift;
	my $Buyer_ID 	= shift;
	my $Input_Table	= shift;
	
	my $query = "select ID,Buyer_Name,Spent_Data_weblink from $Input_Table  where ID=\'$Buyer_ID\'";
	
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@ID,@Buyer_Name,@Spent_Data_weblink,@End_Date,@Type_of_Government,@Expressions,@Request_method,@Host,@Referer,@Hidden_Content,@File_Type,@Robot_Name);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@ID,Trim($record[0]));
		push(@Buyer_Name,Trim($record[1]));
		push(@Spent_Data_weblink,Trim($record[2]));
		push(@End_Date,Trim($record[3]));
		push(@Type_of_Government,Trim($record[4]));
		push(@Expressions,Trim($record[5]));
		push(@Request_method,Trim($record[6]));
		push(@Host,Trim($record[7]));
		push(@Referer,Trim($record[8]));
		push(@Hidden_Content,Trim($record[9]));
		push(@File_Type,Trim($record[10]));
		push(@Robot_Name,Trim($record[11]));
	}
	$sth->finish();
	return (\@ID,\@Buyer_Name,\@Spent_Data_weblink,\@End_Date,\@Type_of_Government,\@Expressions,\@Request_method,\@Host,\@Referer,\@Hidden_Content,\@File_Type,\@Robot_Name);
}

sub Retrieve_Buyers_Download_Details()
{
	my $dbh 		= shift;
	my $Input_Table	= shift;
	
	my $query = "select Buyer_ID,Batch_ID from $Input_Table  where Formatted_Flag=\'N\'";
	
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@Buyer_ID,@Batch_ID);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@Buyer_ID,Trim($record[0]));
		push(@Batch_ID,Trim($record[1]));
	}
	$sth->finish();
	return (\@Buyer_ID,\@Batch_ID);
}

sub Retrieve_Source_Data()
{
	my $dbh 		= shift;
	my $Buyer_ID 	= shift;
	my $Batch_ID 	= shift;
	my $Input_Table	= shift;
	
	my $query = "select Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2 from $Input_Table   where Buyer_ID=\'$Buyer_ID\' and Batch_ID=\'$Batch_ID\'";
	
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@Month_Of_Document,@Processed_Date,@Sno,@Transaction_ID,@Requirement,@Amount,@Start_Date,@End_Date,@Supplier_Name,@Department_Name,@Requirement_2);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@Month_Of_Document,Trim($record[0]));
		push(@Processed_Date,Trim($record[1]));
		push(@Sno,Trim($record[2]));
		push(@Transaction_ID,Trim($record[3]));
		push(@Requirement,Trim($record[4]));
		push(@Amount,Trim($record[5]));
		push(@Start_Date,Trim($record[6]));
		push(@End_Date,Trim($record[7]));
		push(@Supplier_Name,Trim($record[8]));
		push(@Department_Name,Trim($record[9]));
		push(@Requirement_2,Trim($record[10]));
	}
	$sth->finish();
	return (\@Month_Of_Document,\@Processed_Date,\@Sno,\@Transaction_ID,\@Requirement,\@Amount,\@Start_Date,\@End_Date,\@Supplier_Name,\@Department_Name,\@Requirement_2);
}

sub Column_Name_RetrieveUrl()
{
	my $dbh 		= shift;
	my $Buyer_ID 	= shift;
	
	my $query = "select Type,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2,Entity_Field,Entity_Code from field_map_details_Automation where Buyer_ID=\'$Buyer_ID\'";
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my ($Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field,$Entity_Code);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		$Type=Trim($record[0]);
		$Transaction_ID=Trim($record[1]);
		$Requirement=Trim($record[2]);
		$Amount=Trim($record[3]);
		$Start_Date=Trim($record[4]);
		$End_Date=Trim($record[5]);
		$Supplier_Name=Trim($record[6]);
		$Department_Name=Trim($record[7]);
		$Requirement_2=Trim($record[8]);
		$Entity_Field=Trim($record[9]);
		$Entity_Code=Trim($record[10]);
	}
	$sth->finish();
	return ($Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field,$Entity_Code);
}


sub Supplier_Name_Matching()
{
	my $dbh 		= shift;
	my $Supplier_Name_withoutspace 	= shift;
	
	my $query = "select Supplier_Name from SUPPLIER_LIST where ID=(select Matching_ID from SUPPLIER_LIST where Supplier_Name_withoutspace=\'$Supplier_Name_withoutspace\')";
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my ($Supplier_Name);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		$Supplier_Name=Trim($record[0]);
	}
	$sth->finish();
	return ($Supplier_Name);
}


sub BatchID_RetrieveUrl()
{
	my $dbh 		= shift;
	my $Buyer_ID 	= shift;
	my $Input_Table 	= shift;
	
	my $query = "select max(Batch_ID) from $Input_Table  where Buyer_ID=\'$Buyer_ID\'";
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my ($Batch_ID);
	while(my @record = $sth->fetchrow)
	{
		$Batch_ID=Trim($record[0]);
	}
	$sth->finish();
	return ($Batch_ID);
}

sub RetrieveBuyerID()
{
	my $dbh 		 = shift;
	my $Input_Table  = shift;
	my $Buyer_ID 	 = shift;
	my $Entity_Code  = shift;
	
	# my $query = "select b.id from field_map_details_automation f,buyers b where b.id=f.buyer_id and b.Group_ID=$Buyer_ID and f.Entity_Code = \'$Entity_Code\'";
	my $query = "select b.id from field_map_details_automation f,buyers b where b.id=f.buyer_id and b.Group_ID=$Buyer_ID and f.Entity_Code regexp ('\\\\|$Entity_Code\\\\|')";
	
	# print "query :: $query \n";
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my ($id);
	while(my @record = $sth->fetchrow)
	{
		$id=Trim($record[0]);
	}
	$sth->finish();
	return ($id);
}



sub LinkInfo_RetrieveUrl()
{
	my $dbh 		= shift;
	my $Buyer_ID 	= shift;
	my $Input_Table 	= shift;
	
	my $query = "select LinkInfo from $Input_Table  where Buyer_ID=\'$Buyer_ID\'";
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my (@LinkInfo);
	while(my @record = $sth->fetchrow)
	{
		push(@LinkInfo,Trim($record[0]));
	}
	$sth->finish();
	return (\@LinkInfo);
}

# ###### Pdf Download Path ####
# sub DownloadPath()
# {
	# my $dbh 			= shift;
	# my $downloadpath;
	# my $query = "Select Picklistvalue from PickList where PicklistCategory='ONE_APP' and PickListField='PDF_DOWNLOAD_PATH' AND ID=436";
	# my $sth = $dbh->prepare($query);
	# $sth->execute();
	# while(my @record = $sth->fetchrow)
	# {
		# $downloadpath = Trim($record[0]);
	# }
	# $sth->finish();
	# return $downloadpath;
# }

sub Update_Download_Details()
{
	my $dbh 		= shift;
	my $Table_Name	= shift;
	my $Buyer_ID	= shift;
	my $Batch_ID 	= shift;
	my $Supplier_Name_Matched_Count 	= shift;
	my $Supplier_Name_NotMatched_Count	= shift;
	my $Formatted_Flag	= shift;
	my $Supplier_Match	= shift;
	
	my $query = "Update $Table_Name set No_of_Suppliers_Matched=\'$Supplier_Name_Matched_Count\',No_of_Suppliers_Not_Matched=\'$Supplier_Name_NotMatched_Count\',Formatted_Flag=\'$Formatted_Flag\',Supplier_Match=\'$Supplier_Match\' where Buyer_ID=\'$Buyer_ID\' and Batch_ID=\'$Batch_ID\'";
	my $sth = $dbh->prepare($query);
	if($sth->execute())
	{
		# print "Updated\n";
		$sth->finish();	
	}
	else
	{
		print "QUERY:: $query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

sub Update_Import_Flag()
{
	my $dbh 		= shift;
	my $Table_Name	= shift;
	my $Buyer_ID	= shift;
	my $Batch_ID 	= shift;
	my $Import_Flag	= shift;
	my $Field_Match	= shift;
	my $EndDate		= shift;
	my $Number_Of_Records	= shift;
	
	my $query = "Update $Table_Name set Import_Flag=\'$Import_Flag\', Field_Match=\'$Field_Match\',EndDate=\'$EndDate\',No_of_Records=\'$Number_Of_Records\' where Buyer_ID=\'$Buyer_ID\' and Batch_ID=\'$Batch_ID\'";
	my $sth = $dbh->prepare($query);
	if($sth->execute())
	{
		# print "Updated\n";
		$sth->finish();	
	}
	else
	{
		print "QUERY:: $query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

###### Update DB ####
sub Update_Buyers_Details
{
	my $dbh			= shift;
	my $Buyer_ID 	= shift;
	my $Date_Researched 	= shift;
	my $No_of_Document_Researched 	= shift;
	my $Document_Month_Researched 	= shift;

	my $query = "Update Buyers_Tmp  set Date_Researched=\'$Date_Researched\', No_of_Document_Researched=\'$No_of_Document_Researched\', Document_Month_Researched=\'$Document_Month_Researched\' where ID=\'$Buyer_ID\'";

	my $sth = $dbh->prepare($query);
	if($sth->execute())
	{
		# print "Updated\n";
		$sth->finish();	
	}
	else
	{
		print "QUERY:: $query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

###### Select Count Details ####
sub Retrieve_Count_Details
{
	my $dbh			= shift;
	my $Table_Name 	= shift;
	my $Buyer_ID 	= shift;
	my $Batch_ID 	= shift;

	my $query = "select count(*) from $Table_Name where Buyer_ID=\'$Buyer_ID\' and Batch_ID=\'$Batch_ID\'";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my ($Count);
	while(my @record = $sth->fetchrow)
	{
		$Count=Trim($record[0]);
	}
	$sth->finish();
	return ($Count);
}


sub Insert_Spend_Data()
{
	my $dbh 			= shift;
	my $insert_query		= shift;
	 # print "insert_query :: $insert_query\n";exit;
	my $sth = $dbh->prepare($insert_query);
	if($sth->execute())
	{
		# print "Executed\n";
	}
	else
	{
		print "QUERY:: $insert_query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $insert_query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

sub Insert_Cleaned_Spend_Data()
{
	my $dbh 			= shift;
	my $Table_Name		= shift;
	my $Buyer_ID		= shift;
	my $Batch_ID		= shift;
	my $Month_Of_Document= shift;
	my $Processed_Date	= shift;
	my $S_No			= shift;
	my $Transaction_ID	= shift;
	my $Requirement		= shift;
	my $Amount			= shift;
	my $Start_Date		= shift;
	my $End_Date		= shift;
	my $Supplier_Name	= shift;
	my $Department_Name	= shift;
	my $Requirement_2	= shift;
	my $Supplier_Match_Flag	= shift;

	$Supplier_Name=~s/\'/\'\'/igs;
	$Department_Name=~s/\'/\'\'/igs;
	$Requirement=~s/\'/\'\'/igs;
	$Requirement_2=~s/\'/\'\'/igs;
	
	my $insert_query = "insert into $Table_Name   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2,Supplier_Match_Flag) values (\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',\'$Processed_Date\',\'$S_No\',\'$Transaction_ID\',\'$Requirement\',\'$Amount\',\'$Start_Date\',\'$End_Date\',\'$Supplier_Name\',\'$Department_Name\',\'$Requirement_2\',\'$Supplier_Match_Flag\') ";
	# print "$insert_query\n";
	my $sth = $dbh->prepare($insert_query);
	if($sth->execute())
	{
		# print "Executed\n";
	}
	else
	{
		print "QUERY:: $insert_query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $insert_query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

sub Insert_Download_Details()
{
	my $dbh 			= shift;
	my $Table_Name		= shift;
	my $Buyer_ID		= shift;
	my $LinkInfo		= shift;
	my $FileName		= shift;
	my $StartDate		= shift;
	my $EndDate			= shift;
	my $No_Of_Records	= shift;
	my $Status			= shift;
	my $Failed_Reason	= shift;
	
	$LinkInfo=~s/\'/\'\'/igs;
	
	my $insert_query = "insert into $Table_Name  (Buyer_ID,LinkInfo,FileName,StartDate,No_Of_Records,Status,Failed_Reason) values (\'$Buyer_ID\',\'$LinkInfo\',\'$FileName\',\'$StartDate\',\'$No_Of_Records\',\'$Status\',\'$Failed_Reason\') ";
	my $sth = $dbh->prepare($insert_query);
	if($sth->execute())
	{
		# print "Executed\n";
	}
	else
	{
		print "QUERY:: $insert_query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $insert_query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

sub Insert_Errors()
{
	my $dbh 	= shift;
	my $Buyer_ID= shift;
	my $Batch_ID	= shift;
	# my $Date	= shift;
	my $Error	= shift;
	my $S_no	= shift;
	
	
	my $insert_query = "insert into Logs  (Buyer_ID,Batch_ID,Researched_Date,Error,Sno) values (\'$Buyer_ID\',\'$Batch_ID\',now(),\'$Error\',\'$S_no\') ";
	# print "$insert_query\n";
	# <stdin>;
	my $sth = $dbh->prepare($insert_query);
	if($sth->execute())
	{
		# print "Executed\n";
	}
	else
	{
		print "QUERY:: $insert_query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $insert_query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

sub Insert_Batch_Details()
{
	my $dbh 	= shift;
	my $Buyer_ID= shift;
	my $Batch_ID	= shift;
	my $No_of_Records	= shift;
	
	
	my $insert_query = "insert into Batch_Details  (Buyer_ID,Batch_ID,No_of_Records) values (\'$Buyer_ID\',\'$Batch_ID\','$No_of_Records\') ";
	# print "$insert_query\n";
	# <stdin>;
	my $sth = $dbh->prepare($insert_query);
	if($sth->execute())
	{
		# print "Executed\n";
	}
	else
	{
		print "QUERY:: $insert_query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $insert_query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

sub Insert_Pdf_Json_Details()
{
	my $dbh              = shift;
	my $Buyer_ID= shift;
	my $pdf_name = shift;
	my $LinkInfo      = shift;
	my $Month_Of_Document      = shift;
	my $Manual_convertion_Flag = shift;
	$pdf_name =~ s/\'/\'\'/igs;
	$pdf_name =~ s/\\/\\\\/igs;
	$Month_Of_Document =~ s/\\/\\\\/igs;
	$Month_Of_Document =~ s/\'/\'\'/igs;
	
	my $insert_query = "insert into bip_pdf_json_status (Buyer_ID,pdf_name,pdf_created_date,LinkInfo,MONTH_OF_DOC,manual_conversion) values (\'$Buyer_ID\',\'$pdf_name\',now(),\'$LinkInfo\',\'$Month_Of_Document\',\'$Manual_convertion_Flag\') ";

	# print "$insert_query\n";
	# <stdin>;

	my $sth = $dbh->prepare($insert_query);

	if($sth->execute())

	{
		print "Executed\n";
	}
	else
	{

		print "QUERY:: $insert_query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $insert_query."\n";
		close ERR;
		$dbh=&DbConnection();
	}

}

sub Insert_Pdf_Json_Details1()
{
	my $dbh              = shift;
	my $Buyer_ID= shift;
	my $pdf_name = shift;
	my $LinkInfo      = shift;
	my $Month_Of_Document      = shift;
	my $Manual_convertion_Flag = shift;
	my %Months_To_Num=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");
	my @Months1=("January","February","March","April","May","June","July","August","September","October","November","December","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	my $LinkInfo_Temp=$LinkInfo;
	my ($File_Month,$File_Year);
	foreach my $monthss(@Months1)	
	{		
		if($LinkInfo_Temp=~m/$monthss/is)
		{
			$File_Month=$monthss;
			if($LinkInfo_Temp=~m/(2\d{3})/is)
			{
				$File_Year=$1;
			}			
			else
			{
				while($LinkInfo_Temp=~m/(\d{2,4})/igs)
				{					
					$File_Year=$1;
				}
			}	
			last;
		}			
	}	
	my $hash_ref = &BIP_Column_Map_V1::ReadIniFile();	my %inihashvalue = %{$hash_ref};	my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source";  
	$pdf_name =~ s/\'/\'\'/igs;
	$pdf_name =~ s/\\/\\\\/igs;
	$Month_Of_Document =~ s/\\/\\\\/igs;
	$Month_Of_Document =~ s/\'/\'\'/igs;
	
	my $insert_query = "insert into bip_pdf_json_status (Buyer_ID,pdf_name,pdf_created_date,LinkInfo,MONTH_OF_DOC,manual_conversion) values (\'$Buyer_ID\',\'$pdf_name\',now(),\'$LinkInfo\',\'$Month_Of_Document\',\'$Manual_convertion_Flag\') ";

	# print "$insert_query\n";
	# <stdin>;

	my $sth = $dbh->prepare($insert_query);

	if($sth->execute())

	{
		print "Executed\n";
	}
	else
	{

		print "QUERY:: $insert_query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $insert_query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
	my $StartDate=DateTime->now();	
	my ($Status,$Failed_Reason);
my $var2=`C:\\ProgramData\Anaconda3\\python.exe C:\\BIP\\BIP_Live\\pdf2csv.py csv "$pdf_name"`;
print $var2;
my $var3=$pdf_name;
$var3==~ s/\.pdf/\.csv/igs;
my $Doc_Name=$LinkInfo;
&ReadSource($Buyer_ID,$Filestorepath,$var3,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
}

sub RetrieveJSONDATA()
{
	my $dbh   		= shift;
	my $Buyer_ID   	= shift;
	my $Input_Table = shift;
	my $query = "select ID,BUYER_ID,JSON_NAME,LinkInfo,MONTH_OF_DOC from $Input_Table where Buyer_ID=\'$Buyer_ID\' and IMPORTDB_FLAG='N' and JSON_NAME is not null";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	
	my (@ID,@BUYER_ID,@JSON_NAME,@LinkInfo,@MONTH_OF_DOC);
	while(my @record = $sth->fetchrow)
	{
		push(@ID,&Trim($record[0]));
		push(@BUYER_ID,&Trim($record[1]));
		push(@JSON_NAME,&Trim($record[2]));
		push(@LinkInfo,&Trim($record[3]));
		push(@MONTH_OF_DOC,&Trim($record[4]));
	}
	$sth->finish();
	return (\@BUYER_ID,\@JSON_NAME,\@ID,\@LinkInfo,\@MONTH_OF_DOC);
}

sub Json_Flag_Update()
{
	my $dbh   		= shift;
	my $Buyer_ID   	= shift;
	my $ID   	= shift;
	my $Input_Table = shift;
	my $Json_Imported_Flag = shift;
	
	my $query = "update $Input_Table set importDB_Flag=\'$Json_Imported_Flag\' where Buyer_ID=\'$Buyer_ID\' and ID=\'$ID\'";	
	my $sth = $dbh->prepare($query);
	$sth->execute();	
	$sth->finish();	
}

sub Manual_convertion_Update()
{
	my $dbh   		= shift;
	my $Buyer_ID   	= shift;
	my $ID   	= shift;
	my $Input_Table = shift;
	my $Manual_convertion_Flag = shift;
	
	my $query = "update $Input_Table set manual_conversion=\'$Manual_convertion_Flag\' where Buyer_ID=\'$Buyer_ID\' and ID=\'$ID\'";	
	my $sth = $dbh->prepare($query);
	$sth->execute();	
	$sth->finish();	
}

# sub Trim()
# {
	# my $txt = shift;
	
	# $txt =~ s/^\s+|\s+$//g;
	# $txt =~ s/^\n+/\n/g;
	
	# return $txt;
# }









sub ReadSource()
{
	my $Buyer_ID		= shift;
	my $Filestorepath	= shift;
	my $Doc_Name		= shift;
	my $LinkInfo		= shift;
	my $Month_Of_Document = shift;
	my $StartDate		= shift;
	my $Status			= shift;
	my $Failed_Reason	= shift;
	$Failed_Reason	= ~s/\'//igs;
	# my $Buyer_ID1=$Buyer_ID;
	# print "2  Buyer_ID1	:: $Buyer_ID1\n";<STDIN>;
	my ($EndDate,$Number_Of_Records,$Batch_ID,$Mapped_Header_Data_Avail_Column);
	my $dbh = &BIP_DB_V1::DbConnection();
	$EndDate="";
	$Number_Of_Records=0;
	my $PDF_to_Json_flag=0;
	################ Retrived Column mapping details from Field_Map_Details ###########################
	my ($File_Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field,$Entity_Code) = &BIP_DB_V2::Column_Name_RetrieveUrl($dbh,$Buyer_ID);
	
	my @Headerarray;
	push(@Headerarray,($Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field));
	@Headerarray = grep { $_ ne '' } @Headerarray;
	print "########################## Headerarray ::@Headerarray";

	my $Header_Data_Avail_Column = $#Headerarray+1;
	if($Entity_Field)
	{
		&BIP_DB_V1::Insert_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Buyer_ID,$LinkInfo,$Doc_Name,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);
		#Get Batch ID from BUYERS_DOWNLOAD_DETAILS
		$Batch_ID = &BIP_DB_V1::BatchID_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
		# print "Batch_ID :: $Batch_ID \n";
		my $Source_File_Type='pdf';
		my ($Number_Of_Records,$Mapped_Header_Data_Avail_Column,$Ref_Hash_Buyer,$Ref_Hash_Batch,$Ref_Hash_Count,$Ref_Hash_Source_Count) = &Read_Csv_File($Buyer_ID,$Batch_ID,$Filestorepath,$Doc_Name,$Month_Of_Document,\@Headerarray,$LinkInfo,$Doc_Name,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);
		my $batch_entity_code;
		my %Hash_Buyer = %$Ref_Hash_Buyer;
		my %Hash_Batch = %$Ref_Hash_Batch;
		my %Hash_Count = %$Ref_Hash_Count;
		my %Hash_Source_Count = %$Ref_Hash_Source_Count;
		my @Hash_Buyer_Value = keys %Hash_Buyer;
		@Hash_Buyer_Value = grep { $_ ne '' } @Hash_Buyer_Value;
		# print "Hash_Buyer_Value :: @Hash_Buyer_Value \n";# <STDIN>;
		foreach my $Key_Entity_Code (@Hash_Buyer_Value)
		{
			my $Buyer_ID = $Hash_Buyer{$Key_Entity_Code};
			my $Batch_ID = $Hash_Batch{$Key_Entity_Code};
			my $Number_Of_Records = $Hash_Source_Count{$Key_Entity_Code};
			my @values_batch_id = values(%Hash_Batch);
			my @keys_batch_id = keys(%Hash_Batch);
			my @matched_batch_id = grep { $Batch_ID =~ /$_/is } @values_batch_id;
			if($#matched_batch_id  > 0)
			{
				$Number_Of_Records = 0;
				foreach $batch_entity_code (@keys_batch_id)
				{
					if($Hash_Batch{$batch_entity_code} == $Batch_ID )
					{
						$Number_Of_Records = $Number_Of_Records + $Hash_Source_Count{$batch_entity_code};
					}
				}
			}
			my $Field_Match='N';
			if($Mapped_Header_Data_Avail_Column==$Header_Data_Avail_Column)
			{
				$Field_Match='Y';
			}
			
			my ($Records_Count_DB)=&BIP_DB_V1::Retrieve_Count_Details($dbh,"SUPPLIER_SOURCE_DATA",$Buyer_ID,$Batch_ID);
			my $Import_Flag='N';
			if($Records_Count_DB==$Number_Of_Records)
			{
				$Import_Flag='Y';
			}
			print "Import_Flag: $Import_Flag\n";
			print "Field_Match: $Field_Match\n";
			$EndDate=DateTime->now();
			$Number_Of_Records = 0 if($Number_Of_Records eq "");
			# To update Supplier_Match, No_of_Suppliers_Matched, No_of_Suppliers_Not_Matched, Formatted_Flag 
			&BIP_DB_V1::Update_Import_Flag($dbh,"BUYERS_DOWNLOAD_DETAILS",$Buyer_ID,$Batch_ID,$Import_Flag,$Field_Match,$EndDate,$Number_Of_Records,$Filestorepath,$Source_File_Type);
			#############
			
		}
		
	}
	else
	{
		&BIP_DB_V1::Insert_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Buyer_ID,$LinkInfo,$Doc_Name,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);
		#Get Batch ID from BUYERS_DOWNLOAD_DETAILS
		$Batch_ID = &BIP_DB_V1::BatchID_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
		print "Batch_ID :: $Batch_ID \n";
		my $Source_File_Type='pdf';
		if($Doc_Name=~m/\.xlsx?\s*$/is)
		{
			($Number_Of_Records,$Mapped_Header_Data_Avail_Column) = &Read_Excel_File($Buyer_ID,$Batch_ID,$Filestorepath,$Doc_Name,$Month_Of_Document,\@Headerarray);
		}
		elsif($Doc_Name=~m/\.csv\s*$/is)
		{
			($Number_Of_Records,$Mapped_Header_Data_Avail_Column) = &Read_Csv_File($Buyer_ID,$Batch_ID,$Filestorepath,$Doc_Name,$Month_Of_Document,\@Headerarray);
		}
		elsif($Doc_Name=~m/\.pdf\s*$/is)
		{			
			($Number_Of_Records,$Mapped_Header_Data_Avail_Column) = &Read_Json_File($Buyer_ID,$Batch_ID,$Filestorepath,\@Headerarray,$Month_Of_Document);
			$PDF_to_Json_flag=1;
		}
		elsif($Doc_Name=~m/\.ods\s*$/is)
		{			
			($Number_Of_Records,$Mapped_Header_Data_Avail_Column) = &Read_Ods_File($Buyer_ID,$Batch_ID,$Filestorepath,$Doc_Name,$Month_Of_Document,\@Headerarray);
		}
		my $Field_Match='N';
		if($Mapped_Header_Data_Avail_Column==$Header_Data_Avail_Column)
		{
			$Field_Match='Y';
		}
		
		my ($Records_Count_DB)=&BIP_DB_V1::Retrieve_Count_Details($dbh,"SUPPLIER_SOURCE_DATA",$Buyer_ID,$Batch_ID);
		my $Import_Flag='N';
		if($Records_Count_DB==$Number_Of_Records)
		{
			$Import_Flag='Y';
		}
		print "Import_Flag: $Import_Flag\n";
		print "Field_Match: $Field_Match\n";
		$EndDate=DateTime->now();
		# To update Supplier_Match, No_of_Suppliers_Matched, No_of_Suppliers_Not_Matched, Formatted_Flag 
		&BIP_DB_V1::Update_Import_Flag($dbh,"BUYERS_DOWNLOAD_DETAILS",$Buyer_ID,$Batch_ID,$Import_Flag,$Field_Match,$EndDate,$Number_Of_Records,$Filestorepath,$Source_File_Type);
		#############		
	}
	if($PDF_to_Json_flag eq '1')
	{
		return $Number_Of_Records;
	}
}


sub Read_Csv_File
{
	my $Buyer_ID 			= shift;
	my $Batch_ID 			= shift;
	my $Filepath 			= shift;
	my $Docname				= shift;
	my $Month_Of_Document	= shift;
	my $Header_array		= shift;
	my $LinkInfo			= shift;
	my $Doc_Name			= shift;
	my $StartDate			= shift;
	my $EndDate				= shift;
	my $Number_Of_Records	= shift;
	my $Status				= shift;
	my $Failed_Reason		= shift;
	my @Headerarray = @{$Header_array};
	
	my $Outfile = "$Filepath\\$Docname";
	my $dbh = &BIP_DB_V2::DbConnection();
	my ($File_Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2,$Entity_Field,$Entity_Code) = &BIP_DB_V1::Column_Name_RetrieveUrl($dbh,$Buyer_ID);
	my $insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
	my $Headercheck = 0;
	my $Sno = 0;
	my $insert_collect = 0;
	my %Header_col_position;
	my (%Hash_buyer,%Hash_batch,%Hash_count,%Hash_Source_Count);
	print "Entity_Code	:: $Entity_Code\n";
	if ($Entity_Code)
	{
		my $Entity_Code_Temp = $Entity_Code;
		$Entity_Code_Temp =~ s/^\s*\|//igs;
		while($Entity_Code_Temp =~ m/([^<]*?)\|/igs)
		{
			my $Entity_Values = $1;
			print "EntityMAjor	:: $Entity_Values\n";
			$Hash_buyer{$Entity_Values} = $Buyer_ID;
			$Hash_batch{$Entity_Values} = $Batch_ID;
		}
	}
	
	# my $csv = Text::CSV_XS->new ({
	my $csv = Text::CSV->new ({
	binary => 1,
	allow_loose_quotes  => 1,
	allow_loose_escapes => 1,
	auto_diag => 1,
	escape_char => "\"",
	sep_char => ',',
	verbatim => 1
	});           
	$csv->verbatim (0);
	my $sum = 0;
	my $Valid_Records=0;
	open(my $data, '<', "$Outfile") or die "Could not open '$Doc_Name' $!\n";
	while (my $fields = $csv->getline($data)) 
	{
		my @fieldsarray = @{$fields};
		my ($Transaction_ID_Value,$Requirement_Value,$Amount_Value,$Start_Date_Value,$End_Date_Value,$Supplier_Name_Value,$Department_Name_Value,$Requirement_2_Value,$Entity_Field_Value);
		if ( $Headercheck == 0 )
		{
			foreach my $Col (0 .. $#fieldsarray)
			{
				my @Header = grep { $fieldsarray[$Col] =~ /$_/is } @Headerarray;
				print "HEADER :: @Header :: $Col \n";# 
				$Header_col_position{$Header[0]} = $Col if (@Header);
			}
			
			if ($Header_col_position{$Transaction_ID}||$Header_col_position{$Requirement}||$Header_col_position{$Amount}||$Header_col_position{$Start_Date}||$Header_col_position{$End_Date}||$Header_col_position{$Supplier_Name}||$Header_col_position{$Department_Name}||$Header_col_position{$Requirement_2})
			{
				$Headercheck = 1;
			}
		}
		else
		{
			$Transaction_ID_Value 	= &Trim($fieldsarray[$Header_col_position{$Transaction_ID}]) if ($Header_col_position{$Transaction_ID} ne '');
			$Requirement_Value 		= &Trim($fieldsarray[$Header_col_position{$Requirement}]) if ($Header_col_position{$Requirement} ne '');
			$Amount_Value 			= &Trim($fieldsarray[$Header_col_position{$Amount}]) if ($Header_col_position{$Amount} ne '');
			$Start_Date_Value 		= &Trim($fieldsarray[$Header_col_position{$Start_Date}]) if ($Header_col_position{$Start_Date} ne '');
			$End_Date_Value 		= &Trim($fieldsarray[$Header_col_position{$End_Date}]) if ($Header_col_position{$End_Date} ne '');
			$Supplier_Name_Value 	= &Trim($fieldsarray[$Header_col_position{$Supplier_Name}]) if ($Header_col_position{$Supplier_Name} ne '');
			$Department_Name_Value 	= &Trim($fieldsarray[$Header_col_position{$Department_Name}]) if ($Header_col_position{$Department_Name} ne '');
			$Requirement_2_Value	= &Trim($fieldsarray[$Header_col_position{$Requirement_2}]) if ($Header_col_position{$Requirement_2} ne '');
			$Entity_Field_Value		= &Trim($fieldsarray[$Header_col_position{$Entity_Field}]) if ($Header_col_position{$Entity_Field} ne '');
			
			if($Entity_Field)
			{
				unless($Hash_buyer{$Entity_Field_Value})
				{
					my ($Ret_Buyer_ID,$Ret_Entity_Code) = &BIP_DB_V1::RetrieveBuyerID($dbh,"field_map_details_automation",$Buyer_ID,$Entity_Field_Value);
					# print "Ret_Buyer_ID :: $Ret_Buyer_ID :: $Ret_Entity_Code\n";
					if($Ret_Buyer_ID)
					{
						&BIP_DB_V1::Insert_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Ret_Buyer_ID,$LinkInfo,$Doc_Name,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);
						$Batch_ID = &BIP_DB_V1::BatchID_RetrieveUrl($dbh,$Ret_Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
						# print "Batch_ID :: $Entity_Field_Value ::  $Batch_ID \n";
						if ($Ret_Entity_Code)
						{
							my $Entity_Code_Temp = $Ret_Entity_Code;
							$Entity_Code_Temp =~ s/^\s*\|//igs;
							while($Entity_Code_Temp =~ m/([^<]*?)\|/igs)
							{
								my $Entity_Values = $1;
								$Hash_buyer{$Entity_Values} = $Ret_Buyer_ID;
								$Hash_batch{$Entity_Values} = $Batch_ID;
								# print "Batch_ID :: $Entity_Values ::  $Batch_ID ::  $Ret_Buyer_ID \n";
							}	
						}
					}
					else
					{
						$Hash_buyer{$Entity_Field_Value} = '-';
						$Hash_batch{$Entity_Field_Value} = '-';
					}
				}
				# print "Entity_Code###### $Hash_batch{$Entity_Field_Value}\n";
				next if($Hash_buyer{$Entity_Field_Value} eq '-');
				if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
				{
					$insert_collect++;
					$Transaction_ID_Value=~s/\'/\'\'/igs;
					$Supplier_Name_Value=~s/\\\'/\'/igs;
					$Supplier_Name_Value=~s/\'/\'\'/igs;
					$Department_Name_Value=~s/\'/\'\'/igs;
					$Requirement_Value=~s/\'/\'\'/igs;
					$Requirement_2_Value=~s/\'/\'\'/igs;
					$Hash_count{$Entity_Field_Value}++;
					$Hash_Source_Count{$Entity_Field_Value}++;
					# print "Hash_count: $Entity_Field_Value:  $Hash_count{$Entity_Field_Value}\n";
					# print "Hash_Source_Count: $Entity_Field_Value:  $Hash_Source_Count{$Entity_Field_Value}\n";
					if($insert_collect <= 990)
					{
						$insert_query .= "(\'$Hash_buyer{$Entity_Field_Value}\',\'$Hash_batch{$Entity_Field_Value}\',\'$Month_Of_Document\',date(now()),\'$Hash_count{$Entity_Field_Value}\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
					}
					else
					{
						$insert_query .= "(\'$Hash_buyer{$Entity_Field_Value}\',\'$Hash_batch{$Entity_Field_Value}\',\'$Month_Of_Document\',date(now()),\'$Hash_count{$Entity_Field_Value}\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
						$insert_query =~ s/\,\s*$//igs;
						$insert_query .= ';';
						&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
						$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
						$insert_collect = 0;
					}
				}
			}
			else
			{
				# print "ELSE Ret_Buyer_ID :: $Ret_Buyer_ID \n";
				if ($Transaction_ID_Value||$Requirement_Value||$Amount_Value||$Start_Date_Value||$End_Date_Value||$Supplier_Name_Value||$Department_Name_Value||$Requirement_2_Value)
				{
					$Sno++;
					$insert_collect++;
					$Transaction_ID_Value=~s/\\\'/\'/igs;
					$Transaction_ID_Value=~s/\'/\'\'/igs;
					$Supplier_Name_Value=~s/\\\'/\'/igs;
					$Supplier_Name_Value=~s/\'/\'\'/igs;
					$Department_Name_Value=~s/\\\'/\'/igs;
					$Department_Name_Value=~s/\'/\'\'/igs;
					$Requirement_Value=~s/\\\'/\'/igs;
					$Requirement_Value=~s/\'/\'\'/igs;
					$Requirement_2_Value=~s/\\\'/\'/igs;
					$Requirement_2_Value=~s/\'/\'\'/igs;
					if($insert_collect <= 990)
					{
					$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
					}
					else
					{
						$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',date(now()),\'$Sno\',\'$Transaction_ID_Value\',\'$Requirement_Value\',\'$Amount_Value\',\'$Start_Date_Value\',\'$End_Date_Value\',\'$Supplier_Name_Value\',\'$Department_Name_Value\',\'$Requirement_2_Value\'),";
						$insert_query =~ s/\,\s*$//igs;
						$insert_query .= ';';
						&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
						$insert_query = "insert into SUPPLIER_SOURCE_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2) values ";
						$insert_collect = 0;
					}
				}
			}
		}
	}
	if(not $csv->eof)
	{
		$csv->error_diag();
	}
	close $data;
	
	if($insert_collect >= 1)
	{
		$insert_query =~ s/\,\s*$//igs;
		$insert_query .= ';';
		&BIP_DB_V1::Insert_Spend_Data($dbh,$insert_query);
	}
	my @Value = values %Header_col_position;
	print "###################################Value :: @Value\n";
	@Value = grep { $_ ne '' } @Value;
	my $Mapped_Col = $#Value+1;
	# return ($Sno,$Mapped_Col);
	return ($Sno,$Mapped_Col,\%Hash_buyer,\%Hash_batch,\%Hash_count,\%Hash_Source_Count);
}


sub Trim()
{
	my $txt = shift;
	my $Tab=chr(9);
	my $Linefeed=chr(10);
	my $Carr_Return=chr(13);
	$txt =~s/^\s+|\s+$//g;
	$txt =~s/$Tab/ /igs;
	$txt =~s/$Linefeed/ /igs;
	$txt =~s/$Carr_Return/ /igs;
	$txt =~s/\s\s+/ /igs;
	return $txt;
}