# Script for Group Of url's 'http://www.england.nhs.uk/contact-us/pub-scheme/spend/#payments'
# Buyer ID 730,731,732,733,734,877,1134,1135,1136,1137,1138,1139,1140,1141,1142,1143,1144,1145,1146,1147,1148,1149,1150,1151
use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use BIP_DB_V1;
use BIP_Column_Map_V1;

my $Input_Buyer_ID = $0;
$Input_Buyer_ID =~ s/\.pl//igs;
$Input_Buyer_ID =$1 if($Input_Buyer_ID =~ m/[^>]*?\_(\d+?)\s*$/is);
$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;
#my $ua=LWP::UserAgent->new(show_progress=>1);
my $ua=LWP::UserAgent->new(ssl_opts => {SSL_verify_mode => 'SSL_VERIFY_PEER',verify_hostname => 0 }, show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/\.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my %Months_To_Num=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");

############Database Initialization########
my $dbh = &BIP_DB_V1::DbConnection();
###########################################

my $Input_Table='BUYERS';
############ Retrieve Spent Data weblink from Buyers table ###########
my ($ID,$Buyer_Name,$Spent_Data_weblink) = &BIP_DB_V1::RetrieveUrl($dbh,$Input_Buyer_ID,$Input_Table);
my $Buyer_ID 			= @$ID[0];
my $Buyer_Name 			= @$Buyer_Name[0];
my $Buyer_Url 			= @$Spent_Data_weblink[0];
# $Buyer_Url="https://www.england.nhs.uk/publication/payments-greater-than-25k-report/";
###############
print "Buyer_ID	: $Buyer_ID\n";	
print "Buyer_Name	: $Buyer_Name\n";	
print "Buyer_Url	: $Buyer_Url\n";	

my ($Buyer_content,$Status_Line)=&Getcontent($Buyer_Url); &BIP_Column_Map_V1::Link_Status_Update($dbh,$Buyer_ID,$Status_Line);
open(ts,">Buyer_content.html");
print ts "$Buyer_content";
close ts;

my ($Processed_LinkInfo) = &BIP_DB_V1::LinkInfo_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
my @Processed_LinkInfo=@$Processed_LinkInfo;
# To get the Doc_url from the Buyer_content
# while($Buyer_content=~m/Please\s*note\s*\,\s*spend\s*shown\s*is\s*for\s*NHS\s*England[^>]*?>([\w\W]*?)<\/ul>/igs)
# {
	# my $List_content=$1;
	# while($List_content=~m/<li>\s*<a\s*[^>]*?\s*href\=(?:\"|\')([^<]*?)(?:\"|\')\s*[^>]*?>\s*([^>]*?)\s*<\/a>/igs)
	while($Buyer_content=~m/<a[^>]*?href\=\"\s*([^>]*?.xlsx?)\s*\">\s*([^>]*?)\s*<\/a>/igs)
	{
		my $Doc_Link=$1;
		my $LinkInfo=$2;
		$LinkInfo=&Link_Info_Clean($LinkInfo);
		my ($File_Month,$File_Year,$Doc_Name);
		
		my $Doc_Type;
		if ($Doc_Link =~m/^[^>]*?\.([\w]*?)\s*$/is )
		{
			$Doc_Type=$1;
			
		}
		print "Doc_Type: $Doc_Type\n";
		# Get Date, Month, Year from the link info for file location Creation
		if($LinkInfo=~m/report\s*([a-z]+)\s*([0-9]+)\s*/is)
		{
			$File_Month=$1;
			$File_Year=$2;
			print "File_Month	: $File_Month\n";
			print "File_Year	: $File_Year\n";
		}
		my $Month_Of_Document = "$File_Month $File_Year";

		print "LinkInfo	: <$LinkInfo>\n";
		if($LinkInfo ~~ @Processed_LinkInfo || $LinkInfo =~ m/^\s*$/)
		{
			print "File Already Processed\n";
			next;
		}
		if($Doc_Link!~m/http/is)
		{
			my $u1=URI::URL->new($Doc_Link,$Buyer_Url);
			my $u2=$u1->abs;
			$Doc_Link=$u2;
		}
		print "Doc_Link	: $Doc_Link\n";
		print "Month_Of_Document :: $Month_Of_Document\n";
		#exit;
		# File name creation from Link Info
		my $File_Type = $Doc_Type if ($Doc_Type ne '');
		
		$Doc_Name=$LinkInfo;
		$Doc_Name=~s/\W/\_/igs;
		$Doc_Name=~s/_+/\_/igs;
		$Doc_Name.='.'.$File_Type;
		print "Doc_Name	: $Doc_Name\n"; 
		
		# File Path creation
		my $hash_ref = &BIP_Column_Map_V1::ReadIniFile();	
		my %inihashvalue = %{$hash_ref};	
		my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source";  
		
		# File location creation
		unless ( -d $Filestorepath ) 
		{
			$Filestorepath=~s/\//\\/igs;
			system("mkdir $Filestorepath");
		}
		my $Storefile = "$Filestorepath\\$Doc_Name";
		print "STOREPATH  : $Storefile \n";

		my $StartDate=DateTime->now();
		# Spend file download
		my ($Doc_content,$status_line)=&Getcontent($Doc_Link);
		my $fh=FileHandle->new("$Storefile",'w');
		binmode($fh);
		$fh->print($Doc_content);
		$fh->close();
		my ($Status,$Failed_Reason);
		if($status_line!~m/20/is)
		{
			$Failed_Reason=$status_line;
		}
		else
		{
			$Status='Downloaded';
		}
		print "\nStatus:::: $Status\n";
		&BIP_Column_Map_V1::ReadSource($Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
		print "Completed \n";
		############ Retrieve Group ID from Buyers table ###########
		# my ($ID,$Group_ID) = &BIP_DB_V1::Retrieve_Group_ID($dbh,$Input_Buyer_ID,$Input_Table);
		# my $Group_ID_Count = @{$Group_ID};
		# for(my $reccnt = 0; $reccnt < $Group_ID_Count; $reccnt++ )
		# {
			# my $Group_Buyer_ID 	= &Trim(@$ID[$reccnt]);
			# my $Group_ID 	= &Trim(@$Group_ID[$reccnt]);		
			# &BIP_Column_Map_V1::ReadSource($Group_Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
			# print "$Group_Buyer_ID From Group ID $Group_ID Completed \n";
		# }	
	}	
# }

sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"www.england.nhs.uk");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	# my $get_cookie=$res->header('set-cookie');
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	if($Data eq '')
	{
		# $Data='Not provided';
	}
	return $Data;
}

sub Link_Info_Clean()
{
	my $Data=shift;
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\&pound\;//igs;
	$Data=~s/\&nbsp\;/ /igs;
	$Data=~s/\&amp\;/&/igs;
	$Data=~s/�//igs;
	$Data=~s/[^[:print:]]//igs;
	$Data=~s/\s*\([^>]*?\)\s*//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}