use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use BIP_DB_V1;
use BIP_Column_Map_V1;


my $robotname = $0;
my $Input_Buyer_ID=$1 if($robotname =~ m/_([\d+]*?)\.pl/is);
print"Input_Buyer_ID::$Input_Buyer_ID\n";
print"$robotname\n";
my $ua=LWP::UserAgent->new(show_progress=>1);
# my $ua  = LWP::UserAgent->new(
        # ssl_opts => { verify_hostname => 0 },
    # );
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my @Months=("January","February","March","April","May","June","July","August","September","October","November","December");
my %Months_To_Num=("Jan" => "1", "Feb" => "2", "Mar" => "3", "Apr" => "4", "May" => "5", "Jun" => "6", "Jul" => "7", "Aug" => "8", "Sep" => "9", "Oct" => "10", "Nov" => "11", "Dec" => "12");
my %Months_To_FullMonth=("Jan" => "January", "Feb" => "February", "Mar" => "March", "Apr" => "April", "May" => "May", "Jun" => "June", "Jul" => "July", "Aug" => "August", "Sep" => "September", "Oct" => "October", "Nov" => "November", "Dec" => "December");
my %year_To_Fullyear=("10" => "2010", "11" => "2011", "12" => "2012", "13" => "2013", "14" => "2014", "15" => "2015");
my %Months_To_Text=("01" => "January", "02" => "February", "03" => "March", "04" => "April", "05" => "May", "06" => "June", "07" => "July", "08" => "August", "09" => "September", "10" => "October", "11" => "November", "12" => "December");

############Database Initialization########
my $dbh = &BIP_DB_V1::DbConnection();
###########################################
my $hash_ref = &BIP_Column_Map_V1::ReadIniFile;
my %inihashvalue = %{$hash_ref};

my $Input_Table='Buyers';
############ Retrieve Spent Data weblink from Buyers table ###########
my ($ID,$Buyer_Name,$Spent_Data_weblink) = &BIP_DB_V1::RetrieveUrl($dbh,$Input_Buyer_ID,$Input_Table);
my $Buyer_ID 			= @$ID[0];
my $Buyer_Name 			= @$Buyer_Name[0];
my $Buyer_Url 			= @$Spent_Data_weblink[0];
################ Retrived Column mapping details from Field_Map_Details ###########################
my ($File_Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2) = &BIP_DB_V1::Column_Name_RetrieveUrl($dbh,$Buyer_ID);

#To change
# For number of columns available in field mapping table. This is used to update the Field_Match status
my @DB_Column_Headers=($Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2);
my @Header_Data_Avail_Column = grep(/[A-Za-z]/, @DB_Column_Headers);
my $Header_Data_Avail_Column=@Header_Data_Avail_Column;

# $Buyer_Url="https://open.barnet.gov.uk/dataset/expenditure-reporting-2019-20";
###############
print "Buyer_ID	: $Buyer_ID\n";	
print "Buyer_Name	: $Buyer_Name\n";	
print "Buyer_Url	: $Buyer_Url\n";	

my $StartDate=DateTime->now();
my ($Buyer_content,$Status_Line)=&Getcontent($Buyer_Url); &BIP_Column_Map_V1::Link_Status_Update($dbh,$Buyer_ID,$Status_Line);
open(ts,">Buyer_content.html");
print ts "$Buyer_content";
close ts;

my ($Processed_LinkInfo) = &BIP_DB_V1::LinkInfo_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
my @Processed_LinkInfo=@$Processed_LinkInfo;
my ($LinkInfo,$Doc_Link);

# To get block content for getting downloand link.
$Buyer_content=~s/\&nbsp\;/ /igs;
$Buyer_content=decode_entities($Buyer_content);
# my $Block_content=$1 if($Buyer_content=~m/<h2>Expenditure([\w\W]*?)<\/div>/is);
my @duplicate;
# if ($Buyer_content=~m/<ul\s*class\=\"nav\-level\-6[^>]*?>([\w\W]*?)<\/ul>/is)
# {
	# my $block = $1;
	# while ($block=~m/<a\s*href\=\"([^>]*?)\"/igs)
	# {
		# my $year_link = "https://www.barnet.gov.uk".$1;
		# my ($Buyer_content1,$Status_Line)=&Getcontent($year_link);
		# $Buyer_content1=decode_entities($Buyer_content1);
		# while ($Buyer_content1=~m/<a\s*href\=\"([^>]*?((?:csv|xls|xlsx)))\"[^>]*?>\s*([^>]*?)\s*</igs)
		while ($Buyer_content=~m/<a[^>]*?href="([^>]*?\.csv)"[^>]*?><[^>]*?>\s*(Expenditure[^>]*?)\s*<\//igs)
		{
			print "inside the First level\n";
			my $doc_Link  = $1;
			my $File_Type = "csv";
			my $link_info = $2;
			$link_info=~s/\s*\([^>]*?\)\s*$//igs;
			print "link_info=>$link_info\n";
			print "doc_Link=>$doc_Link\n";
			if($link_info ~~ @duplicate)
			{
				print "File Already Exist\n";
				next;
			}
			push (@duplicate,$link_info);
			# Get file link
			if($doc_Link!~m/http/is)
			{
				my $u1=URI::URL->new($doc_Link,$Buyer_Url);
				my $u2=$u1->abs;
				$doc_Link=$u2;
			}
			
			if($link_info ~~ @Processed_LinkInfo)
			{
				print "File Already Processed\n";
				next;
			}
			&csv_write($doc_Link,$link_info,$File_Type);
		}
	# }	
# }

sub csv_write()
{	
	my $Doc_Link	= shift;
	my $LinkInfo	= shift;
	my $File_Type	= shift;
	
	my ($File_Date,$File_Month,$File_Year,$Doc_Name);
	
	$LinkInfo=~s/<[^>]*?>//igs;
	$LinkInfo=~s/\s\s+/ /igs;
	print "LinkInfo	: $LinkInfo\n";
	
	# Get Date, Month, Year from the link info for file location Creation
	if ($LinkInfo=~m/([\sA-Za-z]+)\s([\d]+)/is)
	{
		$File_Month=$1;
		$File_Year =$2;
		# $File_Month=$Months_To_Text{$File_Month};
		# $File_Month=$Months_To_FullMonth{$File_Month};
		# $File_Year=$year_To_Fullyear{$File_Year};
		print "File_Month	: $File_Month\n";
		print "File_Year	: $File_Year\n";
	}
	$File_Month=~s/^\s+//igs;
	print "Doc_Link	: $Doc_Link\n";
	# File name creation from Link Info
	$Doc_Name=$LinkInfo;
	$Doc_Name=~s/\W/\_/igs;
	$Doc_Name.='.'.$File_Type;
	my $Month_Of_Document=$File_Month." ".$File_Year;
	print "Doc_Name	: $Doc_Name\n"; 
	
	# File Path creation
	# my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source";   
	# my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source"; 
	my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source";  	
	print "Filestorepath	: $Filestorepath\n";
	
	# File location creation
	unless ( -d $Filestorepath ) 
	{
		$Filestorepath=~s/\//\\/igs;
		system("mkdir $Filestorepath");
	}
	
	# Spend file download
	my ($Doc_content,$status_line)=&Getcontent($Doc_Link);
	my $fh=FileHandle->new("$Filestorepath\\$Doc_Name",'w');
	binmode($fh);
	$fh->print($Doc_content);
	$fh->close();
	my ($Status,$Failed_Reason);
	if($status_line!~m/20/is)
	{
		$Failed_Reason=$status_line;
	}
	else
	{
		$Status='Downloaded';
	}
	print "\nStatus:::: $Status\n";
	&BIP_Column_Map_V1::ReadSource($Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
	print "Completed \n";
	
	# my ($Transaction_ID_number,$Requirement_Number,$Amount_Number,$Start_Date_Number,$End_Date_Number,$Supplier_Name_Number,$Department_Name_Number,$Requirement_2_Number)=BIP_Column_Map::Map($Filestorepath,$Doc_Name,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2);
	# print "Transaction_ID Position:	$Transaction_ID_number\n";
	# print "Requirement Position:	$Requirement_Number\n";
	# print "Amount Position:	$Amount_Number\n";
	# print "Start_Date Position:	$Start_Date_Number\n";
	# print "End_Date Position:	$End_Date_Number\n";
	# print "Supplier_Name Position:	$Supplier_Name_Number\n";
	# print "Department_Name Position:	$Department_Name_Number\n";
	# print "Requirement_2 Position:	$Requirement_2_Number\n";

	# To change
	# For number of columns matched with csv & DB. This is used to update the Field_Match status
	# my @Mapped_Column=($Transaction_ID_number,$Requirement_Number,$Amount_Number,$Start_Date_Number,$End_Date_Number,$Supplier_Name_Number,$Department_Name_Number,$Requirement_2_Number);
	# my @Mapped_Header_Data_Avail_Column = grep(/[\d]/, @Mapped_Column);
	# my $Mapped_Header_Data_Avail_Column=@Mapped_Header_Data_Avail_Column;
	# my $Field_Match='N';
	# if($Mapped_Header_Data_Avail_Column==$Header_Data_Avail_Column)
	# {
		# $Field_Match='Y';
	# }
	####################
	# my $csv = Text::CSV->new ({
	# binary => 1,
	# auto_diag => 1,
	# sep_char => ','
	# });	
	# my (@Transaction_ID,@Requirement,@Amount,@Start_Date,@End_Date,@Supplier_Name,@Department_Name,@Requirement_2);
	# my $sum = 0;
	# my $Valid_Records=0;
	# open(my $data, '<', "$Filestorepath/$Doc_Name") or die "Could not open '$Doc_Name' $!\n";
	# while (my $fields = $csv->getline($data)) 
	# {
		# next unless grep $_, @$fields;
		# my $Header_Check=&Trim($fields->[$Supplier_Name_Number]);
		# $Header_Check=~s/^\s+|\s$//igs;
		
		# if($Valid_Records==0)
		# {
			# if($Header_Check=~m/^\s*Supplier|^SuppID|Name|Vendor\s*Name|Cleansed\s*Supplier\s*Name/is)
			# {
				# $Valid_Records=1;
				# next;
			# }
			# else
			# {
				# next;
			# }
		# }	
		# if($Valid_Records==1)
		# {
			# if($Transaction_ID_number ne '')
			# {
				# push(@Transaction_ID, &Trim($fields->[$Transaction_ID_number]));
			# }	
			# if($Requirement_Number ne '')
			# {
				# push(@Requirement, &Trim($fields->[$Requirement_Number]));
			# }	
			# if($Amount_Number ne '')
			# {
				# push(@Amount, &Trim($fields->[$Amount_Number]));
			# }
			# if($Start_Date_Number ne '')
			# {
				# push(@Start_Date, &Trim($fields->[$Start_Date_Number]));
			# }	
			# if($End_Date_Number ne '')
			# {
				# push(@End_Date, &Trim($fields->[$End_Date_Number]));
			# }	
			# if($Supplier_Name_Number ne '')
			# {
				# push(@Supplier_Name, &Trim($fields->[$Supplier_Name_Number]));
			# }	
			# if($Department_Name_Number ne '')
			# {
				# push(@Department_Name, &Trim($fields->[$Department_Name_Number]));
			# }	
			# if($Requirement_2_Number ne '')
			# {
				# push(@Requirement_2, &Trim($fields->[$Requirement_2_Number]));
			# }	
			# $sum++;
		# }
	# }
	# if(not $csv->eof)
	# {
		# $csv->error_diag();
	# }
	# close $data;
	# my $Number_Of_Records=@Supplier_Name;
	# my $Month_Of_Document=$File_Month." ".$File_Year;
	# my $EndDate=DateTime->now(); # Download process end time

	# Insert file download details.
	# &BIP_DB_V1::Insert_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Buyer_ID,$LinkInfo,$Doc_Name,$StartDate,$EndDate,$Number_Of_Records,$Status,$Failed_Reason);

	# select Batch ID from BUYERS_DOWNLOAD_DETAILS
	# my ($Batch_ID) = &BIP_DB_V1::BatchID_RetrieveUrl($dbh,$Input_Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
	
	# my $S_no=0;
	# for(my $i=0;$i<@Supplier_Name;$i++)
	# {
		# $S_no++;
		# &BIP_DB_V1::Insert_Spend_Data($dbh,"SUPPLIER_SOURCE_DATA",$Buyer_ID,$Batch_ID,$S_no,$Month_Of_Document,$Transaction_ID[$i],$Requirement[$i],$Amount[$i],$Start_Date[$i],$End_Date[$i],$Supplier_Name[$i],$Department_Name[$i],$Requirement_2[$i]);
	# }

	# To change
	# To identify all the records has been imported or not
	# my $Records_Count_CSV=@Supplier_Name;
	# my ($Records_Count_DB)=&BIP_DB_V1::Retrieve_Count_Details($dbh,"SUPPLIER_SOURCE_DATA",$Buyer_ID,$Batch_ID);
	# my $Import_Flag='N';
	# if($Records_Count_DB == $Records_Count_CSV)
	# {
		# $Import_Flag='Y';
	# }
	# print "Records_Count: $Records_Count_DB\n";
	# print "Import_Flag: $Import_Flag\n";

	# To update Supplier_Match, No_of_Suppliers_Matched, No_of_Suppliers_Not_Matched, Formatted_Flag 
	# &BIP_DB_V1::Update_Import_Flag($dbh,"BUYERS_DOWNLOAD_DETAILS",$Buyer_ID,$Batch_ID,$Import_Flag,$Field_Match);
	############
	
}

sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	# my $get_cookie=$res->header('set-cookie');
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	if($Data eq '')
	{
		# $Data='Not provided';
	}
	return $Data;
}