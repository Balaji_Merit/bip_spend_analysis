## Imports
use strict;
use LWP::UserAgent;
use HTTP::Cookies;
use JSON;
use MIME::Lite;
use MIME::Base64;
use Array::Utils qw(:all);
require BIP_DB;

## User agent setup
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Merit BI OUToader");
# $ua->timeout(50);
push @{ $ua->requests_redirectable }, 'POST';

my $cookiefile = $0;
$cookiefile =~s/\.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile, autosave=>1);
$ua->cookie_jar($cookie);
my $Input_Table='BUYERS_DOWNLOAD_DETAILS';
############Database Initialization########
my $dbh = &BIP_DB::DbConnection();
###########################################
## Tool Login
my $url='http://62.253.177.4:8080/businessIntelligence/j_security_check';

my $response=$ua->post(
	$url,
	[
		# 'j_username' => 'gladys.christopher@meritgroup.co.uk',
		# 'j_password' => 'zOl3BoXy'
		'j_username' => 'arul.kumaran@meritgroup.co.uk',
		'j_password' => 'tomcat'	
	]
);

if ($response->is_success) {
	if (!$response->request()->uri() =~ /http:\/\/62.253.177.4:8080\/businessIntelligence\/businessIntelligence.html/) {
		die "Login Failed";
	}
}

	my $re1=1;
	my $re_check_count=0;
	my $Batch_OUToads_Url='http://62.253.177.4:8080/businessIntelligence/spend/uploads.html';
	Reping:
	$response = $ua->get($Batch_OUToads_Url);
	my $Uploaded_Reference_code=$response->code;	
	if($Uploaded_Reference_code=~m/50/igs)
	{
		print "Code: $Uploaded_Reference_code\n";
		if($re1<=3)
		{
			$re1++;
			goto Reping;
		}	
		else		
		{
			print "\n----------UNABLE TO GET REFERENCE----------\n";
		}
		
	}
	my $Batch_OUToads_Content=$response->content;
	open(OUT,">Spend_Content.html");
	print OUT $Batch_OUToads_Content;
	close OUT;
	
	my $Uploaded_Time;
	# my $ID=1;
	my @Uploaded_Reference_Number;
	while($Batch_OUToads_Content=~m/>\s*ID\s*\:\s*<a\s*href\s*=\s*\"[^>]*?\"\s*>\s*([^>]*?)\s*<\/a>\s*<br\/>\s*Created\s*\:\s*[^>]*?\s*<br\/>\s*Uploader\s*\:\s*arul\.kumaran\@meritgroup\.co\.uk/igs)
	{
		push(@Uploaded_Reference_Number,$1);
		# print "$ID:$1\n";
		# $ID++;
	}	
	my $Uploaded_Reference_Number=join(',',@Uploaded_Reference_Number);
	# print "References: $Uploaded_Reference_Number\n";
	Re_Check_With_DB:
	my $query = "select Upload_Reference from BUYERS_DOWNLOAD_DETAILS where Upload_Reference in ($Uploaded_Reference_Number)";
	# print "\n$query\n";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@Upload_Reference_From_DB);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@Upload_Reference_From_DB,&Trim($record[0]));
	}
	$sth->finish();		
	# print "@Upload_Reference_From_DB";
	# <stdin>;
	print "Uploaded_Reference_Number:\n";
	print "@Uploaded_Reference_Number";
	my @Duplicates = array_diff(@Upload_Reference_From_DB, @Uploaded_Reference_Number);
	print "@Duplicates";
	if(@Duplicates == 0)
	{
		print "\nNo Duplicates Found\n";
	}
	else
	{	
		if($re_check_count<1)
		{
			sleep(180);
			goto Re_Check_With_DB;
			$re_check_count++;
		}
		my $Duplicates=join(',',@Duplicates);
		print "Duplicate ID: $Duplicates\n";
		&send_mail($Duplicates);
	}


sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "application/json;charset=UTF-8");
	$req->header("Host" => "62.253.177.4:8080");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$redir_url);
}
	
sub send_mail()
{
	my $Duplicates	 = shift;

	my $subject="Duplicate Identified";
	my $host ='74.80.234.196'; 
	my $from='autoemailsender@meritgroup.co.uk';
	my $user='meritgroup';
	my $to ='arul.kumaran@meritgroup.co.uk,balaji.ekambaram@meritgroup.co.uk';
	my $pass='11meritgroup11';
	my $body = "Hi Arul, <br><br>\t\tKindly check the below reference numbers.<br><br>\t\t$Duplicates<br><br>Regards<br>BIP TEAM";
	
	my $msg = MIME::Lite->new (
	  From => $from,
	  To => $to,
	  # Cc => $cc,
	  Subject => $subject,
	  Data => $body,
	  Type =>'text/html'
	) or die "Error creating multipart container: $!\n";
    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	$msg->send;
}
# Host: 74.80.234.196
# Username: meritgroup
# Password: 11meritgroup11
# Port: 25
sub Trim()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}
