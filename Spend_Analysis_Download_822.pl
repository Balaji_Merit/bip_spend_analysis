use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use BIP_DB_V1;
use BIP_Column_Map_V1;

my $ua=LWP::UserAgent->new(show_progress=>1);

# my $ua  = LWP::UserAgent->new(
        # ssl_opts => { verify_hostname => 0 },
    # );
	
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/\.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);
my $F_Flag=1;
my $Input_Buyer_ID=$0;
$Input_Buyer_ID=~s/[^>]*?_([\d]+)(\.pl)/$1/igs;
$Input_Buyer_ID=~s/^\s+|\s+$//igs;
print "\nInput_Buyer_ID---------->$Input_Buyer_ID \n";


my @Months=("","January","February","March","April","May","June","July","August","September","October","November","December");
my %Months_To_Num=("Jan" => "1", "Feb" => "2", "Mar" => "3", "Apr" => "4", "May" => "5", "Jun" => "6", "Jul" => "7", "Aug" => "8", "Sep" => "9", "Oct" => "10", "Nov" => "11", "Dec" => "12");
my %Months_To_Text=("01" => "Jan", "02" => "Feb", "03" => "Mar", "04" => "Apr", "05" => "May", "06" => "Jun", "07" => "Jul", "08" => "Aug", "09" => "Sep", "10" => "Oct", "11" => "Nov", "12" => "Dec");

my @Months1=("January","February","March","April","May","June","July","August","September","October","November","December","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");


############Database Initialization########
my $dbh = &BIP_DB_V1::DbConnection();
###########################################

my $Input_Table='Buyers';
############ Retrieve Spent Data weblink from Buyers table ###########
my ($ID,$Buyer_Name,$Spent_Data_weblink) = &BIP_DB_V1::RetrieveUrl($dbh,$Input_Buyer_ID,$Input_Table);
my $Buyer_ID 			= @$ID[0];
my $Buyer_Name 			= @$Buyer_Name[0];
my $Buyer_Url 			= @$Spent_Data_weblink[0];
################ Retrived Column mapping details from Field_Map_Details ###########################
my ($File_Type,$Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2) = &BIP_DB_V1::Column_Name_RetrieveUrl($dbh,$Buyer_ID);

#To change
# For number of columns available in field mapping table. This is used to update the Field_Match status
my @DB_Column_Headers=($Transaction_ID,$Requirement,$Amount,$Start_Date,$End_Date,$Supplier_Name,$Department_Name,$Requirement_2);
my @Header_Data_Avail_Column = grep(/[A-Za-z]/, @DB_Column_Headers);
my $Header_Data_Avail_Column=@Header_Data_Avail_Column;
###############


print "Buyer_ID	: $Buyer_ID\n";	
print "Buyer_Name	: $Buyer_Name\n";	
print "Buyer_Url	: $Buyer_Url\n";	
# my $Doc_content1;
my ($Doc_content1,$status_line1,$F_FileType1);
my $StartDate=DateTime->now();

my ($Buyer_content,$Status_Line)=&Getcontent($Buyer_Url); &BIP_Column_Map_V1::Link_Status_Update($dbh,$Buyer_ID,$Status_Line);
open(ts,">Buyer_content100.html");
print ts "$Buyer_content";
close ts;

my ($Processed_LinkInfo) = &BIP_DB_V1::LinkInfo_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
my @Processed_LinkInfo=@$Processed_LinkInfo;

# To select the files block for every month		
while($Buyer_content=~m/href\s*\=\s*\"\s*([^>]*?)\s*\"\s*[^>]*?\s*>(\s*[^>]*?2\d{3}\/\d{2}[^>]*?\s*)<\/a>/igs)
{	
	my $Doc_Link1=$1;	
	my $LinkInfo1=$2;	
	# my ($Doc_Link,$LinkInfo1);
	if($Doc_Link1!~m/^http/is)
	{	
		print "\nhi\n";
		my $u1=URI::URL->new($Doc_Link1,$Buyer_Url);
		my $u2=$u1->abs;
		$Doc_Link1=$u2;
		$Doc_Link1=~s/\&amp\;/&/igs;		
	}		
	($Doc_content1,$status_line1,$F_FileType1)=&Getcontent($Doc_Link1);		
			
	fitst:while($Doc_content1=~m/href\s*\=\s*\"\s*([^>]*?)\s*\"\s*[^>]*?\s*>(\s*[^>]*?csv[^>]*?\s*)<\/a>/igs)
	{
		my $Doc_Link=$1;	
		my $LinkInfo=$2;
		
		# if($Doc_Link1=~m/Publication/is)
		# {			
			# goto Frist;			
		# }
		# else
		# {
			# $Doc_Link=$Doc_Link1;
		# }
		$LinkInfo=&clean($LinkInfo);
		
		my ($File_Date,$File_Month,$File_Year,$Doc_Name);
		my $LinkInfo_Temp=$LinkInfo;	
		
		# Get Title of the file name
		if($LinkInfo ne '') 
		{	
			$LinkInfo=~s/\s\s+/ /igs;
			print "LinkInfo	: $LinkInfo\n";
			if($LinkInfo ~~ @Processed_LinkInfo)
			{	
				# print "\nLinkInfo---->$LinkInfo\n";
				print "File Already Processed\n";
				next;
			}
			else
			{
				# print "\nLinkInfo---->$LinkInfo\n";
				print "\nThis is new file to download\n";
			}
		}
		
		
		# Get file link 	
		
		if($Doc_Link!~m/^http/is)
		{	
			print "\nhi\n";
			my $u1=URI::URL->new($Doc_Link,$Buyer_Url);
			my $u2=$u1->abs;
			$Doc_Link=$u2;
			$Doc_Link=~s/\&amp\;/&/igs;
		}
		
		print "Doc_Link	: $Doc_Link\n";
		$Doc_Link=~s/\/\.\.\///igs;
		print "\nLinkInfo_Temp	: $LinkInfo_Temp\n";
		# $Doc_Link="http://info.westberks.gov.uk/CHttpHandler.ashx?id=32447&p=0";
			
		foreach my $monthss(@Months1)	
		{		
			if($LinkInfo_Temp=~m/$monthss/is)
			{
				$File_Month=$monthss;
				if($LinkInfo_Temp=~m/(2\d{3})/is)
				{
					$File_Year=$1;
				}			
				else
				{
					while($LinkInfo_Temp=~m/(\d{2,4})/igs)
					{					
						$File_Year=$1;
					}
				}	
				last;
			}			
		}
		
		# if($File_Year eq '')
		# {
			# if($LinkInfo_Temp=~m/(Quarter\s*\d+)/is)
			# {
				# $File_Year=$1;
			# }
		# }			
		# Get Date, Month, Year from the link info for file location Creation
		# if($Doc_Link=~m/\-([\w]+)\-(2\d{3})/is) 
		# {		
			# $File_Month=$1;
			# $File_Year=$2;
		
			print "File_Month	: $File_Month\n";
			print "File_Year	: $File_Year\n";
		# }
		
		# File name creation from Link Info
		$Doc_Name=$LinkInfo;	
		$Doc_Name=&Trim($Doc_Name);	
		$Doc_Name=~s/\s/\_/igs;
		$Doc_Name=~s/\W//igs;
		$Doc_Name.='.'.$File_Type;
		print "Doc_Name	: $Doc_Name\n"; 
		$File_Year=~s/\W//igs;
		my $Month_Of_Document = "$File_Month $File_Year";
		# File Path creation
		my $hash_ref = &BIP_Column_Map_V1::ReadIniFile();	my %inihashvalue = %{$hash_ref};	my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source"; 
		print "Filestorepath	: $Filestorepath\n";
		
		# File location creation
		unless ( -d $Filestorepath ) 
		{
			$Filestorepath=~s/\//\\/igs;
			system("mkdir $Filestorepath");
		}
		
		# Spend file download
		my ($Doc_content,$status_line,$F_FileType)=&Getcontent($Doc_Link);
		if($F_FileType=~m/(xlsx|xls)/is)
		{
			print "\nGoing to process next link\n";
			next;
		}
		my $fh=FileHandle->new("$Filestorepath\\$Doc_Name",'w');
		binmode($fh);
		$fh->print($Doc_content);
		$fh->close();
		my ($Status,$Failed_Reason);
		if($status_line!~m/20/is)
		{
			$Failed_Reason=$status_line;
		}
		else
		{
			$Status='Downloaded';
		}
		print "\nStatus:::: $Status\n";
		
		&BIP_Column_Map_V1::ReadSource($Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
	print "Completed \n";
	}	
}

if($F_Flag eq '1')
{
	if($Buyer_content=~m/href\s*\=\s*\"\s*([^>]*?)\s*\"\s*[^>]*?\s*>(\s*[^>]*?csv[^>]*?\s*)<\/a>/is)
	{
		$Doc_content1=$Buyer_content;
		$F_Flag++;
		goto fitst;
	}
}

sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	# print "\nFile_Type---> $File_Type\n";
	# my $get_cookie=$res->header('set-cookie');
	print "\nCODE---> $code\n";
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$File_Type);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	return $Data;
}

sub clean()
{
	my $C_Clean=shift;
	$C_Clean=~s/^\s*|\s*$//igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/\&nbsp\;/ /igs;
	$C_Clean=~s/\&pound\;/ /igs;
	
	$C_Clean=~s/\#160\;//igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	return $C_Clean;
}