use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use Time::Local qw(timelocal_nocheck);
use Spreadsheet::ParseExcel::Utility qw(ExcelLocaltime);
use Spreadsheet::WriteExcel::Utility; 
use Date::Parse;
use POSIX;
use MIME::Lite;
require HSJ_DB;

# my $Input_Buyer_ID=$ARGV[0];
# my $Batch_ID=$ARGV[0];

my @Months=("","January","February","March","April","May","June","July","August","September","October","November","December");
my %Months1=("jan" => "01", "feb" => "02", "mar" => "03", "apr" => "04", "may" => "05", "jun" => "06", "jul" => "07", "aug" => "08", "sep" => "09", "oct" => "10", "nov" => "11", "dec" => "12");
my @lower_months = keys %Months1;
my %Months2=("january" => "01", "february" => "02", "march" => "03", "april" => "04", "may" => "05", "june" => "06", "july" => "07", "august" => "08", "september" => "09", "october" => "10", "november" => "11", "december" => "12");
my %Months3=("January" => "Jan", "February" => "Feb", "March" => "Mar", "April" => "Apr", "May" => "May", "June" => "Jun", "July" => "Jul", "August" => "Aug", "September" => "Sep", "October" => "Oct", "November" => "Nov", "December" => "Dec");
my %MonthHash =();
 %MonthHash = ('JAN' => '01','FEB' => '02','MAR' => '03','APR' => '04','MAY' => '05','JUN' => '06','JUL' => '07','AUG' => '08','SEP' => '09','OCT' => '10','NOV' => '11','DEC' => '12','JANUARY' => '01','FEBRUARY' => '02','MARCH' => '03','APRIL' => '04','MAY' => '05','JUNE' => '06','JULY' => '07','AUGUST' => '08','SEPTEMBER' => '09','OCTOBER' => '10','NOVEMBER' => '11','DECEMBER' => '12');
my %Months_Num_To_Txt=("01" => "Jan", "02" => "Feb", "03" => "Mar", "04" => "Apr", "05" => "May", "06" => "Jun", "07" => "Jul", "08" => "Aug", "09" => "Sep", "10" => "Oct", "11" => "Nov", "12" => "Dec"); 
 
my ($Second, $Minute, $Hour, $Day, $Month, $Year, $WeekDay, $DayOfYear, $IsDST) = localtime();
 $Month = $Month+1;
 $Year = $Year+1900;
  
my $Cencheck = substr($Year,2,2);
my $Cen  = substr($Year,0,2);
my $Cen1  = $Cen-1;
my $YY; # Year in last 2 digits
if($Year=~m/([\d]{2})$/is)
{
	$YY=$1;
} 
############Database Initialization########
my $dbh = &HSJ_DB::DbConnection();
###########################################

my $Input_Table='SUPPLIER_SOURCE_DATA';
############ Retrieve Buyer_Name, ID, Spent Data weblink from Buyers table ###########
my $import_flag='Y';
my $format_flag='N';
my ($Input_Buyer_ID,$Batch_ID,$Linkinfo) = &HSJ_DB::Retrieve_Buyers_Download_Details_For_Clean($dbh,"BUYERS_DOWNLOAD_DETAILS",$import_flag,$format_flag);
my @Input_Buyer_ID		= @$Input_Buyer_ID;
my @Batch_ID			= @$Batch_ID;
my @Linkinfo			= @$Linkinfo;

for(my $i=0;$i<@Input_Buyer_ID;$i++)
{
	$Input_Buyer_ID=$Input_Buyer_ID[$i];
	$Batch_ID=$Batch_ID[$i];
	$Linkinfo=$Linkinfo[$i];
	print "Input_Buyer_ID: $Input_Buyer_ID: $Batch_ID\n";
	open(wa,">>Batch_ID.txt");
	print wa "$Batch_ID\n";
	close wa;
	####### Identify Month OF Document From LINKINFO ##########
	my ($MOD,@MOD,@YYYY);
	while ($Linkinfo=~m/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)/igs)
	{
		push(@MOD,$1);
	}
	while ($Linkinfo=~m/(20[\d]{2})/igs)
	{
		push(@YYYY,$1);
	}
	if(@YYYY == 0)
	{
		while($Linkinfo=~m/\b([\d]{2})\b/igs)
		{
			my $FY=$1;
			if($FY<=$YY)
			{
				push(@YYYY,$FY);
				last;
			}
		}
	}
	if(@YYYY>1)
	{
		if(@MOD>1)
		{
			$MOD="$MOD[0]-$YYYY[0] $MOD[1]-$YYYY[1]";
		}	
		else
		{
			$MOD="$MOD[0]-$YYYY[0]";
		}	
	}
	elsif(@MOD>1)
	{
		$MOD="$MOD[0]-$YYYY[0] $MOD[1]-$YYYY[0]";
	}
	else
	{
		$MOD="$MOD[0]-$YYYY[0]";
	}
	$MOD='' if($MOD!~m/\w/is);	
	$MOD=~s/\-//igs if($MOD!~m/[a-z]+-[\d]+/is);		
	####### End ##########
	
	my ($ID,$Buyer_Name,$Spent_Data_weblink) = &HSJ_DB::RetrieveUrl($dbh,$Input_Buyer_ID,"Buyers");
	my $Buyer_ID 			= @$ID[0];
	my $Buyer_Name 			= @$Buyer_Name[0];
	my $Buyer_Url 			= @$Spent_Data_weblink[0];
	my ($SIGN_CHECK) = &HSJ_DB::Retrive_Sign_Check($dbh,$Input_Buyer_ID,"Field_Map_Details_Automation");
	my $insert_collect = 0;
	my $MOD_Flag = 0;
	my $insert_query = "insert into SUPPLIER_FORMATTED_DATA (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Department,Entity,Start_Date,End_Date,Expense_Type,Expense_Area,Supplier_Name,Transaction_Reference,Amount,Supplier_Match_Flag) values";
	
	############ Retrieve Spent Data from SUPPLIER_SOURCE_DATA table ###########
	my ($Supplier_Name,$Status,$Month_Of_Document,$Processed_Date,$Sno,$Department,$Entity,$Start_Date,$End_Date,$Expense_Type,$Expense_Area,$Supplier_Name,$Transaction_Reference,$Amount) = &HSJ_DB::Retrieve_Source_Data($dbh,$Input_Buyer_ID,$Batch_ID,$Input_Table);

	my $reccount = @{$Month_Of_Document};
	my ($Supplier_Name_Matched_Count,$Supplier_Name_NotMatched_Count)=(0,0);
	my $Formatted_Flag='N';
	my $Supplier_Match='N';
	my $Minus_values_count=0;
	if($reccount == 0)
	{
		print "RecCount: $reccount\n";
		$Formatted_Flag='Y';
		&HSJ_DB::Update_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Buyer_ID,$Batch_ID,$Supplier_Name_Matched_Count,$Supplier_Name_NotMatched_Count,$Formatted_Flag,$Supplier_Match,$MOD);
		next;
	}
	for(my $reccnt = 0; $reccnt < $reccount; $reccnt++ )
	{
		my $Supplier_Name 		= &Trim(@$Supplier_Name[$reccnt]);
		my $Matching_Status		= &Trim(@$Status[$reccnt]);
		my $Month_Of_Document 	= &Trim(@$Month_Of_Document[$reccnt]);
		my $Processed_Date 		= &Trim(@$Processed_Date[$reccnt]);
		my $Sno 				= &Trim(@$Sno[$reccnt]);
		my $Department			= &Trim(@$Department[$reccnt]);
		my $Entity	 			= &Trim(@$Entity[$reccnt]);
		my $Start_Date 			= &Trim(@$Start_Date[$reccnt]);
		my $End_Date 			= @$End_Date[$reccnt];
		my $Expense_Type	 	= &Trim(@$Expense_Type[$reccnt]);
		my $Expense_Area 		= &Trim(@$Expense_Area[$reccnt]);
		my $Supplier_Name		= &Trim(@$Supplier_Name[$reccnt]);
		my $Transaction_Reference= &Trim(@$$Transaction_Reference[$reccnt]);
		my $Amount 				= &Trim(@$Amount[$reccnt]);

		$Month_Of_Document=$MOD if($Month_Of_Document eq '');
		print "MOD: : $Month_Of_Document\n";
		
		if(($Start_Date!~m/[\d]+/is) and ($Start_Date!~m/^\s*$/is))
		{
			next;
		}
		if($Start_Date=~m/^\s*$/is)
		{
			if($Supplier_Name=~m/^\s*$/is)
			{
				next;
			}	
		}
		if ($Supplier_Name eq '' or $Supplier_Name =~ m/^\s*$/is)
		{
			$Supplier_Name = 'Not provided';
		}
		print "Start_Date: $Start_Date\n";
		($Amount)=&Price_Clean($Amount);
		if($Amount=~m/^\s*$/is){next;}
		my $End_Date1;
		($Start_Date,$End_Date1)=&formatdate($Start_Date,$Month_Of_Document);
		$End_Date=$End_Date1 if($End_Date1 ne '');
		# print "Start_Date: $Start_Date\n";
		# print "End_Date: $End_Date\n";
		
		if($Start_Date=~m/^\s*$/is){next;}
		$End_Date=&Trim($End_Date);
		if($SIGN_CHECK eq 'Minus')
		{
			$Amount=~s/^\s*\-//igs;
		}	
		$Minus_values_count++ if($Amount=~m/^\s*\-/is);
		
		$Formatted_Flag='Y';
		my $Supplier_Name_without_Spaces=$Supplier_Name;
		$Supplier_Name_without_Spaces=~s/\s+//igs;
		# print "Cleaned: $Supplier_Name_without_Spaces\n";
		############ Retrieve Supplier Name Matching from UNIQUE_SUPPLIER_LIST and SUPPLIER_LIST table ###########
		# my ($Retrived_Supplier_Name) = &HSJ_DB::Supplier_Name_Matching($dbh,$Supplier_Name_without_Spaces);
		print "$Sno : $Input_Buyer_ID : $Batch_ID : $Matching_Status: $Supplier_Name\n";

		# Get Month & Year of document from source data. 
		my ($m,$m1,$y6,$MOD1);
		if($MOD_Flag == 0)
		{
			if($Start_Date=~m/^\s*([\d]{1,2})\/([\d]{1,2})\/([\d]{4})\s*$/is)
			{
				$m=$2;
				$y6=$3;
				$m1=$Months_Num_To_Txt{$m};
				if($m1 ne '')
				{
					$MOD1="$m1-$y6";
				}
				$MOD1='' if($MOD1!~m/\w/is);	
				$MOD1=~s/\-//igs if($MOD1!~m/[a-z]+-[\d]+/is);		
				# print "MOD1:$MOD\n";
			}
			if($MOD1 ne '') # If not able to identify the MOD from SOURCE DATA,use the MOD from LINKINFO
			{
				$MOD=$MOD1;
			}
			$MOD_Flag = 1;
		}
		# print "MOD2:$MOD\n";
		my $Retrived_Supplier_Name_tmp=lc($Supplier_Name);
		my $Buyer_Name_tmp=lc($Buyer_Name);
		if($Retrived_Supplier_Name_tmp eq $Buyer_Name_tmp) # Check Supplier name as buyer name
		{
			$Supplier_Name.=' ';
		}
		if($Matching_Status eq 'Y')
		{
			$Supplier_Name_Matched_Count++;
		}
		elsif($Matching_Status eq 'N')
		{
			$Supplier_Name_NotMatched_Count++;
		}
		$insert_collect++;
		$Supplier_Name=~s/\'/\'\'/igs;
		$Department=~s/\'/\'\'/igs;
		$Entity=~s/\'/\'\'/igs;
		$Expense_Type=~s/\'/\'\'/igs;
		$Expense_Area=~s/\'/\'\'/igs;		
		$Transaction_Reference=~s/\'/\'\'/igs;	
		if($insert_collect <= 990)
		{
			$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',\'$Processed_Date\',\'$Sno\',\'$Department\',\'$Entity\',\'$Start_Date\',\'$End_Date\',\'$Expense_Type\',\'$Expense_Area\',\'$Supplier_Name\',\'$Transaction_Reference\',\'$Amount\',\'$Matching_Status\'),";
		}
		else
		{
			$insert_query .= "(\'$Buyer_ID\',\'$Batch_ID\',\'$Month_Of_Document\',\'$Processed_Date\',\'$Sno\',\'$Department\',\'$Entity\',\'$Start_Date\',\'$End_Date\',\'$Expense_Type\',\'$Expense_Area\',\'$Supplier_Name\',\'$Transaction_Reference\',\'$Amount\',\'$Matching_Status\'),";
			$insert_query =~ s/\,\s*$//igs;
			$insert_query .= ';';
			&HSJ_DB::Insert_Cleaned_Spend_Data($dbh,$insert_query);
			$insert_query = "insert into SUPPLIER_FORMATTED_DATA   (Buyer_ID,Batch_ID,Month_Of_Document,Processed_Date,Sno,Department,Entity,Start_Date,End_Date,Expense_Type,Expense_Area,Supplier_Name,Transaction_Reference,Amount,Supplier_Match_Flag) values";
			$insert_collect = 0;
		}		
	}
	if($insert_collect >= 1)
	{
		$insert_query =~ s/\,\s*$//igs;
		$insert_query .= ';';
		&HSJ_DB::Insert_Cleaned_Spend_Data($dbh,$insert_query);
	}
	print "\nStep:\n";	
	
	my ($Records_Count)=&HSJ_DB::Retrieve_Count_Details($dbh,"SUPPLIER_FORMATTED_DATA",$Buyer_ID,$Batch_ID);
	if(($Records_Count == $Supplier_Name_Matched_Count) && ($Supplier_Name_NotMatched_Count == 0))
	{
		$Supplier_Match='Y';
	}
	if($Records_Count!=0)
	{
		if($Records_Count == $Minus_values_count)
		{
			my $warning_msg=$Minus_values_count.' Amount Values contains minus values. Please check the batches.';
			&send_mail($warning_msg,$Input_Buyer_ID,$Batch_ID);
		}
	}	
	print "Records_Count: $Records_Count\n";
	print "Supplier_Match: $Supplier_Match\n";

	if($Records_Count==0)
	{
		$Formatted_Flag='Y';
		&HSJ_DB::Update_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Buyer_ID,$Batch_ID,$Supplier_Name_Matched_Count,$Supplier_Name_NotMatched_Count,$Formatted_Flag,$Supplier_Match,$MOD);
		next;
		open(RC,">>Records_Count.txt");
		print RC "$Buyer_ID\t$Batch_ID\n";
		close RC;
	}
	# To update Supplier_Match, No_of_Suppliers_Matched, No_of_Suppliers_Not_Matched, Formatted_Flag 
	&HSJ_DB::Update_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Buyer_ID,$Batch_ID,$Supplier_Name_Matched_Count,$Supplier_Name_NotMatched_Count,$Formatted_Flag,$Supplier_Match,$MOD);

	# To Select No of records in SUPPLIER_FORMATTED_DATA 
}

sub Trim()
{
	my $Data=shift;
	my $newline=chr(10);
	$Data=~s/$newline/ /igs;
	$Data=~s/\"/ /igs;
	$Data=~s/[^[:print:]]+/ /igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/^\s*|\s*$//igs;
	# $Data=~s/Not\s*provided//igs;
	# if($Data eq '')
	# {
		# $Data='Not provided';
	# }
	return $Data;
}

sub Price_Clean()
{
	my $Data=shift;
	my $Format_Error;
	if($Data=~m/^\s*\([^>]*?\)\s*$/is)
	{
		$Data=~s/[\(\)]//igs;
		$Data='-'.$Data;
	}
	$Data=~s/^\s*-/minussymbol/igs;
	$Data=~s/^minussymbol\W+/minussymbol/igs;
	$Data=~s/^\W+|\W+$//igs;
	$Data=~s/\,//igs;
	$Data=~s/minussymbol/-/igs;
	$Data = sprintf("%.2f", $Data);	
	if(($Data=~m/^\s*$/is) and (($Data!~m/^0.00$/is) or ($Data!~m/^0$/is)))
	{
		$Data='Not Provided'
	}	
	return ($Data);
}

sub Date_Format()
{
	my $Data=shift;
	my $Format_Error;
	my ($DD,$MM,$YY);

	if($Data=~m/^\s*(0*[1-9]|[12][0-9]|3[01])\s*[-\/. ]\s*(0*[1-9]|1[012])\s*[-\/. ]\s*((?:19|20)\d\d)\s*$/is) # DD/MM/YYYY - Format;
	{
		$DD=$1;
		$MM=$2;
		$YY=$3;
	}
	elsif($Data=~m/^\s*((?:19|20)\d\d)\s*[-\/. ]\s*(0*[1-9]|1[012])\s*[-\/. ]\s*(0*[1-9]|[12][0-9]|3[01])\s*$/is) # YYYY/MM/DD - Format
	{
		$YY=$1;
		$MM=$2;
		$DD=$3;
	}
	elsif($Data=~m/^\s*(0*[1-9]|[12][0-9]|3[01])\s*[-\/. ]\s*([a-z]+?)\s*[-\/. ]\s*((?:19|20)\d\d)\s*$/is) # YYYY/MM as Text/DD - Format
	{
		$DD=$1;
		$MM=lc($2);
		$YY=$3;
		my $mon=$Months1{$MM};
		if($mon eq '')
		{
			$mon=$Months2{$MM};
		}
	}
	else
	{
		if($Data ne '')
		{
			$Format_Error='Format Error at Date';
			open(DATA,">>Date_Format_Error.txt");
			print DATA $Format_Error;
			close DATA;
		}	
	}
	return ($Data);
}

sub formatdate
{
	my $value = shift;
	my $Month_Of_Document = shift;
	$value =~ s/^\s+|\s+$//g;
	$value =~ s/\s*00\:00\:00\s*$//g;
	$value =~ s/\s*00\:00\s*$//g;
	my $value2; #End Date
	my ($day,$month,$year);
	if($value eq '')
	{
		if($Month_Of_Document=~m/^\s*([a-z]+)\s*(?:\-)?\s*([\d]+)$/is)
		{
			$month=$1;
			$year=$2
		}
		$value=$month.'/'.$year;
	}	

	# print "V1: $value\n";
	# if($value!~m/^\s*$/is)
	# {
		# eval{
		# $value=xl_parse_date($value);
		# };
	# }
	# print "$value\n";
	if ($value=~m/^(\d+)\s*(?:[\-\/\.])\s*(\d+)\s*(?:[\-\/\.])\s*(\d+)$/)
	{
		my $v1=$1;
		my $v2=$2;
		my $v3=$3;
		$v1='0'.$v1 if(length($v1)==1);
		$v2='0'.$v2 if(length($v2)==1);
		$v3='0'.$v3 if(length($v3)==1);
		$value=$v1.'/'.$v2.'/'.$v3;
	}
	# print "V1: $value\n";
	if($value=~m/(\bjanuary\b|\bfebruary\b|\bmarch\b|\bapril\b|\bmay\b|\bjune\b|\bjuly\b|\baugust\b|\bseptember\b|\boctober\b|\bnovember\b|\bdecember\b|\bjan\b|\bfeb\b|\bmar\b|\bapr\b|\bmay\b|\bjun\b|\bjul\b|\baug\b|\bsep\b|\boct\b|\bnov\b|\bdec\b)/is)
	{
		my $txt=lc($1);
		my $month_t=$Months1{$txt};
		if($month_t=~m/^\s*$/is)
		{
			$month_t=$Months2{$txt};
		}
		$value=~s/$txt/$month_t/igs;
		$value=~s/\./\//igs;
		$value=='' if($value!~m/\d/is);
		print "IF1: $value\n";
	}
	print "V2: $value\n";
	if($value=~m/^\s*([\d]{1,2})\s*(?:\/|\.|\-|\s)\s*([\d]{4})\s*$/)
	{
		$day='01';
		$month=$1;
		$year=$2;
		$value = "$day/$month/$year"; #Start Date
		# # $month--;
		# my @time_data = (0,0,0,$day,$month,$year,0,0,0);
		# $time_data[4]++;   # Next month.
		# $time_data[3] = 0; # Last day of previous month.

		# @time_data = localtime(timelocal_nocheck(@time_data));

		# my $year     = $time_data[5]+1900;
		# my $month    = $time_data[4]+1;    # Make 1-based
		# my $last_day = $time_data[3];
		# $last_day = "0$last_day" if (length($last_day) == 1);
		# $month = "0$month" if (length($month) == 1);
		# $value2 = "$last_day/$month/$year";#End Date
		my $date_t = DateTime->new(
			year  =>  $year,
			month => $month,
			day   => 1,
		);

		my $date2_t = DateTime->last_day_of_month(  
			year  =>  $date_t->year,
			month => $date_t->month,
		);
		$value2=$date2_t->dmy('/');
		
		print "IF2: $value\n";
		print "IF2: $value2\n";
	}
	# print "V3: $value\n";
	if($value=~m/^\s*([\d]{1,2})\s*(?:\/|\.|\-)\s*([\d]{2})\s*$/)
	{
		$day='01';
		$month=$1;
		$year='20'.$2;
		$value = "$day/$month/$year"; #Start Date
		# $month--;
		# my @time_data = (0,0,0,$day,$month,$year,0,0,0);
		# $time_data[4]++;   # Next month.
		# $time_data[3] = 0; # Last day of previous month.

		# @time_data = localtime(timelocal_nocheck(@time_data));

		# my $year     = $time_data[5]+1900;
		# my $month    = $time_data[4]+1;    # Make 1-based
		# my $last_day = $time_data[3];
		# $last_day = "0$last_day" if (length($last_day) == 1);
		# $month = "0$month" if (length($month) == 1);
		# $value2 = "$last_day/$month/$year";#End Date
		my $date_t = DateTime->new(
			year  =>  $year,
			month => $month,
			day   => 1,
		);

		my $date2_t = DateTime->last_day_of_month(  
			year  =>  $date_t->year,
			month => $date_t->month,
		);
		$value2=$date2_t->dmy('/');		
		print "IF 10: $value\n";
		print "IF 10: $value2\n";
	}
	# print "V4: $value\n";
	# if($value=~m/^\s*(\d{5})\s*$/)
	# {
		# my ($iSec, $iMin, $iHour, $iDay, $iMon, $iYear, $iwDay, $iMSec) = ExcelLocaltime($value);
		# $iYear += 1900;
		# $iMon  +=1;
		
		# $iDay = "0$iDay" if (length($iDay) == 1);
		# $iMon = "0$iMon" if (length($iMon) == 1);
		# $value = "$iDay/$iMon/$iYear";
		# print "1: $value\n";
	# }	
	if($value=~m/^\s*([\d]{5})\s*$/)
	{
		if($value>=40000)
		{
			$value=&HSJ_DB::dt_format($dbh,$value);
			print "IF3: $value\t";
		}
	}
	if($value=~m/^\s*([\d]{6,8})\s*$/)
	{
		if($value=~m/^([\d]{4})/is)
		{
			my $Year_Check=$1;
			if($Year_Check>2012)
			{
				$value = strftime("%d/%m/%Y", localtime(str2time($value, 'GMT')));				
				print "IF4: $value\n";
			}	
		}		
		# if($value=~m/^\s*([\d]{2})([\d]{2})([\d]{4})\s*$/is)
		# {
			# $value = "$1/$2/$3";
			# print "IF4: $value\t";
		# }	
	}
	# elsif(length($value)==8 and ($value =~m/^\s*([\d]+)\s*$/))
	# {
		# if($value=~m/([\d]){4}([\d]){2}([\d]){2}/is)
		# {
			# $year = $1;
			# $month =$2;
			# $day  = $3;

			# $day = "0$day" if (length($day) == 1);
			# $month = $MonthHash{$month};
			# if ((length($year) == 2) && ($year le $Cencheck) )
			# {
				# $year = $Cen.$year;
			# }
			# elsif (length($year) == 2)
			# {
				# $year = $Cen1.$year;
			# }
			# $value = "$day/$month/$year";
			# print "IF4: $value";
		# }	
	# }
	# elsif ($value=~ m/^(\d+)\s*(?:[\-\/\.])\s*(\d+)\s*(?:[\-\/\.])\s*(\d+)$/ )
	elsif ($value=~ m/^((?:19|20)\d\d)[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$/igs)
	{
		$year = $1;
		$month = $2;
		$day  = $3;
		# if ((length($year) == 2) && ($year le $Cencheck) )
		# {
			# $year = $Cen.$year;
		# }
		# elsif (length($year) == 2)
		# {
			# $year = $Cen1.$year;
		# }
		$value = "$day/$month/$year";
		print "IF5: $value\n";
	}
	elsif($value=~ m/^(0[1-9]|[12][0-9]|3[01])[-\/.](0[1-9]|1[012])[- \/.]((?:19|20)\d\d)$/)
	{
		$day  = $1;
		$month = $2;
		$year = $3;
		# if ((length($year) == 2) && ($year le $Cencheck) )
		# {
			# $year = $Cen.$year;
		# }
		# elsif (length($year) == 2)
		# {
			# $year = $Cen1.$year;
		# }
		$value = "$day/$month/$year";
		print "IF6: $value\n";
	}
	elsif($value=~ m/^(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[-\/.]((?:19|20)\d\d)$/)
	{
		$month = $1;
		$day  = $2;
		$year = $3;
		# if ((length($year) == 2) && ($year le $Cencheck) )
		# {
			# $year = $Cen.$year;
		# }
		# elsif (length($year) == 2)
		# {
			# $year = $Cen1.$year;
		# }
		$value = "$day/$month/$year";
		print "IF7: $value\n";
	}
	elsif ($value =~ m/^(\d+)\s*(?:[\-\/\.])\s*(\d+)\s*(?:[\-\/\.])\s*((?:\d+)\s*(?:[0-9\:\.]+))/)
	{
		if (length($1) == 4)
		{
			$day  = $3;
			$month = $2;
			$year = $1;
		}
		else
		{
			$day  = $1;
			$month = $2;
			$year = $3;
		}

		$day = "0$day" if (length($day) == 1);
		$month = "0$month" if (length($month) == 1);
		if ((length($year) == 2) && ($year le $Cencheck) )
		{
			$year = $Cen.$year;
		}
		elsif (length($year) == 2)
		{
			$year = $Cen1.$year;
		}

		$value = "$day/$month/$year";
		print "IF8: $value\n";
	}
	elsif ( $value =~ m/^(\d+)\/?\-?\s*([a-zA-Z]+)\/?\-?\s*(\d+)/ )
	{
		$day  = $1;
		$month = uc($2);
		$year = $3;

		$day = "0$day" if (length($day) == 1);
		$month = $MonthHash{$month};
		if ((length($year) == 2) && ($year le $Cencheck) )
		{
			$year = $Cen.$year;
		}
		elsif (length($year) == 2)
		{
			$year = $Cen1.$year;
		}
		$value = "$day/$month/$year";
		print "IF9: $value\n";
	}
	elsif($value=~m/^\s*(0*[1-9]|[12][0-9]|3[01])\s*[-\/. ]\s*(0*[1-9]|1[012])\s*[-\/. ]\s*((?:19|20)\d\d)\s*$/is) # DD/MM/YYYY - Format;
	{
		$day=$1;
		$month=$2;
		$year=$3;
		$value = "$day/$month/$year";
		print "IF10: $value\n";
	}
	elsif($value=~m/^\s*((?:19|20)\d\d)\s*[-\/. ]\s*(0*[1-9]|1[012])\s*[-\/. ]\s*(0*[1-9]|[12][0-9]|3[01])\s*$/is) # YYYY/MM/DD - Format
	{
		$year=$1;
		$month=$2;
		$day=$3;
		$value = "$day/$month/$year";
		print "IF11: $value\n";
	}
	elsif($value=~m/^\s*(0*[1-9]|[12][0-9]|3[01])\s*[-\/. ]\s*([a-z]+?)\s*[-\/. ]\s*((?:19|20)\d\d)\s*$/is) # YYYY/MM as Text/DD - Format
	{
		$day=$1;
		$month=lc($2);
		$year=$3;
		my $mon=$Months1{$month};
		if($mon eq '')
		{
			$mon=$Months2{$month};
		}
		$value = "$day/$mon/$year";
		print "IF12: $value\n";
	}	

	
	$value=~s/\./\//igs;
	my @match_month = grep { $Month_Of_Document =~/$_/is } @lower_months;
	my ($doc_mon,$doc_mon_str);
	$doc_mon_str = $match_month[0] if($match_month[0]);
	$doc_mon = $Months1{$doc_mon_str};
	unless(@match_month)
	{
		if($Month_Of_Document =~m/month\s*\b(\d{1,2})\b/is)
		{
			my $mon = $1;
			$mon='0'.$mon if(length($mon) == 1);
			$doc_mon = $mon if($mon <= 12);
		}
	}
	if($value =~ m/(\d+)\/(\d+)\/(\d+)/igs)
	{
		my $day = $1;
		my $month = $2;
		my $year = $3;
		if($month eq $doc_mon)
		{
			$value = $value;
		}
		elsif($day eq $doc_mon)
		{
			$value = $month.'/'.$day.'/'.$year;
		}
	}
	return ($value,$value2);
}

sub send_mail()
{
	my $error	 = shift;
	my $Buyer_ID = shift;
	my $Batch_ID = shift;
	
	my $subject="Sign Check - $Buyer_ID - $Batch_ID";
	my $host ='74.80.234.196'; 
	my $from='autoemailsender@meritgroup.co.uk';
	my $user='meritgroup';
	my $to ='balaji.ekambaram@meritgroup.co.uk';
	my $pass='11meritgroup11';
	my $body = "Hi Balaji, <br><br>\t\t$error<br><br>Regards<br>HSJ TEAM";
	
	my $msg = MIME::Lite->new (
	  From => $from,
	  To => $to,
	  # Cc => $cc,
	  Subject => $subject,
	  Data => $body,
	  Type =>'text/html'
	) or die "Error creating multipart container: $!\n";
    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	$msg->send;
}