use strict;
use HTML::Entities;
use DBI;
use DBD::ODBC;
use Date::Parse;
use Date::Manip;
use POSIX 'strftime';
use MIME::Lite;
use MIME::Base64;
use Config::IniFiles;
use Text::CSV;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );

my $zip = Archive::Zip->new();
$zip->addFile("Uploaded_Status.xls");
print "Converting to zip file\n";
my $filename="UN_FAO_Output.zip";
unless($zip->writeToFileNamed($filename) == AZ_OK )
{
die 'write error';
}