## Imports
use strict;
use LWP::UserAgent;
use HTTP::Cookies;
use JSON;
use MIME::Lite;
use MIME::Base64;
# use Authen::SASL;
# my $err_stmt;
# my $err_warn;
# $SIG{__DIE__}  = \&die_handler;
# $SIG{__WARN__} = \&warn_handler;
our $Buyer_ID1;

## User agent setup
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Merit BI OUToader");
# $ua->timeout(50);
push @{ $ua->requests_redirectable }, 'POST';

my $cookiefile = $0;
$cookiefile =~s/\.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1);
$ua->cookie_jar($cookie);


	my $url='http://62.253.177.4:8080/businessIntelligence/j_security_check';

	my $response=$ua->post(
		$url,
		[
	
			'j_username' => 'arul.kumaran@meritgroup.co.uk',
			'j_password' => 'tomcat'
		]
	);

	if ($response->is_success) {
		if (!$response->request()->uri() =~ /http\:\/\/62\.253\.177\.4\:8080\/businessIntelligence\/businessIntelligence.html/) {
			die "Login Failed";
		}
		else{
			print "LoggedIn\n";
		}
	}
	
my $filename="BI_Tool_Output_June_10_2019.txt";
open(out,">>$filename");
print out "Buyer Name\tBatch ID\tCreated\tUploader\tProcessed Start Date\tProcessed End Date\n";
close out;
	
my $url1="http://62.253.177.4:8080/businessIntelligence/spend/spendBuyers.html";
my ($Home_content,$Referer)=&Getcontent($url1);
	open(out,">hmv.html");
	print out "$Home_content";
	close out;
	while($Home_content=~m/<tr>([\w\W]*?)<\/tr>/igs)
		{
		my $block=$1;
		my ($buyer_name,$url2);
		if($block=~m/<td><a href="([^>]*?)">View Uploads<\/a><\/td>/is)
		{
		$url2="http://62.253.177.4:8080".$1;
		}
		if($block=~m/<td>([^>]*?)<\/td>\s*<td><a/is)
		{
		$buyer_name=$1;
		}
		print $buyer_name;
	my ($Home_content1,$Referer)=&Getcontent($url2);
	open(out,">hmv1.html");
	print out "$Home_content1";
	close out;
	while($Home_content1=~m/<tr>([\w\W]*?)<\/tr>/igs)
	{
	my $block2=$1;
	my ($batch_id,$created_date,$processed_start_date,$processed_end_date,$uploaded_by);
	if($block2=~m/<td><a[^>]*?>([^>]*?)<\/a><\/td>\s*<td>([^>]*?)<\/td>\s*<td>([^>]*?)<\/td>\s*<td>[^>]*?<\/td>\s*<td>[^>]*?<\/td>\s*<td>([^>]*?)<\/td>\s*<td>([^>]*?)<\/td>/is)
	{
	$batch_id=$1;
	$created_date=$2;
	$uploaded_by=$3;
	$processed_start_date=$4;
	$processed_end_date=$5
	}
	# print $batch_id;
	open(out,">>$filename");
	print out "$buyer_name\t$batch_id\t$created_date\t$uploaded_by\t$processed_start_date\t$processed_end_date\n";
	close out;
}
}	
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "application/json;charset=UTF-8");
	$req->header("Host" => "62.253.177.4:8080");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$redir_url);
}
		