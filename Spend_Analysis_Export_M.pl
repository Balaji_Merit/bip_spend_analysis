use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use Text::CSV;
use JSON;
use HTTP::Cookies;
use File::Copy qw(copy);
require BIP_DB;

# my $Batch_ID_List=$ARGV[0];

############Database Initialization########
my $dbh = &BIP_DB::DbConnection();
###########################################

my $csv = Text::CSV->new({ binary => 1, auto_diag => 1, eol => "\n"})
        or die "Cannot use CSV: " . Text::CSV->error_diag();
		
my $Field_Match	='Y';
my $import_flag='Y';
my $format_flag='Y';
my $Supplier_Match	= 'Y';
my $Export_Flag	= 'N';

my ($Input_Buyer_ID,$Batch_ID,$FileName,$StartDate,$Source_File_Location) = &BIP_DB::Retrieve_Buyers_Download_Details($dbh,"BUYERS_DOWNLOAD_DETAILS",$Field_Match,$import_flag,$format_flag,$Supplier_Match,$Export_Flag);
my @Input_Buyer_ID		= @$Input_Buyer_ID;
my @Batch_ID			= @$Batch_ID;
my @FileName			= @$FileName;
my @StartDate			= @$StartDate;
my @Source_File_Location= @$Source_File_Location;
for(my $i=0;$i<@Input_Buyer_ID;$i++)
{
	$Input_Buyer_ID=$Input_Buyer_ID[$i];
	$Batch_ID=$Batch_ID[$i];
	$FileName=$FileName[$i];
	$StartDate=$StartDate[$i];
	$Source_File_Location=$Source_File_Location[$i];
	print "LOC: $Source_File_Location\n";

	print "StartDate: $StartDate\n";
	my $Export_Flag='N';
	my ($Month_Of_Document) = &BIP_DB::Retrieve_Month_Document($dbh,$Input_Buyer_ID,$Batch_ID,"SUPPLIER_SOURCE_DATA");
	my ($Buyer_Name) = &BIP_DB::Retrieve_Buyer_Name($dbh,$Input_Buyer_ID,"Buyers");
	# File Location
	if($Month_Of_Document=~m/^\s*[a-z]+?\s*([\d]{2,4})\W*[a-z]+?\s*([\d]{2,4})\s*[a-z]*?\s*$/is)
	{
		$Month_Of_Document="$1-$2";
	}
	elsif($Month_Of_Document=~m/[^>]*?\s*([\d]{4})\W*[^>]*?\s*([\d]{4})/is)
	{
		$Month_Of_Document="$1-$2";
	}
	elsif($Month_Of_Document=~m/^\s*[^>]*?([\d]{4})\W*([\d]{4})\W*$/is)
	{
		$Month_Of_Document="$1-$2";
	}
	elsif($Month_Of_Document=~m/^\s*[^>]*?([\d]{4})(?:\W+|_)([\d]{2,4})\W*$/is)
	{
		$Month_Of_Document="$1-$2";
	}
	elsif($Month_Of_Document=~m/^\s*([\d]{4})(?:\W+|_)([\d]{2,4})\W*[^>]*?$/is)
	{
		$Month_Of_Document="$1-$2";
	}
	elsif($Month_Of_Document=~m/([\d]{2,4})(?:\W+|_)([\d]{2,4})/is)
	{
		$Month_Of_Document=$2;
	}
	elsif($Month_Of_Document=~m/^\s*[a-z]+?(?:\W*|_)?([\d]{2,4})\W*$/is)
	{
		$Month_Of_Document=$1;
	}
	elsif($Month_Of_Document=~m/^\s*[a-z]*?\s*([\d]{2,4})\s*[a-z]*?\s*$/is)
	{
		$Month_Of_Document=$1;
	}
	elsif($Month_Of_Document=~m/([\d]{4,6})/is)
	{
		$Month_Of_Document=$1;
	}
	elsif($Month_Of_Document=~m/([\d]+)/is)
	{
		$Month_Of_Document=$1;
	}
	
	$Month_Of_Document=~s/^\s*|\s*$//igs;


	# my $Filestorepath="\"//172.27.137.180/str/BIP/Download/$Input_Buyer_ID/$Month_Of_Document/Upload\""; # Actual planned store location
	my $Filestorepath="\"//172.27.137.180/str/BIP/Export/$StartDate\""; # This is for Ops Team 
	my $Source_Filestorepath="//172.27.137.180/str/BIP/Source/$StartDate"; # This is for Ops Team
	# my $Filestorepath="\"//172.27.137.180/BIPspendAnalysis/Export/$StartDate\""; # This is for Ops Team 
	# my $Source_Filestorepath="//172.27.137.180/BIPspendAnalysis/Source/$StartDate"; # This is for Ops Team
	# my $Filestorepath="C:/BIP/Export";  # For Temporary.... 
	# my $Source_Filestorepath="C:/BIP/Source"; 
	my $Source_File_Rename=$Source_Filestorepath.'/'.$Input_Buyer_ID.'_'.$FileName;
	# $Source_File_Rename=~s/([^>]+?)\"\//$1\//igs;
	# $Source_File_Rename=$Source_File_Rename.'"';
	print "Store path: $Source_Filestorepath\n";
	print "File name: $Source_File_Rename\n";

	my $Source_File_name_Bck=$FileName;
	unless ( -d $Filestorepath ) 
	{
		$Filestorepath=~s/\//\\/igs;
		system("mkdir $Filestorepath");
	}	
	unless ( -d $Source_Filestorepath ) 
	{
		$Source_Filestorepath=~s/\//\\/igs;
		system("mkdir $Source_Filestorepath");
	}

	print "Filestorepath	: $Filestorepath\n";
	print "Buyer_Name	: $Buyer_Name\n";

	$FileName=~s/\_\_+/\_/igs;
	$FileName=~s/\.xlsx?|\.xml$/\.csv/igs;
	# $Filestorepath=~s/\"$//igs;
	# my $output_csv = "$Filestorepath/$Input_Buyer_ID".'_'."$FileName";
	my $output_csv = "Export_".$Input_Buyer_ID.'_'.$FileName;
	print "FileName	: $FileName\n";
	# $output_csv=$output_csv.'"';
	############Database Initialization########
	my $dbh = &BIP_DB::DbConnection();
	###########################################
	my $sort_query="select Transaction_ID,Requirement,Amount,Start_Date,End_Date,Supplier_Name,Department_Name,Requirement_2 from SUPPLIER_FORMATTED_DATA where Buyer_ID=\'$Input_Buyer_ID\' and Batch_ID=\'$Batch_ID\'";
	my $sdata=$dbh->prepare($sort_query);
	$sdata->execute();
	$Filestorepath=~s/^\"|\"$//igs;
	open my $fh, ">", "$Filestorepath\\$output_csv" or die "Failed to open file: $!";
	$csv->print($fh,["Transaction_ID","Requirement","Amount","Start_Date","End_Date","Supplier_Name","Department_Name","Requirement_2"]);
	close $fh;
	print "$Input_Buyer_ID:$Batch_ID:Exporting to CSV.....\n";
	while (my @results = $sdata->fetchrow()) {
		open my $fh, ">>", "$Filestorepath\\$output_csv" or die "Failed to open file: $!";
		$csv->print($fh,\@results);
		close $fh;
		$Export_Flag='Y';	
	}
	if($Export_Flag eq 'N'){unlink "$Filestorepath\\$output_csv";}
	copy ("$Source_File_Location/$Source_File_name_Bck", "$Source_Filestorepath");
	# copy "$Source_File_Location", $Source_Filestorepath;
	rename "$Source_Filestorepath/$Source_File_name_Bck","$Source_File_Rename";	
	$Filestorepath=~s/\\/\\\\/igs;
	&BIP_DB::Update_Export_Flag($dbh,"BUYERS_DOWNLOAD_DETAILS",$Input_Buyer_ID,$Batch_ID,$Export_Flag,$Filestorepath);
}
