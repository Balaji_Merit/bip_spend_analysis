use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use BIP_DB_V1;
use BIP_Column_Map_V1;
use Encode;


my $Input_Buyer_ID = $0;
$Input_Buyer_ID =~ s/\.pl//igs;
$Input_Buyer_ID =$1 if($Input_Buyer_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

# $Input_Buyer_ID='689';
# my $ua  = LWP::UserAgent->new(
        # ssl_opts => { verify_hostname => 1 },
    # );
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my %Months_To_Num=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");
my @Months1=("January","February","March","April","May","June","July","August","September","October","November","December","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
my @months=("01","02","03","04","05","06","07","08","09","10","11","12");

############Database Initialization########
my $dbh = &BIP_DB_V1::DbConnection();
###########################################
my $hash_ref = &BIP_Column_Map_V1::ReadIniFile;
my %inihashvalue = %{$hash_ref};

my $Input_Table='BUYERS';
############ Retrieve Spent Data weblink from Buyers table ###########
my ($ID,$Buyer_Name,$Spent_Data_weblink) = &BIP_DB_V1::RetrieveUrl($dbh,$Input_Buyer_ID,$Input_Table);
my $Buyer_ID 			= @$ID[0];
my $Buyer_Name 			= @$Buyer_Name[0];
my $Buyer_Url 			= @$Spent_Data_weblink[0];

###############
print "Buyer_ID	: $Buyer_ID\n";	
print "Buyer_Name	: $Buyer_Name\n";	
print "Buyer_Url	: $Buyer_Url\n";	

my ($Buyer_content,$Status_Line)=&Getcontent($Buyer_Url); &BIP_Column_Map_V1::Link_Status_Update($dbh,$Buyer_ID,$Status_Line);
open(ts,">Buyer_content215.html");
print ts "$Buyer_content";
close ts;

my ($Processed_LinkInfo) = &BIP_DB_V1::LinkInfo_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
my @Processed_LinkInfo=@$Processed_LinkInfo;

my ($VIEWSTATE,$EVENTVALIDATION);
if ($Buyer_content=~m/VIEWSTATE\"\s*value\=\"([^>]*?)\"/is)
{
	$VIEWSTATE = $1;
}
if ($Buyer_content=~m/EVENTVALIDATION\"\s*value\=\"([^>]*?)\"/is)
{
	$EVENTVALIDATION = $1;
}
$VIEWSTATE=uri_escape($VIEWSTATE);
$EVENTVALIDATION=uri_escape($EVENTVALIDATION);
my ($Buyer_content_1,$status_line,$File_Type,$File_Type_cont,$File_Year,$flag);
while($Buyer_content=~m/Year\s*\:\s*([\w\W]*?)<\/select>/igs)
{
	my $YEAR_BLOCK = $1;
	while($YEAR_BLOCK=~m/<option[^>]*?>\s*([^>]*?)\s*</igs)
	{
		$File_Year=$1;
		print "File Year 	:: $File_Year\n";
		my $month_block;
		my $File_month;
		if($Buyer_content=~m/Month\s*\:\s*([\w\W]*?)<\/select>/is)
		{
		$month_block=$1;
		print $month_block;
		}
			while($month_block=~m/<option[^>]*?value="([^>]*?)">\s*([^>]*?)\s*</igs)
			{
		$File_month=$2;
		my $mon = $1;
		print $File_month;
		# my $postcon1 = '__EVENTTARGET=ddYYYY&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE='.$VIEWSTATE.'&__EVENTVALIDATION='.$EVENTVALIDATION.'&ctl00%24searchBox=&ctl00%24MainContent%24ddYYYY&searchBox='.$File_Year.'&ctl00%24MainContent%24ddMMM='.$File_month.'&ctl00%24MainContent%24Button1=&ctl00%24FileCounter=0';
		# my $postcon1 = '__EVENTTARGET=ctl00%24MainContent%24ddYYYY&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE=%2FwEPDwUJODMzNTE1NDU0D2QWAmYPZBYCAgMPZBYEAgkPFgIeBFRleHQFHzxoMT5TcGVuZGluZyBEYXRhIERvd25sb2FkPC9oMT5kAg4PZBYCAhEPD2QPEBYBZhYBFgIeDlBhcmFtZXRlclZhbHVlBQQyMDEwFgFmZGQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgEFDGN0bDAwJHNlYXJjaAyWQNvLCx3hWDNJxMOi6sjDU5BxoqrSOvmBeOV4cLus&__EVENTVALIDATION=%2FwEdABADLII4rUMTlGafL5wDuVJ7E5bA6PjsYbPvOGJjNwlyAa9S5EB8MvMvEr2bCY19cFmJ7WszDpJU6T1weIR4Ie1xxBSZ60%2FHerXDuuv9taVs1QXOQ9MO7e4yLWykoNwhhxGNPl%2B9JJt5%2FrGRi6uIhxF9pFpM53q34CsSY8OiQWOe69rGvmEnaVCrmt7ClYfqyqXHwLNZx5kDV%2BEZj13QqaspCFmVeQ39T1K%2Fb9iPIPYfU2HtwfH%2Fc9y7GjG%2BtOcSWJ7EU2JFh7Ixy6El%2BG5XPxZ2B3X8hYauMcMdG7MIrzbMzfnd3wctyww89JbDbeLvgrhUM3buSu9%2Bmb7UjNngt1VbZPqdnDP37O41LffVy19INP6J0k351puWoRnnMmjUjP4%3D&ctl00%24searchBox=&ctl00%24MainContent%24ddYYYY=2018&ctl00%24MainContent%24ddMMM=12&ctl00%24FileCounter=0: undefined

		my $flag='Y';
		# my ($Buyer_content_1,$status_line,$File_Type,$File_Type_cont)=&Postcontent($Buyer_Url,$postcon1,"site.dacorum.gov.uk",$Buyer_Url,$flag);
		# open(ts,">Buyer_content427_1.html");
# print ts "$Buyer_content_1";
# close ts;
		# if ($Buyer_content_1=~m/VIEWSTATE\"\s*value\=\"([^>]*?)\"/is)
		# {
			# $VIEWSTATE = $1;
		# }
		# if ($Buyer_content_1=~m/EVENTVALIDATION\"\s*value\=\"([^>]*?)\"/is)
		# {
			# $EVENTVALIDATION = $1;
		# }
		# $VIEWSTATE=uri_escape($VIEWSTATE);
		# $EVENTVALIDATION=uri_escape($EVENTVALIDATION);


		# my $month_block;
		# if($Buyer_content_1=~m/<select\s*name=[\"\']ddMMM[\"\'][^<]*?>([\w\W]*?)<\/select>/is)
		# {
			# $month_block=$1;
		# }

		# while($month_block=~m/<option\s*[^<]*?\s*value=[\"\']([\d]+)[\"\']>([^<]*?)</igs)
		# {

			# my $mon=$1;
			# my $File_Month=$2;
			# my $postcon2 = '__EVENTTARGET=ddYYYY&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE='.$VIEWSTATE.'&__EVENTVALIDATION='.$EVENTVALIDATION.'&searchBox=&ddYYYY='.$File_Year.'&ddMMM='.$mon;
			# my ($Doc_content,$status_line,$File_Type,$File_Type_cont)=&Postcontent($Buyer_Url,$postcon2,"webapps.dacorum.gov.uk",$Buyer_Url,$flag);
			if ($Buyer_content=~m/VIEWSTATE\"\s*value\=\"([^>]*?)\"/is)
			{
				$VIEWSTATE = $1;
			}
			if ($Buyer_content=~m/EVENTVALIDATION\"\s*value\=\"([^>]*?)\"/is)
			{
				$EVENTVALIDATION = $1;
			}
			$VIEWSTATE=uri_escape($VIEWSTATE);
			$EVENTVALIDATION=uri_escape($EVENTVALIDATION);

			# my $postcon3 = '__EVENTTARGET=ddYYYY&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE='.$VIEWSTATE.'&__EVENTVALIDATION='.$EVENTVALIDATION.'&searchBox=&ddYYYY='.$File_Year.'&ddMMM='.$mon.'&Button1=';
			my $postcon3 = '__EVENTTARGET=&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE='.$VIEWSTATE.'&__EVENTVALIDATION='.$EVENTVALIDATION.'&ctl00%24searchBox=&ctl00%24MainContent%24ddYYYY='.$File_Year.'&ctl00%24MainContent%24ddMMM='.$mon.'&ctl00%24MainContent%24Button1=&ctl00%24FileCounter=0';
			# my $postcon3 = '__EVENTTARGET=&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE=%2FwEPDwUJODMzNTE1NDU0D2QWAmYPZBYCAgMPZBYEAgkPFgIeBFRleHQFHzxoMT5TcGVuZGluZyBEYXRhIERvd25sb2FkPC9oMT5kAg4PZBYCAhEPD2QPEBYBZhYBFgIeDlBhcmFtZXRlclZhbHVlBQQyMDE4FgFmZGQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgEFDGN0bDAwJHNlYXJjaHObYZajbV0Qqcyx81vTtkJoyVi1ksSL99cql7PLu055&__VIEWSTATEGENERATOR=7D80A3C7&__EVENTVALIDATION=%2FwEdABaE8Qt1KlV1J9KeNPIQtTBPE5bA6PjsYbPvOGJjNwlyAa9S5EB8MvMvEr2bCY19cFmJ7WszDpJU6T1weIR4Ie1xxBSZ60%2FHerXDuuv9taVs1QXOQ9MO7e4yLWykoNwhhxGNPl%2B9JJt5%2FrGRi6uIhxF9pFpM53q34CsSY8OiQWOe69rGvmEnaVCrmt7ClYfqyqXHwLNZx5kDV%2BEZj13QqaspCFmVeQ39T1K%2Fb9iPIPYfU2HtwfH%2Fc9y7GjG%2BtOcSWJ7EU2JFh7Ixy6El%2BG5XPxZ2SAncKJa2uhrJcYJt61rVn1eMEqCqplFVx56mfisa7fF3PAMvcIeAZxY%2FKu%2Fu9kT0YoThdbZr%2B2CTJLc00jsYikxTT0xjzFpB7dY1OC5GpNRJ%2B7R00ez4JKanodeClR32CIbeuunBml1Q0tx3733lvPnd3wctyww89JbDbeLvgrhUM3buSu9%2Bmb7UjNngt1Vbjm1wj6NbTDVcXUM6tT1KWtAVztwoZPpu3OEXJqTLDPE%3D&ctl00%24searchBox=&ctl00%24MainContent%24ddYYYY=2018&ctl00%24MainContent%24ddMMM=08&ctl00%24MainContent%24Button1=&ctl00%24FileCounter=0';
			my $postcon3 = '__EVENTTARGET=&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE=%2FwEPDwUJODMzNTE1NDU0D2QWAmYPZBYCAgMPZBYEAgkPFgIeBFRleHQFHzxoMT5TcGVuZGluZyBEYXRhIERvd25sb2FkPC9oMT5kAg4PZBYCAhEPD2QPEBYBZhYBFgIeDlBhcmFtZXRlclZhbHVlBQQyMDE4FgFmZGQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgEFDGN0bDAwJHNlYXJjaHObYZajbV0Qqcyx81vTtkJoyVi1ksSL99cql7PLu055&__VIEWSTATEGENERATOR=7D80A3C7&__EVENTVALIDATION=%2FwEdABiE8Qt1KlV1J9KeNPIQtTBPE5bA6PjsYbPvOGJjNwlyAa9S5EB8MvMvEr2bCY19cFmJ7WszDpJU6T1weIR4Ie1xxBSZ60%2FHerXDuuv9taVs1QXOQ9MO7e4yLWykoNwhhxGNPl%2B9JJt5%2FrGRi6uIhxF9pFpM53q34CsSY8OiQWOe69rGvmEnaVCrmt7ClYfqyqXHwLNZx5kDV%2BEZj13QqaspCFmVeQ39T1K%2Fb9iPIPYfU2HtwfH%2Fc9y7GjG%2BtOcSWJ7EU2JFh7Ixy6El%2BG5XPxZ2SAncKJa2uhrJcYJt61rVn1eMEqCqplFVx56mfisa7fF3PAMvcIeAZxY%2FKu%2Fu9kT0YoThdbZr%2B2CTJLc00jsYikxTT0xjzFpB7dY1OC5GpNRJ%2B7R00ez4JKanodeClR32CIbeuunBml1Q0tx3733lvDmQX8UjihxKztmYmU%2FKDsNwgBghZwAWAIHnn6u%2FHfqX%2Bd3fBy3LDDz0lsNt4u%2BCuFQzdu5K736ZvtSM2eC3VVs5G6KuBNlMc8qaoKeip1WOBFSGYJ05H5kMtLTNd8Ex1g%3D%3D&ctl00%24searchBox=&ctl00%24MainContent%24ddYYYY=2018&ctl00%24MainContent%24ddMMM=12&ctl00%24MainContent%24Button1=&ctl00%24FileCounter=0';
my $postcon3 = '__EVENTTARGET=&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE=%2FwEPDwUJODMzNTE1NDU0D2QWAmYPZBYCAgMPZBYEAgkPFgIeBFRleHQFHzxoMT5TcGVuZGluZyBEYXRhIERvd25sb2FkPC9oMT5kAg4PZBYCAhEPD2QPEBYBZhYBFgIeDlBhcmFtZXRlclZhbHVlBQQyMDE5FgFmZGQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgEFDGN0bDAwJHNlYXJjaKC%2Bo1lzQCVxGlLjY8HzuukWqUg%2FeOwQSR5wyZDRcOUz&__VIEWSTATEGENERATOR=7D80A3C7&__EVENTVALIDATION=%2FwEdABEsU2ZWQbT5VHG8s%2FdoVcfCE5bA6PjsYbPvOGJjNwlyAa9S5EB8MvMvEr2bCY19cFmJ7WszDpJU6T1weIR4Ie1xxBSZ60%2FHerXDuuv9taVs1QXOQ9MO7e4yLWykoNwhhxGNPl%2B9JJt5%2FrGRi6uIhxF9pFpM53q34CsSY8OiQWOe69rGvmEnaVCrmt7ClYfqyqXHwLNZx5kDV%2BEZj13QqaspCFmVeQ39T1K%2Fb9iPIPYfU2HtwfH%2Fc9y7GjG%2BtOcSWJ7EU2JFh7Ixy6El%2BG5XPxZ29o9hP%2FueH3eFHH5l8vaTXEgJ3CiWtroayXGCbeta1Z%2F53d8HLcsMPPSWw23i74K4VDN27krvfpm%2B1IzZ4LdVW6e6DOrwI1OA0P85KGYiXJLEht14OgM%2Fl8yJ%2FDprMYFA&ctl00%24searchBox=&ctl00%24MainContent%24ddYYYY=2019&ctl00%24MainContent%24ddMMM=10&ctl00%24MainContent%24Button1=&ctl00%24FileCounter=0';

			my ($Doc_content1,$status_line,$File_Type,$File_Type_cont)=&Postcontent($Buyer_Url,$postcon3,"webapps.dacorum.gov.uk",$Buyer_Url,$flag);

			my $LinkInfo=$File_month.' '.$File_Year;

			if($LinkInfo ne '') 
			{	
				$LinkInfo=~s/\s\s+/ /igs;
				print "LinkInfo	: $LinkInfo\n";
				
				if($LinkInfo ~~ @Processed_LinkInfo || $LinkInfo =~ m/^\s*$/)
				{
					print "File Already Processed\n";
					next;
				}
				else
				{			
					print "\nThis is new file to download\n";
				}
			}
			$flag='Y';


			my ($Doc_Name,$Doc_Type);
			if($File_Type=~m/pdf/is)
			{
				next;
			}
			elsif($File_Type_cont=~m/pdf/is)
			{
				next;
			}

			if($File_Type=~m/(csv|xlsx|xls)/is)
			{
				$Doc_Type="$1";
				print "\nI:1\n";
			}
			elsif($File_Type_cont=~m/(csv)/is)
			{
				$Doc_Type="$1";
				print "\nI:2\n";	
			}


			$Doc_Type=~s/\"//igs;
			$Doc_Type=~s/\'//igs;

			my $Month_Of_Document = "$File_month $File_Year";	

			# File name creation from Link Info
			my $File_Type = $Doc_Type if ($Doc_Type ne '');

			$File_Year=~s/\W+//igs;
			$Doc_Name=$LinkInfo;
			$Doc_Name=~s/\W/\_/igs;
			$Doc_Name=~s/_+/\_/igs;
			$Doc_Name.='.'.$File_Type;

			print "#########################################\n";
			print "Doc_Name	: $Doc_Name\n"; 
			print "File_Month		: $File_month\n";
			print "File_Year		: $File_Year\n";
			print "Doc_Type		: $Doc_Type\n";
			print "Month_Of_Document	: $Month_Of_Document\n";
			print "#########################################";
			# File Path creatione
			# my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source"; 
			my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source";  

			# File location creation
			unless ( -d $Filestorepath ) 
			{
				$Filestorepath=~s/\//\\/igs;
				system("mkdir $Filestorepath");
			}
			my $Storefile = "$Filestorepath\\$Doc_Name";
			print "STOREPATH  : $Storefile \n";
			
			my $StartDate=DateTime->now();
			# Spend file download

			my $fh=FileHandle->new("$Storefile",'w');
			binmode($fh);
			$fh->print($Doc_content1);
			$fh->close();
			my ($Status,$Failed_Reason);
			if($status_line!~m/20/is)
			{
				$Failed_Reason=$status_line;
			}
			else
			{
				$Status='Downloaded';
			}
			print "\nStatus:::: $Status\n";
			
			
			
			# &BIP_DB_V1::Insert_Pdf_Json_Details($dbh,$Buyer_ID,$Storefile,$LinkInfo,$Month_Of_Document);
			&BIP_Column_Map_V1::ReadSource($Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
			print "Completed \n";
			# exit;
		}
	}		
}
# }	
# }


sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"");
	# $req->header("Referer"=>"");
	# $req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	print "\nFile_Type---> $File_Type\n";
	print "\nFile_Type_cont---> $File_Type_cont\n";
	# my $get_cookie=$res->header('set-cookie');
	print "\nCODE---> $code\n";
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$File_Type,$File_Type_cont);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	my $flag = shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Accept"=> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	if ($flag eq 'Y')
	{
		$req->header("Content-Type"=>"application/x-www-form-urlencoded"); 
	}
	elsif ($flag eq 'N')
	{
		$req->header("Content-Type"=>"text/csv; charset=iso-8859-1"); 
	}
	$req->header("Referer"=> "$Referer");
	$req->header("Cookie"=> "__atuvc=3%7C38; __atuvs=56012c65fdda8883002; __utma=122077844.1914530412.1442917477.1442917477.1442917477.1; __utmb=122077844.3.10.1442917477; __utmc=122077844; __utmz=122077844.1442917477.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utmt=1");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	print "\nContent_Disposition---> $Content_Disposition\n";
	print "\nFile_Type---> $File_Type\n";
	print "\nFile_Type_cont---> $File_Type_cont\n";
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$File_Type,$File_Type_cont);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	if($Data eq '')
	{
		# $Data='Not provided';
	}
	return $Data;
}

sub clean()
{
	my $C_Clean=shift;
	$C_Clean=~s/<[^>]*?>//igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/\&nbsp\;/ /igs;
	$C_Clean=~s/\&amp\;/ /igs;
	$C_Clean=~s/\&pound\;/ /igs;
	$C_Clean=~s/\&\#8211\;/ /igs;
	$C_Clean=~s/ndash\;/ /igs;
	$C_Clean=~s/[^[:print:]]+//igs;
	
	
	$C_Clean=~s/\&nbsp\;//igs;
	$C_Clean=~s/\#160\;//igs;
	$C_Clean=~s/\#163\;//igs;
	$C_Clean=~s/\&/ /igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	return $C_Clean;
}
