use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use BIP_DB_V1;
use BIP_Column_Map_V1;

my $Input_Buyer_ID = $0;
$Input_Buyer_ID =~ s/\.pl//igs;
$Input_Buyer_ID =$1 if($Input_Buyer_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my %Months_To_Num=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");

############Database Initialization########
my $dbh = &BIP_DB_V1::DbConnection();
###########################################

my $Input_Table='BUYERS';
############ Retrieve Spent Data weblink from Buyers table ###########
my ($ID,$Buyer_Name,$Spent_Data_weblink) = &BIP_DB_V1::RetrieveUrl($dbh,$Input_Buyer_ID,$Input_Table);
my $Buyer_ID 			= @$ID[0];
my $Buyer_Name 			= @$Buyer_Name[0];
my $Buyer_Url 			= @$Spent_Data_weblink[0];

###############
print "Buyer_ID		: $Buyer_ID\n";	
print "Buyer_Name	: $Buyer_Name\n";	
print "Buyer_Url	: $Buyer_Url\n";	

my ($Buyer_content,$Status_Line)=&Getcontent($Buyer_Url); &BIP_Column_Map_V1::Link_Status_Update($dbh,$Buyer_ID,$Status_Line);
open(ts,">Buyer_content.html");
print ts "$Buyer_content";
close ts;

my ($Processed_LinkInfo) = &BIP_DB_V1::LinkInfo_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
my @Processed_LinkInfo=@$Processed_LinkInfo;
my ($LinkInfo,$Doc_Link,$File_Month,$File_Year,$Doc_Name);

my $page=1;

Next_Page:
# To select the files block for every month
while ($Buyer_content =~ m/<a\s*href\=\'([^<]*?)\'\s*>\s*(Expenditure[^>]*?)\s*<\/a>/igs)
{
	my $Doc_Link='http://www.coventryrugbyccg.nhs.uk'.$1;
	my $LinkInfo=$2;

	$Doc_Link=~s/amp\;//igs;	
	$LinkInfo=~s/\s\s+/ /igs;
	$LinkInfo=~s/\<[^>]*?\>//igs;	
	if($LinkInfo ~~ @Processed_LinkInfo || $LinkInfo =~ m/^\s*$/)
	{
		print "File Already Processed\n";
		next;
	}
	my ($Doc_Link_contect,$Status_Line,$Content_Disposition)=&Getcontent($Doc_Link); # Ping the web link page
		
	my ($File_Month,$File_Year,$Doc_Name,$Doc_Type);
	if ($Content_Disposition =~ m/\.(\w+)$/is)
	{
		$Doc_Type=$1;
	}
	
	print "Doc_Type	: $Doc_Type\n";	
	
	# Get Date, Month, Year from the link info for file location Creation
	if($LinkInfo=~m/\s(\w+)\s([\d]*?)\./is)
	{
		$File_Month=$1;
		$File_Year=$2;
		print "File_Month	: $File_Month\n";
		print "File_Year	: $File_Year\n";
	}
	my $Month_Of_Document = "$File_Month $File_Year";
	print "LinkInfo	: <$LinkInfo>\n";	
	
	if($Doc_Link!~m/http/is)
	{
		my $u1=URI::URL->new($Doc_Link,$Buyer_Url);
		my $u2=$u1->abs;
		$Doc_Link=$u2;
	}
	print "Doc_Link	: $Doc_Link\n";
	
	# File name creation from Link Info
	my $File_Type = $Doc_Type if ($Doc_Type ne '');
	
	$Doc_Name=$LinkInfo;
	$Doc_Name=~s/\W/\_/igs;
	$Doc_Name=~s/_+/\_/igs;
	$Doc_Name.='.'.$File_Type;
	print "Doc_Name	: $Doc_Name\n"; 
	
	# File Path creation
	my $hash_ref = &BIP_Column_Map_V1::ReadIniFile();	my %inihashvalue = %{$hash_ref};	my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source"; 
	# my $Filestorepath='D:/Suresh/2015/July/Scraping/BIP_Analysis/Sathish_Allot/'.$Buyer_ID."/".$File_Year."/Source";  
	
	# File location creation
	unless ( -d $Filestorepath ) 
	{
		$Filestorepath=~s/\//\\/igs;
		system("mkdir $Filestorepath");
	}
	my $Storefile = "$Filestorepath\\$Doc_Name";
	print "STOREPATH  : $Storefile \n";
	my $StartDate=DateTime->now();
	# Spend file download
	my ($Doc_content,$status_line)=&Getcontent($Doc_Link);
	my $fh=FileHandle->new("$Storefile",'w');
	binmode($fh);
	$fh->print($Doc_content);
	$fh->close();
	my ($Status,$Failed_Reason);
	if($status_line!~m/20/is)
	{
		$Failed_Reason=$status_line;
	}
	else
	{
		$Status='Downloaded';
	}
	print "\nStatus:::: $Status\n";
	&BIP_Column_Map_V1::ReadSource($Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
	print "Completed \n";
}

# Next Page.
if($page < 4)
{
	my $viewstate=uri_escape($1) if($Buyer_content=~m/VIEWSTATE\"\s*value\=\"([^<]*?)\"/is);
	
	my $next_cont = 'ctl01%24ToolkitScriptManager=ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24pnlGrid%7Cctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData&ctl01_ToolkitScriptManager_HiddenField=&ctl01%24txtSearch=Search%20this%20site...&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24hdnLibNav_Folder=&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24txtHiddenRefreshID=&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl02%24hdnItemType=1&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl02%24hdnUniqueID=8d8fddfc-e0e0-41a0-9980-f3dc7e57f89f&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl03%24hdnItemType=1&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl03%24hdnUniqueID=367ce9ac-30ec-4872-86dd-2f9cb8b367a6&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl04%24hdnItemType=1&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl04%24hdnUniqueID=1f50708a-387e-46e4-9626-9bb11a09a41c&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl05%24hdnItemType=1&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl05%24hdnUniqueID=3041103c-724f-4be4-8d3d-8aabd12eb12d&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl06%24hdnItemType=1&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl06%24hdnUniqueID=91ca801d-90b2-438e-9689-9f55ac2904e8&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl07%24hdnItemType=1&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl07%24hdnUniqueID=4bc14647-9446-48c4-91de-1d64f3f31269&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl08%24hdnItemType=1&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl08%24hdnUniqueID=a7439843-4bc1-4fa8-b3d1-265ba9084e66&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl09%24hdnItemType=1&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl09%24hdnUniqueID=f05bf47b-b296-4fa8-a6e5-7a40cca8c5da&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl10%24hdnItemType=1&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl10%24hdnUniqueID=bf6e9d91-2280-4de9-ad34-6b0d833ea307&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl11%24hdnItemType=1&ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData%24ctl11%24hdnUniqueID=234e3d05-4a54-4265-bb05-2002a55f6844&__EVENTTARGET=ctl01%24ContentPlaceHolder1%24DynamicPageContent1%24ctl00%24gvwData&__EVENTARGUMENT=Page%24'.$page.'&__VIEWSTATE='.$viewstate.'&__SCROLLPOSITIONX=0&__SCROLLPOSITIONY=0&__ASYNCPOST=true&';
	
		
	($Buyer_content,$Status_Line)=&Postcontent($Buyer_Url,$next_cont,'www.coventryrugbyccg.nhs.uk',$Buyer_Url); # Ping the web link page
	
	$page++;
	goto Next_Page;
}

sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;	
	my $Content_Disposition=$res->header("Content-Disposition");
	# my $get_cookie=$res->header('set-cookie');
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$Content_Disposition);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded; charset=utf-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$Content_Disposition);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	if($Data eq '')
	{
		# $Data='Not provided';
	}
	return $Data;
}