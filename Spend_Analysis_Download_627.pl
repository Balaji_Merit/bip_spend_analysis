use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use DBI;
use filehandle;
use URI::Escape;
use DateTime;
use BIP_DB_V1;
use BIP_Column_Map_V1;

my $Input_Buyer_ID = $0;
$Input_Buyer_ID =~ s/\.pl//igs;
$Input_Buyer_ID =$1 if($Input_Buyer_ID =~ m/[^>]*?\_(\d+?)\s*$/is);

# my $ua=LWP::UserAgent->new(show_progress=>1);
my $ua=LWP::UserAgent->new(ssl_opts => {SSL_verify_mode => 'SSL_VERIFY_PEER',verify_hostname => 0 }, show_progress=>1);
# my $ua  = LWP::UserAgent->new(
        # ssl_opts => { verify_hostname => 0 },
    # );
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->timeout(100); 
# $ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my %Months_To_Num=("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");
my @Months1=("January","February","March","April","May","June","July","August","September","October","November","December","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");

############Database Initialization########
my $dbh = &BIP_DB_V1::DbConnection();
###########################################

my $Input_Table='BUYERS';
############ Retrieve Spent Data weblink from Buyers table ###########
my ($ID,$Buyer_Name,$Spent_Data_weblink) = &BIP_DB_V1::RetrieveUrl($dbh,$Input_Buyer_ID,$Input_Table);
my $Buyer_ID 			= @$ID[0];
my $Buyer_Name 			= @$Buyer_Name[0];
my $Buyer_Url 			= @$Spent_Data_weblink[0];
# $Buyer_Url 			="https://shropshire.gov.uk/open-data/datasets/supplier-payments-over-500/supplier-payments-over-500-2018-19/";
$Buyer_Url 			="https://datamillnorth.org/dataset/sheffieldcitycouncil-spend-over-f250";
###############
print "Buyer_ID	: $Buyer_ID\n";	
print "Buyer_Name	: $Buyer_Name\n";	
print "Buyer_Url	: $Buyer_Url\n";	

my ($Buyer_content,$Status_Line)=&Getcontent($Buyer_Url); &BIP_Column_Map_V1::Link_Status_Update($dbh,$Buyer_ID,$Status_Line);
open(ts,">Buyer_content.html");
print ts "$Buyer_content";
close ts;

my ($Processed_LinkInfo) = &BIP_DB_V1::LinkInfo_RetrieveUrl($dbh,$Buyer_ID,"BUYERS_DOWNLOAD_DETAILS");
my @Processed_LinkInfo=@$Processed_LinkInfo;
#while($Buyer_content=~m/<a\s*href="([^>]*?)"\s*class="browse2-result-name-link"\s*itemprop="url"\s*rel="">([^>]*?)<\/a>/igs)
Next_Page:
# while($Buyer_content=~m/<h2\s*class\s*=\s*\"browse2-result-name\"\s*>([\w\W]*?)<\/h2>/igs)
# while($Buyer_content=~m/<a\s*[^>]*?href="([^>]*?.csv)"\s*[^>]*?>\s*([^>]*?)<\/a>/igs)
# {
	# my $block=$1;
	while($Buyer_content=~m/<a\s*[^>]*?\s*href\s*=\s*(?:\'|\")([^<]*?.(?:csv|xlsm?))(?:\'|\")\s*[^>]*?>\s*([^>]*?)\s*<\/a>/igs)
	{	
		my $Doc_Link=$1;
		my $LinkInfo=$2;
			
		# $LinkInfo=&clean($LinkInfo);	
		my $Link_ID=$1 if($Doc_Link=~m/[^>]+\/(.+)?$/is);
		my ($File_Date,$File_Month,$File_Year,$Doc_Name,$Doc_Type);
		my $LinkInfo_Temp=$LinkInfo;
		print "\nhi: $Doc_Link";		
		print "\nhi: $Link_ID";		
		print "\nhi: $LinkInfo\n";		

		my $Doc_Link_Temp=$Doc_Link;
		next if($Doc_Link=~m/\.pdf/is);	
		
		# $LinkInfo=$File_Month." ".$File_Year;
		# Get Title of the file name
		if($LinkInfo ne '') 
		{	
			$LinkInfo=~s/\s\s+/ /igs;
			print "LinkInfo	: $LinkInfo\n";
			
			if($LinkInfo ~~ @Processed_LinkInfo || $LinkInfo =~ m/^\s*$/)
			{
				print "File Already Processed\n";
				next;
			}
			else
			{			
				print "\nThis is new file to download\n";
			}
		}	
		# Get file link 		
		if($Doc_Link!~m/^http/is)
		{	
			print "\nhi\n";
			my $u1=URI::URL->new($Doc_Link,$Buyer_Url);
			my $u2=$u1->abs;
			$Doc_Link=$u2;
			$Doc_Link=~s/\&amp\;/&/igs;
		}
		foreach my $monthss(@Months1)	
		{		
			if($LinkInfo_Temp=~m/$monthss/is)
			{
				$File_Month=$monthss;
				if($LinkInfo_Temp=~m/(2\d{3})/is)
				{
					$File_Year=$1;
				}			
				else
				{
					while($LinkInfo_Temp=~m/(\d{2,4})/igs)
					{					
						$File_Year=$1;
					}
				}	
				last;
			}			
		}
		print "Doc_Link	: $Doc_Link\n";
		print "\nLinkInfo_Temp	: $LinkInfo_Temp\n";	
			
		
		# if($File_Year eq '')
		# {
			# if($LinkInfo_Temp=~m/(q\d+)\s*(\d+\s*\-\s*\d+)/is)
			# {
				# $File_Month=$1;
				# $File_Year=$2;
			# }
		# }

		# my $post_url='https://data.sheffield.gov.uk/views/INLINE/rows.json?accessType=WEBSITE&method=getByIds&asHashes=true&start=0&length=50&meta=true&%24order=%3Aid';
		# my $Post_Content='{"id":"'.$Link_ID.'","name":"'.$LinkInfo.'","category":"Economy","description":"Spend data over £250 in vlaue.","displayType":"table","licenseId":"PUBLIC_DOMAIN","publicationAppendEnabled":true,"columns":[{"id":451567,"name":"Body Name","fieldName":"body_name","position":1,"width":159,"dataTypeName":"text","tableColumnId":186814,"format":{},"flags":null,"metadata":{}},{"id":451568,"name":"Body","fieldName":"body","position":2,"width":275,"dataTypeName":"text","tableColumnId":186815,"format":{},"flags":null,"metadata":{}},{"id":451569,"name":"Organisational Unit","fieldName":"organisational_unit","position":3,"width":131,"dataTypeName":"text","tableColumnId":186816,"format":{"precisionStyle":"standard","noCommas":"false","align":"right"},"flags":null,"metadata":{}},{"id":451570,"name":"Business Unit No.","fieldName":"business_unit_no","position":4,"width":108,"dataTypeName":"text","tableColumnId":186817,"format":{},"flags":null,"metadata":{}},{"id":451571,"name":"Business Unit Description","fieldName":"business_unit_description","position":5,"width":283,"dataTypeName":"text","tableColumnId":186818,"format":{},"flags":null,"metadata":{}},{"id":451572,"name":"Expense Code","fieldName":"expense_code","position":6,"width":127,"dataTypeName":"text","tableColumnId":186819,"format":{},"flags":null,"metadata":{}},{"id":451573,"name":"Expense Code Description","fieldName":"expense_code_description","position":7,"width":247,"dataTypeName":"text","tableColumnId":186820,"format":{},"flags":null,"metadata":{}},{"id":451574,"name":"GL Date","fieldName":"gl_date","position":8,"width":95,"dataTypeName":"text","tableColumnId":186821,"format":{"align":"left"},"flags":null,"metadata":{}},{"id":451575,"name":"Document No.","fieldName":"document_no","position":9,"width":123,"dataTypeName":"text","tableColumnId":186822,"format":{"precisionStyle":"standard","noCommas":"false","align":"right"},"flags":null,"metadata":{}},{"id":451576,"name":"Invoice No.","fieldName":"invoice_no","position":10,"width":126,"dataTypeName":"text","tableColumnId":186823,"format":{},"flags":null,"metadata":{}},{"id":451577,"name":"Invoiced Value","fieldName":"invoiced_value","position":11,"width":141,"dataTypeName":"money","tableColumnId":186824,"format":{"humane":"false","currency":"GBP","align":"right"},"flags":null,"metadata":{}},{"id":451578,"name":"Supplier Name","fieldName":"supplier_name","position":12,"width":256,"dataTypeName":"text","tableColumnId":186825,"format":{},"flags":null,"metadata":{}},{"id":451579,"name":"Address Book No.","fieldName":"address_book_no","position":13,"width":160,"dataTypeName":"text","tableColumnId":186826,"format":{"precisionStyle":"standard","noCommas":"false","align":"right"},"flags":null,"metadata":{}},{"id":451580,"name":"Proclass Details Level 1","fieldName":"proclass_details_level_1","position":14,"width":194,"dataTypeName":"text","tableColumnId":186827,"format":{},"flags":null,"metadata":{}},{"id":451581,"name":"Thomson Classification Description","fieldName":"thomson_classification_description","position":15,"width":508,"dataTypeName":"text","tableColumnId":186828,"format":{},"flags":null,"metadata":{}}],"metadata":{"custom_fields":{"Transparency Code":{"Service":"Commercial Services","Transparency Code":"Yes","Frequency":"Monthly"}},"renderTypeConfig":{"visible":{"table":true}},"availableDisplayTypes":["table","fatrow","page"],"rdfSubject":"0","rowIdentifier":"0","rdfClass":"","jsonQuery":{}},"query":{},"tags":["transparency","spend"],"flags":["default"],"originalViewId":"'.$Link_ID.'","displayFormat":{}}';
		# my $Host='data.sheffield.gov.uk';
		# my $Referer='https://data.sheffield.gov.uk/Economy/-250-Spend-Data-January-2011-June-2015/n7qa-pkeg';		

		# my ($Doc_content,$status_line,$File_Type,$File_Type_cont)=&Postcontent($post_url,$Post_Content,$Host,$Referer);
		# my $Export_URL='https://data.sheffield.gov.uk/api/views/'.$Link_ID.'/rows.csv?accessType=DOWNLOAD';
		my ($Doc_content,$status_line,$File_Type,$File_Type_cont)=&Getcontent($Doc_Link);
		if($File_Type=~m/\.pdf/is)
		{
			next;
		}
		elsif($File_Type_cont=~m/\.pdf/is)
		{
			next;
		}

		if($File_Type=~m/(csv|xlsx|xls)/is)
		{
			$Doc_Type="$1";
			print "\nI:1\n";
		}
		elsif($File_Type_cont=~m/(csv)/is)
		{
			$Doc_Type="$1";
			print "\nI:2\n";	
		}
		elsif($File_Type_cont=~m/(excel)/is)
		{
			$Doc_Type="xls";
			print "\nI:2\n";	
		}
		elsif($Doc_Link_Temp=~m/^[^>]*?\.([\w]*?)\s*$/is)
		{
			$Doc_Type="$1";		
			print "\nI:5\n";	
		}		
		
		$Doc_Type=~s/\"//igs;
		$Doc_Type=~s/\'//igs;
			
		# if($LinkInfo=~m/\s*([a-zA-Z]+)\s*(\d{4})\s*$/is)
		# {
			# $File_Month=$1;
			# $File_Year=$2;
			
		# }
		my $Month_Of_Document = "$File_Month $File_Year";	
		
		# File name creation from Link Info
		# my $File_Type = $Doc_Type if ($Doc_Type ne '');
		my $File_Type = 'csv';
		
		$Doc_Name=$LinkInfo;
		$Doc_Name=~s/\W/\_/igs;
		$Doc_Name=~s/_+/\_/igs;
		$Doc_Name.='.'.$File_Type;
		$File_Year=~s/\W//igs;
		print "#########################################\n";
		print "Doc_Name	: $Doc_Name\n"; 
		print "File_Month		: $File_Month\n";
		print "File_Year		: $File_Year\n";
		print "Doc_Type		: $Doc_Type\n";
		print "Doc_Link	: $Doc_Link\n";
		print "Month_Of_Document	: $Month_Of_Document\n";
		print "#########################################";
		# File Path creation
		my $hash_ref = &BIP_Column_Map_V1::ReadIniFile();	my %inihashvalue = %{$hash_ref};	my $Filestorepath=$inihashvalue{'path'}.$Buyer_ID."/".$File_Year."/Source"; 
		
		# File location creation
		unless ( -d $Filestorepath ) 
		{
			$Filestorepath=~s/\//\\/igs;
			system("mkdir $Filestorepath");
		}
		my $Storefile = "$Filestorepath\\$Doc_Name";
		print "STOREPATH  : $Storefile \n";
		my $StartDate=DateTime->now();
		# Spend file download
		
		my $fh=FileHandle->new("$Storefile",'w');
		binmode($fh);
		$fh->print($Doc_content);
		$fh->close();
		my ($Status,$Failed_Reason);
		if($status_line!~m/20/is)
		{
			$Failed_Reason=$status_line;
		}
		else
		{
			$Status='Downloaded';
		}
		print "\nStatus:::: $Status\n";
		<>;
		&BIP_Column_Map_V1::ReadSource($Buyer_ID,$Filestorepath,$Doc_Name,$LinkInfo,$Month_Of_Document,$StartDate,$Status,$Failed_Reason);
		print "Completed \n";
	}
# }
if($Buyer_content=~m/<a\s*[^>]*?Next\s*Page[^>]*?href\s*=\s*(?:\'|\")([^<]*?)(?:\'|\")\s*[^>]*?>/is)
{
	my $Next_URl='https://data.sheffield.gov.uk'.$1;
	print "Next_Page : $Next_URl\n";
	($Buyer_content,$Status_Line)=&Getcontent($Next_URl);
	goto Next_Page;
}

sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=>"");
	# $req->header("Referer"=>"");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	print "\nFile_Type---> $File_Type\n";
	print "\nFile_Type_cont---> $File_Type_cont\n";
	# my $get_cookie=$res->header('set-cookie');
	print "\nCODE---> $code\n";
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$File_Type,$File_Type_cont);
}

sub Getcontent_1
{
	my $url = shift;
	my $Referer = shift;
	my $rerun_count=0;
	my $redir_url;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Host"=>"data.sheffield.gov.uk");
	$req->header("Referer"=>"$Referer");
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $File_Type=$res->header("Content-Disposition");
	my $File_Type_cont=$res->header("Content-Type");
	print "\nFile_Type---> $File_Type\n";
	print "\nFile_Type_cont---> $File_Type_cont\n";
	# my $get_cookie=$res->header('set-cookie');
	print "\nCODE---> $code\n";
	my $content;
	if($code=~m/20/is)
	{		
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$status_line,$File_Type,$File_Type_cont);
}

sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;
	# my $Cookie_JSESSIONID=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/json; charset=UTF-8"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}

sub Trim()
{
	my $Data=shift;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s\s+/ /igs;
	if($Data eq '')
	{
		# $Data='Not provided';
	}
	return $Data;
}

sub clean()
{
	my $C_Clean=shift;
	$C_Clean=~s/<[^>]*?>//igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/\&nbsp\;/ /igs;
	$C_Clean=~s/\&amp\;/ /igs;
	$C_Clean=~s/\&pound\;/ /igs;
	$C_Clean=~s/\&\#8211\;/ /igs;
	$C_Clean=~s/ndash\;/ /igs;
	$C_Clean=~s/[^[:print:]]+//igs;
	
	
	$C_Clean=~s/\&nbsp\;//igs;
	$C_Clean=~s/\#160\;//igs;
	$C_Clean=~s/\#163\;//igs;
	$C_Clean=~s/\&/ /igs;
	$C_Clean=~s/\s\s+/ /igs;
	$C_Clean=~s/^\s*|\s*$//igs;
	return $C_Clean;
}